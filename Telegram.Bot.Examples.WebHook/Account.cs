using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot.Api.Examples.WebHook;
using static Telegram.Bot.Examples.WebHook.DataStorage;

namespace Telegram.Bot.Examples.WebHook
{
    public class Account
    {
        public static void Create(int AccountID, string UserName, string CharName, int Money, int Exp, int Attack, int Def, int Radiation, int Resistance, int Strength, int Endurance, int Charisma, int Intelligence, int Agility, int Luck, int TalantPoints, int Energy, int MaxEnergy, int Faction, int Level, int Health, int MaxHealth)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);

            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            sql = "SELECT AccountID FROM users WHERE AccountID = " + AccountID.ToString();
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - CreateAccount] Создание аккаунта: запрос к базе: " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID FROM users WHERE AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string sql_result = string.Empty;
            while (thisReader.Read())
            {
                sql_result += thisReader["AccountID"];
            }
            thisReader.Close();
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - CreateAccount] Создание аккаунта: результат запроса - " + sql_result);
            int keyboard = 1;
            if (sql_result != AccountID.ToString())
            {
                string varquery = AccountID.ToString() + ", " + @"""" + UserName + @"""" + ", " + @"""" + CharName + @"""" + ", " + @"""" + Money + @"""" + ", " + Exp.ToString() + ", " + Attack.ToString() + ", " + Def.ToString() + ", " + Resistance.ToString() + ", " + Radiation.ToString() + ", " + Strength.ToString() + ", " + Endurance.ToString() + ", " + Charisma.ToString() + ", " + Intelligence.ToString() + ", " + Agility.ToString() + ", " + Luck.ToString() + ", " + TalantPoints.ToString() + ", " + Energy.ToString() + ", " + MaxEnergy.ToString() + ", " + Faction.ToString() + ", " + Level.ToString() + ", " + Health.ToString() + ", " + MaxHealth.ToString();
                string report_query = AccountID.ToString() + ", " + "0, 0, 0, 0";
                string inventory_query = AccountID.ToString() + ", " + "0, 0, 0, 0, 0, 0, 0, 0, 0, 0";
                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - CreateAccount] Создание аккаунта: переменные для запроса (varquery) " + varquery);
                sql = "INSERT INTO users (AccountID, UserName, CharName, Money, Exp, Attack, Def, Radiation, Resistance, Strength, Endurance, Charisma, Intelligence, Agility, Luck, TalantPoints, Energy, MaxEnergy, Faction, Level, Health, MaxHealth) VALUES (" + varquery + "); ";
                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - CreateAccount] Создание аккаунта для ID: " + AccountID.ToString());
                script = new MySqlScript(myConnection, sql);
                script.Query = sql;
                script.Connection = myConnection;
                script.Execute();
                var usage = @"<i>Интенсивный писк</i>\n\nНадпись на дисплее: \n*Восстановление нейронных связей...\n*Адаптация к окружению...\n*Подготовка к загрузке данных сознания...";
                try
                {
                    Message.Send(AccountID, "<i>Интенсивный писк</i>\n\nНадпись на дисплее: \n*Восстановление нейронных связей...\n*Адаптация к окружению...\n*Подготовка к загрузке данных сознания...", 3);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - CreateAccount] ОШИБКА!: " + MyEx);
                }

                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - CreateAccount] Создание аккаунта: выполнения запроса: " + sql);
                myConnection.Close(); //Обязательно закрываем соединение!

            }
            else
            {
                string PlayerStatus = GetSingleData(Convert.ToInt32(AccountID), "Status");
                if (PlayerStatus == "1") // Игрок на начальной клавиатуре
                {
                    keyboard = 24;
                }
                else if (PlayerStatus == "0")
                {
                    keyboard = 1;
                }
                var usage = @"<i>Интенсивный писк</i>\n\n⚠️ Error... error ⚠️";
                try
                {
                    Message.Send(AccountID, "<i>Интенсивный писк</i>\n\n⚠️ Error... error ⚠️", keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - CreateAccount] Ошибка: " + MyEx);
                }
                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - CreateAccount] Аккаунт уже найден. Не создаем новый для ID: " + AccountID.ToString());
                myConnection.Close();

            }

        }

        public static string GetSingleData(int AccountID, string DataBlockName) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM users WHERE AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["" + DataBlockName + ""];
            }
            thisReader.Close();
            myConnection.Close();

            // Convert.ToInt64(DataBlockResult);
            return DataBlockResult;
        }

        public static bool SetSingleData(int AccountID, string DataBlockName, string NewParameter)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);

            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;

            sql = "UPDATE users SET " + DataBlockName + " =" + @"""" + NewParameter + @"""" + " WHERE AccountID = " + AccountID.ToString() + ";";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - Account.SetSingleData] Запрос к базе для аккаунта: " + AccountID.ToString() + " datablock " + DataBlockName + " query - " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();

                Finished = false;
                return Finished;
            }

            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static void InviteToClan(int AccountID, int ClanID, int PlayerStatus)
        {
            if (IsInClan(AccountID) == true)
            {
                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, "Вы уже в клане и не можете вступить в новый пока не выйдете из старого!", 1);
                return;
            }
            MySqlConnection myConnection = new MySqlConnection(Connect);

            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;

            string CreationDateTime = DateTime.Now.ToString("h:mm:ss");

            myConnection.Open();
            string varquery = @"""" + ClanID.ToString() + @"""" + ", " + @"""" + AccountID.ToString() + @"""" + ", " + @"""" + PlayerStatus.ToString() + @"""";

            sql = "INSERT INTO clans (ClanID, AccountID, PlayerStatus) VALUES (" + varquery + "); ";

            MySqlScript script = new MySqlScript(myConnection, sql);
            script.Query = sql;
            Console.WriteLine("DEBUG: " + script.Query);
            script.Connection = myConnection;
            script.Execute();

            try
            {
                //Message.Send(FounderID, "<b>Доступен новый член команды:</b>\n\n" + NPC_Face + " " + FirstName + " " + LastName + "\n", 3); // Дописать уведомление о создании
            }
            catch (Exception MyEx)
            {
                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - InviteToClan] ОШИБКА!: " + MyEx);
            }

            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - InviteToClan] Создание клана: выполнения запроса: " + sql);
            myConnection.Close(); //Обязательно закрываем соединение!
        } // Пригласить в клан

        public static bool IsInClan(int AccountID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM clans WHERE AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string PlayerInClanID = string.Empty;
            while (thisReader.Read())
            {
                PlayerInClanID += thisReader["ID"];
            }
            thisReader.Close();
            myConnection.Close();
            if (PlayerInClanID != "" && PlayerInClanID != " ")
            {
                return true;
            }
            else return false;
        } // Проверяет состоит ли игрок в клане

        public static bool IsInActiveClan(int AccountID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ClanID FROM clans WHERE AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string PlayerInClanID = string.Empty;
            while (thisReader.Read())
            {
                PlayerInClanID += thisReader["ClanID"];
            }
            thisReader.Close();
            myConnection.Close();
            if (PlayerInClanID != "" && PlayerInClanID != " " && Clans.GetDataByClanID(Convert.ToInt32(PlayerInClanID), "Active") == "1")
            {
                return true;
            }
            else return false;
        }

        public static bool isHasItem(int AccountID, int ItemID, int ItemCount) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM inventory WHERE AccountID = @AccountID AND ItemID = @ItemID AND ItemCount >= @ItemCount", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            sql_commmand.Parameters.AddWithValue("@ItemCount", ItemCount);
            sql_commmand.Parameters.AddWithValue("@ItemID", ItemID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["ID"];
            }
            thisReader.Close();
            myConnection.Close();

            if (DataBlockResult != "")
            {
                return true;
            }
            else return false;
        }

        public static bool HasItem(int PlayerID, string ItemID1, string ItemCount1)
        {
            Regex REG = new Regex("[0-9]+");
            // ищем совпадения в исходной строке
            MatchCollection mc = REG.Matches(ItemID1);
            StringBuilder sb = new StringBuilder();
            // создаем строку результата
            foreach (Match matc in mc)
                sb.Append(matc.Value);
            // а это твой результат - только цифры
            string MyItemID = sb.ToString();
            char[] charsToTrim = { '*', ' ', '\'', '№', '@', '!', '#', '$', '%', '^', '&', '(', ')', '"', ';', ':', '?', '/' }; // Обрезаем символы
            MyItemID = MyItemID.Replace('(', '@');
            MyItemID = MyItemID.Replace(')', '@');
            MyItemID = MyItemID.Trim(charsToTrim);
            Console.WriteLine("MyItemID result: " + MyItemID);
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_command = new MySqlCommand("SELECT ItemCount FROM inventory WHERE AccountID = @AccountID AND ItemID = @ItemID", myConnection);
            sql_command.Parameters.AddWithValue("@AccountID", PlayerID);
            sql_command.Parameters.AddWithValue("@ItemID", ItemID1);
            try
            {
                MySqlDataReader thisReader = sql_command.ExecuteReader();
                string SlotData = string.Empty;
                while (thisReader.Read())
                {
                    SlotData += thisReader["ItemCount"];
                }
                thisReader.Close();
                Console.WriteLine("Account.HasItemQuery result: " + SlotData);
                if (Convert.ToInt32(SlotData) >= Convert.ToInt32(ItemCount1))
                {
                    return true;
                }
                return false;
            }
            catch (Exception MyEx)
            {
                Console.WriteLine("Ошибка от ID " + PlayerID + " с описанием: " + MyEx.Message);
                return false;
            }
            finally
            {
                myConnection.Close();
            }
        }

        public static bool AddRecipe(int AccountID, string RecipeID, string RecipeLevel, string RecipeLearnLevel)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;
            string report_query = AccountID.ToString() + ", " + @"""" + RecipeID + @"""" + ", " + @"""" + RecipeLevel + @"""" + ", " + @"""" + RecipeLearnLevel + @"""";
            sql = "INSERT INTO players_recipes (AccountID, RecipeID, RecipeLevel, RecipeLearnLevel) VALUES (" + report_query + "); ";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - [Account.AddRecipe] Запрос: " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();

            script.Query = sql;
            script.Connection = myConnection;

            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();

                Finished = false;
                return Finished;
            }

            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static string GetRecipeDataByID(int AccountID, int RecipeID, string DataBlockName) // Функция позволяет получить блок данных из MySQL таблицы recipes по ID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM players_recipes WHERE RecipeID = @RecipeID AND AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@RecipeID", RecipeID);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["" + DataBlockName + ""];
            }
            thisReader.Close();
            myConnection.Close();

            // Convert.ToInt64(DataBlockResult);
            return DataBlockResult;
        }

        public static bool isHasRecipe(int AccountID, int RecipeID) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM players_recipes WHERE AccountID = @AccountID AND RecipeID = @RecipeID ", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            sql_commmand.Parameters.AddWithValue("@RecipeID", RecipeID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["ID"];
            }
            thisReader.Close();
            myConnection.Close();

            if (DataBlockResult != "")
            {
                return true;
            }
            else return false;
        }

        public static bool AddData(int AccountID, string DataBlockName, string NewParameter)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);

            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;

            sql = "UPDATE users SET " + DataBlockName + " =" + DataBlockName + " + " + @"""" + NewParameter + @"""" + "WHERE AccountID = " + AccountID.ToString() + ";";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - Account.AddData] Запрос к базе для аккаунта: " + AccountID.ToString() + " datablock " + DataBlockName + " query - " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();
                Finished = false;
                return Finished;
            }

            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static bool AddRecipeData(int AccountID, string RecipeID, string DataBlockName, string NewParameter)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);

            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;

            sql = "UPDATE players_recipes SET " + DataBlockName + " =" + DataBlockName + " + " + @"""" + NewParameter + @"""" + "WHERE AccountID = " + AccountID.ToString() + " AND RecipeID = " + RecipeID.ToString() + ";";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - AddRecipeData] Запрос к базе для аккаунта: " + AccountID.ToString() + " datablock " + DataBlockName + " query - " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();
                Finished = false;
                return Finished;
            }

            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static bool MinusData(int AccountID, string DataBlockName, string NewParameter)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);

            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;

            sql = "UPDATE users SET " + DataBlockName + " =" + DataBlockName + " - " + @"""" + NewParameter + @"""" + "WHERE AccountID = " + AccountID.ToString() + ";";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - Account.AddData] Запрос к базе для аккаунта: " + AccountID.ToString() + " datablock " + DataBlockName + " query - " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();
                Finished = false;
                return Finished;
            }

            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static void ShowInventory(int AccountID)
        {
            Console.WriteLine("---------------------------");
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT Attack, Def, MainhandSlot, OffhandSlot, HeadSlot, NeckSlot, ChestSlot, PantsSlot, FeetSlot FROM users WHERE AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            Console.WriteLine("Запрос инвентаря от ID: " + AccountID + " запрос - " + sql_commmand.CommandText);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            string InventoryString = string.Empty;
            string InventoryTempString = string.Empty;
            string UsableString = string.Empty;
            string WearableString = string.Empty;
            string PriceString = string.Empty;

            string Attack = string.Empty;
            string Def = string.Empty;
            string MainhandSlot = string.Empty;
            string OffhandSlot = string.Empty;
            string HeadSlot = string.Empty;
            string NeckSlot = string.Empty;
            string ChestSlot = string.Empty;
            string PantsSlot = string.Empty;
            string FeetSlot = string.Empty;
            string MainhandSlotString = string.Empty;
            string OffhandSlotString = string.Empty;
            string HeadSlotString = string.Empty;
            string NeckSlotString = string.Empty;
            string ChestSlotString = string.Empty;
            string PantsSlotString = string.Empty;
            string FeetSlotString = string.Empty;

            while (thisReader.Read())
            {


                UsableString = string.Empty;
                WearableString = string.Empty;
                PriceString = string.Empty;
                Attack += thisReader["Attack"];
                Def += thisReader["Def"];
                MainhandSlot += thisReader["MainhandSlot"];
                OffhandSlot += thisReader["OffhandSlot"];
                HeadSlot += thisReader["HeadSlot"];
                NeckSlot += thisReader["NeckSlot"];
                ChestSlot += thisReader["ChestSlot"];
                PantsSlot += thisReader["PantsSlot"];
                FeetSlot += thisReader["FeetSlot"];

            }
            if (Convert.ToInt32(MainhandSlot) > 0)
            {
               Inventory.Items.Item.GetAllInfo(Convert.ToInt32(MainhandSlot), out string MainhandName, out string MainhandUsable, out string MainhandWearable, out string MainhandWearSlot, out string MainhandPrice, out string MainhandType, out string MainhandStrength, out string MainhandCharisma, out string MainhandIntelligence, out string MainhandAgility, out string MainhandLuck, out string MainhandAttack, out string MainhandDef, out string ItemPower, out string ItemText);
                MainhandSlotString = MainhandName + " | ⚔️" + MainhandAttack + " 🛡" + MainhandDef + " | /off_" + MainhandSlot + "\n";
            }
            if (Convert.ToInt32(OffhandSlot) > 0)
            {
                Inventory.Items.Item.GetAllInfo(Convert.ToInt32(OffhandSlot), out string OffhandName, out string OffhandUsable, out string OffhandWearable, out string OffhandWearSlot, out string OffhandPrice, out string OffhandType, out string OffhandStrength, out string OffhandCharisma, out string OffhandIntelligence, out string OffhandAgility, out string OffhandLuck, out string OffhandAttack, out string OffhandDef, out string ItemPower, out string ItemText);
                OffhandSlotString = OffhandName + " | ⚔️" + OffhandAttack + " 🛡" + OffhandDef + " | /off_" + OffhandSlot + "\n";
            }
            if (Convert.ToInt32(HeadSlot) > 0)
            {
                Inventory.Items.Item.GetAllInfo(Convert.ToInt32(HeadSlot), out string HeadName, out string HeadUsable, out string HeadWearable, out string HeadWearSlot, out string HeadPrice, out string HeadType, out string HeadStrength, out string HeadCharisma, out string HeadIntelligence, out string HeadAgility, out string HeadLuck, out string HeadAttack, out string HeadDef, out string ItemPower, out string ItemText);
                HeadSlotString = HeadName + " | ⚔️" + HeadAttack + " 🛡" + HeadDef + " | /off_" + HeadSlot + "\n";
            }
            if (Convert.ToInt32(NeckSlot) > 0)
            {
                Inventory.Items.Item.GetAllInfo(Convert.ToInt32(NeckSlot), out string NeckName, out string NeckUsable, out string NeckWearable, out string NeckWearSlot, out string NeckPrice, out string NeckType, out string NeckStrength, out string NeckCharisma, out string NeckIntelligence, out string NeckAgility, out string NeckLuck, out string NeckAttack, out string NeckDef, out string ItemPower, out string ItemText);
                NeckSlotString = NeckName + " | ⚔️" + NeckAttack + " 🛡" + NeckDef + " | /off_" + NeckSlot + "\n";
            }
            if (Convert.ToInt32(ChestSlot) > 0)
            {
                Inventory.Items.Item.GetAllInfo(Convert.ToInt32(ChestSlot), out string ChestName, out string ChestUsable, out string ChestWearable, out string ChestWearSlot, out string ChestPrice, out string ChestType, out string ChestStrength, out string ChestCharisma, out string ChestIntelligence, out string ChestAgility, out string ChestLuck, out string ChestAttack, out string ChestDef, out string ItemPower, out string ItemText);
                ChestSlotString = ChestName + " |⚔️" + ChestAttack + " 🛡" + ChestDef + " | /off_" + ChestSlot + "\n";
            }
            if (Convert.ToInt32(PantsSlot) > 0)
            {
                Inventory.Items.Item.GetAllInfo(Convert.ToInt32(PantsSlot), out string PantsName, out string PantsUsable, out string PantsWearable, out string PantsWearSlot, out string PantsPrice, out string PantsType, out string PantsStrength, out string PantsCharisma, out string PantsIntelligence, out string PantsAgility, out string PantsLuck, out string PantsAttack, out string PantsDef, out string ItemPower, out string ItemText);
                PantsSlotString = PantsName + " | ⚔️" + PantsAttack + " 🛡" + PantsDef + " | /off_" + PantsSlot + "\n";
            }
            if (Convert.ToInt32(FeetSlot) > 0)
            {
                Inventory.Items.Item.GetAllInfo(Convert.ToInt32(FeetSlot), out string FeetName, out string FeetUsable, out string FeetWearable, out string FeetWearSlot, out string FeetPrice, out string FeetType, out string FeetStrength, out string FeetCharisma, out string FeetIntelligence, out string FeetAgility, out string FeetLuck, out string FeetAttack, out string FeetDef, out string ItemPower, out string ItemText);
                FeetSlotString = FeetName + " | ⚔️" + FeetAttack + " 🛡" + FeetDef + " | /off_" + FeetSlot + "\n";
            }
            InventoryString = "⚔️ Атака: " + Attack + "\n🛡 Защита: " + Def + "\n\n📦 Экипировка:\n" + MainhandSlotString + OffhandSlotString + HeadSlotString + NeckSlotString + ChestSlotString + PantsSlotString + FeetSlotString + "";

            thisReader.Close();

            try
            {
                Message.Send(AccountID, InventoryString, 5);
            }
            catch (Exception MyEx)
            {
                Console.WriteLine(MyEx);
            }

            Console.WriteLine("---------------------------");
            myConnection.Close();

        }

        #region Account.SetGold(int AccountID, int GoldCount, bool Minus) - Добавляет Деньги игроку. Если последний параметр True - отнимает Деньги.
        public static bool SetGold(int AccountID, int GoldCount, bool Minus)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT Money FROM users WHERE AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string Gold = string.Empty;
            while (thisReader.Read())
            {
                Gold += thisReader["Money"];
            }
            thisReader.Close();

            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            int GoldResult;
            if (Minus == true)
            {
                GoldResult = Convert.ToInt32(Gold) - Convert.ToInt32(GoldCount);
            }
            else
                GoldResult = Convert.ToInt32(Gold) + Convert.ToInt32(GoldCount);
            sql = "UPDATE users SET Money = " + GoldResult.ToString() + " WHERE AccountID = " + AccountID.ToString();
            MySqlScript script = new MySqlScript(myConnection, sql);
            script.Query = sql;
            script.Connection = myConnection;
            Console.WriteLine("");
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine(sqlEx);
                myConnection.Close();

            }

            myConnection.Close();
            bool Finished = true;
            return Finished;
        }
        #endregion

        public static bool DeleteItem(int AccountID, string ItemID, string DeleteCount)
        {
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "--------------Account.DeleteItemID " + AccountID + " -------------");
            string ItemCount = Inventory.GetSingleData(AccountID, Convert.ToInt32(ItemID), "ItemCount");
            bool ReturnStatement = false;
            Console.WriteLine("Item Count = " + ItemCount + " DeleteCount = " + DeleteCount);
            if (ItemCount == "")
            {
                ReturnStatement = false;
            }
            if (Convert.ToInt32(ItemCount) > Convert.ToInt32(DeleteCount)) // Если предметов у игрока больше чем мы удаляем
            {
                Inventory.AddSingleData(AccountID, Convert.ToInt32(ItemID), "ItemCount", DeleteCount, true); // Удаляем количество предметов
                ReturnStatement = true;
            }
            else if (Convert.ToInt32(ItemCount) < Convert.ToInt32(DeleteCount)) // Если предметов у игрока меньше, чем мы пытаемся удалить
            {
                ReturnStatement = false;
            }
            else if (Convert.ToInt32(ItemCount) == Convert.ToInt32(DeleteCount))
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();

                string sql;
                sql = "DELETE FROM inventory WHERE AccountID = " + AccountID.ToString() + " AND ItemID = " + ItemID.ToString();
                MySqlScript script = new MySqlScript(myConnection, sql);
                script.Query = sql;
                script.Connection = myConnection;
                try
                {
                    script.Execute();
                    Console.WriteLine(sql);
                }
                catch (MySqlException sqlEx)
                {
                    Console.WriteLine(sqlEx);
                    myConnection.Close();

                }

                myConnection.Close();
                ReturnStatement = true;
            }
            else
            {
                ReturnStatement = false;
            }
            Console.WriteLine("---------------------------");
            return ReturnStatement;
        }

        public static void GiveItem(int AccountID, string ItemID, string AddCount)
        {
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "--------------Account.GiveItem ID " + AccountID + " -------------");
            string ItemCount = Inventory.GetSingleData(AccountID, Convert.ToInt32(ItemID), "ItemCount");

            string ItemName, ItemUsable, ItemWearable, ItemWearSlot, ItemPrice, ItemType, ItemPower;

            Inventory.Items.Item.GetInfo(Convert.ToInt32(ItemID), out ItemName, out ItemUsable, out ItemWearable, out ItemWearSlot, out ItemPrice, out ItemType, out ItemPower);

            Console.WriteLine("DEBUG STRING OUT: " + ItemName + " - " + ItemUsable + " - " + ItemWearable + " - " + ItemWearSlot + " - " + ItemPrice + " - " + ItemType);

            if (ItemCount == "")
            {
                Inventory.AddSingleData(AccountID, ItemID, ItemType, AddCount);
            }
            else if (Convert.ToInt32(ItemCount) > 1) // Если предметов у игрока больше 1
            {
                Inventory.AddSingleData(AccountID, Convert.ToInt32(ItemID), "ItemCount", AddCount, false); // Добавляем количество предметов
            }
            else if (Convert.ToInt32(ItemCount) == 1)
            {
                Inventory.AddSingleData(AccountID, Convert.ToInt32(ItemID), "ItemCount", AddCount, false); // Добавляем количество предметов
            }
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - Account.GiveItem] Item gived!");
            Console.WriteLine("---------------------------");
        }

        public static int TakeGoldForced(int PlayerID, int TakeGoldCount)
        {
            int GoldTaken = 0;
            int CurrentGoldCountHavePlayer = Convert.ToInt32(GetGold(PlayerID));
            if (CurrentGoldCountHavePlayer >= TakeGoldCount) // Если золота у игрока больше, чем нужно отнять
            {
                GoldTaken = TakeGoldCount;
                SetGold(PlayerID, TakeGoldCount, true);
            }
            else if (CurrentGoldCountHavePlayer < TakeGoldCount) // Если у игрока золота меньше, чем нужно отнять
            {
                GoldTaken = TakeGoldCount - CurrentGoldCountHavePlayer;
                SetGold(PlayerID, GoldTaken, true);
            }
            return GoldTaken;
        }

        public static bool TakeGold(int PlayerID, int TakeGoldCount)
        {
            int GoldTaken = 0;
            int CurrentGoldCountHavePlayer = Convert.ToInt32(GetGold(PlayerID));
            if (CurrentGoldCountHavePlayer >= TakeGoldCount) // Если золота у игрока больше, чем нужно отнять
            {
                GoldTaken = TakeGoldCount;
                SetGold(PlayerID, TakeGoldCount, true);
                return true;
            }
            else if (CurrentGoldCountHavePlayer < TakeGoldCount) // Если у игрока золота меньше, чем нужно отнять
            {
                return false;
            }
            return false;
        }

        public static string GetGold(int AccountID) // Старая функция. Позволяет получить кол-во золота игрока по его AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT Money FROM users WHERE AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string Gold = string.Empty;
            while (thisReader.Read())
            {
                Gold += thisReader["Money"];
            }
            thisReader.Close();
            myConnection.Close();

            Convert.ToInt32(Gold);
            return Gold;
        }

        public static int AddHealth(int AccountID, int HealthCount)
        {
            string MaxPlayerHealth = Account.GetSingleData(AccountID, "MaxHealth");
            string CurrentPlayerHealth = Account.GetSingleData(AccountID, "Health");
            if (Convert.ToInt32(MaxPlayerHealth) == Convert.ToInt32(CurrentPlayerHealth))
            {
                return 0;
            }
            else if (Convert.ToInt32(CurrentPlayerHealth) < Convert.ToInt32(MaxPlayerHealth))
            {
                if ((HealthCount + Convert.ToInt32(CurrentPlayerHealth)) <= (Convert.ToInt32(MaxPlayerHealth)))
                {
                    int HealthToRecover = HealthCount + Convert.ToInt32(CurrentPlayerHealth);
                    Account.SetSingleData(AccountID, "Health", HealthToRecover.ToString());
                    return HealthToRecover;
                }
                else
                {
                    int HealthNeedToRestore = Convert.ToInt32(MaxPlayerHealth) - Convert.ToInt32(CurrentPlayerHealth);
                    HealthNeedToRestore = HealthNeedToRestore + Convert.ToInt32(CurrentPlayerHealth);
                    Account.SetSingleData(AccountID, "Health", HealthNeedToRestore.ToString());
                    return HealthNeedToRestore;
                }
            }
            else return 0;
        }

        public static bool DeleteHealth(int AccountID, int HealthCount)
        {
            string MaxPlayerHealth = Account.GetSingleData(AccountID, "MaxHealth");
            string CurrentPlayerHealth = Account.GetSingleData(AccountID, "Health");
            if (Convert.ToInt32(CurrentPlayerHealth) < 1)
            {
                return false;
            }
            if (Convert.ToInt32(CurrentPlayerHealth) <= HealthCount)
            {
                Account.SetDeath(AccountID);
                return true;
            }
            else
            {
                Account.MinusData(AccountID, "Health", HealthCount.ToString());
                return true;
            }
        }

        public static void SetDeath(int AccountID)
        {
            Account.SetSingleData(AccountID, "X", "0");
            Account.SetSingleData(AccountID, "Y", "0");
            Ships.Ship.SetSingleData(AccountID, "X", "0");
            Ships.Ship.SetSingleData(AccountID, "Y", "0");
            Program.PlayerDeleteBetweenEvent(AccountID);
            Program.PlayerDeleteEvent(AccountID);
            Program.PlayerDeleteMoveShipEvent(AccountID);
            //string CurrentPlayerMoney = Account.GetSingleData(AccountID, "Money");
            //int TakeGold = ProcentResultCount(Convert.ToInt32(CurrentPlayerMoney), 50);
            //int GoldTaken = Account.TakeGoldForced(AccountID, TakeGold);
            Message.Send(AccountID, "☠️ Вы получили повреждения не совместимые с дальнейшей жизнедеятельностью. ☠️\n/revive", 1);
            Account.SetSingleData(AccountID, "Status", "666");
            Account.SetSingleData(AccountID, "CurrentAction", "666");
        }

        public static void SetDeathSilent(int AccountID)
        {
            Account.SetSingleData(AccountID, "X", "0");
            Account.SetSingleData(AccountID, "Y", "0");
            Ships.Ship.SetSingleData(AccountID, "X", "0");
            Ships.Ship.SetSingleData(AccountID, "Y", "0");
            Program.PlayerDeleteBetweenEvent(AccountID);
            Program.PlayerDeleteEvent(AccountID);
            Program.PlayerDeleteMoveShipEvent(AccountID);
            //string CurrentPlayerMoney = Account.GetSingleData(AccountID, "Money");
            //int TakeGold = ProcentResultCount(Convert.ToInt32(CurrentPlayerMoney), 50);
            //int GoldTaken = Account.TakeGoldForced(AccountID, TakeGold);
            Account.SetSingleData(AccountID, "Status", "666");
            Account.SetSingleData(AccountID, "CurrentAction", "666");
        }

        public static bool ChangeNick(int AccountID, string CharName)
        {
            int keyboard = 1;
            string PlayerStatus = Account.GetSingleData(Convert.ToInt32(AccountID), "Status");
            if (PlayerStatus == "1") // Игрок на начальной клавиатуре
            {
                keyboard = 25;
            }
            else
            {
                keyboard = 1;
            }
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            char[] charsToTrim = { '*', ' ', '\'', '№', '@', '!', '#', '$', '%', '^', '&', '(', ')', '"', ';', ':', '?', '/' }; // Обрезаем символы
            CharName = CharName.Replace('(', '@');
            CharName = CharName.Replace(')', '@');
            CharName = CharName.Trim(charsToTrim);

            sql = "UPDATE users SET CharName =" + @"""" + CharName + @"""" + " WHERE AccountID = " + AccountID.ToString() + ";";
            Console.WriteLine("Account.ChangeNick Command from: " + AccountID.ToString() + " query - " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();

            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine(sqlEx);

                myConnection.Close();
                return false;
            }
            Console.WriteLine("Nick changing: AcID: " + AccountID + " keyboard: " + keyboard);
            Account.SetSingleData(AccountID, "NickChanged", "1");

            myConnection.Close(); //Обязательно закрываем соединение!
            return true;
        }

        public static string GetAccountExists(int AccountID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID FROM users WHERE AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string AccountExists = string.Empty;
            while (thisReader.Read())
            {
                AccountExists += thisReader["AccountID"];
            }
            thisReader.Close();
            myConnection.Close();
            return AccountExists;
        }

        public static bool IsInClan(int AccountID, int ClanID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM clans WHERE AccountID = @AccountID AND ClanID = @ClanID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            sql_commmand.Parameters.AddWithValue("@ClanID", ClanID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string PlayerInClanID = string.Empty;
            while (thisReader.Read())
            {
                PlayerInClanID += thisReader["ID"];
            }
            thisReader.Close();
            myConnection.Close();
            if (PlayerInClanID != "" && PlayerInClanID != " ")
            {
                return true;
            }
            else return false;
        }

        public static bool StatDiceCheck(int PlayerID, string Stat)
        {
            int StatToCheck = Convert.ToInt32(Account.GetSingleData(PlayerID, Stat));
            int StatChance = rndmzr.Next(1, 101);

            if (StatToCheck >= 5 && StatToCheck <= 10)
            {
                if (StatChance >= 1 && StatChance <= 50)
                {
                    return true;
                }
                else return false;
            }
            else if (StatToCheck >= 11 && StatToCheck <= 20)
            {
                if (StatChance >= 1 && StatChance <= 60)
                {
                    return true;
                }
                else return false;
            }
            else if (StatToCheck >= 21 && StatToCheck <= 30)
            {
                if (StatChance >= 1 && StatChance <= 70)
                {
                    return true;
                }
                else return false;
            }
            else if (StatToCheck >= 31)
            {
                if (StatChance >= 1 && StatChance <= 80)
                {
                    return true;
                }
                else return false;
            }
            else return false;


        }

        public static void SetLevelUp(int AccountID)
        {
            string TalantPoints = Account.GetSingleData(Convert.ToInt32(AccountID), "TalantPoints");
            int NewTalantPoints = Convert.ToInt32(TalantPoints) + 1;
            Account.SetSingleData(Convert.ToInt32(AccountID), "TalantPoints", Convert.ToString(NewTalantPoints));
            string TalantPointSpend = Account.GetSingleData(Convert.ToInt32(AccountID), "TalantPointSpend");
            int NewTalantPointSpend = Convert.ToInt32(TalantPointSpend) + 1;
            Account.SetSingleData(Convert.ToInt32(AccountID), "TalantPointSpend", Convert.ToString(NewTalantPointSpend));
            string PlayerLevel = Account.GetSingleData(Convert.ToInt32(AccountID), "Level");
            int NewPlayerLevel = Convert.ToInt32(PlayerLevel) + 1;
            Account.SetSingleData(Convert.ToInt32(AccountID), "Level", Convert.ToString(NewPlayerLevel));
            Message.Send(Convert.ToInt32(AccountID), "Получено очко талантов! Здоровье востановленно!", 1);
            string MaxHealth = Account.GetSingleData(AccountID, "MaxHealth");
            Account.SetSingleData(AccountID, "Health", MaxHealth);
            //SendStickerMessage(Convert.ToInt32(AccountID), "CAADAgADqRUAAlbs5wNVBXd0yJ1aUQI");
        }

        public static string CheckOnEvent(int AccountID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID FROM on_event WHERE AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string AccountOnEvent = string.Empty;
            while (thisReader.Read())
            {
                AccountOnEvent += thisReader["AccountID"];
            }
            thisReader.Close();
            myConnection.Close();


            if (AccountOnEvent == "0" || AccountOnEvent == "")
            {
                myConnection = new MySqlConnection(Connect);
                myConnection.Open();

                sql_commmand = new MySqlCommand("SELECT AccountID FROM on_move_event WHERE AccountID = @AccountID", myConnection);
                sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
                thisReader = sql_commmand.ExecuteReader();
                while (thisReader.Read())
                {
                    AccountOnEvent += thisReader["AccountID"];
                }
                thisReader.Close();
                myConnection.Close();
            }

            if (AccountOnEvent == "0" || AccountOnEvent == "")
            {
                myConnection = new MySqlConnection(Connect);
                myConnection.Open();

                sql_commmand = new MySqlCommand("SELECT AccountID FROM on_planet_move_event WHERE AccountID = @AccountID", myConnection);
                sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
                thisReader = sql_commmand.ExecuteReader();
                while (thisReader.Read())
                {
                    AccountOnEvent += thisReader["AccountID"];
                }
                thisReader.Close();
                myConnection.Close();
            }


            return AccountOnEvent;

        }

        public static string CheckOnBetweenEvents(int AccountID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID FROM on_between_events WHERE AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string AccountOnEvent = string.Empty;
            while (thisReader.Read())
            {
                AccountOnEvent += thisReader["AccountID"];
            }
            thisReader.Close();
            myConnection.Close();

            return AccountOnEvent;
        }

        public static void PlanetMapMove(int PlayerID, string PlayerCourse)
        {

            if (PlayerCourse == "UP")
            {
                try
                {
                    Program.PlayerPlanetMoveSetEvent(PlayerID, 1, 300, 0);
                    Message.Send(PlayerID, "Вы пошли на север. \nБудете идти ⏱ " + (300) + " сек.", 1);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine("ОШИБКА!: " + MyEx);
                }
            }

            if (PlayerCourse == "DOWN")
            {
                try
                {
                    Program.PlayerPlanetMoveSetEvent(PlayerID, 2, 300, 0);
                    Message.Send(PlayerID, "Вы пошли на юг. \nБудете идти ⏱ " + (300) + " сек.", 1);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine("ОШИБКА!: " + MyEx);
                }
            }

            if (PlayerCourse == "LEFT")
            {
                try
                {
                    Program.PlayerPlanetMoveSetEvent(PlayerID, 3, 300, 0);
                    Message.Send(PlayerID, "Вы пошли на запад. \nБудете идти ⏱ " + (300) + " сек.", 1);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine("ОШИБКА!: " + MyEx);
                }

            }

            if (PlayerCourse == "RIGHT")
            {
                try
                {
                    Program.PlayerPlanetMoveSetEvent(PlayerID, 4, 300, 0);
                    Message.Send(PlayerID, "Вы пошли на восток. \nБудете идти ⏱ " + (300) + " сек.", 1);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine("ОШИБКА!: " + MyEx);
                }
            }
        }

        public static void MapMove(int PlayerID, string PlayerCourse)
        {
            int OverweightBonusEnergy = Program.OverweightCalculation(PlayerID);

            string OverweightStringWarning = string.Empty;
            if (OverweightBonusEnergy == 1)
            {
                OverweightStringWarning = "<b>Превышен максимальный вес груза!</b>\nРасход энергии на перемещение увеличен до ⚡️" + (OverweightBonusEnergy + 1) + " ед.\n";
                Ships.Ship.SetSingleData(PlayerID, "ShipEnergyOverweight", OverweightBonusEnergy.ToString());
            }
            if (OverweightBonusEnergy == 2)
            {
                OverweightStringWarning = "<b>Превышен максимальный вес груза!</b>\nРасход энергии на перемещение увеличен до ⚡️" + (OverweightBonusEnergy + 1) + " ед.\n";
                Ships.Ship.SetSingleData(PlayerID, "ShipEnergyOverweight", OverweightBonusEnergy.ToString());
            }
            if (OverweightBonusEnergy == 3)
            {
                OverweightStringWarning = "<b>Превышен максимальный вес груза!</b>\nРасход энергии на перемещение увеличен до ⚡️" + (OverweightBonusEnergy + 1) + " ед.\n";
                Ships.Ship.SetSingleData(PlayerID, "ShipEnergyOverweight", OverweightBonusEnergy.ToString());
            }
            if (OverweightBonusEnergy == 4)
            {
                OverweightStringWarning = "<b>Превышен максимальный вес груза!</b>\nРасход энергии на перемещение увеличен до ⚡️" + (OverweightBonusEnergy + 1) + " ед.\n";
                Ships.Ship.SetSingleData(PlayerID, "ShipEnergyOverweight", OverweightBonusEnergy.ToString());
            }
            if (OverweightBonusEnergy == 5)
            {
                OverweightStringWarning = "<b>Превышен максимальный вес груза!</b>\nРасход энергии на перемещение увеличен до ⚡️" + (OverweightBonusEnergy + 1) + " ед.\n";
                Ships.Ship.SetSingleData(PlayerID, "ShipEnergyOverweight", OverweightBonusEnergy.ToString());
            }

            if (PlayerCourse == "UP")
            {
                try
                {
                    Program.PlayerMoveSetEvent(PlayerID, 1, 300, OverweightBonusEnergy);
                    string SecondSpeed = Ships.Ship.GetSingleData(PlayerID, "ShipSpeed"); // Получаем скорость корабля
                    double BonusSecondBySpeed = Convert.ToDouble(SecondSpeed) / 2.0f;
                    Message.Send(PlayerID, OverweightStringWarning + "Вы взяли курс на север. \nПолет продлится ⏱ " + (300 - BonusSecondBySpeed) + " сек.", 1);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine("ОШИБКА!: " + MyEx);
                }
            }

            if (PlayerCourse == "DOWN")
            {
                try
                {
                    Program.PlayerMoveSetEvent(PlayerID, 2, 300, OverweightBonusEnergy);
                    string SecondSpeed = Ships.Ship.GetSingleData(PlayerID, "ShipSpeed"); // Получаем скорость корабля
                    double BonusSecondBySpeed = Convert.ToDouble(SecondSpeed) / 2.0f;
                    Message.Send(PlayerID, OverweightStringWarning + "Вы взяли курс на юг. \nПолет продлится ⏱ " + (300 - BonusSecondBySpeed) + " сек.", 1);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine("ОШИБКА!: " + MyEx);
                }
            }

            if (PlayerCourse == "LEFT")
            {
                try
                {
                    Program.PlayerMoveSetEvent(PlayerID, 3, 300, OverweightBonusEnergy);
                    string SecondSpeed = Ships.Ship.GetSingleData(PlayerID, "ShipSpeed"); // Получаем скорость корабля
                    double BonusSecondBySpeed = Convert.ToDouble(SecondSpeed) / 2.0f;
                    Message.Send(PlayerID, OverweightStringWarning + "Вы взяли курс на запад. \nПолет продлится ⏱ " + (300 - BonusSecondBySpeed) + " сек.", 1);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine("ОШИБКА!: " + MyEx);
                }

            }

            if (PlayerCourse == "RIGHT")
            {
                try
                {
                    Program.PlayerMoveSetEvent(PlayerID, 4, 300, OverweightBonusEnergy);
                    string SecondSpeed = Ships.Ship.GetSingleData(PlayerID, "ShipSpeed"); // Получаем скорость корабля
                    double BonusSecondBySpeed = Convert.ToDouble(SecondSpeed) / 2.0f;
                    Message.Send(PlayerID, OverweightStringWarning + "Вы взяли курс на восток. \nПолет продлится ⏱ " + (300 - BonusSecondBySpeed) + " сек.", 1);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine("ОШИБКА!: " + MyEx);
                }
            }
        }
    }
}
