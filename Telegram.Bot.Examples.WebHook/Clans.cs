using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Telegram.Bot.Examples.WebHook.DataStorage;

namespace Telegram.Bot.Examples.WebHook
{
    public class Clans
    {
        public static string GetDataByClanID(int ClanID, string DataBlockName) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM clans_data WHERE ID = @ClanID", myConnection);
            sql_commmand.Parameters.AddWithValue("@ClanID", ClanID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["" + DataBlockName + ""];
            }
            thisReader.Close();
            myConnection.Close();

            // Convert.ToInt64(DataBlockResult);
            return DataBlockResult;
        }

        public static string GetDataByOwnerID(int ClanOwnerID, string DataBlockName)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM clans_data WHERE ClanOwner = @ClanOwnerID", myConnection);
            sql_commmand.Parameters.AddWithValue("@ClanOwnerID", ClanOwnerID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["" + DataBlockName + ""];
            }
            thisReader.Close();
            myConnection.Close();

            // Convert.ToInt64(DataBlockResult);
            return DataBlockResult;
        }

        public static string GetDataByAccountID(int AccountID, string DataBlockName)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM clans WHERE AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["" + DataBlockName + ""];
            }
            thisReader.Close();
            myConnection.Close();

            // Convert.ToInt64(DataBlockResult);
            return DataBlockResult;
        }

        public static string GetShipsList(int ClanID)
        {
            string HireString = string.Empty;
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID, ID FROM clans WHERE ClanID = " + ClanID + " ORDER BY ID DESC;", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            string ClanList = string.Empty;
            int Number = 1;

            while (thisReader.Read())
            {
                string TempString = string.Empty;
                string AccountID = string.Empty;
                string ID = string.Empty;
                AccountID += thisReader["AccountID"];
                ID += thisReader["ID"];
                #region Перечисление типов кораблей
                string ShipType = Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "ShipType");
                string ShipTypeName = "";
                switch (Convert.ToInt32(ShipType)) // Классы кораблей, названия кораблей
                {
                    case 1:
                        ShipTypeName = "[ЛП]";
                        break;
                    case 2:
                        ShipTypeName = "[СП]";
                        break;
                    case 3:
                        ShipTypeName = "[ТП]";
                        break;
                    case 4:
                        ShipTypeName = "[Б]";
                        break;
                    case 5:
                        ShipTypeName = "[ТБ]";
                        break;
                    case 6:
                        ShipTypeName = "[Ф]";
                        break;
                    case 7:
                        ShipTypeName = "[ТФ]";
                        break;
                    case 8:
                        ShipTypeName = "[Э]";
                        break;
                    case 9:
                        ShipTypeName = "[К]";
                        break;
                    case 10:
                        ShipTypeName = "[КТ]";
                        break;
                    case 11:
                        ShipTypeName = "[Л]";
                        break;
                    case 12:
                        ShipTypeName = "[УЛ]";
                        break;
                    case 13:
                        ShipTypeName = "[ТЛ]";
                        break;
                    case 14:
                        ShipTypeName = "[А]";
                        break;
                    case 15:
                        ShipTypeName = "[МК]";
                        break;
                }
                #endregion

                TempString = Number + ". " + "🚀 <b>" + Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "ShipName") + "</b> " + ShipTypeName + "\n(кп. " + Account.GetSingleData(Convert.ToInt32(AccountID), "CharName") + ")\n" + "❤️ " + Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "ShipHealth") + @"\" + Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "ShipMaxHealth") + " ⚔️ " + Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "ShipAttack") + " 🛡 " + Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "ShipDef") + "\n⚡️ " + Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "ShipEnergy") + " 🌀 " + Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "ShipManeuv") + " 🏁 " + Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "ShipSpeed") + "\n📡 " + Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "X") + ":" + Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "Y") + "\n\n";

                ClanList = ClanList + TempString;
                Number++;
            }
            thisReader.Close();
            myConnection.Close();
            return ClanList;
        }

        public static string GetPlayersList(int ClanID)
        {
            string HireString = string.Empty;
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID, ID FROM clans WHERE ClanID = " + ClanID + " ORDER BY ID DESC;", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            string ClanList = string.Empty;
            int Number = 1;

            while (thisReader.Read())
            {
                string TempString = string.Empty;
                string AccountID = string.Empty;
                string ID = string.Empty;
                AccountID += thisReader["AccountID"];
                ID += thisReader["ID"];

                TempString = Number + ". " + Account.GetSingleData(Convert.ToInt32(AccountID), "CharName") + " (" + Account.GetSingleData(Convert.ToInt32(AccountID), "Level") + " ур.)" + "\n";

                ClanList = ClanList + TempString;
                Number++;
            }
            thisReader.Close();
            myConnection.Close();
            return ClanList;
        }

        public static bool IsExists(int ClanID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM clans_data WHERE ID = @ClanID", myConnection);
            sql_commmand.Parameters.AddWithValue("@ClanID", ClanID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string PlayerInClanID = string.Empty;
            while (thisReader.Read())
            {
                PlayerInClanID += thisReader["ID"];
            }
            thisReader.Close();
            myConnection.Close();
            if (PlayerInClanID != "" && PlayerInClanID != " ")
            {
                return true;
            }
            else return false;
        }

        public static bool IsExistsByClanOwner(int ClanOwner)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM clans_data WHERE ClanOwner = @ClanOwner", myConnection);
            sql_commmand.Parameters.AddWithValue("@ClanOwner", ClanOwner);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string PlayerInClanID = string.Empty;
            while (thisReader.Read())
            {
                PlayerInClanID += thisReader["ID"];
            }
            thisReader.Close();
            myConnection.Close();
            if (PlayerInClanID != "" && PlayerInClanID != " ")
            {
                return true;
            }
            else return false;
        }

        public static bool IsExists(string ClanName)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM clans_data WHERE ClanName = @ClanName", myConnection);
            sql_commmand.Parameters.AddWithValue("@ClanName", ClanName);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string PlayerInClanID = string.Empty;
            while (thisReader.Read())
            {
                PlayerInClanID += thisReader["ID"];
            }
            thisReader.Close();
            myConnection.Close();
            if (PlayerInClanID != "" && PlayerInClanID != " ")
            {
                return true;
            }
            else return false;
        }

        public static bool IsExistsByClanTag(string ClanTag)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM clans_data WHERE ClanTag = @ClanTag", myConnection);
            sql_commmand.Parameters.AddWithValue("@ClanTag", ClanTag);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string PlayerInClanID = string.Empty;
            while (thisReader.Read())
            {
                PlayerInClanID += thisReader["ID"];
            }
            thisReader.Close();
            myConnection.Close();
            if (PlayerInClanID != "" && PlayerInClanID != " ")
            {
                return true;
            }
            else return false;
        }

        public static void GetAllInfo(int ClanID, out string ReturnClanOwner, out string ReturnClanName, out string ReturnClanTag, out string ReturnClanLevel, out string ReturnClanPlayersCount, out string ReturnClanMaxPlayersCount, out string ReturnActive)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_command = new MySqlCommand("SELECT ClanOwner, ClanName, ClanTag, ClanLevel, ClanPlayersCount, ClanMaxPlayersCount, Active FROM clans_data WHERE ID = @ClanID", myConnection);
            sql_command.Parameters.AddWithValue("@ClanID", ClanID);
            MySqlDataReader thisReader = sql_command.ExecuteReader();
            string ClanOwner = string.Empty;
            string ClanName = string.Empty;
            string ClanTag = string.Empty;
            string ClanLevel = string.Empty;
            string ClanPlayersCount = string.Empty;
            string ClanMaxPlayersCount = string.Empty;
            string Active = string.Empty;

            while (thisReader.Read())
            {
                ClanOwner += thisReader["ClanOwner"];
                ClanName += thisReader["ClanName"];
                ClanTag += thisReader["ClanTag"];
                ClanLevel += thisReader["ClanLevel"];
                ClanPlayersCount += thisReader["ClanPlayersCount"];
                ClanMaxPlayersCount += thisReader["ClanMaxPlayersCount"];
                Active += thisReader["Active"];
            }
            thisReader.Close();
            myConnection.Close();

            ReturnClanOwner = ClanOwner;
            ReturnClanName = ClanName;
            ReturnClanTag = ClanTag;
            ReturnClanLevel = ClanLevel;
            ReturnClanPlayersCount = ClanPlayersCount;
            ReturnClanMaxPlayersCount = ClanMaxPlayersCount;
            ReturnActive = Active;
        }

        public static int Create(int FounderID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            using (MySqlCommand cmd = new MySqlCommand("INSERT INTO clans_data (ClanOwner, ClanName, ClanTag, ClanLevel, ClanPlayersCount, ClanMaxPlayersCount, Active) VALUES (@ClanOwner, @ClanName, @ClanTag, @ClanLevel, @ClanPlayersCount, @ClanMaxPlayersCount, @Active); SELECT LAST_INSERT_ID();", myConnection))
            {
                cmd.Parameters.AddWithValue("@ClanOwner", FounderID);
                cmd.Parameters.AddWithValue("@ClanName", "");
                cmd.Parameters.AddWithValue("@ClanTag", "");
                cmd.Parameters.AddWithValue("@ClanLevel", "1");
                cmd.Parameters.AddWithValue("@ClanPlayersCount", "1");
                cmd.Parameters.AddWithValue("@ClanMaxPlayersCount", "5");
                cmd.Parameters.AddWithValue("@Active", "0");
                myConnection.Open();

                int modified = Convert.ToInt32(cmd.ExecuteScalar());

                if (myConnection.State == System.Data.ConnectionState.Open)
                    myConnection.Close();

                return modified;
            }
        }

        public static bool SetClanData(int ClanID, string DataBlockName, string NewParameter)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);

            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;

            sql = "UPDATE clans_data SET " + DataBlockName + " =" + @"""" + NewParameter + @"""" + " WHERE ID = " + ClanID.ToString() + ";";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - Clans.SetClanData] Запрос к базе для аккаунта: " + ClanID.ToString() + " datablock " + DataBlockName + " query - " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();

                Finished = false;
                return Finished;
            }

            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static bool SetClanDataByAccountID(int AccountID, string DataBlockName, string NewParameter)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);

            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;

            sql = "UPDATE clans SET " + DataBlockName + " =" + @"""" + NewParameter + @"""" + " WHERE ID = " + AccountID.ToString() + ";";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - Clans.SetClanDataByAccountID] Запрос к базе для аккаунта: " + AccountID.ToString() + " datablock " + DataBlockName + " query - " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();

                Finished = false;
                return Finished;
            }

            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

    }
}
