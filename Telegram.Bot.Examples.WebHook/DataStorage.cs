using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telegram.Bot.Examples.WebHook
{
    public class DataStorage
    {
        public static Random rndmzr = new Random(); // Инициация рандомайзера для дропа предметов
        public static string CommandText = "";
        public static string Connect = "";

        public static int Charisma1Min3MaxDiscProcent = 5;
        public static int Charisma4Min8MaxDiscProcent = 10;
        public static int Charisma9Min15MaxDiscProcent = 15;

        public static int ClanCreationPrice = 3500;

        // Конец
        public static int GlobalWeather = 0;
        public static string GlobalWeatherName = "☀️ Солнечно";
        public static int ConnectionsCount = 0;
    }
}
