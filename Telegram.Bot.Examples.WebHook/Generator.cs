using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telegram.Bot.Examples.WebHook
{
    class Generator
    {
        public class Generate
        {
            public static Random rnd = new Random();

            public static string BotsFirstNameMale()
            {
                List<string> FirstNameMale = new List<string>();
                string str = string.Empty;
                FirstNameMale.Add("Адипоз");
                FirstNameMale.Add("Малу");
                FirstNameMale.Add("Гират");
                FirstNameMale.Add("Аки");
                FirstNameMale.Add("Керус");
                FirstNameMale.Add("Сог");
                FirstNameMale.Add("Миркас");
                FirstNameMale.Add("Дармаму");
                FirstNameMale.Add("Гират");
                FirstNameMale.Add("Вашта");
                FirstNameMale.Add("Корин");
                FirstNameMale.Add("Мельтаз");
                FirstNameMale.Add("Крисп");
                FirstNameMale.Add("Винвоччи");
                FirstNameMale.Add("Сайрус");
                FirstNameMale.Add("Джа");
                FirstNameMale.Add("Джагарот");
                FirstNameMale.Add("Джудун");
                FirstNameMale.Add("Сиан");
                FirstNameMale.Add("Зоччи");
                FirstNameMale.Add("Зарби");
                FirstNameMale.Add("Кролл");
                FirstNameMale.Add("Танитрас");
                FirstNameMale.Add("Ит");
                FirstNameMale.Add("Зерхан");
                FirstNameMale.Add("Дерек");
                FirstNameMale.Add("Дейв");
                FirstNameMale.Add("Кларк");
                FirstNameMale.Add("Бенедикт");
                FirstNameMale.Add("Джек");
                str = FirstNameMale.OrderBy(xx => rnd.Next()).First();
                return str;
            }

            public static string BotsLastName()
            {
                List<string> LastNameMale = new List<string>();
                string str = string.Empty;
                LastNameMale.Add("Свонсон");
                LastNameMale.Add("Диксон");
                LastNameMale.Add("Ромулус");
                LastNameMale.Add("Малик");
                LastNameMale.Add("Кроу");
                LastNameMale.Add("Шэкнести");
                LastNameMale.Add("Вашаки");
                LastNameMale.Add("Сахэм");
                LastNameMale.Add("Чайтуа");
                LastNameMale.Add("Такота");
                LastNameMale.Add("Юма");
                LastNameMale.Add("Локлир");
                LastNameMale.Add("Тейлор");
                LastNameMale.Add("Мартинес");
                LastNameMale.Add("Мур");
                LastNameMale.Add("Ли");
                LastNameMale.Add("Янг");
                LastNameMale.Add("Чавис");
                LastNameMale.Add("Кент");
                LastNameMale.Add("Кембербетч");
                LastNameMale.Add("Дэвис");
                LastNameMale.Add("Росс");
                str = LastNameMale.OrderBy(xx => rnd.Next()).First();
                return str;
            }

            public static string BotsFirstNameFemale()
            {
                List<string> FirstNameFemale = new List<string>();
                string str = string.Empty;
                FirstNameFemale.Add("Малу");
                FirstNameFemale.Add("Кира");
                FirstNameFemale.Add("Элайза");
                FirstNameFemale.Add("Гроу");
                FirstNameFemale.Add("Варна");
                FirstNameFemale.Add("Зета");
                FirstNameFemale.Add("Претория");
                FirstNameFemale.Add("Михаса");
                FirstNameFemale.Add("Уна");
                FirstNameFemale.Add("Ника");
                FirstNameFemale.Add("Альзария");
                FirstNameFemale.Add("Рина");
                FirstNameFemale.Add("Анксенамон");
                FirstNameFemale.Add("Викария");
                FirstNameFemale.Add("Артемида");
                str = FirstNameFemale.OrderBy(xx => rnd.Next()).First();
                return str;
            }

            public static string SituationDanger()
            {
                List<string> Danger = new List<string>(); // О ЧЕМ?
                string str = string.Empty;
                Danger.Add("стае псевдопсов");
                Danger.Add("группе бандитов");
                Danger.Add("засаде");
                Danger.Add("смертельной аномалии Трамплин");
                Danger.Add("смертельной аномалии Воронка");
                Danger.Add("смертельной аномалии Карусель");
                Danger.Add("смертельной аномалии Жарка");
                Danger.Add("смертельной аномалии Электра");
                Danger.Add("смертельной аномалии Кисель");
                Danger.Add("смертельной аномалии Битум");
                Danger.Add("смертельной аномалии Котёл");
                Danger.Add("смертельной аномалии Воронка");
                Danger.Add("смертельной аномалии Разлом");
                Danger.Add("взбесившемся кровососе");
                Danger.Add("громадном псевдогиганте");
                Danger.Add("двух снорках");
                Danger.Add("трёх снорках");
                Danger.Add("стае снорков");
                Danger.Add("безумной псевдоплоти");
                Danger.Add("кровожадной пси-собаке");
                Danger.Add("злобной химере");
                Danger.Add("нескольких зомби");
                Danger.Add("взбесившемся кровососе");
                Danger.Add("стае кабанов");
                Danger.Add("странном сталкере");
                Danger.Add("группе монолитовцев");
                str = Danger.OrderBy(xx => rnd.Next()).First();
                return str;
            }

            public static string NPC_Enemy()
            {
                List<string> DangerDude = new List<string>(); // На вас напал кто?
                string str = string.Empty;
                DangerDude.Add("Космический пират");
                DangerDude.Add("Бандит");
                DangerDude.Add("Мародёр");
                DangerDude.Add("Капитан пиратов");
                str = DangerDude.OrderBy(xx => rnd.Next()).First();
                return str;
            }

        }
    }
}
