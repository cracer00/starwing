using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Api.Examples.WebHook;
using static Telegram.Bot.Examples.WebHook.DataStorage;
using static Telegram.Bot.Examples.WebHook.Message;

namespace Telegram.Bot.Examples.WebHook
{
    public class Inventory
    {
        public static string GetItemCount(int AccountID, string ItemID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand sql_commmand;
            myConnection.Open();
            sql_commmand = new MySqlCommand("SELECT ItemCount FROM inventory WHERE AccountID = " + AccountID.ToString() + " AND ItemID = " + ItemID.ToString(), myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            string ItemCount = string.Empty;

            while (thisReader.Read())
            {
                ItemCount += thisReader["ItemCount"];
            }
            thisReader.Close();
            myConnection.Close();
            return ItemCount;
        }

        public static string GetSellList(int AccountID, int Page)
        {
            Console.WriteLine("--- Блок 1");
            string HireString = string.Empty;
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand sql_commmand;
            myConnection.Open();
            switch (Page)
            {
                case 1:
                    sql_commmand = new MySqlCommand("SELECT ItemID, ItemCount FROM inventory WHERE AccountID = " + AccountID.ToString() + " ORDER BY ItemID ASC LIMIT 0, 10", myConnection);
                    break;
                case 2:
                    sql_commmand = new MySqlCommand("SELECT ItemID, ItemCount FROM inventory WHERE AccountID = " + AccountID.ToString() + " ORDER BY ItemID ASC LIMIT 10, 10", myConnection);
                    break;
                case 3:
                    sql_commmand = new MySqlCommand("SELECT ItemID, ItemCount FROM inventory WHERE AccountID = " + AccountID.ToString() + " ORDER BY ItemID ASC LIMIT 20, 10", myConnection);
                    break;
                case 4:
                    sql_commmand = new MySqlCommand("SELECT ItemID, ItemCount FROM inventory WHERE AccountID = " + AccountID.ToString() + " ORDER BY ItemID ASC LIMIT 30, 10", myConnection);
                    break;
                case 5:
                    sql_commmand = new MySqlCommand("SELECT ItemID, ItemCount FROM inventory WHERE AccountID = " + AccountID.ToString() + " ORDER BY ItemID ASC LIMIT 40, 10", myConnection);
                    break;
                case 6:
                    sql_commmand = new MySqlCommand("SELECT ItemID, ItemCount FROM inventory WHERE AccountID = " + AccountID.ToString() + " ORDER BY ItemID ASC LIMIT 50, 10", myConnection);
                    break;
                case 7:
                    sql_commmand = new MySqlCommand("SELECT ItemID, ItemCount FROM inventory WHERE AccountID = " + AccountID.ToString() + " ORDER BY ItemID ASC LIMIT 60, 10", myConnection);
                    break;
                default:
                    sql_commmand = new MySqlCommand("SELECT ItemID, ItemCount FROM inventory WHERE AccountID = " + AccountID.ToString() + " ORDER BY ItemID ASC LIMIT 0, 10", myConnection);
                    break;
            }
            Console.WriteLine("--- Блок 2\n" + sql_commmand.CommandText); // Выполняется досюда
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            
            string RecipeList = string.Empty;

            while (thisReader.Read())
            {
                string TempString = string.Empty;
                string ItemID = string.Empty;
                string ItemCount = string.Empty;

                ItemID += thisReader["ItemID"];
                ItemCount += thisReader["ItemCount"];

                Items.Item.GetInfo(Convert.ToInt32(ItemID), out string ItemName, out string ItemUsable, out string ItemWearable, out string ItemWearSlot, out string ItemPrice, out string ItemType, out string ItemPower);

                float ItemFinalPrice = Program.ProcentResultCount(Convert.ToInt32(ItemPrice), 50);
                int ItemFinalPriceInt = (int)Math.Round(ItemFinalPrice);
                int PriceSum = Convert.ToInt32(ItemCount) * ItemFinalPriceInt;

                TempString = "📦 " + "" + ItemCount + " " + ItemName + " за 💳 " + PriceSum.ToString() + " | /sell_" + ItemID + "_" + ItemCount + "\n";
                Console.WriteLine("--- Блок 3 Reader");
                RecipeList = RecipeList + TempString;
            }
            Console.WriteLine("--- Блок 4");
            string NavigationCurrentPage = "Страница " + Page;
            string NavigationBack = "Назад: /sells_1";
            string NavigationForward = "Вперед: /sells_2";
            string Navigation = NavigationCurrentPage + "\n" + NavigationBack + "\n" + NavigationForward;
            if (Page > 1)
            {
                NavigationBack = "Назад: /sells_" + (Page - 1);
                NavigationForward = "Вперед: /sells_" + (Page + 1);
                Navigation = NavigationBack + "\n" + NavigationForward;
            }
            Console.WriteLine("--- Блок 5");
            string ReturnRecipeList = "🔷 Продать предметы:\n\n" + RecipeList + "\n" + Navigation;
            thisReader.Close();
            myConnection.Close();
            Console.WriteLine("--- Блок 6");
            return ReturnRecipeList;
        }

        public static bool AddSingleData(int AccountID, int ItemID, string DataBlockName, string NewParameter, bool Minus)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);

            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;
            if (Minus == true)
            {
                sql = "UPDATE inventory SET " + DataBlockName + " = " + DataBlockName + " - " + @"""" + NewParameter + @"""" + "WHERE AccountID = " + AccountID.ToString() + " AND ItemID = " + ItemID.ToString() + ";";
            }
            else
            {
                sql = "UPDATE inventory SET " + DataBlockName + " = " + DataBlockName + " + " + @"""" + NewParameter + @"""" + "WHERE AccountID = " + AccountID.ToString() + " AND ItemID = " + ItemID.ToString() + ";";
            }
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - Account.AddData] Запрос к базе для аккаунта: " + AccountID.ToString() + " datablock " + DataBlockName + " query - " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();
                Finished = false;
                return Finished;
            }

            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static string GetSingleData(int AccountID, int ItemID, string DataBlockName) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM inventory WHERE AccountID = @AccountID AND ItemID = @ItemID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            sql_commmand.Parameters.AddWithValue("@ItemID", ItemID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["" + DataBlockName + ""];
            }
            thisReader.Close();
            myConnection.Close();

            // Convert.ToInt64(DataBlockResult);
            return DataBlockResult;
        }

        public static bool AddSingleData(int AccountID, string ItemID, string ItemType, string ItemCount)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;
            string report_query = AccountID.ToString() + ", " + @"""" + ItemID + @"""" + ", " + @"""" + ItemType + @"""" + ", " + @"""" + ItemCount + @"""";
            sql = "INSERT INTO inventory (AccountID, ItemID, ItemType, ItemCount) VALUES (" + report_query + "); ";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - [Inventory.AddSingleData] Запрос: " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();

            script.Query = sql;
            script.Connection = myConnection;

            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();

                Finished = false;
                return Finished;
            }

            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public class Items
        {
            public class Item
            {

                public static string GetSingleData(int ItemID, string DataBlockName) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
                {
                    MySqlConnection myConnection = new MySqlConnection(Connect);
                    myConnection.Open();

                    MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM items WHERE ID = @ItemID", myConnection);
                    sql_commmand.Parameters.AddWithValue("@ItemID", ItemID);
                    MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                    string DataBlockResult = string.Empty;
                    while (thisReader.Read())
                    {
                        DataBlockResult += thisReader["" + DataBlockName + ""];
                    }
                    thisReader.Close();
                    myConnection.Close();

                    // Convert.ToInt64(DataBlockResult);
                    return DataBlockResult;
                }

                public static void GetAllInfo(int ItemID, out string ReturnItemName, out string ReturnItemUsable, out string ReturnItemWearable, out string ReturnItemWearSlot, out string ReturnItemPrice, out string ReturnItemType, out string ReturnItemStrength, out string ReturnItemCharisma, out string ReturnItemIntelligence, out string ReturnItemAgility, out string ReturnItemLuck, out string ReturnItemAttack, out string ReturnItemDef, out string ReturnItemPower, out string ReturnItemText)
                {
                    MySqlConnection myConnection = new MySqlConnection(Connect);
                    myConnection.Open();

                    MySqlCommand sql_command = new MySqlCommand("SELECT Name, Usable, Wearable, WearSlot, Price, ItemType, Strength, Charisma, Intelligence, Agility, Luck, Attack, Def, ItemPower, ItemText FROM items WHERE ID = @ItemID", myConnection);
                    sql_command.Parameters.AddWithValue("@ItemID", ItemID);
                    MySqlDataReader thisReader = sql_command.ExecuteReader();
                    string ItemName = string.Empty;
                    string ItemUsable = string.Empty;
                    string ItemWearable = string.Empty;
                    string ItemWearSlot = string.Empty;
                    string ItemPrice = string.Empty;
                    string ItemType = string.Empty;
                    string ItemStrength = string.Empty;
                    string ItemCharisma = string.Empty;
                    string ItemIntelligence = string.Empty;
                    string ItemAgility = string.Empty;
                    string ItemLuck = string.Empty;
                    string ItemAttack = string.Empty;
                    string ItemDef = string.Empty;
                    string ItemPower = string.Empty;
                    string ItemText = string.Empty;

                    while (thisReader.Read())
                    {
                        ItemName += thisReader["Name"];
                        ItemUsable += thisReader["Usable"];
                        ItemPrice += thisReader["Price"];
                        ItemWearable += thisReader["Wearable"];
                        ItemWearSlot += thisReader["WearSlot"];
                        ItemType += thisReader["ItemType"];
                        ItemStrength += thisReader["Strength"];
                        ItemCharisma += thisReader["Charisma"];
                        ItemIntelligence += thisReader["Intelligence"];
                        ItemAgility += thisReader["Agility"];
                        ItemLuck += thisReader["Luck"];
                        ItemAttack += thisReader["Attack"];
                        ItemDef += thisReader["Def"];
                        ItemPower += thisReader["ItemPower"];
                        ItemText += thisReader["ItemText"];
                    }
                    thisReader.Close();
                    myConnection.Close();

                    ReturnItemName = ItemName;
                    ReturnItemUsable = ItemUsable;
                    ReturnItemWearable = ItemWearable;
                    ReturnItemPrice = ItemPrice;
                    ReturnItemWearSlot = ItemWearSlot;
                    ReturnItemType = ItemType;
                    ReturnItemStrength = ItemStrength;
                    ReturnItemCharisma = ItemCharisma;
                    ReturnItemIntelligence = ItemIntelligence;
                    ReturnItemAgility = ItemAgility;
                    ReturnItemLuck = ItemLuck;
                    ReturnItemAttack = ItemAttack;
                    ReturnItemDef = ItemDef;
                    ReturnItemPower = ItemPower;
                    ReturnItemText = ItemText;
                    //  return;
                }

                public static string GetName(int ItemID)
                {
                    MySqlConnection myConnection = new MySqlConnection(Connect);
                    myConnection.Open();

                    MySqlCommand sql_command = new MySqlCommand("SELECT Name FROM items WHERE ID = @ItemID", myConnection);
                    sql_command.Parameters.AddWithValue("@ItemID", ItemID);
                    MySqlDataReader thisReader = sql_command.ExecuteReader();
                    string ItemName = string.Empty;

                    while (thisReader.Read())
                    {
                        ItemName += thisReader["Name"];
                    }
                    myConnection.Close();

                    thisReader.Close();
                    return ItemName;
                }

                public static void GetName(int ItemID1, int ItemID2, out string ReturnItemName1, out string ReturnItemName2)
                {
                    MySqlConnection myConnection = new MySqlConnection(Connect);
                    myConnection.Open();

                    MySqlCommand sql_command = new MySqlCommand("SELECT Name FROM items WHERE ID = @ItemID", myConnection);
                    sql_command.Parameters.AddWithValue("@ItemID", ItemID1);
                    MySqlDataReader thisReader = sql_command.ExecuteReader();
                    string ItemName1 = string.Empty;
                    string ItemName2 = string.Empty;

                    while (thisReader.Read())
                    {
                        ItemName1 += thisReader["Name"];
                    }
                    thisReader.Close();

                    sql_command = new MySqlCommand("SELECT Name FROM items WHERE ID = @ItemID", myConnection);
                    sql_command.Parameters.AddWithValue("@ItemID", ItemID2);
                    thisReader = sql_command.ExecuteReader();
                    while (thisReader.Read())
                    {
                        ItemName2 += thisReader["Name"];
                    }
                    thisReader.Close();
                    ReturnItemName1 = ItemName1;
                    ReturnItemName2 = ItemName2;
                    myConnection.Close();

                }

                public static string GetText(int ItemID)
                {
                    MySqlConnection myConnection = new MySqlConnection(Connect);
                    myConnection.Open();

                    MySqlCommand sql_command = new MySqlCommand("SELECT ItemText FROM items WHERE ID = @ItemID", myConnection);
                    sql_command.Parameters.AddWithValue("@ItemID", ItemID);
                    MySqlDataReader thisReader = sql_command.ExecuteReader();
                    string ItemText = string.Empty;

                    while (thisReader.Read())
                    {
                        ItemText += thisReader["ItemText"];
                    }
                    myConnection.Close();

                    thisReader.Close();
                    return ItemText;
                }

                public static void GetInfo(int ItemID, out string ReturnItemName, out string ReturnItemUsable, out string ReturnItemWearable, out string ReturnItemWearSlot, out string ReturnItemPrice, out string ReturnItemType, out string ReturnItemPower)
                {
                    MySqlConnection myConnection = new MySqlConnection(Connect);
                    myConnection.Open();

                    MySqlCommand sql_command = new MySqlCommand("SELECT Name, Usable, Wearable, WearSlot, Price, ItemType, ItemPower FROM items WHERE ID = @ItemID", myConnection);
                    sql_command.Parameters.AddWithValue("@ItemID", ItemID);
                    MySqlDataReader thisReader = sql_command.ExecuteReader();
                    string ItemName = string.Empty;
                    string ItemUsable = string.Empty;
                    string ItemWearable = string.Empty;
                    string ItemWearSlot = string.Empty;
                    string ItemPrice = string.Empty;
                    string ItemType = string.Empty;
                    string ItemPower = string.Empty;

                    while (thisReader.Read())
                    {
                        ItemName += thisReader["Name"];
                        ItemUsable += thisReader["Usable"];
                        ItemPrice += thisReader["Price"];
                        ItemWearable += thisReader["Wearable"];
                        ItemWearSlot += thisReader["WearSlot"];
                        ItemType += thisReader["ItemType"];
                        ItemPower += thisReader["ItemPower"];
                    }
                    thisReader.Close();
                    myConnection.Close();

                    ReturnItemName = ItemName;
                    ReturnItemUsable = ItemUsable;
                    ReturnItemWearable = ItemWearable;
                    ReturnItemPrice = ItemPrice;
                    ReturnItemWearSlot = ItemWearSlot;
                    ReturnItemType = ItemType;
                    ReturnItemPower = ItemPower;
                }
            }
        }
    }

    
}
