using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Api.Examples.WebHook;
using static Telegram.Bot.Examples.WebHook.DataStorage;
using static Telegram.Bot.Examples.WebHook.Account;
using Telegram.Bot.Examples.WebHook;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot.Types.Enums;
using MySql.Data.MySqlClient;

namespace Telegram.Bot.Examples.WebHook
{
    public class Message
    {
        public static async void Send(int ChatID, string Message, int KeyboardNum)
        {
            var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
            var keyboard2 = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton("⬆️ На север"),
                            new KeyboardButton("⬇️ На юг"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("⬅️ На запад"),
                            new KeyboardButton("➡️ На восток"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
            var keyboard3 = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton("🌐 Загрузка матрицы"),
                        }
                        }, true);
            var keyboard24 = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton("🌐 Протокол наименования"),
                        }
                        }, true);
            var keyboard4 = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton("🚀 Корабль"),
                        }
                        }, true);
            var keyboard5 = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("🔪 Оружие"),
                            new KeyboardButton("👕 Одежда"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🍞 Еда, вода"),
                            new KeyboardButton("💎 Модули"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("💊 Медикаменты"),
                            new KeyboardButton("🔩 Амуниция"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🀄️ Разное"),
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
            var keyboard6 = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🔳 Изучение чертежей"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);
            var keyboard7 = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🚻 Члены клана"),
                            new KeyboardButton("⚙️ Управление кланом"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚀 Ангар клана"),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);
            var keyboard8 = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🚻 Текущий список"),
                            new KeyboardButton("🔄 Обновить"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        }
                        }, true);
            var keyboard9 = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🛌 Команда"),
                            new KeyboardButton("🛏 Найм команды"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🖲 Статус корабля"),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);
            var keyboard10 = new ReplyKeyboardMarkup(new[]
            {
                new [] // first row
                {
                    new KeyboardButton("🌐 Сканировать"),
                    new KeyboardButton(""),
                },
                new [] // last row
                {
                    new KeyboardButton("📗 Игроки"),
                    new KeyboardButton(""),
                },
                new [] // last row
                {
                    new KeyboardButton("🚪 Назад"),
                }
            }, true);
            var keyboard11 = new ReplyKeyboardMarkup(new[]
            {
                new [] // first row
                {
                    new KeyboardButton("⚔️ Атаковать"),
                },
                new [] // last row
                {
                    new KeyboardButton("🌐 Сканировать"),
                },
                new [] // last row
                {
                    new KeyboardButton("🚪 Назад"),
                },
            }, true);

            var keyboard12 = new ReplyKeyboardMarkup(new[]
                       {
                        new [] // first row
                        {
                            new KeyboardButton("🔼 На север"),
                            new KeyboardButton("🔽 На юг"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("◀️ На запад"),
                            new KeyboardButton("▶️ На восток"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);

            try
            {
                if (KeyboardNum == 1)
                {
                    await Telegram.Bot.Api.Examples.WebHook.Bot.Api.SendTextMessageAsync(ChatID, Message, replyMarkup: keyboard, parseMode: ParseMode.Html);
                }
                else if (KeyboardNum == 2)
                {
                    await Telegram.Bot.Api.Examples.WebHook.Bot.Api.SendTextMessageAsync(ChatID, Message, replyMarkup: keyboard2, parseMode: ParseMode.Html);
                }
                else if (KeyboardNum == 3)
                {
                    await Telegram.Bot.Api.Examples.WebHook.Bot.Api.SendTextMessageAsync(ChatID, Message, replyMarkup: keyboard3, parseMode: ParseMode.Html);
                }
                else if (KeyboardNum == 4)
                {
                    await Telegram.Bot.Api.Examples.WebHook.Bot.Api.SendTextMessageAsync(ChatID, Message, replyMarkup: keyboard4, parseMode: ParseMode.Html);
                }
                else if (KeyboardNum == 5)
                {
                    await Telegram.Bot.Api.Examples.WebHook.Bot.Api.SendTextMessageAsync(ChatID, Message, replyMarkup: keyboard5, parseMode: ParseMode.Html);
                }
                else if (KeyboardNum == 6)
                {
                    await Telegram.Bot.Api.Examples.WebHook.Bot.Api.SendTextMessageAsync(ChatID, Message, replyMarkup: keyboard6, parseMode: ParseMode.Html);
                }
                else if (KeyboardNum == 7)
                {
                    await Telegram.Bot.Api.Examples.WebHook.Bot.Api.SendTextMessageAsync(ChatID, Message, replyMarkup: keyboard7, parseMode: ParseMode.Html);
                }
                else if (KeyboardNum == 8)
                {
                    await Telegram.Bot.Api.Examples.WebHook.Bot.Api.SendTextMessageAsync(ChatID, Message, replyMarkup: keyboard8, parseMode: ParseMode.Html);
                }
                else if (KeyboardNum == 9)
                {
                    await Telegram.Bot.Api.Examples.WebHook.Bot.Api.SendTextMessageAsync(ChatID, Message, replyMarkup: keyboard9, parseMode: ParseMode.Html);
                }
                else if (KeyboardNum == 10)
                {
                    await Telegram.Bot.Api.Examples.WebHook.Bot.Api.SendTextMessageAsync(ChatID, Message, replyMarkup: keyboard10, parseMode: ParseMode.Html);
                }
                else if (KeyboardNum == 11)
                {
                    await Telegram.Bot.Api.Examples.WebHook.Bot.Api.SendTextMessageAsync(ChatID, Message, replyMarkup: keyboard11, parseMode: ParseMode.Html);
                }

                else if (KeyboardNum == 12)
                {
                    await Telegram.Bot.Api.Examples.WebHook.Bot.Api.SendTextMessageAsync(ChatID, Message, replyMarkup: keyboard12, parseMode: ParseMode.Html);
                }
                else if (KeyboardNum == 24)
                {
                    await Telegram.Bot.Api.Examples.WebHook.Bot.Api.SendTextMessageAsync(ChatID, Message, replyMarkup: keyboard24, parseMode: ParseMode.Html);
                }
            }
            catch (Exception MyEx)
            {
                Console.WriteLine(MyEx);
                System.IO.File.AppendAllText("LogSendMessageError.txt", DateTime.Now.ToString("h:mm:ss") + ": " + MyEx + "\r\n");
            }

        }

        public static void  SendToAllByShip(int ShipID, string Message)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID FROM users WHERE ShipID = @ShipID", myConnection);
            sql_commmand.Parameters.AddWithValue("@ShipID", ShipID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string AccountID = string.Empty;
            while (thisReader.Read())
            {
                AccountID += thisReader["AccountID"];
                Send(Convert.ToInt32(AccountID), Message, 1);
            }
            thisReader.Close();
            myConnection.Close();

            // Convert.ToInt64(DataBlockResult);
        }

        public static async void SendProfileMessage(int ChatID, string Message)
        {
            var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
            var keyboard2 = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton("🌐 Загрузка матрицы"),
                        }
                        }, true);
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID, Status, UserName, CharName, Money, Exp, Strength, Endurance, Charisma, Intelligence, Agility, Luck, TalantPoints FROM users WHERE AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", ChatID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string AccountID = string.Empty;
            string Status = string.Empty;
            string UserName = string.Empty;
            string CharName = string.Empty;
            string Money = string.Empty;
            string Exp = string.Empty;
            string Strength = string.Empty;
            string Endurance = string.Empty;
            string Charisma = string.Empty;
            string Intelligence = string.Empty;
            string Agility = string.Empty;
            string Luck = string.Empty;
            string TalantPoints = string.Empty;
            while (thisReader.Read())
            {
                AccountID += thisReader["AccountID"];
                Status += thisReader["Status"];
                UserName += thisReader["UserName"];
                CharName += thisReader["CharName"];
                Money += thisReader["Money"];
                Exp += thisReader["Exp"];
                Strength += thisReader["Strength"];
                Endurance += thisReader["Endurance"];
                Charisma += thisReader["Charisma"];
                Intelligence += thisReader["Intelligence"];
                Agility += thisReader["Agility"];
                Luck += thisReader["Luck"];
                TalantPoints += thisReader["TalantPoints"];
            }
            thisReader.Close();
            myConnection.Close();


            string StrengthButton = string.Empty;
            string EnduranceButton = string.Empty;
            string CharismaButton = string.Empty;
            string IntelligenceButton = string.Empty;
            string AgilityButton = string.Empty;
            string LuckButton = string.Empty;

            if (Convert.ToInt32(TalantPoints) > 0)
            {
                StrengthButton = "/strength+";
                EnduranceButton = "/endurance+";
                CharismaButton = "/charisma+";
                IntelligenceButton = "/intelligence+";
                AgilityButton = "/agility+";
                LuckButton = "/luck+";
            }
            else
            {
                StrengthButton = string.Empty;
                EnduranceButton = string.Empty;
                CharismaButton = string.Empty;
                IntelligenceButton = string.Empty;
                AgilityButton = string.Empty;
                LuckButton = string.Empty;
            }

            string Level = string.Empty;

            var MeInfo = @"" + Message + "\n\n" + "👤 " + CharName + "\n" +
            //  "@" + UserName + "\n" +
            "✨ Опыт: " + Exp + "\n" +
            "💳 " + Money + "\n" +
            "🔰 Очков талантов: " + TalantPoints + "\n\n" + "🔮 Характеристики: \n" +
            "  " + Strength + " - Сила  " + StrengthButton + "\n" +
            "  " + Endurance + " - Выносливость  " + EnduranceButton + "\n" +
            "  " + Charisma + " - Харизма  " + CharismaButton + "\n" +
            "  " + Intelligence + " - Интеллект  " + IntelligenceButton + "\n" +
            "  " + Agility + " - Ловкость  " + AgilityButton + "\n" +
            "  " + Luck + " - Удача  " + LuckButton + "\n";

            if (Status == "1") // Игрок на начальной клавиатуре
            {
                try
                {
                    await Telegram.Bot.Api.Examples.WebHook.Bot.Api.SendTextMessageAsync(ChatID, MeInfo, replyMarkup: keyboard2);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }
            }
            else if (Status == "0")
            {
                try
                {
                    await Telegram.Bot.Api.Examples.WebHook.Bot.Api.SendTextMessageAsync(ChatID, MeInfo, replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }
            }
        }
    }
}
