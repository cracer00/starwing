using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Telegram.Bot.Examples.WebHook.DataStorage;
using static Telegram.Bot.Examples.WebHook.Ships;
using static Telegram.Bot.Examples.WebHook.Generator;
using Telegram.Bot.Examples.WebHook;
using MySql.Data.MySqlClient;
using Telegram.Bot.Api.Examples.WebHook;
using Newtonsoft.Json.Linq;

namespace Telegram.Bot.Examples.WebHook
{
    public class NPC
    {
        public class Enemy
        {
            public static void Add(int AccountID, int NPCType, int X, int Y, int InAction)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);

                MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
                string sql;

                string eNPCName = String.Empty;

                if (NPCType == 0)
                {
                    eNPCName = Generator.Generate.NPC_Enemy();
                }
                string PlayerLevel = Account.GetSingleData(AccountID, "Level");
                string ShipMaxHealth = Ship.GetSingleData(AccountID, "ShipMaxHealth");

                int eNPCLevel = rndmzr.Next(Convert.ToInt32(PlayerLevel) - 3, Convert.ToInt32(PlayerLevel) + 3);
                int DangerFactorCountMinus = 100;
                int DangerFactorCountPlus = 50;
                string PlayerShipAttack = Ship.GetSingleData(AccountID, "ShipAttack");
                string PlayerShipDef = Ship.GetSingleData(AccountID, "ShipDef");
                double eNPCAttack = Convert.ToInt32(PlayerShipAttack);
                double eNPCDef = Convert.ToInt32(PlayerShipDef);
                int eNPCDanger = 0;
                int ShipAdditionalHealth = 100;

                string MapLocDang = Program.GetMapSingleData(X, Y, "LocationDanger");
                switch (Convert.ToInt32(MapLocDang))
                {
                    case 0:
                        DangerFactorCountMinus = 100;
                        DangerFactorCountPlus = 50;
                        eNPCDanger = 0;
                        ShipAdditionalHealth = rndmzr.Next(Program.ProcentResultCount(Convert.ToInt32(ShipMaxHealth), 5), Program.ProcentResultCount(Convert.ToInt32(ShipMaxHealth), 20));
                        eNPCAttack = eNPCAttack * 0.75;
                        eNPCDef = eNPCDef * 0.75;
                        break;
                    case 1:
                        DangerFactorCountMinus = 75;
                        DangerFactorCountPlus = 75;
                        eNPCDanger = 0;
                        ShipAdditionalHealth = rndmzr.Next(Program.ProcentResultCount(Convert.ToInt32(ShipMaxHealth), 20), Program.ProcentResultCount(Convert.ToInt32(ShipMaxHealth), 35));
                        eNPCAttack = eNPCAttack * 0.95;
                        eNPCDef = eNPCDef * 0.95;
                        break;
                    case 2:
                        DangerFactorCountMinus = 40;
                        DangerFactorCountPlus = 90;
                        eNPCDanger = 1;
                        ShipAdditionalHealth = rndmzr.Next(Program.ProcentResultCount(Convert.ToInt32(ShipMaxHealth), 35), Program.ProcentResultCount(Convert.ToInt32(ShipMaxHealth), 50));
                        eNPCAttack = eNPCAttack * 1.15;
                        eNPCDef = eNPCDef * 1.15;
                        break;
                    case 3:
                        DangerFactorCountMinus = 20;
                        DangerFactorCountPlus = 130;
                        eNPCDanger = 1;
                        ShipAdditionalHealth = rndmzr.Next(Program.ProcentResultCount(Convert.ToInt32(ShipMaxHealth), 50), Program.ProcentResultCount(Convert.ToInt32(ShipMaxHealth), 65));
                        eNPCAttack = eNPCAttack * 1.25;
                        eNPCDef = eNPCDef * 1.25;
                        break;
                    case 4:
                        DangerFactorCountMinus = 0;
                        DangerFactorCountPlus = 150;
                        eNPCAttack = eNPCAttack * 1.35;
                        eNPCDef = eNPCDef * 1.35;
                        eNPCDanger = 2;
                        ShipAdditionalHealth = rndmzr.Next(Program.ProcentResultCount(Convert.ToInt32(ShipMaxHealth), 65), Program.ProcentResultCount(Convert.ToInt32(ShipMaxHealth), 85));
                        break;
                    case 5:
                        DangerFactorCountMinus = 35;
                        DangerFactorCountPlus = 190;
                        eNPCAttack = eNPCAttack * 1.55;
                        eNPCDef = eNPCDef * 1.55;
                        eNPCDanger = 2;
                        ShipAdditionalHealth = rndmzr.Next(Program.ProcentResultCount(Convert.ToInt32(ShipMaxHealth), 85), Program.ProcentResultCount(Convert.ToInt32(ShipMaxHealth), 100));
                        break;
                }

                int eNPCRoundedAttack = Convert.ToInt32(Math.Round(eNPCAttack, 0));
                int eNPCRoundedDef = Convert.ToInt32(Math.Round(eNPCDef, 0));

                int eNPCHealth = Convert.ToInt32(ShipMaxHealth) + ShipAdditionalHealth;
                int eNPCType = NPCType;
                int eNPCShipType = 1;
                string PlayerLuck = Account.GetSingleData(AccountID, "Luck");

                int eNPCMoney = 10;
                int eNPCCargoType = rndmzr.Next(0, 5);
                int eNPCAgression = 1;

                if (Convert.ToInt32(PlayerLuck) <= 8)
                {
                    eNPCMoney = rndmzr.Next(35, 110);
                }
                else if (Convert.ToInt32(PlayerLuck) > 8 && Convert.ToInt32(PlayerLuck) <= 15)
                {
                    eNPCMoney = rndmzr.Next(130, 250);
                }
                else if (Convert.ToInt32(PlayerLuck) > 15)
                {
                    eNPCMoney = rndmzr.Next(250, 400);
                }

                myConnection.Open();
                string varquery = @"""" + AccountID.ToString() + @"""" + ", " + @"""" + eNPCName.ToString() + @"""" + ", " + @"""" + eNPCLevel.ToString() + @"""" + ", " + @"""" + eNPCHealth.ToString() + @"""" + ", " + @"""" + eNPCHealth.ToString() + @""""
                                  + ", " + @"""" + eNPCType.ToString() + @"""" + ", " + @"""" + eNPCShipType.ToString() + @"""" + ", " + @"""" + eNPCMoney.ToString() + @"""" + ", " + @"""" + eNPCCargoType.ToString() + @"""" + ", " + @""""
                                  + eNPCAgression.ToString() + @"""" + ", " + @"""" + X.ToString() + @"""" + ", " + @"""" + Y.ToString() + @"""" + ", " + @"""" + InAction.ToString() + @"""" + ", " + @""""
                                  + eNPCRoundedAttack.ToString() + @"""" + ", " + @"""" + eNPCRoundedDef.ToString() + @"""" + ", " + @"""" + eNPCDanger.ToString() + @"""";

                sql = "INSERT INTO npc_enemy (AccountID, Name, Level, HP, MaxHP, NpcType, ShipType, Money, CargoType, Aggression, X, Y, InAction, Attack, Def, Danger) VALUES (" + varquery + "); ";

                MySqlScript script = new MySqlScript(myConnection, sql);
                script.Query = sql;
                Console.WriteLine("DEBUG: " + script.Query);
                script.Connection = myConnection;
                script.Execute();

                try
                {
                    //Message.Send(FounderID, "<b>Доступен новый член команды:</b>\n\n" + NPC_Face + " " + FirstName + " " + LastName + "\n", 3); // Дописать уведомление о создании
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - CreateEnemyNPC] ОШИБКА!: " + MyEx);
                }

                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - CreateEnemyNPC] Создание NPC: выполнения запроса: " + sql);
                myConnection.Close(); //Обязательно закрываем соединение!
            }

            public static void Delete(int AccountID)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();

                string sql;
                sql = "DELETE FROM npc_enemy WHERE AccountID = " + AccountID.ToString();
                MySqlScript script = new MySqlScript(myConnection, sql);
                script.Query = sql;
                script.Connection = myConnection;
                try
                {
                    script.Execute();
                    Console.WriteLine(sql);
                }
                catch (MySqlException sqlEx)
                {
                    Console.WriteLine(sqlEx);
                    myConnection.Close();

                }

                myConnection.Close();
            }

            public static bool IsExists(int AccountID)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM npc_enemy WHERE AccountID = @AccountID", myConnection);
                sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                string NPCExists = string.Empty;
                while (thisReader.Read())
                {
                    NPCExists += thisReader["ID"];
                }
                thisReader.Close();
                myConnection.Close();
                if (NPCExists != "" && NPCExists != " ")
                {
                    return true;
                }
                else return false;
            }

            public static string Generate(int AccountID)
            {
                Enemy.Delete(AccountID);
                NPC.Enemy.Add(AccountID, 0, Convert.ToInt32(Ships.Ship.GetSingleData(AccountID, "X")), Convert.ToInt32(Ships.Ship.GetSingleData(AccountID, "Y")), 0);
                string HireString = string.Empty;
                MySqlConnection myConnection = new MySqlConnection(Connect);
                MySqlCommand sql_commmand;
                myConnection.Open();
                sql_commmand = new MySqlCommand("SELECT ID, Name, NpcType, Level, HP, Danger, Attack, Def, Money FROM npc_enemy WHERE AccountID = " + AccountID.ToString() + " ORDER BY ID ASC limit 0, 1", myConnection);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();

                string NPCEnemy = string.Empty;

                while (thisReader.Read())
                {
                    string TempString = string.Empty;
                    string ID = string.Empty;
                    string Level = string.Empty;
                    string NpcType = string.Empty;
                    string Name = string.Empty;
                    string HP = string.Empty;
                    string Danger = string.Empty;
                    string Attack = string.Empty;
                    string Def = string.Empty;
                    string Money = string.Empty;

                    ID += thisReader["ID"];
                    Level += thisReader["Level"];
                    NpcType += thisReader["NpcType"];
                    Name += thisReader["Name"];
                    HP += thisReader["HP"];
                    Danger += thisReader["Danger"];
                    Attack += thisReader["Attack"];
                    Def += thisReader["Def"];
                    Money += thisReader["Money"];

                    string ItemIcon = "🚀 ";
                    switch (Convert.ToInt32(NpcType))
                    {
                        case 7:
                            ItemIcon = "🔩 ";
                            break;
                        case 6:
                            ItemIcon = "📦 ";
                            break;
                        case 5:
                            ItemIcon = "💊 ";
                            break;
                        case 4:
                            ItemIcon = "🍞 ";
                            break;
                        case 3:
                            ItemIcon = "💎 ";
                            break;
                        case 2:
                            ItemIcon = "👮🏼‍♂️ ";
                            break;
                        case 1:
                            ItemIcon = "📦 ";
                            break;
                        case 0:
                            ItemIcon = "☠️ ";
                            break;
                        default:
                            ItemIcon = "🚀 ";
                            break;
                    }

                    if (Ships.Ship.isHasModuleByItemID(AccountID, 62) == true) // Сканер I установлен
                    {
                        TempString = ItemIcon + " " + "<b>" + Name + "</b> 🎖" + Level + "" + "\n(/attack)\n";
                    }
                    else if (Ships.Ship.isHasModuleByItemID(AccountID, 63) == true) // Сканер II установлен
                    {
                        TempString = ItemIcon + " " + "<b>" + Name + "</b> 🎖" + Level + "" + "\n🔥 Ур. угрозы: " + Danger + "\n❤️ " + HP + "\n(/attack)\n";
                    }
                    else if (Ships.Ship.isHasModuleByItemID(AccountID, 64) == true) // Сканер III установлен
                    {
                        TempString = ItemIcon + " " + "<b>" + Name + "</b> 🎖" + Level + "" + "\n🔥 Ур. угрозы: " + Danger + "\n❤️ " + HP + @"\" + HP + " ⚔️ " + Attack + " 🛡 " + Def + "\n💳 " + Money + "\n(/attack)\n";
                    }
                    else // Сканеры не установлены
                    {
                        TempString = ItemIcon + " " + "<b>" + Name + "</b>" + "\n(/attack)\n";
                    }

                    NPCEnemy = NPCEnemy + TempString;
                }
                thisReader.Close();
                myConnection.Close();

                return NPCEnemy;
            }

            public static string GetListInPointOfMap(int AccountID, int X, int Y, int Page)
            {
                string HireString = string.Empty;
                MySqlConnection myConnection = new MySqlConnection(Connect);
                MySqlCommand sql_commmand;
                myConnection.Open();

                switch (Page)
                {
                    case 1:
                        sql_commmand = new MySqlCommand("SELECT ID, Name, NpcType, Level, HP, Danger FROM npc_enemy WHERE " + "X = " + X.ToString() + " AND Y = " + Y.ToString() + " ORDER BY ID ASC limit 0, 10", myConnection);
                        break;
                    case 2:
                        sql_commmand = new MySqlCommand("SELECT ID, Name, NpcType, Level, HP, Danger FROM npc_enemy WHERE " + "X = " + X.ToString() + " AND Y = " + Y.ToString() + " ORDER BY ID ASC limit 10, 10", myConnection);
                        break;
                    case 3:
                        sql_commmand = new MySqlCommand("SELECT ID, Name, NpcType, Level, HP, Danger FROM npc_enemy WHERE " + "X = " + X.ToString() + " AND Y = " + Y.ToString() + " ORDER BY ID ASC limit 20, 10", myConnection);
                        break;
                    case 4:
                        sql_commmand = new MySqlCommand("SELECT ID, Name, NpcType, Level, HP, Danger FROM npc_enemy WHERE " + "X = " + X.ToString() + " AND Y = " + Y.ToString() + " ORDER BY ID ASC limit 30, 10", myConnection);
                        break;
                    case 5:
                        sql_commmand = new MySqlCommand("SELECT ID, Name, NpcType, Level, HP, Danger FROM npc_enemy WHERE " + "X = " + X.ToString() + " AND Y = " + Y.ToString() + " ORDER BY ID ASC limit 40, 10", myConnection);
                        break;
                    case 6:
                        sql_commmand = new MySqlCommand("SELECT ID, Name, NpcType, Level, HP, Danger FROM npc_enemy WHERE " + "X = " + X.ToString() + " AND Y = " + Y.ToString() + " ORDER BY ID ASC limit 50, 10", myConnection);
                        break;
                    case 7:
                        sql_commmand = new MySqlCommand("SELECT ID, Name, NpcType, Level, HP, Danger FROM npc_enemy WHERE " + "X = " + X.ToString() + " AND Y = " + Y.ToString() + " ORDER BY ID ASC limit 60, 10", myConnection);
                        break;
                    default:
                        sql_commmand = new MySqlCommand("SELECT ID, Name, NpcType, Level, HP, Danger FROM npc_enemy WHERE " + "X = " + X.ToString() + " AND Y = " + Y.ToString() + " ORDER BY ID ASC limit 0, 10", myConnection);
                        break;
                }
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();

                string NPCEnemyList = string.Empty;

                while (thisReader.Read())
                {
                    string TempString = string.Empty;
                    string ID = string.Empty;
                    string Level = string.Empty;
                    string NpcType = string.Empty;
                    string Name = string.Empty;
                    string HP = string.Empty;
                    string Danger = string.Empty;
                    ID += thisReader["ID"];
                    Level += thisReader["Level"];
                    NpcType += thisReader["NpcType"];
                    Name += thisReader["Name"];
                    HP += thisReader["HP"];
                    Danger += thisReader["Danger"];

                    string ItemIcon = "🚀 ";
                    switch (Convert.ToInt32(NpcType))
                    {
                        case 7:
                            ItemIcon = "🔩 ";
                            break;
                        case 6:
                            ItemIcon = "📦 ";
                            break;
                        case 5:
                            ItemIcon = "💊 ";
                            break;
                        case 4:
                            ItemIcon = "🍞 ";
                            break;
                        case 3:
                            ItemIcon = "💎 ";
                            break;
                        case 2:
                            ItemIcon = "👮🏼‍♂️ ";
                            break;
                        case 1:
                            ItemIcon = "📦 ";
                            break;
                        case 0:
                            ItemIcon = "☠️ ";
                            break;
                        default:
                            ItemIcon = "🚀 ";
                            break;
                    }

                    TempString = ItemIcon + " " + Name + " (/einf_" + ID + ")\n";

                    NPCEnemyList = NPCEnemyList + TempString;
                }
                string NavigationCurrentPage = "Страница " + Page;
                string NavigationBack = "Назад: /scan_1";
                string NavigationForward = "Вперед: /scan_2";
                string Navigation = NavigationCurrentPage + "\n" + NavigationBack + "\n" + NavigationForward;
                if (Page > 1)
                {
                    NavigationBack = "Назад: /scan_" + (Page - 1);
                    NavigationForward = "Вперед: /scan_" + (Page + 1);
                    Navigation = NavigationBack + "\n" + NavigationForward;
                }
                NPCEnemyList = NPCEnemyList + "\n" + Navigation;
                thisReader.Close();
                myConnection.Close();
                return NPCEnemyList;
            }

            public static void GetAllInfoByAccountID(int AccountID, out string ID, out string Name, out string Level, out string HP, out string MaxHP, out string NpcType, out string ShipType, out string Money, out string CargoType, out string Agression, out string X, out string Y, out string InAction, out string Attack, out string Def, out string Danger)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();

                MySqlCommand sql_command = new MySqlCommand("SELECT ID, Name, Level, HP, MaxHP, NpcType, ShipType, Money, CargoType, Aggression, X, Y, InAction, Attack, Def, Danger FROM npc_enemy WHERE AccountID = @AccountID", myConnection);
                sql_command.Parameters.AddWithValue("@AccountID", AccountID);
                MySqlDataReader thisReader = sql_command.ExecuteReader();
                ID = "";
                Name = "";
                Level = "";
                HP = "";
                MaxHP = "";
                NpcType = "";
                ShipType = "";
                Money = "";
                CargoType = "";
                Agression = "";
                X = "";
                Y = "";
                InAction = "";
                Attack = "";
                Def = "";
                Danger = "";

                while (thisReader.Read())
                {
                    ID += thisReader["ID"];
                    Name += thisReader["Name"];
                    Level += thisReader["Level"];
                    HP += thisReader["HP"];
                    MaxHP += thisReader["MaxHP"];
                    NpcType += thisReader["NpcType"];
                    ShipType += thisReader["ShipType"];
                    Money += thisReader["Money"];
                    CargoType += thisReader["CargoType"];
                    Agression += thisReader["Aggression"];
                    X += thisReader["X"];
                    Y += thisReader["Y"];
                    Level += thisReader["Level"];
                    InAction += thisReader["InAction"];
                    Attack += thisReader["Attack"];
                    Def += thisReader["Def"];
                    Danger += thisReader["Danger"];
                }
                thisReader.Close();
                myConnection.Close();

            }

            public static bool MinusData(int AccountID, string DataBlockName, string NewParameter)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);

                MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
                string sql;
                bool Finished = false;

                sql = "UPDATE npc_enemy SET " + DataBlockName + " =" + DataBlockName + " - " + @"""" + NewParameter + @"""" + "WHERE AccountID = " + AccountID.ToString() + ";";
                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - MinusDataToEnemy] Запрос к базе для аккаунта: " + AccountID.ToString() + " datablock " + DataBlockName + " query - " + sql);
                MySqlScript script = new MySqlScript(myConnection, sql);
                myConnection.Open();
                script.Query = sql;
                script.Connection = myConnection;
                try
                {
                    script.Execute();
                }
                catch (MySqlException sqlEx)
                {
                    Console.WriteLine("ОШИБКА!: " + sqlEx);
                    myConnection.Close();
                    Finished = false;
                    return Finished;
                }

                myConnection.Close(); //Обязательно закрываем соединение!
                Finished = true;
                return Finished;
            }
        }

        public class Friend
        {
            public static bool IsExistsInMapPoint(int X, int Y)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM npc_friend WHERE X = @X AND Y = @Y", myConnection);
                sql_commmand.Parameters.AddWithValue("@X", X);
                sql_commmand.Parameters.AddWithValue("@Y", Y);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                string NPCExists = string.Empty;
                while (thisReader.Read())
                {
                    NPCExists += thisReader["ID"];
                }
                thisReader.Close();
                myConnection.Close();
                if (NPCExists != "" && NPCExists != " ")
                {
                    return true;
                }
                else return false;
            }

            public static bool IsExistsInMapPointByType(int X, int Y, int Type)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM npc_friend WHERE X = @X AND Y = @Y AND Type = @Type", myConnection);
                sql_commmand.Parameters.AddWithValue("@X", X);
                sql_commmand.Parameters.AddWithValue("@Y", Y);
                sql_commmand.Parameters.AddWithValue("@Type", Type);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                string NPCExists = string.Empty;
                while (thisReader.Read())
                {
                    NPCExists += thisReader["ID"];
                }
                thisReader.Close();
                myConnection.Close();
                if (NPCExists != "" && NPCExists != " ")
                {
                    return true;
                }
                else return false;
            }


            public static int GetIDInMapPoint(int X, int Y)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM npc_friend WHERE X = @X AND Y = @Y", myConnection);
                sql_commmand.Parameters.AddWithValue("@X", X);
                sql_commmand.Parameters.AddWithValue("@Y", Y);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                string NPCExists = string.Empty;
                while (thisReader.Read())
                {
                    NPCExists += thisReader["ID"];
                }
                thisReader.Close();
                myConnection.Close();
                if (NPCExists != "" && NPCExists != " ")
                {
                    return Convert.ToInt32(NPCExists);
                }
                else return Convert.ToInt32(NPCExists);
            }

            public static string GetNPCList(int X, int Y)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ID, Name, Type, Sex FROM npc_friend WHERE X = @X AND Y = @Y", myConnection);
                sql_commmand.Parameters.AddWithValue("@X", X);
                sql_commmand.Parameters.AddWithValue("@Y", Y);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                string ReturnString = string.Empty;
                string CharTemp = string.Empty;
                string StringTemp = string.Empty;
                string ID = string.Empty;
                string Sex = string.Empty;
                string Name = string.Empty;
                string Type = string.Empty;
                while (thisReader.Read())
                {
                    ID += thisReader["ID"];
                    Name += thisReader["Name"];
                    Type += thisReader["Type"];
                    Sex += thisReader["Sex"];

                    string CharIcon = string.Empty;
                    string CharProf = string.Empty;
                    if (Sex == "0") // Мужчина
                    {
                        switch (Convert.ToInt32(Type))
                        {
                            case 0: // Продавец
                                CharProf = "💰";
                                CharIcon = "👨🏻‍💼";
                                break;
                            case 1: // Механик
                                CharProf = "🔧";
                                CharIcon = "👨🏼‍🔧";
                                break;
                            case 2: // Доктор
                                CharProf = "💉";
                                CharIcon = "👨🏼‍⚕️";
                                break;
                        }
                    }
                    else if (Sex == "1") // Женщина
                    {
                        switch (Convert.ToInt32(Type))
                        {
                            case 0: // Продавец
                                CharProf = "💰";
                                CharIcon = "👩🏽‍💼";
                                break;
                            case 1: // Механик
                                CharProf = "🔧";
                                CharIcon = "👩🏻‍🏭";
                                break;
                            case 2: // Доктор
                                CharProf = "💉";
                                CharIcon = "👩🏼‍⚕️";
                                break;
                        }
                    }
                    else if (Sex == "2") // Инопланетянин
                    {

                    }

                    CharTemp = CharIcon + " <b>" + Name + "</b>" + CharProf + " - /talk_" + ID + "\n";
                    StringTemp += CharTemp;
                }
                thisReader.Close();
                myConnection.Close();
                ReturnString = "🗒 <b>Персонажи в локации:</b>\n" + StringTemp;
                return ReturnString;
            }

            public static string GetShopListInFriend(int ID) // Типы: 0 - Продавец, 1 - Механик, 2 - Доктор
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                MySqlCommand sql_commmand = new MySqlCommand("SELECT Type, Name FROM shops WHERE NPCID = @ID", myConnection);
                sql_commmand.Parameters.AddWithValue("@ID", ID);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                string ReturnString = string.Empty;
                string Type = string.Empty;
                string Name = string.Empty;
                while (thisReader.Read())
                {
                    Type += thisReader["Type"];
                    Name += thisReader["Name"];
                }
                thisReader.Close();
                myConnection.Close();

                
                ReturnString = "💰 <b>Торговля с:</b>" + Name + "\n⤵️ <b>Купить:</b>\n" + "";
                return ReturnString;
            }

            public static string GetData(int ID, string DataBlockName)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();

                MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM npc_friend WHERE ID = @ID", myConnection);
                sql_commmand.Parameters.AddWithValue("@ID", ID);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                string DataBlockResult = string.Empty;
                while (thisReader.Read())
                {
                    DataBlockResult += thisReader["" + DataBlockName + ""];
                }
                thisReader.Close();
                myConnection.Close();

                // Convert.ToInt64(DataBlockResult);
                return DataBlockResult;
            }
        }

        public static string ProfessionExists(int AccountID, int ProfessionType)
        {
            // Пустая - 0
            // Старпом - 1
            // Суперкарго - 2
            // Навигатор - 3
            // Техник-механик - 4 
            // Тактик - 5
            // Стрелок - 6
            // Аналитик - 7 
            // Медик - 8
            // Пилот - 9
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM npc_data WHERE FounderID = " + AccountID + " AND Hired = 1", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataToReturn = "";
            while (thisReader.Read())
            {
                string TempString = string.Empty;
                string ID = string.Empty;
                ID += thisReader["ID"];
                string ProfessionTypeID = NPC.GetDataByClanID(Convert.ToInt32(ID), "Profession");
                if (Convert.ToInt32(ProfessionTypeID) == ProfessionType)
                {
                    DataToReturn = ID;
                }
            }
            return DataToReturn;
        }

        public static bool DeleteAllUnemployed(int AccountID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            string sql;
            bool Finished = true;
            sql = "DELETE FROM npc_data WHERE FounderID = " + AccountID.ToString() + " AND Hired = 0";
            MySqlScript script = new MySqlScript(myConnection, sql);
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
                Console.WriteLine(sql);
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine(sqlEx);
                myConnection.Close();

                Finished = false;
                return Finished;
            }

            myConnection.Close();
            return Finished;
        }

        #region GenerateStats
        public static void GenerateStats(int AccountID, out string FirstName, out string LastName, out string UpgradeFocus1, out string UpgradeFocus2,
            out string Strength, out string Endurance, out string Charisma,
            out string Intelligence, out string Agility, out string Luck,
            out string Exp, out string Level,
            out string Apperception, out string Candor, out string Vivacity,
            out string Coordination, out string Meekness, out string Humility,
            out string Cruelty, out string Selfpreservation, out string Patience,
            out string Decisiveness, out string Imagination, out string Curiosity,
            out string Aggression, out string Loyalty, out string Empathy,
            out string Tenacity, out string Courage, out string Sensuality,
            out string Charm, out string Humor, out string Profession)
            {

            FirstName = "";
            LastName = "";
            Strength = "1";
            Endurance = "1";
            Charisma = "1";
            Intelligence = "1";
            Agility = "1";
            Luck = "1";

            int NPC_Sex = rndmzr.Next(1, 3);
            if (NPC_Sex == 1) // Male
            {
                FirstName = Generator.Generate.BotsFirstNameMale();
                LastName = Generator.Generate.BotsLastName();
            }
            else if (NPC_Sex == 2) // Female
            {
                FirstName = Generator.Generate.BotsFirstNameFemale();
                LastName = Generator.Generate.BotsLastName();
            }

            UpgradeFocus1 = rndmzr.Next(1, 7).ToString();
            UpgradeFocus2 = rndmzr.Next(1, 7).ToString();

            int Chance = rndmzr.Next(1, 101);

            if (Chance > 0 && Chance <= 65) // Обычные NPC
            {
                #region
                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Strength = rndmzr.Next(rndmzr.Next(1, 5), 6).ToString();
                }
                else Strength = rndmzr.Next(1, 4).ToString();

                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Endurance = rndmzr.Next(rndmzr.Next(1, 5), 6).ToString();
                }
                else Endurance = rndmzr.Next(1, 4).ToString();

                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Charisma = rndmzr.Next(rndmzr.Next(1, 5), 6).ToString();
                }
                else Charisma = rndmzr.Next(1, 4).ToString();

                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Intelligence = rndmzr.Next(rndmzr.Next(1, 5), 6).ToString();
                }
                else Intelligence = rndmzr.Next(1, 4).ToString();

                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Agility = rndmzr.Next(rndmzr.Next(1, 5), 6).ToString();
                }
                else Agility = rndmzr.Next(1, 4).ToString();

                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Luck = rndmzr.Next(rndmzr.Next(1, 5), 6).ToString();
                }
                else Luck = rndmzr.Next(1, 4).ToString();
                #endregion
            }
            else if (Chance > 65 && Chance <= 85) // Редкие NPC
            {
                #region
                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Strength = rndmzr.Next(rndmzr.Next(3, 7), 8).ToString();
                }
                else Strength = rndmzr.Next(2, 6).ToString();

                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Endurance = rndmzr.Next(rndmzr.Next(3, 7), 8).ToString();
                }
                else Endurance = rndmzr.Next(2, 6).ToString();

                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Charisma = rndmzr.Next(rndmzr.Next(3, 7), 8).ToString();
                }
                else Charisma = rndmzr.Next(2, 6).ToString();

                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Intelligence = rndmzr.Next(rndmzr.Next(3, 7), 8).ToString();
                }
                else Intelligence = rndmzr.Next(2, 6).ToString();

                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Agility = rndmzr.Next(rndmzr.Next(3, 7), 8).ToString();
                }
                else Agility = rndmzr.Next(2, 6).ToString();

                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Luck = rndmzr.Next(rndmzr.Next(3, 7), 8).ToString();
                }
                else Luck = rndmzr.Next(2, 6).ToString();
                #endregion
            }
            else if (Chance > 85 && Chance <= 101) // Легендарные NPC
            {
                #region
                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Strength = rndmzr.Next(rndmzr.Next(5, 10), 11).ToString();
                }
                else Strength = rndmzr.Next(4, 11).ToString();

                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Endurance = rndmzr.Next(rndmzr.Next(5, 10), 11).ToString();
                }
                else Endurance = rndmzr.Next(4, 11).ToString();

                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Charisma = rndmzr.Next(rndmzr.Next(5, 10), 11).ToString();
                }
                else Charisma = rndmzr.Next(4, 11).ToString();

                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Intelligence = rndmzr.Next(rndmzr.Next(5, 10), 11).ToString();
                }
                else Intelligence = rndmzr.Next(4, 11).ToString();

                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Agility = rndmzr.Next(rndmzr.Next(5, 10), 11).ToString();
                }
                else Agility = rndmzr.Next(4, 11).ToString();

                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Luck = rndmzr.Next(rndmzr.Next(5, 10), 11).ToString();
                }
                else Luck = rndmzr.Next(4, 11).ToString();
                #endregion
            }

            Exp = "0";
            Level = "1";

            Profession = rndmzr.Next(1, 10).ToString();

            Apperception = rndmzr.Next(1, 21).ToString();
            Candor = rndmzr.Next(1, 21).ToString();
            Vivacity = rndmzr.Next(1, 21).ToString();
            Coordination = rndmzr.Next(1, 21).ToString();
            Meekness = rndmzr.Next(1, 21).ToString();
            Humility = rndmzr.Next(1, 21).ToString();
            Cruelty = rndmzr.Next(1, 21).ToString();
            Selfpreservation = rndmzr.Next(1, 21).ToString();
            Patience = rndmzr.Next(1, 21).ToString();
            Decisiveness = rndmzr.Next(1, 21).ToString();
            Imagination = rndmzr.Next(1, 21).ToString();
            Curiosity = rndmzr.Next(1, 21).ToString();
            Aggression = rndmzr.Next(1, 21).ToString();
            Loyalty = rndmzr.Next(1, 21).ToString();
            Empathy = rndmzr.Next(1, 21).ToString();
            Tenacity = rndmzr.Next(1, 21).ToString();
            Courage = rndmzr.Next(1, 21).ToString();
            Sensuality = rndmzr.Next(1, 21).ToString();
            Charm = rndmzr.Next(1, 21).ToString();
            Humor = rndmzr.Next(1, 21).ToString();
        }
        #endregion

        #region GetAllInfoByNPCID
        public static void GetAllInfoByNPCID(int NPCID, out string ReturnFirstName, out string ReturnLastName, out string ReturnUpgradeFocus1, out string ReturnUpgradeFocus2, out string ReturnStrength, out string ReturnEndurance, out string ReturnCharisma, out string ReturnIntelligence, out string ReturnAgility, out string ReturnLuck, out string ReturnExp, out string ReturnLevel,
            out string ReturnApperception, out string ReturnCandor, out string ReturnVivacity,
            out string ReturnCoordination, out string ReturnMeekness, out string ReturnHumility,
            out string ReturnCruelty, out string ReturnSelfpreservation, out string ReturnPatience,
            out string ReturnDecisiveness, out string ReturnImagination, out string ReturnCuriosity,
            out string ReturnAggression, out string ReturnLoyalty, out string ReturnEmpathy,
            out string ReturnTenacity, out string ReturnCourage, out string ReturnSensuality,
            out string ReturnCharm, out string ReturnHumor, out string ReturnProfession)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_command = new MySqlCommand("SELECT FirstName, LastName, UpgradeFocus1, UpgradeFocus2, Strength, Endurance, Charisma, Intelligence, Agility, Luck, Exp, Level, Apperception, Candor, Vivacity, Coordination, Meekness, Humility, Cruelty, Selfpreservation, Patience, Decisiveness, Imagination, Curiosity, Aggression, Loyalty, Empathy, Tenacity, Courage, Sensuality, Charm, Humor, Profession FROM npc_data WHERE ID = @ID", myConnection);
            sql_command.Parameters.AddWithValue("@ID", NPCID);
            MySqlDataReader thisReader = sql_command.ExecuteReader();
            string FirstName = string.Empty;
            string LastName = string.Empty;
            string UpgradeFocus1 = string.Empty;
            string UpgradeFocus2 = string.Empty;
            string Strength = string.Empty;
            string Endurance = string.Empty;
            string Charisma = string.Empty;
            string Intelligence = string.Empty;
            string Agility = string.Empty;
            string Luck = string.Empty;
            string Exp = string.Empty;
            string Level = string.Empty;
            string Apperception = string.Empty;
            string Candor = string.Empty;
            string Vivacity = string.Empty;
            string Coordination = string.Empty;
            string Meekness = string.Empty;
            string Humility = string.Empty;
            string Cruelty = string.Empty;
            string Selfpreservation = string.Empty;
            string Patience = string.Empty;
            string Decisiveness = string.Empty;
            string Imagination = string.Empty;
            string Curiosity = string.Empty;
            string Aggression = string.Empty;
            string Loyalty = string.Empty;
            string Empathy = string.Empty;
            string Tenacity = string.Empty;
            string Courage = string.Empty;
            string Sensuality = string.Empty;
            string Charm = string.Empty;
            string Humor = string.Empty;
            string Profession = string.Empty;

            while (thisReader.Read())
            {
                FirstName += thisReader["FirstName"];
                LastName += thisReader["LastName"];
                UpgradeFocus1 += thisReader["UpgradeFocus1"];
                UpgradeFocus2 += thisReader["UpgradeFocus2"];
                Strength += thisReader["Strength"];
                Endurance += thisReader["Endurance"];
                Charisma += thisReader["Charisma"];
                Intelligence += thisReader["Intelligence"];
                Agility += thisReader["Agility"];
                Luck += thisReader["Luck"];
                Exp += thisReader["Exp"];
                Level += thisReader["Level"];
                Apperception += thisReader["Apperception"];
                Candor += thisReader["Candor"];
                Vivacity += thisReader["Vivacity"];
                Coordination += thisReader["Coordination"];
                Meekness += thisReader["Meekness"];
                Humility += thisReader["Humility"];
                Cruelty += thisReader["Cruelty"];
                Selfpreservation += thisReader["Selfpreservation"];
                Patience += thisReader["Patience"];
                Decisiveness += thisReader["Decisiveness"];
                Imagination += thisReader["Imagination"];
                Curiosity += thisReader["Curiosity"];
                Aggression += thisReader["Aggression"];
                Loyalty += thisReader["Loyalty"];
                Empathy += thisReader["Empathy"];
                Tenacity += thisReader["Tenacity"];
                Courage += thisReader["Courage"];
                Sensuality += thisReader["Sensuality"];
                Charm += thisReader["Charm"];
                Humor += thisReader["Humor"];
                Profession += thisReader["Profession"];
            }
            thisReader.Close();
            myConnection.Close();


            ReturnFirstName = FirstName;
            ReturnLastName = LastName;
            ReturnUpgradeFocus1 = UpgradeFocus1;
            ReturnUpgradeFocus2 = UpgradeFocus2;
            ReturnStrength = Strength;
            ReturnEndurance = Endurance;
            ReturnCharisma = Charisma;
            ReturnIntelligence = Intelligence;
            ReturnAgility = Agility;
            ReturnLuck = Luck;
            ReturnExp = Exp;
            ReturnLevel = Level;
            ReturnApperception = Apperception;
            ReturnCandor = Candor;
            ReturnVivacity = Vivacity;
            ReturnCoordination = Coordination;
            ReturnMeekness = Meekness;
            ReturnHumility = Humility;
            ReturnCruelty = Cruelty;
            ReturnSelfpreservation = Selfpreservation;
            ReturnPatience = Patience;
            ReturnDecisiveness = Decisiveness;
            ReturnImagination = Imagination;
            ReturnCuriosity = Curiosity;
            ReturnAggression = Aggression;
            ReturnLoyalty = Loyalty;
            ReturnEmpathy = Empathy;
            ReturnTenacity = Tenacity;
            ReturnCourage = Courage;
            ReturnSensuality = Sensuality;
            ReturnCharm = Charm;
            ReturnHumor = Humor;
            ReturnProfession = Profession;
        }
        #endregion

        public static int GetQualityPointsCountExtra(int NPCID) // Получаем количество очков основных характеристик
        {
            int NPC_Quality_Points = 0;
            NPC.GetAllInfoByNPCID(NPCID, out string ReturnFirstName, out string ReturnLastName, out string ReturnUpgradeFocus1, out string ReturnUpgradeFocus2,
                    out string ReturnStrength, out string ReturnEndurance, out string ReturnCharisma,
                    out string ReturnIntelligence, out string ReturnAgility, out string ReturnLuck,
                    out string ReturnExp, out string ReturnLevel,
                    out string ReturnApperception, out string ReturnCandor, out string ReturnVivacity,
                    out string ReturnCoordination, out string ReturnMeekness, out string ReturnHumility,
                    out string ReturnCruelty, out string ReturnSelfpreservation, out string ReturnPatience,
                    out string ReturnDecisiveness, out string ReturnImagination, out string ReturnCuriosity,
                    out string ReturnAggression, out string ReturnLoyalty, out string ReturnEmpathy,
                    out string ReturnTenacity, out string ReturnCourage, out string ReturnSensuality,
                    out string ReturnCharm, out string ReturnHumor, out string ReturnProfession);

            NPC_Quality_Points = Convert.ToInt32(ReturnApperception) + Convert.ToInt32(ReturnCandor)
                + Convert.ToInt32(ReturnVivacity) + Convert.ToInt32(ReturnCoordination) + Convert.ToInt32(ReturnMeekness)
                + Convert.ToInt32(ReturnHumility) + Convert.ToInt32(ReturnCruelty) + Convert.ToInt32(ReturnSelfpreservation)
                 + Convert.ToInt32(ReturnPatience) + Convert.ToInt32(ReturnDecisiveness) + Convert.ToInt32(ReturnImagination)
                  + Convert.ToInt32(ReturnCuriosity) + Convert.ToInt32(ReturnAggression) + Convert.ToInt32(ReturnLoyalty)
                   + Convert.ToInt32(ReturnEmpathy) + Convert.ToInt32(ReturnTenacity) + Convert.ToInt32(ReturnCourage)
                    + Convert.ToInt32(ReturnSensuality) + Convert.ToInt32(ReturnCharm) + Convert.ToInt32(ReturnHumor);
            return NPC_Quality_Points;
        }

        public static void Create(int FounderID, string Hired)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);

            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;

            GenerateStats(FounderID, out string FirstName, out string LastName, out string UpgradeFocus1, out string UpgradeFocus2, out string Strength, out string Endurance, out string Charisma, out string Intelligence, out string Agility, out string Luck, out string Exp, out string Level,
            out string Apperception, out string Candor, out string Vivacity,
            out string Coordination, out string Meekness, out string Humility,
            out string Cruelty, out string Selfpreservation, out string Patience,
            out string Decisiveness, out string Imagination, out string Curiosity,
            out string Aggression, out string Loyalty, out string Empathy,
            out string Tenacity, out string Courage, out string Sensuality,
            out string Charm, out string Humor, out string Profession);

            string CreationDateTime = DateTime.Now.ToString("h:mm:ss");

            myConnection.Open();
            string varquery = @"""" + FounderID.ToString() + @"""" + ", " + @"""" + FirstName + @"""" + ", " + @"""" + LastName + @"""" + ", " + @"""" + UpgradeFocus1 + @"""" + ", " + @"""" + UpgradeFocus2 + @"""" + ", " + @"""" + Strength.ToString() + @"""" + ", " + @"""" + Endurance.ToString() + @"""" + ", " + @"""" + Charisma.ToString() + @"""" + ", " + @"""" + Intelligence.ToString() + @"""" + ", " + @"""" + Agility.ToString() + @"""" + ", " + @"""" + Luck.ToString() + @"""" + ", " + @"""" + Exp.ToString() + @"""" + ", " + @"""" + Level.ToString() + @"""" + ", " + @"""" + Apperception.ToString() + @"""" + ", " + @"""" + Candor.ToString() + @"""" + ", " + @"""" + Vivacity.ToString() + @"""" + ", " + @"""" + Coordination.ToString() + @"""" + ", " + @"""" + Meekness.ToString() + @"""" + ", " + @"""" + Humility.ToString() + @"""" + ", " + @"""" + Cruelty.ToString() + @"""" + ", " + @"""" + Selfpreservation.ToString() + @"""" + ", " + @"""" + Patience.ToString() + @"""" + ", " + @"""" + Decisiveness.ToString() + @"""" + ", " + @"""" + Imagination.ToString() + @"""" + ", " + @"""" + Curiosity.ToString() + @"""" + ", " + @"""" + Aggression.ToString() + @"""" + ", " + @"""" + Loyalty.ToString() + @"""" + ", " + @"""" + Empathy.ToString() + @"""" + ", " + @"""" + Tenacity.ToString() + @"""" + ", " + @"""" + Courage.ToString() + @"""" + ", " + @"""" + Sensuality.ToString() + @"""" + ", " + @"""" + Charm.ToString() + @"""" + ", " + @"""" + Humor.ToString() + @"""" + ", " + @"""" + CreationDateTime.ToString() + @"""" + ", " + @"""" + Profession.ToString() + @"""" + ", " + @"""" + Hired.ToString() + @"""";

            sql = "INSERT INTO npc_data (FounderID, FirstName, LastName, UpgradeFocus1, UpgradeFocus2, Strength, Endurance, Charisma, Intelligence, Agility, Luck, Exp, Level, Apperception, Candor, Vivacity, Coordination, Meekness, Humility, Cruelty, Selfpreservation, Patience, Decisiveness, Imagination, Curiosity, Aggression, Loyalty, Empathy, Tenacity, Courage, Sensuality, Charm, Humor, Date, Profession, Hired) VALUES (" + varquery + "); ";

            MySqlScript script = new MySqlScript(myConnection, sql);
            script.Query = sql;
            Console.WriteLine("DEBUG: " + script.Query);
            script.Connection = myConnection;
            script.Execute();

            string NPC_Face = "🙂";

            int[] NPC_Stats_Array = { Convert.ToInt32(Apperception), Convert.ToInt32(Candor), Convert.ToInt32(Vivacity),
            Convert.ToInt32(Coordination), Convert.ToInt32(Meekness), Convert.ToInt32(Humility),
            Convert.ToInt32(Cruelty), Convert.ToInt32(Selfpreservation), Convert.ToInt32(Patience),
            Convert.ToInt32(Decisiveness), Convert.ToInt32(Imagination), Convert.ToInt32(Curiosity),
            Convert.ToInt32(Aggression), Convert.ToInt32(Loyalty), Convert.ToInt32(Empathy),
            Convert.ToInt32(Tenacity), Convert.ToInt32(Courage), Convert.ToInt32(Sensuality),
            Convert.ToInt32(Charm), Convert.ToInt32(Humor)};

            int max = int.MaxValue;
            int MaxStatPosition = -1;

            for (MaxStatPosition = 0; MaxStatPosition < NPC_Stats_Array.Length; MaxStatPosition++)
            {
                if (NPC_Stats_Array[MaxStatPosition] > max)
                    max = NPC_Stats_Array[MaxStatPosition];
            }

            switch (max) // max - позиция статы в эррее
            {
                case 0: // Осознанность
                    break;

                case 1: // Откровенность
                    break;

                default:
                    break;
            }

            // Здесь задаем эмоджи для NPC

            try
            {
                //Message.Send(FounderID, "<b>Доступен новый член команды:</b>\n\n" + NPC_Face + " " + FirstName + " " + LastName + "\n", 3); // Дописать уведомление о создании
            }
            catch (Exception MyEx)
            {
                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - CreateNPC] ОШИБКА!: " + MyEx);
            }

            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - CreateNPC] Создание NPC: выполнения запроса: " + sql);
            myConnection.Close(); //Обязательно закрываем соединение!
        }

        public static string GetDataByClanID(int ID, string DataBlockName) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM npc_data WHERE ID = @ID", myConnection);
            sql_commmand.Parameters.AddWithValue("@ID", ID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["" + DataBlockName + ""];
            }
            thisReader.Close();
            myConnection.Close();

            // Convert.ToInt64(DataBlockResult);
            return DataBlockResult;
        }

        public static bool SetData(int ID, string DataBlockName, string NewParameter)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);

            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;

            sql = "UPDATE npc_data SET " + DataBlockName + " =" + @"""" + NewParameter + @"""" + " WHERE ID = " + ID.ToString() + ";";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - Account.SetSingleData] Запрос к базе для аккаунта: " + ID.ToString() + " datablock " + DataBlockName + " query - " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();

                Finished = false;
                return Finished;
            }

            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static bool SetDataForPlayerHired(int AccountID, int ID, string DataBlockName, string NewParameter, string onHired)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);

            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = true;

            sql = "UPDATE npc_data SET " + DataBlockName + " = " + @"""" + NewParameter + @"""" + " WHERE ID = " + ID.ToString() + " AND FounderID = " + AccountID.ToString() + " AND Hired = " + onHired + ";";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - NPC.SetDataForPlayerHired] Запрос к базе для аккаунта: " + ID.ToString() + " datablock " + DataBlockName + " query - " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();

                Finished = false;
                return Finished;
            }

            myConnection.Close(); //Обязательно закрываем соединение!
            return Finished;
        }

        public static int GetQualityPointsCountMain(int NPCID) // Получаем количество очков основных характеристик
        {
            int NPC_Quality_Points = 0;
            NPC.GetAllInfoByNPCID(NPCID, out string ReturnFirstName, out string ReturnLastName, out string ReturnUpgradeFocus1, out string ReturnUpgradeFocus2,
                out string ReturnStrength, out string ReturnEndurance, out string ReturnCharisma,
                out string ReturnIntelligence, out string ReturnAgility, out string ReturnLuck,
                out string ReturnExp, out string ReturnLevel,
                out string ReturnApperception, out string ReturnCandor, out string ReturnVivacity,
                out string ReturnCoordination, out string ReturnMeekness, out string ReturnHumility,
                out string ReturnCruelty, out string ReturnSelfpreservation, out string ReturnPatience,
                out string ReturnDecisiveness, out string ReturnImagination, out string ReturnCuriosity,
                out string ReturnAggression, out string ReturnLoyalty, out string ReturnEmpathy,
                out string ReturnTenacity, out string ReturnCourage, out string ReturnSensuality,
                out string ReturnCharm, out string ReturnHumor, out string ReturnProfession);

            NPC_Quality_Points = Convert.ToInt32(ReturnStrength) + Convert.ToInt32(ReturnEndurance)
                                                                 + Convert.ToInt32(ReturnCharisma) + Convert.ToInt32(ReturnIntelligence) + Convert.ToInt32(ReturnAgility)
                                                                 + Convert.ToInt32(ReturnLuck);
            return NPC_Quality_Points;
        }

        public static int GetHirePrice(int NPCID)
        {
            int NPCHirePrice = 0;
            float NPCHireTempPrice = 0.0f;
            float NPCPrice_Multiplier_Main = 1.0f;
            float NPCPrice_Base_Price_Main = 300.0f;
            float NPCPrice_Base_Price_Extra = 30.0f;

            int NPC_QualityPoints_Main = NPC.GetQualityPointsCountMain(NPCID);
            int NPC_QualityPoints_Extra = NPC.GetQualityPointsCountExtra(NPCID);

            if (NPC_QualityPoints_Main > 0 && NPC_QualityPoints_Main <= 10)
            {
                NPCPrice_Multiplier_Main = 1.0f;
                NPCPrice_Base_Price_Main = 50.0f;
                NPCPrice_Base_Price_Extra = 10.0f;
            }
            if (NPC_QualityPoints_Main > 10 && NPC_QualityPoints_Main <= 20)
            {
                NPCPrice_Multiplier_Main = 1.0f;
                NPCPrice_Base_Price_Main = 50.0f;
                NPCPrice_Base_Price_Extra = 10.0f;
            }
            else if (NPC_QualityPoints_Main > 20 && NPC_QualityPoints_Main <= 40)
            {
                NPCPrice_Multiplier_Main = 2.0f;
                NPCPrice_Base_Price_Main = 400.0f;
                NPCPrice_Base_Price_Extra = 50.0f;
            }
            else if (NPC_QualityPoints_Main > 40 && NPC_QualityPoints_Main <= 60)
            {
                NPCPrice_Multiplier_Main = 3.0f;
                NPCPrice_Base_Price_Main = 700.0f;
                NPCPrice_Base_Price_Extra = 90.0f;
            }
            Console.WriteLine("DEBUG: NPC_QualityPoints_Main = " + NPC_QualityPoints_Main);
            Console.WriteLine("DEBUG: NPC_QualityPoints_Extra = " + NPC_QualityPoints_Extra);
            NPCHireTempPrice = ((NPCPrice_Multiplier_Main * NPCPrice_Base_Price_Main) * 6) + (NPC_QualityPoints_Extra * 50) + (NPC_QualityPoints_Main * NPCPrice_Multiplier_Main);
            NPCHirePrice = Convert.ToInt32(NPCHireTempPrice);
            return NPCHirePrice;
        }

        public static bool IsExists(int AccountID, int NPCID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT FirstName FROM npc_data WHERE FounderID = @FounderID AND ID = @NPCID", myConnection);
            sql_commmand.Parameters.AddWithValue("@FounderID", AccountID);
            sql_commmand.Parameters.AddWithValue("@NPCID", NPCID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string NPCExists = string.Empty;
            while (thisReader.Read())
            {
                NPCExists += thisReader["FirstName"];
            }
            thisReader.Close();
            myConnection.Close();
            if (NPCExists != "" && NPCExists != " ")
            {
                return true;
            }
            else return false;
        }

        public static bool isHired(int ShipID, int NPCID) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT Hired FROM npc_data WHERE FounderID = @FounderID AND ID = @ID", myConnection);
            sql_commmand.Parameters.AddWithValue("@FounderID", ShipID);
            sql_commmand.Parameters.AddWithValue("@ID", NPCID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["Hired"];
            }
            thisReader.Close();
            myConnection.Close();

            if (DataBlockResult != "0")
            {
                return true;
            }
            else return false;
        }

    }

}
