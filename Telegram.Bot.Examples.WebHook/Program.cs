using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin.Hosting;
using Owin;
using File = System.IO.File;
using MySql.Data.MySqlClient;
using System.Timers;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using Telegram.Bot.Args;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using static Telegram.Bot.Examples.WebHook.Ships;
using static Telegram.Bot.Examples.WebHook.Profession;
using static Telegram.Bot.Examples.WebHook.Generator;
using static Telegram.Bot.Examples.WebHook.DataStorage;
using static Telegram.Bot.Examples.WebHook.Account;
using static Telegram.Bot.Examples.WebHook.Message;
using static Telegram.Bot.Examples.WebHook.NPC;
using static Telegram.Bot.Examples.WebHook.Clans;
using static Telegram.Bot.Examples.WebHook.Recipes;
using Telegram.Bot.Examples.WebHook;
using Message = Telegram.Bot.Examples.WebHook.Message; // ЭТЕНШН
using Newtonsoft.Json.Linq;

// Осторожно! Быдлокод!
// "Я" версии 2022 года, со всей ответственностью заявляю: сейчас бы я сделал гораздо лучше.
// Соблюдая принципы SOLID и ООП. Вынося функционал в классы и методы, а не накрываясь с головой простынёй кода.
// Используя геттеры и сеттеры. Не пихая бездумно всё в паблик, потому что некогда разбираться с модификаторами доступа.
// Про работу c SQL ваще молчу... Наверное, вышеперечисленное, как и отсутствие целостной архитектуры - издержки одиночной разработки.
// Всегда хочется запилить больше контента для игроков, больше функционала. Надо было потратить время на изучение матчасти.
// Надо было. Но... Получилось так, как получилось на тот момент.
// Но опыт был хороший, безусловно.

#region  EnumerableExtension
public static class EnumerableExtension
{
    public static T PickRandom<T>(this IEnumerable<T> source)
    {
        return source.PickRandom(1).Single();
    }

    public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
    {
        return source.Shuffle().Take(count);
    }

    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
    {
        return source.OrderBy(x => Guid.NewGuid());
    }
}
#endregion

class CraftMaterials
{
    public int MaterialsID { get; set; }
    public int MaterialsCount { get; set; }
}

class BlackJackCards
{
    public int CardType { get; set; }
    public int CardValue { get; set; }
}

public static class StringCipher
{
    // This constant is used to determine the keysize of the encryption algorithm in bits.
    // We divide this by 8 within the code below to get the equivalent number of bytes.
    private const int Keysize = 256;

    // This constant determines the number of iterations for the password bytes generation function.
    private const int DerivationIterations = 1000;

    public static string Encrypt(string plainText, string passPhrase)
    {
        // Salt and IV is randomly generated each time, but is preprended to encrypted cipher text
        // so that the same Salt and IV values can be used when decrypting.  
        var saltStringBytes = Generate256BitsOfRandomEntropy();
        var ivStringBytes = Generate256BitsOfRandomEntropy();
        var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
        {
            var keyBytes = password.GetBytes(Keysize / 8);
            using (var symmetricKey = new RijndaelManaged())
            {
                symmetricKey.BlockSize = 256;
                symmetricKey.Mode = CipherMode.CBC;
                symmetricKey.Padding = PaddingMode.PKCS7;
                using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                        {
                            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                            cryptoStream.FlushFinalBlock();
                            // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                            var cipherTextBytes = saltStringBytes;
                            cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                            cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                            memoryStream.Close();
                            cryptoStream.Close();
                            return Convert.ToBase64String(cipherTextBytes);
                        }
                    }
                }
            }
        }
    }

    public static string Decrypt(string cipherText, string passPhrase)
    {
        // Get the complete stream of bytes that represent:
        // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
        var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
        // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
        var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
        // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
        var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
        // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
        var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

        using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
        {
            var keyBytes = password.GetBytes(Keysize / 8);
            using (var symmetricKey = new RijndaelManaged())
            {
                symmetricKey.BlockSize = 256;
                symmetricKey.Mode = CipherMode.CBC;
                symmetricKey.Padding = PaddingMode.PKCS7;
                using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                {
                    using (var memoryStream = new MemoryStream(cipherTextBytes))
                    {
                        using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                        {
                            var plainTextBytes = new byte[cipherTextBytes.Length];
                            var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                            memoryStream.Close();
                            cryptoStream.Close();
                            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                        }
                    }
                }
            }
        }
    }

    private static byte[] Generate256BitsOfRandomEntropy()
    {
        var randomBytes = new byte[32]; // 32 Bytes will give us 256 bits.
        using (var rngCsp = new RNGCryptoServiceProvider())
        {
            // Fill the array with cryptographically secure random bytes.
            rngCsp.GetBytes(randomBytes);
        }
        return randomBytes;
    }
}

static class SubstringExtensions
{
    /// <summary>
    /// Get string value between [first] a and [last] b.
    /// </summary>
    public static string Between(this string value, string a, string b)
    {
        int posA = value.IndexOf(a);
        int posB = value.LastIndexOf(b);
        if (posA == -1)
        {
            return "";
        }
        if (posB == -1)
        {
            return "";
        }
        int adjustedPosA = posA + a.Length;
        if (adjustedPosA >= posB)
        {
            return "";
        }
        return value.Substring(adjustedPosA, posB - adjustedPosA);
    }

    /// <summary>
    /// Get string value after [first] a.
    /// </summary>
    public static string Before(this string value, string a)
    {
        int posA = value.IndexOf(a);
        if (posA == -1)
        {
            return "";
        }
        return value.Substring(0, posA);
    }

    /// <summary>
    /// Get string value after [last] a.
    /// </summary>
    public static string After(this string value, string a)
    {
        int posA = value.LastIndexOf(a);
        if (posA == -1)
        {
            return "";
        }
        int adjustedPosA = posA + a.Length;
        if (adjustedPosA >= value.Length)
        {
            return "";
        }
        return value.Substring(adjustedPosA);
    }

    public static string OnlyNumbers(this string value)
    {
        Regex REG = new Regex("[0-9]+");
        MatchCollection mc = REG.Matches(value);
        StringBuilder sb = new StringBuilder();
        foreach (Match matc in mc)
            sb.Append(matc.Value);
        value = sb.ToString();
        return value;
    }

    public static string OnlyLetters(this string value)
    {
        Regex REG = new Regex("^[\\w ]+$");
        MatchCollection mc = REG.Matches(value);
        StringBuilder sb = new StringBuilder();
        foreach (Match matc in mc)
            sb.Append(matc.Value);
        value = sb.ToString();
        return value;
    }
}

namespace Telegram.Bot.Api.Examples.WebHook
{
    public class Bot
    {
        public static readonly TelegramBotClient Api = new TelegramBotClient(""); // Нужен Token
    }

    public static class Program
    {
        
        public static System.Timers.Timer EventTimer;
        public static System.Timers.Timer DonationTimer;
        public static System.Timers.Timer BattleTimer;
        public static System.Timers.Timer HealthRecoveryTimer;
        public static System.Timers.Timer MapMoveShipEventTimer;
        public static System.Timers.Timer PlanetMapMoveEventTimer;
        public static System.Timers.Timer WeatherTimer;
        public static System.Timers.Timer EnergyRecoveryTimer;
        public static System.Timers.Timer ShipEnergyRecoveryTimer;
        public static System.Timers.Timer NPCActionsTimer;
        public static System.Timers.Timer MapCooldownTimer;
        public static System.Timers.Timer PlanetMapCooldownTimer;
        public static System.Timers.Timer AntiSpamTimer;
        public static System.Timers.Timer LevelupTimer;
        public static System.Timers.Timer NPCListRefreshTimer;
        public static System.Timers.Timer BetweenMissionsTimer;
        public static string GameBotName = "@Starwing";

        // Список базовых характеристик кораблей
        public static List<BaseShips> BaseShipList = new List<BaseShips>
        {

        };
        // Список профессий
        public static List<Professions> ProfessionsList = new List<Professions>
        {

        };

        static DateTime GlobalBattle1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 5, 0, 0);
        static DateTime GlobalBattle2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 9, 0, 0);
        static DateTime GlobalBattle3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 13, 0, 0);
        static DateTime GlobalBattle4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 17, 0, 0);
        static DateTime GlobalBattle5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 21, 0, 0);
        static DateTime GlobalBattle6 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 0, 0);

        // Массивы дроп-листы
        static List<int> very_high_chance_drop = new List<int>(); // DropClass = 5
        static List<int> high_chance_drop = new List<int>(); // DropClass = 4
        static List<int> medium_chance_drop = new List<int>(); // DropClass = 3
        static List<int> low_chance_drop = new List<int>(); // DropClass = 2
        static List<int> very_low_chance_drop = new List<int>(); // DropClass = 1
        // DropClass = 0 - не падает вообще


        public static void Main(string[] args)
        {
            BaseShipList.Add(new BaseShips
            {
                Name = "Пустой",
                HP = 0,
                Def = 0,
                Attack = 0,
                Maneuv = 0,
                Speed = 0,
                Energy = 0,
                MaxCrew = 0,
                MaxModules = 0,
                MaxCargo = 0
            }); // Пустой
            BaseShipList.Add(new BaseShips
            {
                Name = "Легкий перехватчик",
                HP = 100,
                Def = 10,
                Attack = 10,
                Maneuv = 80,
                Speed = 60,
                Energy = 50,
                MaxCrew = 2,
                MaxModules = 2,
                MaxCargo = 50
            }); // Легкий перехватчик
            BaseShipList.Add(new BaseShips
            {
                Name = "Средний перехватчик",
                HP = 150,
                Def = 15,
                Attack = 20,
                Maneuv = 65,
                Speed = 50,
                Energy = 50,
                MaxCrew = 2,
                MaxModules = 2,
                MaxCargo = 60
            }); // Средний перехватчик
            BaseShipList.Add(new BaseShips
            {
                Name = "Тяжелый перехватчик",
                HP = 200,
                Def = 25,
                Attack = 25,
                Maneuv = 45,
                Speed = 40,
                Energy = 50,
                MaxCrew = 3,
                MaxModules = 3,
                MaxCargo = 70
            }); // Тяжелый перехватчик
            BaseShipList.Add(new BaseShips
            {
                Name = "Бомбардировщик",
                HP = 275,
                Def = 25,
                Attack = 40,
                Maneuv = 25,
                Speed = 15,
                Energy = 75,
                MaxCrew = 5,
                MaxModules = 4,
                MaxCargo = 120
            }); // Бомбардировщик
            BaseShipList.Add(new BaseShips
            {
                Name = "Тяжелый Бомбардировщик",
                HP = 375,
                Def = 30,
                Attack = 60,
                Maneuv = 15,
                Speed = 10,
                Energy = 80,
                MaxCrew = 5,
                MaxModules = 4,
                MaxCargo = 150
            }); // Тяжелый Бомбардировщик
            BaseShipList.Add(new BaseShips
            {
                Name = "Фрегат",
                HP = 300,
                Def = 35,
                Attack = 35,
                Maneuv = 45,
                Speed = 40,
                Energy = 85,
                MaxCrew = 5,
                MaxModules = 4,
                MaxCargo = 200
            }); // Фрегат
            BaseShipList.Add(new BaseShips
            {
                Name = "Тяжелый Фрегат",
                HP = 350,
                Def = 40,
                Attack = 45,
                Maneuv = 40,
                Speed = 30,
                Energy = 90,
                MaxCrew = 7,
                MaxModules = 5,
                MaxCargo = 250
            }); // Тяжелый Фрегат
            BaseShipList.Add(new BaseShips
            {
                Name = "Эсминец",
                HP = 280,
                Def = 50,
                Attack = 50,
                Maneuv = 40,
                Speed = 30,
                Energy = 100,
                MaxCrew = 7,
                MaxModules = 5,
                MaxCargo = 300
            }); // Эсминец
            BaseShipList.Add(new BaseShips
            {
                Name = "Крейсер",
                HP = 330,
                Def = 55,
                Attack = 55,
                Maneuv = 35,
                Speed = 35,
                Energy = 125,
                MaxCrew = 7,
                MaxModules = 6,
                MaxCargo = 350
            }); // Крейсер
            BaseShipList.Add(new BaseShips
            {
                Name = "Тяжелый Крейсер",
                HP = 350,
                Def = 65,
                Attack = 65,
                Maneuv = 30,
                Speed = 30,
                Energy = 125,
                MaxCrew = 8,
                MaxModules = 6,
                MaxCargo = 400
            }); // Тяжелый Крейсер
            BaseShipList.Add(new BaseShips
            {
                Name = "Линкор",
                HP = 375,
                Def = 60,
                Attack = 80,
                Maneuv = 25,
                Speed = 20,
                Energy = 125,
                MaxCrew = 8,
                MaxModules = 7,
                MaxCargo = 450
            }); // Линкор
            BaseShipList.Add(new BaseShips
            {
                Name = "Ударный Линкор",
                HP = 400,
                Def = 70,
                Attack = 90,
                Maneuv = 20,
                Speed = 15,
                Energy = 125,
                MaxCrew = 8,
                MaxModules = 7,
                MaxCargo = 500
            }); // Ударный Линкор
            BaseShipList.Add(new BaseShips
            {
                Name = "Трансорбитальный Линкор",
                HP = 500,
                Def = 90,
                Attack = 110,
                Maneuv = 15,
                Speed = 10,
                Energy = 160,
                MaxCrew = 10,
                MaxModules = 7,
                MaxCargo = 750
            }); // Трансорбитальный Линкор
            BaseShipList.Add(new BaseShips
            {
                Name = "Атлант",
                HP = 1000,
                Def = 180,
                Attack = 200,
                Maneuv = 10,
                Speed = 10,
                Energy = 250,
                MaxCrew = 10,
                MaxModules = 8,
                MaxCargo = 1000
            }); // Атлант
            BaseShipList.Add(new BaseShips
            {
                Name = "Материнский Корабль",
                HP = 5000,
                Def = 500,
                Attack = 650,
                Maneuv = 5,
                Speed = 5,
                Energy = 500,
                MaxCrew = 15,
                MaxModules = 8,
                MaxCargo = 3500
            }); // Материнский корабль
            // Профессии
            ProfessionsList.Add(new Professions
            {
                Profession = ""
            }); // Пустая - 0
            ProfessionsList.Add(new Professions
            {
                Profession = "Старпом"
            }); // Старпом - 1
            ProfessionsList.Add(new Professions
            {
                Profession = "Суперкарго"
            }); // Суперкарго - 2
            ProfessionsList.Add(new Professions
            {
                Profession = "Навигатор"
            }); // Навигатор - 3
            ProfessionsList.Add(new Professions
            {
                Profession = "Техник-механик"
            }); // Техник-механик - 4 
            ProfessionsList.Add(new Professions
            {
                Profession = "Тактик"
            }); // Тактик - 5
            ProfessionsList.Add(new Professions
            {
                Profession = "Стрелок"
            }); // Стрелок - 6
            ProfessionsList.Add(new Professions
            {
                Profession = "Аналитик"
            }); // Аналитик - 7 
            ProfessionsList.Add(new Professions
            {
                Profession = "Медик"
            }); // Медик - 8
            ProfessionsList.Add(new Professions
            {
                Profession = "Пилот"
            }); // Пилот - 9

            Bot.Api.OnCallbackQuery += BotOnCallbackQueryReceived;
            //Bot.Api.OnMessage += BotOnMessageReceived;
            //Bot.Api.OnMessageEdited += BotOnMessageEdited;
            Bot.Api.OnInlineQuery += BotOnInlineQueryReceived;
            Bot.Api.OnInlineResultChosen += BotOnChosenInlineResultReceived;
            Bot.Api.OnReceiveError += Callbacks.BotOnReceiveError;

            LoadDropLists();

            SetEventTimer();
            SetDonationTimer();
            SetHealthRecoveryTimer();
            SetMapMoveShipEventTimer();
            SetPlanetMapMoveEventTimer();
            SetWeatherTimer();
            WeatherController();
            SetBetweenMissionsTimer();
            SetNPCListRefreshTimer();
            SetEnergyRecoveryTimer();
            SetNPCActionsTimer();
            SetShipEnergyRecoveryTimer();
            SetMapCooldownTimer();
            SetPlanetMapCooldownTimer();
            SetAntiSpamTimer();
            SetBattleTimer();
            SetLevelUpTimer();

            // Endpoint must be configured with netsh:
            // netsh http add urlacl url=https://+:8443/ user=<username>
            // netsh http add sslcert ipport=0.0.0.0:8443 certhash=<cert thumbprint> appid=<random guid>

            using (WebApp.Start<Startup>("http://+:8443/"))
            {
                // Register WebHook
                // You should replace {YourHostname} with your Internet accessible hosname
                Bot.Api.SetWebhookAsync("https://bf0830bc.ngrok.io/WebHook").Wait();

                Console.WriteLine("Server Started");
                Console.WriteLine("v.1.1");
 
                // Stop Server after <Enter>
                string Command = Console.ReadLine();

                // Unregister WebHook
                Bot.Api.DeleteWebhookAsync().Wait();
            }

            
        }

        public static async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            try
            {
                await Bot.Api.AnswerCallbackQueryAsync(callbackQueryEventArgs.CallbackQuery.Id,
                    $"Received {callbackQueryEventArgs.CallbackQuery.Data}");
            }
            catch
            {

            }

        }

        public static void LoadDropLists()
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID, DropClass FROM items", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            string ID = string.Empty;
            string DropClass = string.Empty;

            while (thisReader.Read())
            {
                ID = string.Empty;
                DropClass = string.Empty;

                ID += thisReader["ID"];
                Console.WriteLine("New id: " + ID);
                DropClass += thisReader["DropClass"];
                switch (DropClass)
                {
                    case "0":
                        break;
                    case "5":
                        very_high_chance_drop.Add(Convert.ToInt32(ID));
                        break;
                    case "4":
                        high_chance_drop.Add(Convert.ToInt32(ID));
                        break;
                    case "3":
                        medium_chance_drop.Add(Convert.ToInt32(ID));
                        break;
                    case "2":
                        low_chance_drop.Add(Convert.ToInt32(ID));
                        break;
                    case "1":
                        very_low_chance_drop.Add(Convert.ToInt32(ID));
                        break;
                    default:
                        break;
                }
            }
            Console.WriteLine("1 Drop list:");
            very_high_chance_drop.ForEach(Console.WriteLine);
            Console.WriteLine("2 Drop list:");
            high_chance_drop.ForEach(Console.WriteLine);
            Console.WriteLine("3 Drop list:");
            medium_chance_drop.ForEach(Console.WriteLine);
            Console.WriteLine("4 Drop list:");
            low_chance_drop.ForEach(Console.WriteLine);
            Console.WriteLine("5 Drop list:");
            very_low_chance_drop.ForEach(Console.WriteLine);

            thisReader.Close();
            myConnection.Close();
            
        }

        public static bool SetGlobalSingleData(string DataBlockName, string NewParameter)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;
            sql = "UPDATE global SET " + DataBlockName + " =" + @"""" + NewParameter + @"""" + ";";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - SetGlobalSingleData] Записываю в секцию global: " + "datablock " + DataBlockName + " query - " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - SetGlobalSingleData] ОШИБКА!: " + sqlEx);
                myConnection.Close();
                
                Finished = false;
                return Finished;
            }
            
            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static void CreateItemsBD()
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID FROM users", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DBAccountID = string.Empty;
            while (thisReader.Read())
            {
                DBAccountID = string.Empty;
                DBAccountID += thisReader["AccountID"];
                MySqlConnection SelCon = new MySqlConnection(Connect);
                
                SelCon.Open();
                string sql;
                string inventory_query = DBAccountID.ToString() + ", " + "0, 0, 0, 0, 0, 0, 0, 0, 0, 0";
                sql = "INSERT INTO inventory (AccountID, Slot1, Slot2, Slot3, Slot4, Slot5, Slot6, Slot7, Slot8, Slot9, Slot10) VALUES (" + inventory_query + "); ";
                MySqlScript script = new MySqlScript(SelCon, sql);
                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - CreateItemsBD] Создание инвентаря для ID: " + DBAccountID.ToString());
                script = new MySqlScript(SelCon, sql);
                script.Query = sql;
                script.Connection = SelCon;
                script.Execute();
                SelCon.Close();
                
            }
            thisReader.Close();
            myConnection.Close();
            
        }

        public static bool SetBotSingleData(int ID, string DataBlockName, string NewParameter)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;

            sql = "UPDATE ship_bots SET " + DataBlockName + " =" + @"""" + NewParameter + @"""" + " WHERE ID = " + ID.ToString() + ";";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - SetBotSingleData] Запрос к базе для аккаунта: " + ID.ToString() + " datablock " + DataBlockName + " query - " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();
                
                Finished = false;
                return Finished;
            }
            
            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static void SetMapData(int X, int Y, int LocationType, string LocationName, string LocationOwnerID, string LocationOwnerName, int LocationDanger, int IsCooldown, int CooldownTime)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            sql = "INSERT INTO map SET " + "X" + " = " + @"""" + X.ToString() + @"""" + ", " + "Y" + " = " + @"""" + Y.ToString() + @"""" + ", " + "LocationType" + " = " + @"""" + LocationType.ToString() + @"""" + ", " + "LocationName" + " = " + @"""" + LocationName + @"""" + ", " + "LocationOwnerID" + " = " + @"""" + LocationOwnerID + @"""" + ", " + "LocationOwnerName" + " = " + @"""" + LocationOwnerName + @"""" + ", " + "LocationDanger" + " = " + @"""" + LocationDanger.ToString() + @"""" + ", " + "IsCooldown" + " = " + @"""" + IsCooldown.ToString() + @"""" + ", " + "CooldownTime" + " = " + @"""" + CooldownTime.ToString() + @"""" + ";";
            //Message.Send(65530966, sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();
                
            }
            
            myConnection.Close(); //Обязательно закрываем соединение!
        }

        public static void SetPlanetMapData(int X, int Y, int planetX, int planetY, int LocationType, string LocationName, string LocationOwnerID, string LocationOwnerName, int LocationDanger, int IsCooldown, int CooldownTime)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);

            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            sql = "INSERT INTO map_planet SET " + "X" + " = " + @"""" + X.ToString() + @"""" + ", " + "planetX" + " = " + @"""" + planetX.ToString() + @"""" + ", " + "Y" + " = " + @"""" + Y.ToString() + @"""" + ", " + "planetY" + " = " + @"""" + planetY.ToString() + @"""" + ", " + "LocationType" + " = " + @"""" + LocationType.ToString() + @"""" + ", " + "LocationName" + " = " + @"""" + LocationName + @"""" + ", " + "LocationOwnerID" + " = " + @"""" + LocationOwnerID + @"""" + ", " + "LocationOwnerName" + " = " + @"""" + LocationOwnerName + @"""" + ", " + "LocationDanger" + " = " + @"""" + LocationDanger.ToString() + @"""" + ", " + "IsCooldown" + " = " + @"""" + IsCooldown.ToString() + @"""" + ", " + "CooldownTime" + " = " + @"""" + CooldownTime.ToString() + @"""" + ";";
            //Message.Send(65530966, sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();

            }

            myConnection.Close(); //Обязательно закрываем соединение!
        }

        public static void DeleteBaseBattleData(int AccountID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            string sql;
            sql = "DELETE FROM players_battle WHERE ShipID = " + AccountID.ToString();
            MySqlScript script = new MySqlScript(myConnection, sql);
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
                Console.WriteLine(sql);
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine(sqlEx);
                myConnection.Close();
                
            }
            
            myConnection.Close();
        }

        public static void SetPlayerBattleAction(int AccountID, string TargetX, string TargetY, string Action, string ClanID)
        {
            if (IsPlayerBattleActionExists(AccountID) == true)
            {
                DeleteBaseBattleData(AccountID);
                AddBaseBattleData(AccountID, Action, TargetX.ToString(), TargetY.ToString(), ClanID);
            }
            else AddBaseBattleData(AccountID, Action, TargetX.ToString(), TargetY.ToString(), ClanID);
        }

        

        public static string CraftItemToPlayer(int AccountID, int RecipeID, int Count)
        {
            Console.WriteLine("RecipeID = " + RecipeID);
            Console.WriteLine("Count = " + Count);
            Recipes.GetAllInfo(Convert.ToInt32(RecipeID), out string RecipeName, out string RecipePrice, out string RecipeCraftedItemID, out string RecipeCraftedItemPower, out string RecipeMaterial1, out string RecipeMaterial2, out string RecipeMaterial3, out string RecipeMaterial4, out string RecipeMaterial5, out string RecipeMaterial6, out string RecipeMaterial1_Count, out string RecipeMaterial2_Count, out string RecipeMaterial3_Count, out string RecipeMaterial4_Count, out string RecipeMaterial5_Count, out string RecipeMaterial6_Count, out string RecipeCraftPriceMoney, out string RecipeCraftPriceEnergy);
            Console.WriteLine("RecipeMaterial1 = " + RecipeMaterial1);
            Console.WriteLine("RecipeMaterial1_Count = " + RecipeMaterial1_Count);
            int NotNeededMaterial = 0; // Если равен 6 - выдаем ошибку пустого рецепта.
            //bool isPlayerHaveMaterials = true;
            List<CraftMaterials> MatList = new List<CraftMaterials>
            {

            };
            int EnergyToCraft = 0; // Стоимость энергии за крафт
            int MoneyToCraft = 0; // Стоимость в деньгах за крафт
            #region Расчет энергии за крафт
            // Расчет энергии за крафт
            string ShipEnergy = Ships.Ship.GetSingleData(AccountID, "ShipEnergy");
            if (Convert.ToInt32(ShipEnergy) >= Convert.ToInt32(RecipeCraftPriceEnergy) * Count)
            {
                EnergyToCraft = Convert.ToInt32(RecipeCraftPriceEnergy) * Count;
            }
            else
            {
                return "<b>Не хватает ⚡️ Энергии для создания предмета!</b>";
            }
            ///////
            #endregion
            #region Расчет денег за крафт
            // Расчет денег за крафт
            string PlayerMoney = Account.GetSingleData(AccountID, "Money");
            if (Convert.ToInt32(PlayerMoney) >= Convert.ToInt32(RecipeCraftPriceMoney) * Count)
            {
                MoneyToCraft = Convert.ToInt32(RecipeCraftPriceMoney) * Count;
            }
            else
            {
                return "<b>Не хватает 💳 Денег для создания предмета!</b>";
            }
            #endregion

            // Проверка материалов и создание листа для уничтожения материалов
            #region Проверка Материала №1
            if (RecipeMaterial1 != "")
            {
                if (Convert.ToInt32(RecipeMaterial1) > 0)
                {
                    if (Account.isHasItem(AccountID, Convert.ToInt32(RecipeMaterial1), Convert.ToInt32(RecipeMaterial1_Count) * Count) == true)
                    {
                        MatList.Add(new CraftMaterials { MaterialsID = Convert.ToInt32(RecipeMaterial1), MaterialsCount = (Convert.ToInt32(RecipeMaterial1_Count) * Count) });
                    }
                    else
                    {
                        // Нехватает материалов для крафта
                        return "<b>Не хватает 📦 Материалов для создания предмета!</b>";
                    }
                }
                else
                {
                    // Материал не требуется и равен 0
                    NotNeededMaterial++;
                }
            }
            else
            {
                return "Ошибка! Сбой в базе чертежей. ID чертежа: " + RecipeID;
            }
            #endregion
            #region Проверка Материала №2
            if (RecipeMaterial2 != "")
            {
                if (Convert.ToInt32(RecipeMaterial2) > 0)
                {
                    if (Account.isHasItem(AccountID, Convert.ToInt32(RecipeMaterial2), Convert.ToInt32(RecipeMaterial2_Count) * Count) == true)
                    {
                        MatList.Add(new CraftMaterials { MaterialsID = Convert.ToInt32(RecipeMaterial2), MaterialsCount = (Convert.ToInt32(RecipeMaterial2_Count) * Count) });
                    }
                    else
                    {
                        // Нехватает материалов для крафта
                        return "<b>Не хватает 📦 Материалов для создания предмета!</b>";
                    }
                }
                else
                {
                    // Материал не требуется и равен 0
                    NotNeededMaterial++;
                }
            }
            else
            {
                return "Ошибка! Сбой в базе чертежей. ID чертежа: " + RecipeID;
            }
            #endregion
            #region Проверка Материала №3
            if (RecipeMaterial3 != "")
            {
                if (Convert.ToInt32(RecipeMaterial3) > 0)
                {
                    if (Account.isHasItem(AccountID, Convert.ToInt32(RecipeMaterial3), Convert.ToInt32(RecipeMaterial3_Count) * Count) == true)
                    {
                        MatList.Add(new CraftMaterials { MaterialsID = Convert.ToInt32(RecipeMaterial3), MaterialsCount = (Convert.ToInt32(RecipeMaterial3_Count) * Count) });
                    }
                    else
                    {
                        // Нехватает материалов для крафта
                        return "<b>Не хватает 📦 Материалов для создания предмета!</b>";
                    }
                }
                else
                {
                    // Материал не требуется и равен 0
                    NotNeededMaterial++;
                }
            }
            else
            {
                return "Ошибка! Сбой в базе чертежей. ID чертежа: " + RecipeID;
            }
            #endregion
            #region Проверка Материала №4
            if (RecipeMaterial4 != "")
            {
                if (Convert.ToInt32(RecipeMaterial4) > 0)
                {
                    if (Account.isHasItem(AccountID, Convert.ToInt32(RecipeMaterial4), Convert.ToInt32(RecipeMaterial4_Count) * Count) == true)
                    {
                        MatList.Add(new CraftMaterials { MaterialsID = Convert.ToInt32(RecipeMaterial4), MaterialsCount = (Convert.ToInt32(RecipeMaterial4_Count) * Count) });
                    }
                    else
                    {
                        // Нехватает материалов для крафта
                        return "<b>Не хватает 📦 Материалов для создания предмета!</b>";
                    }
                }
                else
                {
                    // Материал не требуется и равен 0
                    NotNeededMaterial++;
                }
            }
            else
            {
                return "Ошибка! Сбой в базе чертежей. ID чертежа: " + RecipeID;
            }
            #endregion
            #region Проверка Материала №5
            if (RecipeMaterial5 != "")
            {
                if (Convert.ToInt32(RecipeMaterial5) > 0)
                {
                    if (Account.isHasItem(AccountID, Convert.ToInt32(RecipeMaterial5), Convert.ToInt32(RecipeMaterial5_Count) * Count) == true)
                    {
                        MatList.Add(new CraftMaterials { MaterialsID = Convert.ToInt32(RecipeMaterial5), MaterialsCount = (Convert.ToInt32(RecipeMaterial5_Count) * Count) });
                    }
                    else
                    {
                        // Нехватает материалов для крафта
                        return "<b>Не хватает 📦 Материалов для создания предмета!</b>";
                    }
                }
                else
                {
                    // Материал не требуется и равен 0
                    NotNeededMaterial++;
                }
            }
            else
            {
                return "Ошибка! Сбой в базе чертежей. ID чертежа: " + RecipeID;
            }
            #endregion
            #region Проверка Материала №6
            if (RecipeMaterial6 != "")
            {
                if (Convert.ToInt32(RecipeMaterial6) > 0)
                {
                    if (Account.isHasItem(AccountID, Convert.ToInt32(RecipeMaterial6), Convert.ToInt32(RecipeMaterial6_Count) * Count) == true)
                    {
                        MatList.Add(new CraftMaterials { MaterialsID = Convert.ToInt32(RecipeMaterial6), MaterialsCount = (Convert.ToInt32(RecipeMaterial6_Count) * Count) });
                    }
                    else
                    {
                        // Нехватает материалов для крафта
                        return "<b>Не хватает 📦 Материалов для создания предмета!</b>";
                    }
                }
                else
                {
                    // Материал не требуется и равен 0
                    NotNeededMaterial++;
                }
            }
            else
            {
                return "Ошибка! Сбой в базе чертежей. ID чертежа: " + RecipeID;
            }
            #endregion

            foreach (var Material in MatList)
            {
                Console.WriteLine("ID is {0} and count is {1}", Material.MaterialsID, Material.MaterialsCount);
            }

            // Если все значения Materials пустые
            if (NotNeededMaterial == 6)
            {
                return "<b>Ошибка! Откуда у вас этот чертеж? Обратитесь к администрации за помощью.</b>";
            }
            #region Отбираем предметы и создаем крафт
            //Забираем предметы и создаем крафт
            Ships.Ship.MinusData(AccountID, "ShipEnergy", RecipeCraftPriceEnergy); // Отнимаем энергию у корабля
            Account.MinusData(AccountID, "Money", RecipeCraftPriceMoney);
            int MaterialToDeleteCount = 0;
            for (var i = 0; i < MatList.Count; i++)
            {
                Console.WriteLine("Материал " + i + ": " + MatList[i].MaterialsID.ToString() + " в размере " + MatList[i].MaterialsCount.ToString());
                bool Deleted = Account.DeleteItem(AccountID, MatList[i].MaterialsID.ToString(), MatList[i].MaterialsCount.ToString());
                if (Deleted == true)
                {
                    MaterialToDeleteCount++;
                }
            }
            if (MatList.Count == MaterialToDeleteCount)
            {
                Account.GiveItem(AccountID, RecipeCraftedItemID, Count.ToString());
                string CraftedItemName = Inventory.Items.Item.GetSingleData(Convert.ToInt32(RecipeCraftedItemID), "Name");
                return "<b>Создан предмет:</b>\n📦 " + CraftedItemName + " (" + Count.ToString() + ")";
            }
            else
            {
                return "Ошибка 478278!";
            }
            #endregion
        }

        

        public static bool AddPlayerReportSingleData(int AccountID, string DataBlockName, string NewParameter)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;
            sql = "UPDATE reports SET " + DataBlockName + " =" + @"""" + NewParameter + @"""" + " WHERE AccountID = " + AccountID.ToString() + ";";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "- [AddPlayerReportSingleData] Запрос к базе репортов для аккаунта: " + AccountID.ToString() + " datablock " + DataBlockName + " query - " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();
                
                Finished = false;
                return Finished;
            }
            
            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static void PlayerBetweenEventsSetQuery(int AccountID, int EventID, DateTime StartTime, DateTime EndTime)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            string varquery = AccountID.ToString() + ", " + @"""" + EventID + @"""" + ", " + @"""" + StartTime + @"""" + ", " + @"""" + EndTime + @"""";
            sql = "INSERT INTO on_between_events (AccountID, EventID, StartTime, EndTime) VALUES (" + varquery + "); ";
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - PlayerBetweenEventsSetQuery] Создание ивента для ID: " + AccountID.ToString());
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - PlayerBetweenEventsSetQuery] Запрос: " + sql);
            script = new MySqlScript(myConnection, sql);
            script.Query = sql;
            script.Connection = myConnection;
            script.Execute();
            
            myConnection.Close(); //Обязательно закрываем соединение!
        }

        public static bool AddReportData(int AccountID, int TargetX, int TargetY, string Result, string RewardMoney, string RewardItems, string RewardExp)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;
            string report_query = AccountID.ToString() + ", " + @"""" + TargetX.ToString() + @"""" + ", " + @"""" + TargetY.ToString() + @"""" + ", " + @"""" + Result + @"""" + ", " + @"""" + RewardMoney + @"""" + ", " + @"""" + RewardItems + @"""" + ", " + @"""" + RewardExp + @"""";
            sql = "INSERT INTO reports (AccountID, TargetX, TargetY, Result, RewardMoney, RewardItems, RewardExp) VALUES (" + report_query + "); ";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - [AddReportData] Запрос: " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();
                
                Finished = false;
                return Finished;
            }
            
            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static bool DeleteAllReportData()
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;
            sql = "DELETE FROM reports";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - [DeleteAllReportData] Запрос: " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();
                
                Finished = false;
                return Finished;
            }
            
            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static bool AddShipModule(int ShipID, string ModuleID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;
            string report_query = ShipID.ToString() + ", " + @"""" + ModuleID + @"""";
            sql = "INSERT INTO ships_modules (ShipID, ModuleID) VALUES (" + report_query + ");";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - [AddShipModule] Запрос: " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();
                
                Finished = false;
                return Finished;
            }
            
            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static void DeleteShipModule(int ID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            string sql;
            sql = "DELETE FROM ships_modules WHERE ID = " + ID.ToString();
            MySqlScript script = new MySqlScript(myConnection, sql);
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
                Console.WriteLine(sql);
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine(sqlEx);
                myConnection.Close();
                
            }
            
            myConnection.Close();
        }

        public static bool AddBaseBattleData(int ShipID, string Action, string X, string Y, string ClanID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;
            string ReturnTextAction = string.Empty;
            string ReturnBaseName = string.Empty;
            string report_query = ShipID.ToString() + ", " + @"""" + Action + @"""" + ", " + @"""" + X + @"""" + ", " + @"""" + Y + @"""" + ", " + @"""" + ClanID + @"""";
            sql = "INSERT INTO players_battle (ShipID, Action, X, Y, ClanID) VALUES (" + report_query + "); ";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - [AddBaseBattleData] Запрос: " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();
                
                Finished = false;
                return Finished;
            }
            if (Action == "Attack")
            {
                Message.Send(ShipID, "Вы приготовились к атаке. Любое перемещение или действие отменит ваше решение.", 1);
                Account.SetSingleData(ShipID, "CurrentAction", "15");
            }
            else if (Action == "Def")
            {
                Message.Send(ShipID, "Вы приготовились к защите. Любое перемещение или действие отменит ваше решение.", 1);
                Account.SetSingleData(ShipID, "CurrentAction", "10");
            }
            

            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static void PlayerEventSetQuery(int AccountID, int EventID, DateTime StartTime, DateTime EndTime, string TechID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            string varquery = AccountID.ToString() + ", " + @"""" + EventID + @"""" + ", " + @"""" + StartTime + @"""" + ", " + @"""" + EndTime + @"""" + ", " + @"""" + TechID + @"""";
            sql = "INSERT INTO on_event (AccountID, EventID, StartTime, EndTime, Tech) VALUES (" + varquery + "); ";
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - PlayerEventSetQuery] Создание ивента для ID: " + AccountID.ToString());
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - PlayerEventSetQuery] Запрос: " + sql);
            script = new MySqlScript(myConnection, sql);
            script.Query = sql;
            script.Connection = myConnection;
            script.Execute();
            
            myConnection.Close(); //Обязательно закрываем соединение!
        }

        public static int ProcentResultCount(int From, int Procent)
        {
            float Result = From * Procent / 100;
            return (int)Result;
        }

        public static void MapMoveSetQuery(int AccountID, int EventID, DateTime StartTime, DateTime EndTime)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            string varquery = AccountID.ToString() + ", " + @"""" + EventID + @"""" + ", " + @"""" + StartTime + @"""" + ", " + @"""" + EndTime + @"""";
            sql = "INSERT INTO on_move_event (AccountID, EventID, StartTime, EndTime) VALUES (" + varquery + "); ";
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - Account.MapMoveSetQuery] Создание ивента для ID: " + AccountID.ToString());
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - Account.MapMoveSetQuery] Запрос: " + sql);
            script = new MySqlScript(myConnection, sql);
            script.Query = sql;
            script.Connection = myConnection;
            script.Execute();
            
            myConnection.Close(); //Обязательно закрываем соединение!
        }

        public static void PlanetMapMoveSetQuery(int AccountID, int EventID, DateTime StartTime, DateTime EndTime)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            string varquery = AccountID.ToString() + ", " + @"""" + EventID + @"""" + ", " + @"""" + StartTime + @"""" + ", " + @"""" + EndTime + @"""";
            sql = "INSERT INTO on_planet_move_event (AccountID, EventID, StartTime, EndTime) VALUES (" + varquery + "); ";
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();

            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - Account.PlanetMapMoveSetQuery] Создание ивента для ID: " + AccountID.ToString());
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - Account.PlanetMapMoveSetQuery] Запрос: " + sql);
            script = new MySqlScript(myConnection, sql);
            script.Query = sql;
            script.Connection = myConnection;
            script.Execute();

            myConnection.Close(); //Обязательно закрываем соединение!
        }

        public static int OverweightCalculation(int PlayerID)
        {
            string ShipCargoMax = Ships.Ship.GetSingleData(PlayerID, "ShipCargoMax");
            string OverweightStringWarning = string.Empty;

            int BonusEnergy = 0;
            int ShipCargoMaximum = Convert.ToInt32(ShipCargoMax);
            int CurrentItemsCount = InventoryCalculation(PlayerID);
            int CurrentOverweight = 0;

            int OverweightProc25 = ProcentResultCount(ShipCargoMaximum, 25);
            int OverweightProc50 = ProcentResultCount(ShipCargoMaximum, 50);
            int OverweightProc100 = ProcentResultCount(ShipCargoMaximum, 100);
            int OverweightProc150 = ProcentResultCount(ShipCargoMaximum, 150);
            int OverweightProc200 = ProcentResultCount(ShipCargoMaximum, 200);

            if (ShipCargoMaximum < CurrentItemsCount)
            {
                OverweightStringWarning = "<b>Превышен максимальный вес груза!</b>\nРасход энергии на перемещение увеличен!\n";
                CurrentOverweight = CurrentItemsCount - ShipCargoMaximum;
                if (OverweightProc25 <= CurrentOverweight)
                {
                    BonusEnergy = 1;
                    OverweightStringWarning = "<b>Превышен максимальный вес груза!</b>\nРасход энергии на перемещение увеличен до ⚡️" + (BonusEnergy + 1) + " ед.\n";
                    Ships.Ship.SetSingleData(PlayerID, "ShipEnergyOverweight", BonusEnergy.ToString());
                }
                if (OverweightProc50 <= CurrentOverweight)
                {
                    BonusEnergy = 2;
                    OverweightStringWarning = "<b>Превышен максимальный вес груза!</b>\nРасход энергии на перемещение увеличен до ⚡️" + (BonusEnergy + 1) + " ед.\n";
                    Ships.Ship.SetSingleData(PlayerID, "ShipEnergyOverweight", BonusEnergy.ToString());
                }
                if (OverweightProc100 <= CurrentOverweight)
                {
                    BonusEnergy = 3;
                    OverweightStringWarning = "<b>Превышен максимальный вес груза!</b>\nРасход энергии на перемещение увеличен до ⚡️" + (BonusEnergy + 1) + " ед.\n";
                    Ships.Ship.SetSingleData(PlayerID, "ShipEnergyOverweight", BonusEnergy.ToString());
                }
                if (OverweightProc150 <= CurrentOverweight)
                {
                    BonusEnergy = 4;
                    OverweightStringWarning = "<b>Превышен максимальный вес груза!</b>\nРасход энергии на перемещение увеличен до ⚡️" + (BonusEnergy + 1) + " ед.\n";
                    Ships.Ship.SetSingleData(PlayerID, "ShipEnergyOverweight", BonusEnergy.ToString());
                }
                if (OverweightProc200 <= CurrentOverweight)
                {
                    BonusEnergy = 5;
                    OverweightStringWarning = "<b>Превышен максимальный вес груза!</b>\nРасход энергии на перемещение увеличен до ⚡️" + (BonusEnergy + 1) + " ед.\n";
                    Ships.Ship.SetSingleData(PlayerID, "ShipEnergyOverweight", BonusEnergy.ToString());
                }
            }
            return BonusEnergy;
        }

        public static int InventoryCalculation(int AccountID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ItemCount FROM inventory WHERE AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            int ItemsCountSum = 0;
            string GetItemsCount = string.Empty;

            while (thisReader.Read())
            {
                GetItemsCount = string.Empty;
                GetItemsCount += thisReader["ItemCount"];
                Console.WriteLine("InventoryCalculation: " + GetItemsCount);
                ItemsCountSum += Convert.ToInt32(GetItemsCount);
            }
            thisReader.Close();
            myConnection.Close();

            return ItemsCountSum;
        }

        public static int ModulesCalculation(int ShipID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ModuleID FROM ships_modules WHERE ShipID = @ShipID", myConnection);
            sql_commmand.Parameters.AddWithValue("@ShipID", ShipID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            int ModulesCountSum = 0;
            string ModuleID = string.Empty;

            while (thisReader.Read())
            {
                ModuleID = string.Empty;
                ModuleID += thisReader["ModuleID"];
                Console.WriteLine("ModulesCalculation: " + ModuleID);
                ModulesCountSum += 1;
            }
            thisReader.Close();
            myConnection.Close();

            return ModulesCountSum;
        }

        public static void ShipModulesStatsCalculation(int ShipID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID, ModuleID FROM ships_modules WHERE ShipID = @ShipID", myConnection);
            sql_commmand.Parameters.AddWithValue("@ShipID", ShipID);
            Console.WriteLine("Запрос модулей от ID: " + ShipID + " запрос - " + sql_commmand.CommandText);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            string ItemName = string.Empty;
            string ModulesTempString = string.Empty;
            string ModulesString = string.Empty;
            string ShipType = Ships.Ship.GetSingleData(ShipID, "ShipType");

            int ShipBaseDef = BaseShipList[Convert.ToInt32(ShipType)].Def;
            int ShipBaseAttack = BaseShipList[Convert.ToInt32(ShipType)].Attack;
            int ShipBaseSpeed = BaseShipList[Convert.ToInt32(ShipType)].Speed;
            int ShipBaseManeuv = BaseShipList[Convert.ToInt32(ShipType)].Maneuv;
            int ShipBaseEnergy = BaseShipList[Convert.ToInt32(ShipType)].Energy;

            while (thisReader.Read())
            {
                string ID = string.Empty;
                string ModuleID = string.Empty;
                ID += thisReader["ID"];
                ModuleID += thisReader["ModuleID"];
                // Agility - определяется как Speed
                // Luck - определяется как Maneuve
                // Intelligence - определяется как Energy
                Inventory.Items.Item.GetAllInfo(Convert.ToInt32(ModuleID), out string Name, out string Usable, out string Wearable, out string WearSlot, out string Price, out string Type, out string Strength, out string Charisma, out string Intelligence, out string Agility, out string Luck, out string Attack, out string Def, out string ItemPower, out string ItemText);
                ShipBaseDef = ShipBaseDef + Convert.ToInt32(Def);
                ShipBaseAttack = ShipBaseAttack + Convert.ToInt32(Attack);
                ShipBaseSpeed = ShipBaseSpeed + Convert.ToInt32(Agility);
                ShipBaseManeuv = ShipBaseManeuv + Convert.ToInt32(Luck);
                ShipBaseEnergy = ShipBaseEnergy + Convert.ToInt32(Intelligence);
            }
            Ships.Ship.SetSingleData(ShipID, "ShipDef", ShipBaseDef.ToString());
            Ships.Ship.SetSingleData(ShipID, "ShipAttack", ShipBaseAttack.ToString());
            Ships.Ship.SetSingleData(ShipID, "ShipSpeed", ShipBaseSpeed.ToString());
            Ships.Ship.SetSingleData(ShipID, "ShipManeuv", ShipBaseManeuv.ToString());
            Ships.Ship.SetSingleData(ShipID, "ShipMaxEnergy", ShipBaseEnergy.ToString());
            thisReader.Close();
            Console.WriteLine("---------------------------");
            myConnection.Close();
            
        }

        public static void NPCStatsCalculation(int AccountID, int NPCID) // Приём на работу
        {
            string HireString = string.Empty;
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM npc_data WHERE FounderID = " + AccountID + " AND Hired = 1 AND ID = " + NPCID, myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            string NPCList = string.Empty;

            while (thisReader.Read())
            {
                string TempString = string.Empty;
                string ID = string.Empty;
                ID += thisReader["ID"];
                // Расчет увеличения характеристик
                NPC.GetAllInfoByNPCID(Convert.ToInt32(ID), out string ReturnFirstName, out string ReturnLastName, out string ReturnUpgradeFocus1, out string ReturnUpgradeFocus2,
                    out string ReturnStrength, out string ReturnEndurance, out string ReturnCharisma,
                    out string ReturnIntelligence, out string ReturnAgility, out string ReturnLuck,
                    out string ReturnExp, out string ReturnLevel,
                    out string ReturnApperception, out string ReturnCandor, out string ReturnVivacity,
                    out string ReturnCoordination, out string ReturnMeekness, out string ReturnHumility,
                    out string ReturnCruelty, out string ReturnSelfpreservation, out string ReturnPatience,
                    out string ReturnDecisiveness, out string ReturnImagination, out string ReturnCuriosity,
                    out string ReturnAggression, out string ReturnLoyalty, out string ReturnEmpathy,
                    out string ReturnTenacity, out string ReturnCourage, out string ReturnSensuality,
                    out string ReturnCharm, out string ReturnHumor, out string ReturnProfession);

                // Пустая - 0
                // Старпом - 1
                // Суперкарго - 2
                // Навигатор - 3
                // Техник-механик - 4 
                // Тактик - 5
                // Стрелок - 6
                // Аналитик - 7 
                // Медик - 8
                // Пилот - 9

                if (ReturnProfession == "1")
                {

                }
                else if (ReturnProfession == "2") // Суперкарго
                {
                    int NewCargo = 0;
                    NewCargo += Convert.ToInt32(ReturnStrength) * 4;
                    NewCargo += Convert.ToInt32(ReturnImagination) * 2;
                    NewCargo += Convert.ToInt32(Convert.ToInt32(ReturnCoordination) * 1.7);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine("Новый найм. Значение: " + Ships.Ship.GetSingleData(AccountID, "ShipCargoMax") + " + " + NewCargo);
                    Console.ResetColor();
                    Ships.Ship.AddData(AccountID, "ShipCargoMax", NewCargo.ToString(), false);
                }
                else if (ReturnProfession == "3") // Навигатор
                {
                    int NewSpeed = 0;
                    NewSpeed += Convert.ToInt32(Convert.ToInt32(ReturnIntelligence) * 1.6);
                    NewSpeed += Convert.ToInt32(Convert.ToInt32(ReturnCoordination) * 1.6);
                    Ships.Ship.AddData(AccountID, "ShipSpeed", NewSpeed.ToString(), false);
                }
                else if (ReturnProfession == "4") // Техник-механик
                {
                    int NewSpeed = 0;
                    int NewShipMaxEnergy = 0;
                    NewSpeed += Convert.ToInt32(Convert.ToInt32(ReturnAgility) * 1.5);
                    NewShipMaxEnergy += Convert.ToInt32(Convert.ToInt32(ReturnIntelligence) * 1.5);
                    Ships.Ship.AddData(AccountID, "ShipSpeed", NewSpeed.ToString(), false);
                    Ships.Ship.AddData(AccountID, "ShipMaxEnergy", NewShipMaxEnergy.ToString(), false);
                }
                else if (ReturnProfession == "5") // Тактик
                {
                    int NewAttack = 0;
                    int NewDef = 0;
                    NewAttack += Convert.ToInt32(Convert.ToInt32(ReturnIntelligence) * 1.5);
                    NewDef += Convert.ToInt32(Convert.ToInt32(ReturnIntelligence) * 1.5);
                    NewAttack += Convert.ToInt32(Convert.ToInt32(ReturnCourage) * 1.7);
                    NewDef += Convert.ToInt32(Convert.ToInt32(ReturnSelfpreservation) * 1.7);
                    Ships.Ship.AddData(AccountID, "ShipAttack", NewAttack.ToString(), false);
                    Ships.Ship.AddData(AccountID, "ShipDef", NewDef.ToString(), false);
                }
                else if (ReturnProfession == "6") // Стрелок
                {
                    int NewAttack = 0;
                    NewAttack += Convert.ToInt32(Convert.ToInt32(ReturnAgility) * 2);
                    if (Convert.ToInt32(ReturnAggression) > Convert.ToInt32(ReturnSelfpreservation))
                    {
                        NewAttack += Convert.ToInt32(Convert.ToInt32(ReturnAggression) * 1.7);
                    }
                    else if (Convert.ToInt32(ReturnAggression) < Convert.ToInt32(ReturnSelfpreservation))
                    {
                        NewAttack -= Convert.ToInt32(Convert.ToInt32(ReturnSelfpreservation) * 1.7);
                    }
                    Ships.Ship.AddData(AccountID, "ShipAttack", NewAttack.ToString(), false);
                }
                else if (ReturnProfession == "7") // Аналитик
                {

                }
                else if (ReturnProfession == "8") // Медик
                {

                }
                else if (ReturnProfession == "9") // Пилот
                {
                    int NewSpeed = 0;
                    NewSpeed += Convert.ToInt32(Convert.ToInt32(ReturnCoordination) * 1.7);
                    NewSpeed += Convert.ToInt32(Convert.ToInt32(ReturnAgility) * 1.8);
                    Ships.Ship.AddData(AccountID, "ShipSpeed", NewSpeed.ToString(), false);
                }
            }
            thisReader.Close();
            myConnection.Close();
        }

        public static void NPCStatsCalculation(int AccountID, int NPCID, int Debug) // Увольнение
        {
            string HireString = string.Empty;
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM npc_data WHERE FounderID = " + AccountID + " AND Hired = 0 AND ID = " + NPCID.ToString(), myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            string NPCList = string.Empty;

            while (thisReader.Read())
            {
                string TempString = string.Empty;
                string ID = string.Empty;
                ID += thisReader["ID"];
                // Расчет увеличения характеристик
                NPC.GetAllInfoByNPCID(Convert.ToInt32(ID), out string ReturnFirstName, out string ReturnLastName, out string ReturnUpgradeFocus1, out string ReturnUpgradeFocus2,
                    out string ReturnStrength, out string ReturnEndurance, out string ReturnCharisma,
                    out string ReturnIntelligence, out string ReturnAgility, out string ReturnLuck,
                    out string ReturnExp, out string ReturnLevel,
                    out string ReturnApperception, out string ReturnCandor, out string ReturnVivacity,
                    out string ReturnCoordination, out string ReturnMeekness, out string ReturnHumility,
                    out string ReturnCruelty, out string ReturnSelfpreservation, out string ReturnPatience,
                    out string ReturnDecisiveness, out string ReturnImagination, out string ReturnCuriosity,
                    out string ReturnAggression, out string ReturnLoyalty, out string ReturnEmpathy,
                    out string ReturnTenacity, out string ReturnCourage, out string ReturnSensuality,
                    out string ReturnCharm, out string ReturnHumor, out string ReturnProfession);

                // Пустая - 0
                // Старпом - 1
                // Суперкарго - 2
                // Навигатор - 3
                // Техник-механик - 4 
                // Тактик - 5
                // Стрелок - 6
                // Аналитик - 7 
                // Медик - 8
                // Пилот - 9

                if (ReturnProfession == "1")
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("Debug 1");
                    Console.ResetColor();
                }
                else if (ReturnProfession == "2") // Суперкарго
                {
                    int NewCargo = 0;
                    NewCargo += Convert.ToInt32(ReturnStrength) * 4;
                    NewCargo += Convert.ToInt32(ReturnImagination) * 2;
                    NewCargo += Convert.ToInt32(Convert.ToInt32(ReturnCoordination) * 1.7);
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("Увольнение: " + Ships.Ship.GetSingleData(AccountID, "ShipCargoMax") + " - " + NewCargo);
                    Console.ResetColor();
                    Ships.Ship.AddData(AccountID, "ShipCargoMax", NewCargo.ToString(), true);
                }
                else if (ReturnProfession == "3") // Навигатор
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("Debug 3");
                    Console.ResetColor();
                    int NewSpeed = 0;
                    NewSpeed += Convert.ToInt32(ReturnIntelligence) * 2;
                    NewSpeed += Convert.ToInt32(Convert.ToInt32(ReturnCoordination) * 1.6);
                    Ships.Ship.AddData(AccountID, "ShipSpeed", NewSpeed.ToString(), true);
                }
                else if (ReturnProfession == "4") // Техник-механик
                {
                    int NewSpeed = 0;
                    int NewShipMaxEnergy = 0;
                    NewSpeed += Convert.ToInt32(Convert.ToInt32(ReturnAgility) * 1.5);
                    NewShipMaxEnergy += Convert.ToInt32(Convert.ToInt32(ReturnIntelligence) * 1.5);
                    Ships.Ship.AddData(AccountID, "ShipSpeed", NewSpeed.ToString(), true);
                    Ships.Ship.AddData(AccountID, "ShipMaxEnergy", NewShipMaxEnergy.ToString(), true);
                }
                else if (ReturnProfession == "5") // Тактик
                {
                    int NewAttack = 0;
                    int NewDef = 0;
                    NewAttack += Convert.ToInt32(Convert.ToInt32(ReturnIntelligence) * 1.5);
                    NewDef += Convert.ToInt32(Convert.ToInt32(ReturnIntelligence) * 1.5);
                    NewAttack += Convert.ToInt32(Convert.ToInt32(ReturnCourage) * 1.7);
                    NewDef += Convert.ToInt32(Convert.ToInt32(ReturnSelfpreservation) * 1.7);
                    Ships.Ship.AddData(AccountID, "ShipAttack", NewAttack.ToString(), true);
                    Ships.Ship.AddData(AccountID, "ShipDef", NewDef.ToString(), true);
                }
                else if (ReturnProfession == "6") // Стрелок
                {
                    int NewAttack = 0;
                    NewAttack += Convert.ToInt32(Convert.ToInt32(ReturnAgility) * 2);
                    if (Convert.ToInt32(ReturnAggression) > Convert.ToInt32(ReturnSelfpreservation))
                    {
                        NewAttack += Convert.ToInt32(Convert.ToInt32(ReturnAggression) * 1.7);
                    }
                    else if (Convert.ToInt32(ReturnAggression) < Convert.ToInt32(ReturnSelfpreservation))
                    {
                        NewAttack -= Convert.ToInt32(Convert.ToInt32(ReturnSelfpreservation) * 1.7);
                    }
                    Ships.Ship.AddData(AccountID, "ShipAttack", NewAttack.ToString(), true);
                }
                else if (ReturnProfession == "7") // Аналитик
                {

                }
                else if (ReturnProfession == "8") // Медик
                {

                }
                else if (ReturnProfession == "9") // Пилот
                {
                    int NewSpeed = 0;
                    NewSpeed += Convert.ToInt32(Convert.ToInt32(ReturnCoordination) * 1.7);
                    NewSpeed += Convert.ToInt32(Convert.ToInt32(ReturnAgility) * 1.8);
                    Ships.Ship.AddData(AccountID, "ShipSpeed", NewSpeed.ToString(), true);
                }
            }
            thisReader.Close();
            myConnection.Close();
        }

        public static void DefAttackCalculation(int AccountID)
        {
            float NewDef = 1.0f;
            float NewAttack = 1.0f;
            float NewMaxHealth = 100.0f;

            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID, Level, Strength, Endurance, Charisma, Intelligence, Agility, Luck, MainhandSlot, OffhandSlot, HeadSlot, NeckSlot, ChestSlot, PantsSlot, FeetSlot FROM users WHERE AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            int LuckStat = 1; // Переменная получает значение увеличения статы от удачи
            int LuckSelectStat = 0; // Переменная для выбора какой стат увеличивать

            string Level = string.Empty;
            string Strength = string.Empty;
            string Endurance = string.Empty;
            string Charisma = string.Empty;
            string Intelligence = string.Empty;
            string Agility = string.Empty;
            string Luck = string.Empty;
            string MainhandSlot = string.Empty;
            string OffhandSlot = string.Empty;
            string HeadSlot = string.Empty;
            string NeckSlot = string.Empty;
            string ChestSlot = string.Empty;
            string PantsSlot = string.Empty;
            string FeetSlot = string.Empty;

            while (thisReader.Read())
            {

                Level += thisReader["Level"];
                Strength += thisReader["Strength"];
                Endurance += thisReader["Endurance"];
                Charisma += thisReader["Charisma"];
                Intelligence += thisReader["Intelligence"];
                Agility += thisReader["Agility"];
                Luck += thisReader["Luck"];
                MainhandSlot += thisReader["MainhandSlot"];
                OffhandSlot += thisReader["OffhandSlot"];
                HeadSlot += thisReader["HeadSlot"];
                NeckSlot += thisReader["NeckSlot"];
                ChestSlot += thisReader["ChestSlot"];
                PantsSlot += thisReader["PantsSlot"];
                FeetSlot += thisReader["FeetSlot"];
            }
            thisReader.Close();
            myConnection.Close();
            

            if (Convert.ToInt32(MainhandSlot) > 0)
            {
                Inventory.Items.Item.GetAllInfo(Convert.ToInt32(MainhandSlot), out string MainhandName, out string MainhandUsable, out string MainhandWearable, out string MainhandWearSlot, out string MainhandPrice, out string MainhandType, out string MainhandStrength, out string MainhandCharisma, out string MainhandIntelligence, out string MainhandAgility, out string MainhandLuck, out string MainhandAttack, out string MainhandDef, out string ItemPower, out string ItemText);
                NewDef = NewDef + (Convert.ToInt32(MainhandDef));
                NewAttack = NewAttack + (Convert.ToInt32(MainhandAttack));
            }
            if (Convert.ToInt32(OffhandSlot) > 0)
            {
                Inventory.Items.Item.GetAllInfo(Convert.ToInt32(OffhandSlot), out string OffhandName, out string OffhandUsable, out string OffhandWearable, out string OffhandWearSlot, out string OffhandPrice, out string OffhandType, out string OffhandStrength, out string OffhandCharisma, out string OffhandIntelligence, out string OffhandAgility, out string OffhandLuck, out string OffhandAttack, out string OffhandDef, out string ItemPower, out string ItemText);
                NewDef = NewDef + (Convert.ToInt32(OffhandDef));
                NewAttack = NewAttack + (Convert.ToInt32(OffhandAttack));
            }
            if (Convert.ToInt32(HeadSlot) > 0)
            {
                Inventory.Items.Item.GetAllInfo(Convert.ToInt32(HeadSlot), out string HeadName, out string HeadUsable, out string HeadWearable, out string HeadWearSlot, out string HeadPrice, out string HeadType, out string HeadStrength, out string HeadCharisma, out string HeadIntelligence, out string HeadAgility, out string HeadLuck, out string HeadAttack, out string HeadDef, out string ItemPower, out string ItemText);
                NewDef = NewDef + (Convert.ToInt32(HeadDef));
                NewAttack = NewAttack + (Convert.ToInt32(HeadAttack));
            }
            if (Convert.ToInt32(NeckSlot) > 0)
            {
                Inventory.Items.Item.GetAllInfo(Convert.ToInt32(NeckSlot), out string NeckName, out string NeckUsable, out string NeckWearable, out string NeckWearSlot, out string NeckPrice, out string NeckType, out string NeckStrength, out string NeckCharisma, out string NeckIntelligence, out string NeckAgility, out string NeckLuck, out string NeckAttack, out string NeckDef, out string ItemPower, out string ItemText);
                NewDef = NewDef + (Convert.ToInt32(NeckDef));
                NewAttack = NewAttack + (Convert.ToInt32(NeckAttack));
            }
            if (Convert.ToInt32(ChestSlot) > 0)
            {
                Inventory.Items.Item.GetAllInfo(Convert.ToInt32(ChestSlot), out string ChestName, out string ChestUsable, out string ChestWearable, out string ChestWearSlot, out string ChestPrice, out string ChestType, out string ChestStrength, out string ChestCharisma, out string ChestIntelligence, out string ChestAgility, out string ChestLuck, out string ChestAttack, out string ChestDef, out string ItemPower, out string ItemText);
                NewDef = NewDef + (Convert.ToInt32(ChestDef));
                NewAttack = NewAttack + (Convert.ToInt32(ChestAttack));
            }
            if (Convert.ToInt32(PantsSlot) > 0)
            {
                Inventory.Items.Item.GetAllInfo(Convert.ToInt32(PantsSlot), out string PantsName, out string PantsUsable, out string PantsWearable, out string PantsWearSlot, out string PantsPrice, out string PantsType, out string PantsStrength, out string PantsCharisma, out string PantsIntelligence, out string PantsAgility, out string PantsLuck, out string PantsAttack, out string PantsDef, out string ItemPower, out string ItemText);
                NewDef = NewDef + (Convert.ToInt32(PantsDef));
                NewAttack = NewAttack + (Convert.ToInt32(PantsAttack));
            }
            if (Convert.ToInt32(FeetSlot) > 0)
            {
                Inventory.Items.Item.GetAllInfo(Convert.ToInt32(FeetSlot), out string FeetName, out string FeetUsable, out string FeetWearable, out string FeetWearSlot, out string FeetPrice, out string FeetType, out string FeetStrength, out string FeetCharisma, out string FeetIntelligence, out string FeetAgility, out string FeetLuck, out string FeetAttack, out string FeetDef, out string ItemPower, out string ItemText);
                NewDef = NewDef + (Convert.ToInt32(FeetDef));
                NewAttack = NewAttack + (Convert.ToInt32(FeetAttack));
            }

            //// Расчет

            NewDef = NewDef + (Convert.ToInt32(Level) * 1.3f);
            NewAttack = NewAttack + (Convert.ToInt32(Level) * 1.3f);
            //Message.Send(AccountID, "Деф от уровня: " + NewDef + " Атака от уровня: " + NewAttack, 1);

            NewAttack = NewAttack + (Convert.ToInt32(Strength) * 1.3f);
            NewDef = NewDef + (Convert.ToInt32(Strength) * 1.3f);
            NewMaxHealth = NewMaxHealth + (Convert.ToInt32(Endurance) * 3.0f);
            //Message.Send(AccountID, "Здоровье от силы: " + NewMaxHealth + " Атака от силы: " + NewAttack, 1);

            NewDef = NewDef + (Convert.ToInt32(Endurance) * 1.6f);
            NewMaxHealth = NewMaxHealth + (Convert.ToInt32(Endurance) * 10.0f);
            //Message.Send(AccountID, "Здоровье от выносливости: " + NewMaxHealth, 1);

            NewAttack = NewAttack + (Convert.ToInt32(Agility) * 1.6f);
            //Message.Send(AccountID, "Деф от ловкости: " + NewDef + " Атака от ловкости: " + NewAttack);

            string NewDefString = Convert.ToString(NewDef);
            string NewAttackString = Convert.ToString(NewAttack);
            string NewMaxHealthString = Convert.ToString(NewMaxHealth);

            Account.SetSingleData(AccountID, "Def", NewDefString);
            Account.SetSingleData(AccountID, "Attack", NewAttackString);
            Account.SetSingleData(AccountID, "MaxHealth", NewMaxHealthString);

        }

        public static void DefAttackBotCalculationByShip(int ShipID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID, Strength, Endurance, Charisma, Intelligence, Agility, Luck, BotMood FROM ship_bots WHERE ShipID = @ShipID", myConnection);
            sql_commmand.Parameters.AddWithValue("@ShipID", ShipID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            int AllDef = 0;
            int AllAttack = 0;

            float MoodMultiplierA = 1.3f;
            float MoodMultiplierB = 1.6f;

            float ExcellentMoodMultiplierA = 1.3f;
            float ExcellentMoodMultiplierB = 1.6f;
            float GoodMoodMultiplierA = 1.1f;
            float GoodMoodMultiplierB = 1.3f;
            float NormalMoodMultiplierA = 0.9f;
            float NormalMoodMultiplierB = 1.0f;
            float BadMoodMultiplierA = 0.5f;
            float BadMoodMultiplierB = 0.7f;
            float WorstMoodMultiplierA = 0.2f;
            float WorstMoodMultiplierB = 0.4f;

            while (thisReader.Read())
            {
                float NewDef = 1.0f;
                float NewAttack = 1.0f;

                string ID = string.Empty;
                string Strength = string.Empty;
                string Endurance = string.Empty;
                string Charisma = string.Empty;
                string Intelligence = string.Empty;
                string Agility = string.Empty;
                string Luck = string.Empty;
                string BotMood = string.Empty;

                ID += thisReader["ID"];
                Strength += thisReader["Strength"];
                Endurance += thisReader["Endurance"];
                Charisma += thisReader["Charisma"];
                Intelligence += thisReader["Intelligence"];
                Agility += thisReader["Agility"];
                Luck += thisReader["Luck"];
                BotMood += thisReader["BotMood"];

                //// Расчет
                if (Convert.ToInt32(BotMood) == 5)
                {
                    MoodMultiplierA = ExcellentMoodMultiplierA;
                    MoodMultiplierB = ExcellentMoodMultiplierB;
                }
                else if (Convert.ToInt32(BotMood) == 4)
                {
                    MoodMultiplierA = GoodMoodMultiplierA;
                    MoodMultiplierB = GoodMoodMultiplierB;
                }
                else if (Convert.ToInt32(BotMood) == 3)
                {
                    MoodMultiplierA = NormalMoodMultiplierA;
                    MoodMultiplierB = NormalMoodMultiplierB;
                }
                else if (Convert.ToInt32(BotMood) == 2)
                {
                    MoodMultiplierA = BadMoodMultiplierA;
                    MoodMultiplierB = BadMoodMultiplierB;
                }
                else if (Convert.ToInt32(BotMood) == 1)
                {
                    MoodMultiplierA = WorstMoodMultiplierA;
                    MoodMultiplierB = WorstMoodMultiplierB;
                }

                NewAttack = NewAttack + (Convert.ToInt32(Strength) * MoodMultiplierA);
                NewDef = NewDef + (Convert.ToInt32(Strength) * MoodMultiplierA);
                NewDef = NewDef + (Convert.ToInt32(Endurance) * MoodMultiplierB);
                NewAttack = NewAttack + (Convert.ToInt32(Agility) * MoodMultiplierB);

                //// Окончание расчета

                string NewDefString = Convert.ToString(NewDef);
                string NewAttackString = Convert.ToString(NewAttack);

                Console.WriteLine("Calc: " + ID + ": Attack: " + NewAttackString + " | Def: " + NewDefString);

                AllDef = AllDef + Convert.ToInt32(NewDef);
                AllAttack = AllAttack + Convert.ToInt32(NewAttack);

                SetBotSingleData(Convert.ToInt32(ID), "Def", NewDefString);
                SetBotSingleData(Convert.ToInt32(ID), "Attack", NewAttackString);
            }
            Ships.Ship.SetSingleData(ShipID, "Def", Convert.ToString(AllDef));
            Ships.Ship.SetSingleData(ShipID, "Attack", Convert.ToString(AllAttack));

            thisReader.Close();
            myConnection.Close();
            

        }

        #region PlayerSetEvent(int AccountID, int EventID, int EventLongTime) - Функция позволяет создать ивент. Добавьте проверку нового  EventID чтобы добавить
        public static void PlayerSetEvent(int AccountID, int EventID, int EventLongTime, int TechID)
        {

            string PlayerOnEvent = Account.CheckOnEvent(AccountID);
            if (PlayerOnEvent != AccountID.ToString())
            {
                if (IsPlayerBattleActionExists(AccountID) == true)
                {
                    DeleteBaseBattleData(AccountID);
                }
                DateTime StartTime; // Стартовое время запуска ивента
                DateTime EndTime; // Время окончания ивента
                string EndTimeString; // Время окончания в строке
                string StartTimeString; // Стартовое время в строке
                StartTime = DateTime.Now; // Получаем текущее время сервера
                EndTime = DateTime.Now; // Получаем текущее время сервера
                EndTime = EndTime.AddMinutes(EventLongTime); // Добавляем к времени окончания + EventLongTime (длина ивента)
                EndTimeString = EndTime.ToString(); // Смирись
                StartTimeString = DateTime.Now.ToString("HH:mm:ss"); // Переводим стартовое время в строку с временем определенного формата
                Console.WriteLine(StartTimeString); // Выводим в лог

                if (EventID == 0) // REVIVE Event
                {
                    PlayerEventSetQuery(AccountID, EventID, StartTime, EndTime, TechID.ToString());
                    var EventReturnText = @"Вы запросили восстановление структурных связей ДНК. Необходимо подождать ⏱5 минут.";
                    Account.SetSingleData(AccountID, "CurrentAction", "12");
                    Message.Send(AccountID, EventReturnText, 1);
                }

                if (EventID == 1)
                {
                    string CurrentPlayerEnergy = Account.GetSingleData(AccountID, "Energy");
                    if (Convert.ToInt32(CurrentPlayerEnergy) >= 1)
                    {
                        int NewPlayerEnergy = Convert.ToInt32(CurrentPlayerEnergy) - 1;
                        Account.SetSingleData(AccountID, "Energy", Convert.ToString(NewPlayerEnergy));
                        PlayerEventSetQuery(AccountID, EventID, StartTime, EndTime, TechID.ToString());
                        // TechID - В данном случае ID механика
                        NPC.GetAllInfoByNPCID(Convert.ToInt32(TechID), out string ReturnFirstName, out string ReturnLastName, out string ReturnUpgradeFocus1, out string ReturnUpgradeFocus2,
                            out string ReturnStrength, out string ReturnEndurance, out string ReturnCharisma,
                            out string ReturnIntelligence, out string ReturnAgility, out string ReturnLuck,
                            out string ReturnExp, out string ReturnLevel,
                            out string ReturnApperception, out string ReturnCandor, out string ReturnVivacity,
                            out string ReturnCoordination, out string ReturnMeekness, out string ReturnHumility,
                            out string ReturnCruelty, out string ReturnSelfpreservation, out string ReturnPatience,
                            out string ReturnDecisiveness, out string ReturnImagination, out string ReturnCuriosity,
                            out string ReturnAggression, out string ReturnLoyalty, out string ReturnEmpathy,
                            out string ReturnTenacity, out string ReturnCourage, out string ReturnSensuality,
                            out string ReturnCharm, out string ReturnHumor, out string ReturnProfession);
                        var EventReturnText = @"Ваш механик " + ReturnFirstName + " " + ReturnLastName + " приступил к ремонту корабля. Необходимо подождать ⏱5 минут.";
                        Account.SetSingleData(AccountID, "CurrentAction", "11");
                        Message.Send(AccountID, EventReturnText, 1);
                    }
                    else
                    {
                        PlayerDeleteBetweenEvent(AccountID);
                        Message.Send(AccountID, "У вас недостаточно 🔋 Энергии чтобы отдать приказ!", 1);
                    }

                }
                if (EventID == 2)
                {
                    PlayerEventSetQuery(AccountID, EventID, StartTime, EndTime, TechID.ToString());
                    var EventReturnText = @"Вы пошли в 💦Плакательную. Вы проплачете ⏱15 минут.";
                    Message.Send(AccountID, EventReturnText, 1);
                }
                if (EventID == 3)
                {

                    string CurrentPlayerEnergy = Account.GetSingleData(AccountID, "Energy");
                    if (Convert.ToInt32(CurrentPlayerEnergy) >= 1)
                    {
                        if (TechID == 1)
                        {
                            var EventReturnText = @"Вы пошли в лес. Вернетесь через ⏱5 минут.";
                            PlayerEventSetQuery(AccountID, EventID, StartTime, EndTime, TechID.ToString());
                            int NewPlayerEnergy = Convert.ToInt32(CurrentPlayerEnergy) - 1;
                            Account.SetSingleData(AccountID, "Energy", Convert.ToString(NewPlayerEnergy));
                            Message.Send(AccountID, EventReturnText, 1);
                        }
                        if (TechID == 2)
                        {
                            var EventReturnText = @"Вы решили продолжить приключение еще на ⏱5 минут.";
                            PlayerEventSetQuery(AccountID, EventID, StartTime, EndTime, TechID.ToString());
                            int NewPlayerEnergy = Convert.ToInt32(CurrentPlayerEnergy) - 1;
                            Account.SetSingleData(AccountID, "Energy", Convert.ToString(NewPlayerEnergy));
                            Message.Send(AccountID, EventReturnText, 1);
                        }

                    }
                    else
                    {
                        PlayerDeleteBetweenEvent(AccountID);
                        Message.Send(AccountID, "У вас недостаточно энергии!", 1);
                    }

                }
                if (EventID == 4) // Изучение рецептов
                {
                    string CurrentPlayerEnergy = Account.GetSingleData(AccountID, "Energy");
                    if (Convert.ToInt32(CurrentPlayerEnergy) >= 1)
                    {
                        int NewPlayerEnergy = Convert.ToInt32(CurrentPlayerEnergy) - 1;
                        Account.SetSingleData(AccountID, "Energy", Convert.ToString(NewPlayerEnergy));
                        Recipes.GetAllInfo(TechID, out string RecipeName, out string RecipePrice, out string RecipeCraftedItemID, out string RecipeCraftedItemPower, out string RecipeMaterial1, out string RecipeMaterial2, out string RecipeMaterial3, out string RecipeMaterial4, out string RecipeMaterial5, out string RecipeMaterial6, out string RecipeMaterial1_Count, out string RecipeMaterial2_Count, out string RecipeMaterial3_Count, out string RecipeMaterial4_Count, out string RecipeMaterial5_Count, out string RecipeMaterial6_Count, out string RecipeCraftPriceMoney, out string RecipeCraftPriceEnergy);
                        PlayerEventSetQuery(AccountID, EventID, StartTime, EndTime, TechID.ToString());
                        var EventReturnText = @"Вы приступили к изучению рецепта: 🔳 " + RecipeName + "\nПроцесс изучения продлится ⏱ 5 мин.";
                        Message.Send(AccountID, EventReturnText, 1);
                    }
                    else
                    {
                        PlayerDeleteBetweenEvent(AccountID);
                        Message.Send(AccountID, "У вас недостаточно энергии!", 1);
                    }
                }
                if (EventID == 10) // Посадка на планету
                {
                    string CurrentPlayerEnergy = Account.GetSingleData(AccountID, "Energy");
                    if (Convert.ToInt32(CurrentPlayerEnergy) >= 1)
                    {
                        int NewPlayerEnergy = Convert.ToInt32(CurrentPlayerEnergy) - 1;
                        Account.SetSingleData(AccountID, "Energy", Convert.ToString(NewPlayerEnergy));
                        
                        PlayerEventSetQuery(AccountID, EventID, StartTime, EndTime, TechID.ToString());
                        var EventReturnText = @"Вы отдали приказ о посадке на поверхность планеты. Процесс посадки займет ⏱ 5 мин.";
                        Message.Send(AccountID, EventReturnText, 1);
                    }
                    else
                    {
                        PlayerDeleteBetweenEvent(AccountID);
                        Message.Send(AccountID, "У вас недостаточно энергии!", 1);
                    }
                }

            }
            else
            {
                var EventReturnText = @"Вы сейчас заняты и не можете этого сделать.";
                Message.Send(AccountID, EventReturnText, 1);
            }
        }
        #endregion

        #region PlayerMoveSetEvent(int AccountID, int EventID, int EventLongTime) - Функция позволяет создать ивент. Добавьте проверку нового  EventID чтобы добавить
        public static void PlayerMoveSetEvent(int AccountID, int EventID, int EventLongTime, int TechID)
        {
            Console.WriteLine("Started: PlayerMoveSetEvent -  EventID: " + EventID + " | EventLongTime: " + EventLongTime + " | TechID: " + TechID);

            string PlayerOnEvent = Account.CheckOnEvent(AccountID);
            if (PlayerOnEvent != AccountID.ToString())
            {
                DateTime StartTime; // Стартовое время запуска ивента
                DateTime EndTime; // Время окончания ивента
                string EndTimeString; // Время окончания в строке
                string StartTimeString; // Стартовое время в строке
                StartTime = DateTime.Now; // Получаем текущее время сервера
                EndTime = DateTime.Now; // Получаем текущее время сервера
                string SecondSpeed = Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "ShipSpeed"); // Получаем скорость корабля
                double BonusSecondBySpeed = Convert.ToDouble(SecondSpeed) / 2.0f;
                EndTime = EndTime.AddSeconds(EventLongTime - Convert.ToInt32(BonusSecondBySpeed)); // Добавляем к времени окончания + EventLongTime (длина ивента)
                EndTimeString = EndTime.ToString(); // Смирись
                StartTimeString = DateTime.Now.ToString("HH:mm:ss"); // Переводим стартовое время в строку с временем определенного формата
                Console.WriteLine(StartTimeString); // Выводим в лог

                if (EventID == 1) // На север
                {
                    Program.MapMoveSetQuery(AccountID, EventID, StartTime, EndTime);
                    DeleteBaseBattleData(AccountID);
                    Account.SetSingleData(AccountID, "CurrentAction", "1");
                    //Message.SendToAllByShip(ShipID, "Капитан отдал приказ об изменении курса. Идем на север!");
                    // AddShipSingleData(ShipID, "Y", "1");
                }
                if (EventID == 2) // На Юг
                {
                    Program.MapMoveSetQuery(AccountID, EventID, StartTime, EndTime);
                    DeleteBaseBattleData(AccountID);
                    Account.SetSingleData(AccountID, "CurrentAction", "2");
                    //Message.SendToAllByShip(ShipID, "Капитан отдал приказ об изменении курса. Идем на юг!");
                }
                if (EventID == 3) // На запад
                {
                    Program.MapMoveSetQuery(AccountID, EventID, StartTime, EndTime);
                    DeleteBaseBattleData(AccountID);
                    Account.SetSingleData(AccountID, "CurrentAction", "3");
                    //Message.SendToAllByShip(ShipID, "Капитан отдал приказ об изменении курса. Идем на запад!");
                }
                if (EventID == 4) // На восток
                {
                    Program.MapMoveSetQuery(AccountID, EventID, StartTime, EndTime);
                    DeleteBaseBattleData(AccountID);
                    Account.SetSingleData(AccountID, "CurrentAction", "4");
                    //Message.SendToAllByShip(ShipID, "Капитан отдал приказ об изменении курса. Идем на восток!");

                }

            }
            else
            {
                var EventReturnText = @"Вы сейчас заняты и не можете этого сделать.";
                Message.SendToAllByShip(AccountID, EventReturnText);
            }
        }
        #endregion

        public static void PlayerPlanetMoveSetEvent(int AccountID, int EventID, int EventLongTime, int TechID)
        {
            Console.WriteLine("Started: PlayerMoveSetEvent -  EventID: " + EventID + " | EventLongTime: " + EventLongTime + " | TechID: " + TechID);

            string PlayerOnEvent = Account.CheckOnEvent(AccountID);
            if (PlayerOnEvent != AccountID.ToString())
            {
                DateTime StartTime; // Стартовое время запуска ивента
                DateTime EndTime; // Время окончания ивента
                string EndTimeString; // Время окончания в строке
                string StartTimeString; // Стартовое время в строке
                StartTime = DateTime.Now; // Получаем текущее время сервера
                EndTime = DateTime.Now; // Получаем текущее время сервера
                EndTime = EndTime.AddSeconds(EventLongTime); // Добавляем к времени окончания + EventLongTime (длина ивента)
                EndTimeString = EndTime.ToString(); // Смирись
                StartTimeString = DateTime.Now.ToString("HH:mm:ss"); // Переводим стартовое время в строку с временем определенного формата
                Console.WriteLine(StartTimeString); // Выводим в лог

                if (EventID == 1) // На север
                {
                    Program.PlanetMapMoveSetQuery(AccountID, EventID, StartTime, EndTime);
                    DeleteBaseBattleData(AccountID);
                    Account.SetSingleData(AccountID, "CurrentAction", "1");
                }
                if (EventID == 2) // На Юг
                {
                    Program.PlanetMapMoveSetQuery(AccountID, EventID, StartTime, EndTime);
                    DeleteBaseBattleData(AccountID);
                    Account.SetSingleData(AccountID, "CurrentAction", "2");
                }
                if (EventID == 3) // На запад
                {
                    Program.PlanetMapMoveSetQuery(AccountID, EventID, StartTime, EndTime);
                    DeleteBaseBattleData(AccountID);
                    Account.SetSingleData(AccountID, "CurrentAction", "3");
                }
                if (EventID == 4) // На восток
                {
                    Program.PlanetMapMoveSetQuery(AccountID, EventID, StartTime, EndTime);
                    DeleteBaseBattleData(AccountID);
                    Account.SetSingleData(AccountID, "CurrentAction", "4");
                }
            }
            else
            {
                var EventReturnText = @"Вы сейчас заняты и не можете этого сделать.";
                Message.SendToAllByShip(AccountID, EventReturnText);
            }
        }

        public static void PlayerSetBetweenEvent(int AccountID, int EventID, int EventLongTime)
        {
            DateTime StartTime; // Стартовое время запуска ивента
            DateTime EndTime; // Время окончания ивента
            string EndTimeString; // Время окончания в строке
            string StartTimeString; // Стартовое время в строке
            StartTime = DateTime.Now; // Получаем текущее время сервера
            EndTime = DateTime.Now; // Получаем текущее время сервера
            EndTime = EndTime.AddMinutes(EventLongTime); // Добавляем к времени окончания + EventLongTime (длина ивента)
            EndTimeString = EndTime.ToString(); // Смирись
            StartTimeString = DateTime.Now.ToString("HH:mm:ss"); // Переводим стартовое время в строку с временем определенного формата
            Console.WriteLine(StartTimeString); // Выводим в лог

            PlayerBetweenEventsSetQuery(AccountID, EventID, StartTime, EndTime);
            var EventReturnText = @"Вы можете отправиться домой нажав /home прямо сейчас, либо вы автоматически продолжите искать приключений.";
            Message.Send(AccountID, EventReturnText, 1);
        }

        #region PlayerDeleteEvent(int AccountID) - Функция удаления записи об ивенте 
        public static void PlayerDeleteEvent(int AccountID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            string sql;
            sql = "DELETE FROM on_event WHERE AccountID = " + AccountID.ToString();
            MySqlScript script = new MySqlScript(myConnection, sql);
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
                Console.WriteLine(sql);
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine(sqlEx);
                myConnection.Close();
                
            }
            
            myConnection.Close();
        }
        #endregion
 
        #region PlayerDeleteDonate(int ID) - Функция удаления записи об донате 
        public static void PlayerDeleteDonate(int ID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            string sql;
            sql = "DELETE FROM donations WHERE ID = " + ID.ToString();
            MySqlScript script = new MySqlScript(myConnection, sql);
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
                Console.WriteLine(sql);
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine(sqlEx);
                myConnection.Close();
                
            }
            
            myConnection.Close();
        }
        #endregion

        #region PlayerDeleteMoveShipEvent(int AccountID) - Функция удаления записи об ивенте 
        public static void PlayerDeleteMoveShipEvent(int AccountID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            string sql;
            sql = "DELETE FROM on_move_event WHERE AccountID = " + AccountID.ToString();
            MySqlScript script = new MySqlScript(myConnection, sql);
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
                Console.WriteLine(sql);
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine(sqlEx);
                myConnection.Close();
                
            }
            
            myConnection.Close();
        }
        #endregion

        public static void PlayerDeleteMoveEvent(int AccountID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            string sql;
            sql = "DELETE FROM on_planet_move_event WHERE AccountID = " + AccountID.ToString();
            MySqlScript script = new MySqlScript(myConnection, sql);
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
                Console.WriteLine(sql);
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine(sqlEx);
                myConnection.Close();

            }

            myConnection.Close();
        }

        public static void PlayerDeleteBetweenEvent(int AccountID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            string sql;
            sql = "DELETE FROM on_between_events WHERE AccountID = " + AccountID.ToString();
            MySqlScript script = new MySqlScript(myConnection, sql);
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
                Console.WriteLine(sql);
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine(sqlEx);
                myConnection.Close();
                
            }
            
            myConnection.Close();
        }

        #region PlayerEventController(bool Finished) - Контроллер ивентов используется для получения данных из таблицы on_event
        public static bool PlayerEventController(bool Finished)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID, EventID, StartTime, EndTime, Tech FROM on_event", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            while (thisReader.Read())
            {

                string AccountID = string.Empty;
                string StartTime = string.Empty;
                string EndTime = string.Empty;
                string EventID = string.Empty;
                string Tech = string.Empty;

                AccountID += thisReader["AccountID"];
                StartTime += thisReader["StartTime"];
                EndTime += thisReader["EndTime"];
                EventID += thisReader["EventID"];
                Tech += thisReader["Tech"];
                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - EventTimer] For id: " + AccountID + " - from " + StartTime + " to " + EndTime);
                int CompareResult = DateTime.Compare((Convert.ToDateTime(EndTime)), DateTime.Now);
                if (CompareResult <= 0)
                {
                    PlayerDeleteEvent(Convert.ToInt32(AccountID));
                    PlayerSendEventResult(AccountID, EventID, Tech);
                }

            }
            myConnection.Close();
            
            thisReader.Close();
            return Finished;
        }
        #endregion

        public static bool EmissionEventController(bool Finished) // Контроллер выброса
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT EmissionTime FROM global", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            while (thisReader.Read())
            {

                string EmissionTime = string.Empty;
                EmissionTime += thisReader["EmissionTime"];

                int CompareResult = DateTime.Compare((Convert.ToDateTime(EmissionTime)), DateTime.Now);
                if (CompareResult <= 0)
                {
                    int MinutesToAdd = rndmzr.Next(160, 370);
                    SetGlobalSingleData("EmissionTime", DateTime.Now.AddMinutes(MinutesToAdd).ToString());
                    // Здесь убивать всех игроков в зоне выброса
                }

            }
            myConnection.Close();
            
            thisReader.Close();
            return Finished;
        }

        public static void BattleController() // Контроллер битвы
        {
            bool BattleStarted = false;
            BattleTimer.Enabled = false;
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT BattleTime FROM global", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            DateTime newDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 0, 0);
            newDate = newDate.AddDays(1);

            GlobalBattle1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 5, 0, 0);
            GlobalBattle2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 9, 0, 0);
            GlobalBattle3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 13, 0, 0);
            GlobalBattle4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 17, 0, 0);
            GlobalBattle5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 21, 0, 0);
            GlobalBattle6 = newDate;

            while (thisReader.Read())
            {
                string BattleTime = string.Empty;
                BattleTime += thisReader["BattleTime"];

                int CompareResult = DateTime.Compare((Convert.ToDateTime(BattleTime)), DateTime.Now);
                if (CompareResult <= 0)
                {
                    BattleStarted = true;
                    Console.WriteLine("Battle start...");
                    #region Расчет нового времени битвы
                    switch (GetGlobalSingleData("GlobalBattle"))
                    {
                        case "GlobalBattle1":
                            SetGlobalSingleData("BattleTime", GlobalBattle2.ToString());
                            SetGlobalSingleData("GlobalBattle", "GlobalBattle2");
                            break;
                        case "GlobalBattle2":
                            SetGlobalSingleData("BattleTime", GlobalBattle3.ToString());
                            SetGlobalSingleData("GlobalBattle", "GlobalBattle3");
                            break;
                        case "GlobalBattle3":
                            SetGlobalSingleData("BattleTime", GlobalBattle4.ToString());
                            SetGlobalSingleData("GlobalBattle", "GlobalBattle4");
                            break;
                        case "GlobalBattle4":
                            SetGlobalSingleData("BattleTime", GlobalBattle5.ToString());
                            SetGlobalSingleData("GlobalBattle", "GlobalBattle5");
                            break;
                        case "GlobalBattle5":
                            SetGlobalSingleData("BattleTime", GlobalBattle6.ToString());
                            SetGlobalSingleData("GlobalBattle", "GlobalBattle6");
                            break;
                        case "GlobalBattle6":
                            SetGlobalSingleData("BattleTime", GlobalBattle1.ToString());
                            SetGlobalSingleData("GlobalBattle", "GlobalBattle1");
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    BattleStarted = false;

                }
            }
            myConnection.Close();
            thisReader.Close();
            if (BattleStarted == true)
            {
                DeleteAllReportData(); // Удаляем старые данные о репортах
                #endregion
                // Устанавливает маркер OnBattle в таблице Global
                SetGlobalSingleData("OnBattle", "true");
                Console.WriteLine("Marker 1");
                // Здесь расчет битвы
                #region Данные по карте --> Бой
                // Здесь получаем данные о всей карте, координаты для соотношения с данными в players_battle
                MySqlConnection BattleConnection = new MySqlConnection(Connect);
                BattleConnection.Open();
                Console.WriteLine("Marker 2");
                MySqlCommand sql_commmand2 = new MySqlCommand("SELECT X, Y, LocationType FROM map", BattleConnection);
                MySqlDataReader thisReader1 = sql_commmand2.ExecuteReader();
                Console.WriteLine("Marker 3");
                string X = string.Empty;
                string Y = string.Empty;
                string LocationType = string.Empty;

                while (thisReader1.Read())
                {
                    //Console.WriteLine("Reader 1");
                    X = string.Empty;
                    Y = string.Empty;
                    LocationType = string.Empty;

                    X += thisReader1["X"];
                    Y += thisReader1["Y"];
                    LocationType += thisReader1["LocationType"];

                    //////////////////////////
                    if (IsPlayerBattleActionExists(Convert.ToInt32(X), Convert.ToInt32(Y)) == true)
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.WriteLine("Found XY point");
                        Console.ResetColor();
                        #region Если точку кто-то атакует или защищает - расчет
                        MySqlConnection BattlePlayersConnection = new MySqlConnection(Connect);
                        BattlePlayersConnection.Open();
                        sql_commmand = new MySqlCommand("SELECT ShipID, Action, ClanID FROM players_battle WHERE X = " + X.ToString() + " AND Y = " + Y.ToString(), BattlePlayersConnection);
                        MySqlDataReader thisReader2 = sql_commmand.ExecuteReader();

                        string ShipID = string.Empty;
                        string Action = string.Empty;
                        string ClanID = string.Empty;

                        List<int> Attack_ShipIDs = new List<int>() { }; // Список атакующих кораблей
                        List<int> Def_ShipIDs = new List<int>() { }; // Список защищающих кораблей

                        int Def_Ships_Money = 0;
                        int Attack_Ships_Money = 0;

                        double Attack_Sum = 0.0f; // Сумма дефа для игроков на точке
                        double Def_Sum = 0.0f; // Сумма атаки для игроков на точке

                        while (thisReader2.Read())
                        {
                            Console.WriteLine("Reader 2");
                            ShipID = string.Empty;
                            Action = string.Empty;
                            ClanID = string.Empty;

                            ShipID += thisReader2["ShipID"];
                            Action += thisReader2["Action"];
                            ClanID += thisReader2["ClanID"]; // Не используется до введения кланов

                            string ShipDef = string.Empty;
                            string ShipAttack = string.Empty;

                            // Добавляем ИД корабля в списки
                            if (Action == "Attack")
                            {
                                Attack_ShipIDs.Add(Convert.ToInt32(ShipID));
                                ShipAttack = Ships.Ship.GetSingleData(Convert.ToInt32(ShipID), "ShipAttack");
                                Attack_Sum = Attack_Sum + Convert.ToDouble(ShipAttack);
                                Attack_Ships_Money = Attack_Ships_Money + Convert.ToInt32(Account.GetSingleData(Convert.ToInt32(ShipID), "Money"));
                            }
                            else if (Action == "Def")
                            {
                                Def_ShipIDs.Add(Convert.ToInt32(ShipID));
                                ShipDef = Ships.Ship.GetSingleData(Convert.ToInt32(ShipID), "ShipDef");
                                Def_Sum = Def_Sum + Convert.ToDouble(ShipDef);
                                Def_Ships_Money = Def_Ships_Money + Convert.ToInt32(Account.GetSingleData(Convert.ToInt32(ShipID), "Money"));
                            }
                        }
                        Console.WriteLine("After Reader 2");
                        thisReader2.Close();
                        BattlePlayersConnection.Close();
                        // Расчет битвы для точки - финальная стадия
                        int Players_Deffenders = Def_ShipIDs.Count(); // Количество игроков которые защищали точку
                        int Players_Attacks = Attack_ShipIDs.Count(); // Количество игроков которые атаковали точку

                        double Def_Money_Award_Sum = ProcentResultCount(Def_Ships_Money, 10); // Получаем 10% от суммы денег у деферов
                        double Attack_Money_Award_Sum = ProcentResultCount(Attack_Ships_Money, 10); // Получаем 10% от суммы денег у атакеров

                        double Money_Award_For_One_Player_If_Attack_Wins = Def_Money_Award_Sum / Players_Attacks; // Делим 10% от денег деферов на количество атакеров
                        double Money_Award_For_One_Player_If_Def_Wins = Attack_Money_Award_Sum / Players_Deffenders; // Делим 10% от денег атакеров на количество деферов

                        double Difficulty_Multiplier = 0.0f; // Коэффициент сложности боя
                        string Difficulty_Level_String = string.Empty;

                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.WriteLine("Players_Deffenders: " + Players_Deffenders);
                        Console.WriteLine("Players_Attacks: " + Players_Attacks);
                        Console.ResetColor();

                        if (Players_Deffenders == 0 && Players_Attacks == 0) // Никто не атаковал/защищал точку.
                        {
                            Console.WriteLine("No players");
                            // Никто не атаковал/защищал точку. Ничего не делаем.
                        }
                        else if (Players_Attacks > Players_Deffenders) // Атакующих больше защищающихся
                        {
                            Console.WriteLine("Attacker > def");
                            if (Players_Deffenders == 0)
                            {
                                Difficulty_Multiplier = 1.0f;
                            }
                            else
                            {
                                Difficulty_Multiplier = Convert.ToDouble(Players_Attacks) / Convert.ToDouble(Players_Deffenders);
                            }
                            if (Difficulty_Multiplier >= 3.5)
                            {
                                Console.WriteLine("Hard mode");
                                Difficulty_Level_String = "Hard";
                            }
                            else if (Difficulty_Multiplier < 3.5 && Difficulty_Multiplier >= 1.3)
                            {
                                Console.WriteLine("Medium mode");
                                Difficulty_Level_String = "Medium";
                            }
                            else if (Difficulty_Multiplier < 1.3)
                            {
                                Console.WriteLine("Easy mode");
                                Difficulty_Level_String = "Easy";
                            }

                            if (Def_Sum > Attack_Sum) // Очков дефа больше чем очков атаки
                            {
                                Console.WriteLine("Def > Attack");
                                // Деферы выиграли, атакеров при этом больше
                                foreach (int fShipID in Def_ShipIDs)
                                {
                                    Console.WriteLine("BattleAward_Win_" + Difficulty_Level_String);
                                    GiveBattleAward(fShipID, "BattleAward_Win_" + Difficulty_Level_String, Convert.ToInt32(Money_Award_For_One_Player_If_Attack_Wins), Convert.ToInt32(X), Convert.ToInt32(Y));
                                }
                                foreach (int fShipID in Attack_ShipIDs)
                                {
                                    Console.WriteLine("BattleAward_Loss_" + Difficulty_Level_String);
                                    GiveBattleAward(fShipID, "BattleAward_Loss_" + Difficulty_Level_String, Convert.ToInt32(Money_Award_For_One_Player_If_Def_Wins), Convert.ToInt32(X), Convert.ToInt32(Y));
                                }
                            }
                            else if (Attack_Sum > Def_Sum) // Очков атаки больше чем очков дефа
                            {
                                Console.WriteLine("Attack > Def");
                                // Атакеры выиграли, атакеров при этом больше
                                foreach (int fShipID in Def_ShipIDs)
                                {
                                    Console.WriteLine("BattleAward_Loss_" + Difficulty_Level_String);
                                    GiveBattleAward(fShipID, "BattleAward_Loss_" + Difficulty_Level_String, Convert.ToInt32(Money_Award_For_One_Player_If_Def_Wins), Convert.ToInt32(X), Convert.ToInt32(Y));
                                }
                                foreach (int fShipID in Attack_ShipIDs)
                                {
                                    Console.WriteLine("BattleAward_Win_" + Difficulty_Level_String);
                                    GiveBattleAward(fShipID, "BattleAward_Win_" + Difficulty_Level_String, Convert.ToInt32(Money_Award_For_One_Player_If_Attack_Wins), Convert.ToInt32(X), Convert.ToInt32(Y));
                                }
                            }
                            else if (Attack_Sum == Def_Sum) // Очков поровну
                            {
                                Console.WriteLine("Attack = Def");
                                // Атакеры и деферы не смогли побороть друг друга
                                foreach (int fShipID in Def_ShipIDs)
                                {
                                    Console.WriteLine("BattleAward_Draw");
                                    GiveBattleAward(fShipID, "BattleAward_Draw", 0, Convert.ToInt32(X), Convert.ToInt32(Y));
                                }
                                foreach (int fShipID in Attack_ShipIDs)
                                {
                                    Console.WriteLine("BattleAward_Draw");
                                    GiveBattleAward(fShipID, "BattleAward_Draw", 0, Convert.ToInt32(X), Convert.ToInt32(Y));
                                }
                            }
                        }
                        else if (Players_Deffenders > Players_Attacks) // Защищающихся больше атакующих
                        {
                            Console.WriteLine("Players_Deffenders > Players_Attacks");
                            if (Players_Attacks == 0)
                            {
                                Difficulty_Multiplier = 1.0f;
                            }
                            else
                            {
                                Difficulty_Multiplier = Convert.ToDouble(Players_Deffenders) / Convert.ToDouble(Players_Attacks);
                            }
                            Console.WriteLine("Difficulty_Multiplier: " + Difficulty_Multiplier);
                            if (Difficulty_Multiplier >= 3.5)
                            {
                                Console.WriteLine("Hard mode");
                                Difficulty_Level_String = "Hard";
                            }
                            else if (Difficulty_Multiplier < 3.5 && Difficulty_Multiplier >= 1.3)
                            {
                                Console.WriteLine("Medium mode");
                                Difficulty_Level_String = "Medium";
                            }
                            else if (Difficulty_Multiplier < 1.3)
                            {
                                Console.WriteLine("Easy mode");
                                Difficulty_Level_String = "Easy";
                            }

                            if (Def_Sum > Attack_Sum) // Очков дефа больше чем очков атаки
                            {
                                // Деферы выиграли, деферов при этом больше
                                Console.WriteLine("Def wins");
                                foreach (int fShipID in Def_ShipIDs)
                                {
                                    Console.WriteLine("BattleAward_Win_" + Difficulty_Level_String);
                                    GiveBattleAward(fShipID, "BattleAward_Win_" + Difficulty_Level_String, Convert.ToInt32(Money_Award_For_One_Player_If_Def_Wins), Convert.ToInt32(X), Convert.ToInt32(Y));
                                }
                                foreach (int fShipID in Attack_ShipIDs)
                                {
                                    Console.WriteLine("BattleAward_Loss_" + Difficulty_Level_String);
                                    GiveBattleAward(fShipID, "BattleAward_Loss_" + Difficulty_Level_String, Convert.ToInt32(Money_Award_For_One_Player_If_Attack_Wins), Convert.ToInt32(X), Convert.ToInt32(Y));
                                }
                            }
                            else if (Attack_Sum > Def_Sum) // Очков атаки больше чем очков дефа
                            {
                                // Атакеры выиграли, деферов при этом больше
                                Console.WriteLine("Attack wins");
                                foreach (int fShipID in Def_ShipIDs)
                                {
                                    Console.WriteLine("BattleAward_Loss_" + Difficulty_Level_String);
                                    GiveBattleAward(fShipID, "BattleAward_Loss_" + Difficulty_Level_String, Convert.ToInt32(Money_Award_For_One_Player_If_Attack_Wins), Convert.ToInt32(X), Convert.ToInt32(Y));
                                }
                                foreach (int fShipID in Attack_ShipIDs)
                                {
                                    Console.WriteLine("BattleAward_Win_" + Difficulty_Level_String);
                                    GiveBattleAward(fShipID, "BattleAward_Win_" + Difficulty_Level_String, Convert.ToInt32(Money_Award_For_One_Player_If_Def_Wins), Convert.ToInt32(X), Convert.ToInt32(Y));
                                }
                            }
                            else if (Attack_Sum == Def_Sum) // Очков поровну
                            {
                                Console.WriteLine("Draw");
                                // Атакеры и деферы не смогли побороть друг друга
                                foreach (int fShipID in Def_ShipIDs)
                                {
                                    Console.WriteLine("BattleAward_Draw");
                                    GiveBattleAward(fShipID, "BattleAward_Draw", 0, Convert.ToInt32(X), Convert.ToInt32(Y));
                                }
                                foreach (int fShipID in Attack_ShipIDs)
                                {
                                    Console.WriteLine("BattleAward_Draw");
                                    GiveBattleAward(fShipID, "BattleAward_Draw", 0, Convert.ToInt32(X), Convert.ToInt32(Y));
                                }
                            }
                        }
                        Console.WriteLine("All if passed by");
                        #endregion
                    }
                    //////////////////////////
                }
                Console.WriteLine("Getted all map data");
                thisReader1.Close();
                BattleConnection.Close();
                SetGlobalSingleData("OnBattle", "false");
                BattleTimer.Enabled = true;
                BattleStarted = false;
                #endregion
            }
            else BattleTimer.Enabled = true;
        }

        // ДОПИСАТЬ: Создание репорта, вывод репорта по /report
        public static void GiveBattleAward(int ShipID, string Mode, int Money, int X, int Y)
        {
            DeleteBaseBattleData(ShipID);
            Account.SetSingleData(ShipID, "Status", "0");
            int HardExp = rndmzr.Next(140, 171);
            int MediumExp = rndmzr.Next(100, 141);
            int EasyExp = rndmzr.Next(70, 101);
            int DrawExp = rndmzr.Next(10, 41);
            double HardExpLose = HardExp / 2;
            double MediumExpLose = MediumExp / 2;
            double EasyExpLose = EasyExp / 2;
            if (Mode == "BattleAward_Win_Hard") // Победа в сложной битве
            {
                //Message.Send(ShipID, "BattleAward_Win_Hard", 1);
                Console.WriteLine("GiveBattleAward: BattleAward_Win_Hard");
                Account.AddData(ShipID, "Exp", HardExp.ToString());
                Account.SetGold(ShipID, Money, false);
                AddReportData(ShipID, X, Y, Mode, Money.ToString(), "", HardExp.ToString());
            }
            else if (Mode == "BattleAward_Win_Medium") // Победа в средней битве
            {
                //Message.Send(ShipID, "BattleAward_Win_Medium", 1);
                Console.WriteLine("GiveBattleAward: BattleAward_Win_Medium");
                Account.AddData(ShipID, "Exp", MediumExp.ToString());
                Account.SetGold(ShipID, Money, false);
                AddReportData(ShipID, X, Y, Mode, Money.ToString(), "", MediumExp.ToString());
            }
            else if (Mode == "BattleAward_Win_Easy") // Победа в легкой битве
            {
                //Message.Send(ShipID, "BattleAward_Win_Easy", 1);
                Console.WriteLine("GiveBattleAward: BattleAward_Win_Easy");
                Account.AddData(ShipID, "Exp", EasyExp.ToString());
                Account.SetGold(ShipID, Money, false);
                AddReportData(ShipID, X, Y, Mode, Money.ToString(), "", EasyExp.ToString());
            }
            else if (Mode == "BattleAward_Loss_Hard") // Поражение в сложной битве
            {
                //Message.Send(ShipID, "BattleAward_Loss_Hard", 1);
                Console.WriteLine("GiveBattleAward: BattleAward_Loss_Hard");
                Account.AddData(ShipID, "Exp", Convert.ToInt32(HardExpLose).ToString());
                Account.SetGold(ShipID, Money, true);
                AddReportData(ShipID, X, Y, Mode, Money.ToString(), "", Convert.ToInt32(HardExpLose).ToString());
            }
            else if (Mode == "BattleAward_Loss_Medium") // Поражение в средней битве
            {
                //Message.Send(ShipID, "BattleAward_Loss_Medium", 1);
                Console.WriteLine("GiveBattleAward: BattleAward_Loss_Medium");
                Account.AddData(ShipID, "Exp", Convert.ToInt32(MediumExpLose).ToString());
                Account.SetGold(ShipID, Money, true);
                AddReportData(ShipID, X, Y, Mode, Money.ToString(), "", Convert.ToInt32(MediumExpLose).ToString());
            }
            else if (Mode == "BattleAward_Loss_Easy") // Поражение в легкой битве
            {
                //Message.Send(ShipID, "BattleAward_Loss_Easy", 1);
                Console.WriteLine("GiveBattleAward: BattleAward_Loss_Easy");
                Account.AddData(ShipID, "Exp", Convert.ToInt32(EasyExpLose).ToString());
                Account.SetGold(ShipID, Money, true);
                AddReportData(ShipID, X, Y, Mode, Money.ToString(), "", Convert.ToInt32(EasyExpLose).ToString());
            }
            else if (Mode == "BattleAward_Draw") // Битва закончилась ничьей
            {
                // Message.Send(ShipID, "BattleAward_Draw", 1);
                Console.WriteLine("GiveBattleAward: BattleAward_Draw");
                Account.AddData(ShipID, "Exp", DrawExp.ToString());
                AddReportData(ShipID, X, Y, Mode, Money.ToString(), "", DrawExp.ToString());
            }
        }

        public static bool DonationController(bool Finished) // Контроллер доната
        {
            // Console.WriteLine("Donation Controller DEBUG");
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID, AccountID, Amount FROM donations", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            string ID = string.Empty;
            string AccountID = string.Empty;
            string Amount = string.Empty;

            while (thisReader.Read())
            {

                ID = string.Empty;
                AccountID = string.Empty;
                Amount = string.Empty;

                ID += thisReader["ID"];
                AccountID += thisReader["AccountID"];
                Amount += thisReader["Amount"];

                Console.WriteLine("Donate from: " + AccountID.ToString() + " with money count: " + Amount.ToString());

                switch (Amount)
                {

                    case "4.90":
                        Account.AddData(Convert.ToInt32(AccountID), "Credits", "1");
                        Message.Send(Convert.ToInt32(AccountID), "<b>Сообщение на 📱 Коммуникатор:</b>\nПеречисление кристаллов этериума на счет: <b>💳 1 </b>", 1);
                        PlayerDeleteDonate(Convert.ToInt32(ID));
                        break;
                    case "74.62":
                        Account.AddData(Convert.ToInt32(AccountID), "Credits", "30");
                        Message.Send(Convert.ToInt32(AccountID), "<b>Сообщение на 📱 Коммуникатор:</b>\nПеречисление кристаллов этериума на счет: <b>💳 30 </b>", 1);
                        PlayerDeleteDonate(Convert.ToInt32(ID));
                        break;
                    case "73.50":
                        Account.AddData(Convert.ToInt32(AccountID), "Credits", "30");
                        Message.Send(Convert.ToInt32(AccountID), "<b>Сообщение на 📱 Коммуникатор:</b>\nПеречисление кристаллов этериума на счет: <b>💳 30 </b>", 1);
                        PlayerDeleteDonate(Convert.ToInt32(ID));
                        break;
                    case "198.00":
                        Account.AddData(Convert.ToInt32(AccountID), "Credits", "100");
                        Message.Send(Convert.ToInt32(AccountID), "<b>Сообщение на 📱 Коммуникатор:</b>\nПеречисление кристаллов этериума на счет: <b>💳 100 </b>", 1);
                        PlayerDeleteDonate(Convert.ToInt32(ID));
                        break;
                    case "195.02":
                        Account.AddData(Convert.ToInt32(AccountID), "Credits", "100");
                        Message.Send(Convert.ToInt32(AccountID), "<b>Сообщение на 📱 Коммуникатор:</b>\nПеречисление кристаллов этериума на счет: <b>💳 100 </b>", 1);
                        PlayerDeleteDonate(Convert.ToInt32(ID));
                        break;
                    case "347.25":
                        Account.AddData(Convert.ToInt32(AccountID), "Credits", "200");
                        Message.Send(Convert.ToInt32(AccountID), "<b>Сообщение на 📱 Коммуникатор:</b>\nПеречисление кристаллов этериума на счет: <b>💳 200 </b>", 1);
                        PlayerDeleteDonate(Convert.ToInt32(ID));
                        break;
                    case "342.02":
                        Account.AddData(Convert.ToInt32(AccountID), "Credits", "200");
                        Message.Send(Convert.ToInt32(AccountID), "<b>Сообщение на 📱 Коммуникатор:</b>\nПеречисление кристаллов этериума на счет: <b>💳 200 </b>", 1);
                        PlayerDeleteDonate(Convert.ToInt32(ID));
                        break;
                    case "695.50":
                        Account.AddData(Convert.ToInt32(AccountID), "Credits", "500");
                        Message.Send(Convert.ToInt32(AccountID), "<b>Сообщение на 📱 Коммуникатор:</b>\nПеречисление кристаллов этериума на счет: <b>💳 500 </b>", 1);
                        PlayerDeleteDonate(Convert.ToInt32(ID));
                        break;
                    case "685.02":
                        Account.AddData(Convert.ToInt32(AccountID), "Credits", "500");
                        Message.Send(Convert.ToInt32(AccountID), "<b>Сообщение на 📱 Коммуникатор:</b>\nПеречисление кристаллов этериума на счет: <b>💳 500 </b>", 1);
                        PlayerDeleteDonate(Convert.ToInt32(ID));
                        break;
                    case "1084.05":
                        Account.AddData(Convert.ToInt32(AccountID), "Credits", "1000");
                        Message.Send(Convert.ToInt32(AccountID), "<b>Сообщение на 📱 Коммуникатор:</b>\nПеречисление кристаллов этериума на счет: <b>💳 1000 </b>", 1);
                        PlayerDeleteDonate(Convert.ToInt32(ID));
                        break;
                    case "1166.20":
                        Account.AddData(Convert.ToInt32(AccountID), "Credits", "1000");
                        Message.Send(Convert.ToInt32(AccountID), "<b>Сообщение на 📱 Коммуникатор:</b>\nПеречисление кристаллов этериума на счет: <b>💳 1000 </b>", 1);
                        PlayerDeleteDonate(Convert.ToInt32(ID));
                        break;
                    case "3472.55":
                        Account.AddData(Convert.ToInt32(AccountID), "Credits", "3000");
                        Message.Send(Convert.ToInt32(AccountID), "<b>Сообщение на 📱 Коммуникатор:</b>\nПеречисление кристаллов этериума на счет: <b>💳 3000 </b>", 1);
                        PlayerDeleteDonate(Convert.ToInt32(ID));
                        break;
                    case "3420.20":
                        Account.AddData(Convert.ToInt32(AccountID), "Credits", "3000");
                        Message.Send(Convert.ToInt32(AccountID), "<b>Сообщение на 📱 Коммуникатор:</b>\nПеречисление кристаллов этериума на счет: <b>💳 3000 </b>", 1);
                        PlayerDeleteDonate(Convert.ToInt32(ID));
                        break;
                    default:
                        Console.WriteLine("DONATE DEFAULT: " + Amount);
                        break;
                }
            }
            thisReader.Close();
            myConnection.Close();
            
            return Finished;

        }

        #region MapMoveShipEventController(bool Finished) - Контроллер ивентов используется для получения данных из таблицы on_ship_event
        public static void MapMoveShipEventController()
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID, EventID, StartTime, EndTime FROM on_move_event", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            while (thisReader.Read())
            {

                string AccountID = string.Empty;
                string StartTime = string.Empty;
                string EndTime = string.Empty;
                string EventID = string.Empty;
                AccountID += thisReader["AccountID"];
                StartTime += thisReader["StartTime"];
                EndTime += thisReader["EndTime"];
                EventID += thisReader["EventID"];
                int CompareResult = DateTime.Compare((Convert.ToDateTime(EndTime)), DateTime.Now);
                if (CompareResult <= 0)
                {
                    PlayerDeleteMoveShipEvent(Convert.ToInt32(AccountID));
                    Console.WriteLine("В Account.MapMoveShipEventController вызываем SendAccount.MapMoveEventResult с EventID: " + EventID);
                    SendMapMoveShipEventResult(AccountID, EventID);
                }

            }
            myConnection.Close();
            
            thisReader.Close();
        }
        #endregion

        public static void PlanetMapMoveEventController()
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID, EventID, StartTime, EndTime FROM on_planet_move_event", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            while (thisReader.Read())
            {

                string AccountID = string.Empty;
                string StartTime = string.Empty;
                string EndTime = string.Empty;
                string EventID = string.Empty;
                AccountID += thisReader["AccountID"];
                StartTime += thisReader["StartTime"];
                EndTime += thisReader["EndTime"];
                EventID += thisReader["EventID"];
                int CompareResult = DateTime.Compare((Convert.ToDateTime(EndTime)), DateTime.Now);
                if (CompareResult <= 0)
                {
                    PlayerDeleteMoveEvent(Convert.ToInt32(AccountID));
                    SendPlanetMapMoveEventResult(AccountID, EventID);
                }

            }
            myConnection.Close();

            thisReader.Close();
        }

        public static string IsMapCoordExists(int X, int Y)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM map WHERE X = @X AND Y = @Y", myConnection);
            sql_commmand.Parameters.AddWithValue("@X", X);
            sql_commmand.Parameters.AddWithValue("@Y", Y);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string MapExists = string.Empty;
            while (thisReader.Read())
            {
                MapExists += thisReader["ID"];
            }
            thisReader.Close();
            myConnection.Close();
            return MapExists;

        }

        public static string IsPlanetMapCoordExists(int X, int Y, int planetX, int planetY)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM map_planet WHERE X = @X AND Y = @Y AND planetX = @pX AND planetY = @pY", myConnection);
            sql_commmand.Parameters.AddWithValue("@X", X);
            sql_commmand.Parameters.AddWithValue("@Y", Y);
            sql_commmand.Parameters.AddWithValue("@pX", planetX);
            sql_commmand.Parameters.AddWithValue("@pY", planetY);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string MapExists = string.Empty;
            while (thisReader.Read())
            {
                MapExists += thisReader["ID"];
            }
            thisReader.Close();
            myConnection.Close();
            return MapExists;

        }

        public static bool IsReportExists(int AccountID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM reports WHERE AccountID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string ReportExists = string.Empty;
            while (thisReader.Read())
            {
                ReportExists += thisReader["ID"];
            }
            thisReader.Close();
            myConnection.Close();
            if (ReportExists != "" && ReportExists != " ")
            {
                return true;
            }
            else return false;

        }

        public static bool IsPlayerBattleActionExists(int ShipID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM players_battle WHERE ShipID = @ShipID", myConnection);
            sql_commmand.Parameters.AddWithValue("@ShipID", ShipID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string BattleAction = string.Empty;
            while (thisReader.Read())
            {
                BattleAction += thisReader["ID"];
            }
            thisReader.Close();
            myConnection.Close();
            if (BattleAction != "" && BattleAction != " ")
            {
                return true;
            }
            else return false;
        }

        public static bool IsPlayerBattleActionExists(int X, int Y)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM players_battle WHERE X = @X AND Y = @Y", myConnection);
            sql_commmand.Parameters.AddWithValue("@X", X);
            sql_commmand.Parameters.AddWithValue("@Y", Y);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string BattleAction = string.Empty;
            while (thisReader.Read())
            {
                BattleAction += thisReader["ID"];
            }
            thisReader.Close();
            myConnection.Close();
            if (BattleAction != "" && BattleAction != " ")
            {
                return true;
            }
            else return false;

        }

        public static void ShowMapForPlayer(int PlayerID, int Keyboard)
        {
            Account.SetSingleData(PlayerID, "Status", "0");
            int LocationType = 0;
            int YWork = 3;
            string MapString = string.Empty;
            int X = Convert.ToInt32(Ships.Ship.GetSingleData(PlayerID, "X"));
            int Y = Convert.ToInt32(Ships.Ship.GetSingleData(PlayerID, "Y"));
            int Coord1, Coord2, Coord3, Coord4, Coord5, Coord6, Coord7;
            ///
            while (YWork >= -3)
            {
                for (int i = 3; i > -1; i--)
                {
                    Console.WriteLine("Current i = " + i);
                    string isMapExists = IsMapCoordExists(X - i, Y + YWork);
                    int TEST_DELETEME = X - i;
                    Console.WriteLine("Current XMinusI = " + TEST_DELETEME);
                    Console.WriteLine("Current isMapExists = " + isMapExists);
                    if (isMapExists != "") // Если координаты найдены
                    {
                        if (X == X - i && Y == Y + YWork)
                        {
                            LocationType = -1;
                        }
                        else
                        {
                            LocationType = Convert.ToInt32(GetMapSingleData(X - i, Y + YWork, "LocationType"));
                        }
                        MapString = MapString + GetEmojiForLocationType(LocationType);
                        Console.WriteLine("Current LocationType = " + LocationType);
                        Console.WriteLine("Current MapString = " + MapString);
                    }
                    else
                    {
                        Console.WriteLine("Current IfCoordsNO mapstring = " + MapString);
                        MapString = MapString + GetEmojiForLocationType(0);
                    }
                }
                for (int i = 1; i <= 3; i++)
                {
                    Console.WriteLine("Current i = " + i);
                    string isMapExists = IsMapCoordExists(X + i, Y + YWork);
                    int TEST_DELETEME = X + i;
                    Console.WriteLine("Current XPlusI = " + TEST_DELETEME);
                    Console.WriteLine("Current isMapExists = " + isMapExists);
                    if (isMapExists != "") // Если координаты найдены
                    {
                        if (X == X - i && Y == Y + YWork)
                        {
                            LocationType = -1;
                        }
                        else
                        {
                            LocationType = Convert.ToInt32(GetMapSingleData(X + i, Y + YWork, "LocationType"));
                        }
                        MapString = MapString + GetEmojiForLocationType(LocationType);
                        Console.WriteLine("Current LocationType = " + LocationType);
                        Console.WriteLine("Current MapString = " + MapString);
                    }
                    else
                    {
                        Console.WriteLine("Current IfCoordsNO mapstring = " + MapString);
                        MapString = MapString + GetEmojiForLocationType(0);
                    }
                }
                MapString = MapString + "\n";
                YWork--;
            }
            string CurrentLocationName = GetMapSingleData(X, Y, "LocationName");
            string CurrentLocationType = GetMapSingleData(X, Y, "LocationType");
            ///
            Message.Send(PlayerID, "Текущая локация:\n" + GetEmojiForLocationType(Convert.ToInt32(CurrentLocationType)) + " " + CurrentLocationName + "\n\n" + MapString + "\nУсловные обозначения:\n🚀: <b>Вы</b>\n🌫: Неисследованно\n🌏: Планета\n⬛️: Космос\n🌑: Астероид\n🌀: Искривление пространства\n⚙️: Космическая станция\n💥: Аномалия\n", Keyboard);

        }

        public static void ShowPlanetMapForPlayer(int PlayerID, int Keyboard, int planetX, int planetY)
        {
            Account.SetSingleData(PlayerID, "Status", "0");
            int LocationType = 0;
            int YWork = 3;
            string MapString = string.Empty;
            int X = Convert.ToInt32(Account.GetSingleData(PlayerID, "X"));
            int Y = Convert.ToInt32(Account.GetSingleData(PlayerID, "Y"));
            int Coord1, Coord2, Coord3, Coord4, Coord5, Coord6, Coord7;
            ///
            while (YWork >= -3)
            {
                for (int i = 3; i > -1; i--)
                {
                    //Console.WriteLine("Current i = " + i);
                    string isMapExists = IsPlanetMapCoordExists(X - i, Y + YWork, planetX, planetY);
                    int TEST_DELETEME = X - i;
                    //Console.WriteLine("Current XMinusI = " + TEST_DELETEME);
                    //Console.WriteLine("Current isMapExists = " + isMapExists);
                    if (isMapExists != "") // Если координаты найдены
                    {
                        if (X == X - i && Y == Y + YWork)
                        {
                            LocationType = -1;
                        }
                        else
                        {
                            LocationType = Convert.ToInt32(GetPlanetMapSingleData(X - i, Y + YWork, planetX, planetY, "LocationType"));
                        }
                        MapString = MapString + GetEmojiForPlanetLocationType(LocationType);
                        //Console.WriteLine("Current LocationType = " + LocationType);
                        //Console.WriteLine("Current MapString = " + MapString);
                    }
                    else
                    {
                        //Console.WriteLine("Current IfCoordsNO mapstring = " + MapString);
                        MapString = MapString + GetEmojiForPlanetLocationType(0);
                    }
                }
                for (int i = 1; i <= 3; i++)
                {
                    //Console.WriteLine("Current i = " + i);
                    string isMapExists = IsPlanetMapCoordExists(X + i, Y + YWork, planetX, planetY);
                    int TEST_DELETEME = X + i;
                    //Console.WriteLine("Current XPlusI = " + TEST_DELETEME);
                    //Console.WriteLine("Current isMapExists = " + isMapExists);
                    if (isMapExists != "") // Если координаты найдены
                    {
                        if (X == X - i && Y == Y + YWork)
                        {
                            LocationType = -1;
                        }
                        else
                        {
                            LocationType = Convert.ToInt32(GetPlanetMapSingleData(X + i, Y + YWork, planetX, planetY, "LocationType"));
                        }
                        MapString = MapString + GetEmojiForPlanetLocationType(LocationType);
                        //Console.WriteLine("Current LocationType = " + LocationType);
                        //Console.WriteLine("Current MapString = " + MapString);
                    }
                    else
                    {
                        //Console.WriteLine("Current IfCoordsNO mapstring = " + MapString);
                        MapString = MapString + GetEmojiForPlanetLocationType(0);
                    }
                }
                MapString = MapString + "\n";
                YWork--;
            }
            string CurrentLocationName = GetPlanetMapSingleData(X, Y, planetX, planetY, "LocationName");
            string CurrentLocationType = GetPlanetMapSingleData(X, Y, planetX, planetY, "LocationType");
            Console.WriteLine("BEFORE SEND");
            ///
            Message.Send(PlayerID, "Текущая локация:\n" + GetEmojiForPlanetLocationType(Convert.ToInt32(CurrentLocationType)) + " " + CurrentLocationName + "\n\n" + MapString + "\nУсловные обозначения:\n👨🏼‍🚀: <b>Вы</b>\n🌫: Неисследованно\n🏘: Поселение\n🌳: Лес\n🏔: Горы\n⛺️: Лагерь\n🛣: Дорога\n🏢: Космодром\n", Keyboard);

        }

        public static string GetEmojiForPlanetLocationType(int Type)
        {
            // Типы локаций: 1 - Поселение, 2 - Лес, 3 - Горы, 4 - Лагерь, 5 - Дорога, 6 - Здание
            string Emoji = string.Empty;
            switch (Type)
            {
                case -1:
                    Emoji = "👨🏼‍🚀";
                    break;
                case 0:
                    Emoji = "🌫";
                    break;
                case 1:
                    Emoji = "🏘";
                    break;
                case 2:
                    Emoji = "🌳";
                    break;
                case 3:
                    Emoji = "🏔";
                    break;
                case 4:
                    Emoji = "⛺️";
                    break;
                case 5:
                    Emoji = "🛣";
                    break;
                case 6:
                    Emoji = "🏢";
                    break;
            }
            return Emoji;
        }

        public static string GetEmojiForLocationType(int Type)
        {
            // Типы локаций: 1 - Планета, 2 - Космос, 3 - Астероид, 4 - Искривление пространства, 5 - Космическая станция
            string Emoji = string.Empty;
            switch (Type)
            {
                case -1:
                    Emoji = "🚀";
                    break;
                case 0:
                    Emoji = "🌫";
                    break;
                case 1:
                    Emoji = "🌎";
                    break;
                case 2:
                    Emoji = "⬛️";
                    break;
                case 3:
                    Emoji = "🌑";
                    break;
                case 4:
                    Emoji = "🌀";
                    break;
                case 5:
                    Emoji = "⚙️";
                    break;
                case 6:
                    Emoji = "💥";
                    break;
                case 10:
                    Emoji = "🇲🇭";
                    break;
                case 11:
                    Emoji = "🇵🇬";
                    break;
                case 12:
                    Emoji = "🇨🇩";
                    break;
                case 13:
                    Emoji = "🇹🇹";
                    break;
                case 14:
                    Emoji = "🇱🇨";
                    break;
            }
            return Emoji;
        }

        public static string GenerateItemDrop(int AccountID)
        {
            Random rnd_items_count_to_drop = new Random();
            int Items_Names_Count = rndmzr.Next(1, 4);
            int Random_Items_Id_Drop = 1;
            int Random_Items_Count = 1;
            string ItemMainDropString = string.Empty;
            string Item1Drop_String = string.Empty;
            string Item2Drop_String = string.Empty;
            string Item3Drop_String = string.Empty;
            string Item4Drop_String = string.Empty;
            int Item1RandomID = 0;
            int Item2RandomID = 0;
            int Item3RandomID = 0;
            int Item4RandomID = 0;
            int Item1RandomCount = 1;
            int Item2RandomCount = 1;
            int Item3RandomCount = 1;
            int Item4RandomCount = 1;
            if (Items_Names_Count == 1)
            {
                GetRandomItemDrop(rndmzr, out Random_Items_Id_Drop, out Random_Items_Count); // Получаем случайный ID предмета для дропа
                Item1RandomID = Random_Items_Id_Drop;
                Item1RandomCount = Random_Items_Count;
                Item1Drop_String = GetRandomDropItemNameByID(Item1RandomID) + " (" + Item1RandomCount + ")\n"; // Создаем строку Название + Кол-во
                Account.GiveItem(Convert.ToInt32(AccountID), Convert.ToString(Item1RandomID), Convert.ToString(Item1RandomCount));
            }
            else if (Items_Names_Count == 2)
            {
                GetRandomItemDrop(rndmzr, out Random_Items_Id_Drop, out Random_Items_Count); // Получаем случайный ID предмета для дропа
                Item1RandomID = Random_Items_Id_Drop;
                Item1RandomCount = Random_Items_Count;
                Item1Drop_String = GetRandomDropItemNameByID(Item1RandomID) + " (" + Item1RandomCount + ")\n"; // Создаем строку Название + Кол-во
                Account.GiveItem(Convert.ToInt32(AccountID), Convert.ToString(Item1RandomID), Convert.ToString(Item1RandomCount));

                GetRandomItemDrop(rndmzr, out Random_Items_Id_Drop, out Random_Items_Count); // Получаем случайный ID предмета для дропа
                Item2RandomID = Random_Items_Id_Drop;
                Item2RandomCount = Random_Items_Count;
                Item2Drop_String = GetRandomDropItemNameByID(Item2RandomID) + " (" + Item2RandomCount + ")\n"; // Создаем строку Название + Кол-во
                Account.GiveItem(Convert.ToInt32(AccountID), Convert.ToString(Item2RandomID), Convert.ToString(Item2RandomCount));
            }
            else if (Items_Names_Count == 3)
            {
                GetRandomItemDrop(rndmzr, out Random_Items_Id_Drop, out Random_Items_Count); // Получаем случайный ID предмета для дропа
                Item1RandomID = Random_Items_Id_Drop;
                Item1RandomCount = Random_Items_Count;
                Item1Drop_String = GetRandomDropItemNameByID(Item1RandomID) + " (" + Item1RandomCount + ")\n"; // Создаем строку Название + Кол-во
                Account.GiveItem(Convert.ToInt32(AccountID), Convert.ToString(Item1RandomID), Convert.ToString(Item1RandomCount));

                GetRandomItemDrop(rndmzr, out Random_Items_Id_Drop, out Random_Items_Count); // Получаем случайный ID предмета для дропа
                Item2RandomID = Random_Items_Id_Drop;
                Item2RandomCount = Random_Items_Count;
                Item2Drop_String = GetRandomDropItemNameByID(Item2RandomID) + " (" + Item2RandomCount + ")\n"; // Создаем строку Название + Кол-во
                Account.GiveItem(Convert.ToInt32(AccountID), Convert.ToString(Item2RandomID), Convert.ToString(Item2RandomCount));

                GetRandomItemDrop(rndmzr, out Random_Items_Id_Drop, out Random_Items_Count); // Получаем случайный ID предмета для дропа
                Item3RandomID = Random_Items_Id_Drop;
                Item3RandomCount = Random_Items_Count;
                Item3Drop_String = GetRandomDropItemNameByID(Item3RandomID) + " (" + Item3RandomCount + ")\n"; // Создаем строку Название + Кол-во
                Account.GiveItem(Convert.ToInt32(AccountID), Convert.ToString(Item3RandomID), Convert.ToString(Item3RandomCount));
            }
            else if (Items_Names_Count == 4)
            {
                GetRandomItemDrop(rndmzr, out Random_Items_Id_Drop, out Random_Items_Count); // Получаем случайный ID предмета для дропа
                Item1RandomID = Random_Items_Id_Drop;
                Item1RandomCount = Random_Items_Count;
                Item1Drop_String = GetRandomDropItemNameByID(Item1RandomID) + " (" + Item1RandomCount + ")\n"; // Создаем строку Название + Кол-во
                Account.GiveItem(Convert.ToInt32(AccountID), Convert.ToString(Item1RandomID), Convert.ToString(Item1RandomCount));

                GetRandomItemDrop(rndmzr, out Random_Items_Id_Drop, out Random_Items_Count); // Получаем случайный ID предмета для дропа
                Item2RandomID = Random_Items_Id_Drop;
                Item2RandomCount = Random_Items_Count;
                Item2Drop_String = GetRandomDropItemNameByID(Item2RandomID) + " (" + Item2RandomCount + ")\n"; // Создаем строку Название + Кол-во
                Account.GiveItem(Convert.ToInt32(AccountID), Convert.ToString(Item2RandomID), Convert.ToString(Item2RandomCount));

                GetRandomItemDrop(rndmzr, out Random_Items_Id_Drop, out Random_Items_Count); // Получаем случайный ID предмета для дропа
                Item3RandomID = Random_Items_Id_Drop;
                Item3RandomCount = Random_Items_Count;
                Item3Drop_String = GetRandomDropItemNameByID(Item3RandomID) + " (" + Item3RandomCount + ")\n"; // Создаем строку Название + Кол-во
                Account.GiveItem(Convert.ToInt32(AccountID), Convert.ToString(Item3RandomID), Convert.ToString(Item3RandomCount));

                GetRandomItemDrop(rndmzr, out Random_Items_Id_Drop, out Random_Items_Count); // Получаем случайный ID предмета для дропа
                Item4RandomID = Random_Items_Id_Drop;
                Item4RandomCount = Random_Items_Count;
                Item4Drop_String = GetRandomDropItemNameByID(Item4RandomID) + " (" + Item4RandomCount + ")\n"; // Создаем строку Название + Кол-во
                Account.GiveItem(Convert.ToInt32(AccountID), Convert.ToString(Item4RandomID), Convert.ToString(Item4RandomCount));
            }

            ItemMainDropString = Item1Drop_String + Item2Drop_String + Item3Drop_String + Item4Drop_String; // Формируем строку для сообщения
            return ItemMainDropString;
        }

        public static void GenerateSituation(int AccountID, int EventID, int LocationDanger, string SituationCondition, string Message)
        {
            int GenerateChanceGood_EventID1 = rndmzr.Next(1, 4); // ПЛАНЕТА - Количество запрограммированных событий + 1 (для хороших) 
            int GenerateChanceBad_EventID1 = rndmzr.Next(1, 3); // ПЛАНЕТА - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceGood_EventID2 = rndmzr.Next(1, 5); // КОСМОС - Количество запрограммированных событий + 1 (для хороших)
            int GenerateChanceBad_EventID2 = rndmzr.Next(1, 3); // КОСМОС - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceGood_EventID3 = rndmzr.Next(1, 4); // АСТЕРОИД - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceBad_EventID3 = rndmzr.Next(1, 3); // АСТЕРОИД - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceGood_EventID4 = rndmzr.Next(1, 3); // ИСКРИВЛЕНИЕ - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceBad_EventID4 = rndmzr.Next(1, 3); // ИСКРИВЛЕНИЕ - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceGood_EventID5 = rndmzr.Next(1, 3); // СТАНЦИЯ - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceBad_EventID5 = rndmzr.Next(1, 3); // СТАНЦИЯ - Количество запрограммированных событий + 1 (для плохих)

            int RareChance = rndmzr.Next(1, 100); // Рандомизатор редких событий


            int RndmGold = rndmzr.Next(25, 76);
            int RndmExp = rndmzr.Next(25, 76);

            string RndmCharFirstName = Generator.Generate.BotsFirstNameFemale();
            string RndmCharLastName = Generator.Generate.BotsFirstNameMale();
            string RndmDanger = Generator.Generate.SituationDanger();
            //string RndmDangerDude = NameGenerator.Generate.DangerMeet();

            string RndmChar = RndmCharFirstName + " " + RndmCharLastName;
            // Типы локаций: 1 - Планета, 2 - Космос, 3 - Астероид, 4 - Искривление пространства, 5 - Космическая станция
            // Расчет наибольшего показателя профиля игрока, генерация строки положительного события
            int CaptainStrength = Convert.ToInt32(Account.GetSingleData(AccountID, "Strength"));
            int CaptainCharisma = Convert.ToInt32(Account.GetSingleData(AccountID, "Charisma"));
            int CaptainIntelligence = Convert.ToInt32(Account.GetSingleData(AccountID, "Intelligence"));
            int CaptainAgility = Convert.ToInt32(Account.GetSingleData(AccountID, "Agility"));
            int CaptainLuck = Convert.ToInt32(Account.GetSingleData(AccountID, "Luck"));
            string GoodCase1String = string.Empty;
            string GoodCase2String = string.Empty;
            // Профессии
            // Пустая - 0
            // Старпом - 1
            // Суперкарго - 2
            // Навигатор - 3
            // Техник-механик - 4 
            // Тактик - 5
            // Стрелок - 6
            // Аналитик - 7 
            // Медик - 8
            // Пилот - 9

            string X = Ships.Ship.GetSingleData(AccountID, "X");
            string Y = Ships.Ship.GetSingleData(AccountID, "Y");


            if (SituationCondition == "Good")
            {
                Console.WriteLine("GenerateSituation - SituationCondition == Good");
                #region Хорошие события - локация "Планета"
                if (EventID == 1) // Планета
                {
                    Console.WriteLine("GenerateSituation - EventID == 1");
                    #region Расчет денег и опыта
                    if (CaptainLuck <= 15 && CaptainLuck >= 5)
                    {
                        RndmGold = rndmzr.Next(30, 101);
                    }
                    else if (CaptainLuck <= 20 && CaptainLuck >= 16)
                    {
                        RndmGold = rndmzr.Next(50, 151);
                    }
                    else if (CaptainLuck >= 21)
                    {
                        RndmGold = rndmzr.Next(75, 201);
                    }

                    if (CaptainIntelligence <= 21 && CaptainIntelligence >= 5)
                    {
                        RndmExp = rndmzr.Next(30, 101);
                    }
                    else if (CaptainIntelligence <= 30 && CaptainIntelligence >= 22)
                    {
                        RndmExp = rndmzr.Next(50, 151);
                    }
                    else if (CaptainIntelligence >= 31)
                    {
                        RndmExp = rndmzr.Next(75, 201);
                    }
                    // Конец расчета
                    #endregion

                    string MissionText = string.Empty;
                    switch (GenerateChanceGood_EventID1) // Генерация события для города - хорошее
                    {

                        case 1:
                            MissionText = "Вы успешно вышли на орбиту планеты.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        case 2:
                            // if (Account.StatDiceCheck(AccountID, "Charisma") == true && CaptainCharisma > CaptainIntelligence)
                            string SupercargoID = NPC.ProfessionExists(AccountID, 2);
                            if (SupercargoID != "")
                            {
                                string NPCName = NPC.GetDataByClanID(Convert.ToInt32(SupercargoID), "FirstName");
                                string NPCSurname = NPC.GetDataByClanID(Convert.ToInt32(SupercargoID), "LastName");

                                MissionText = "Вы успешно вошли в атмосферу планеты. Ваш суперкарго " + NPCName + " " + NPCSurname + " заметил бесхозный грузовой контейнер, по всей видимости отцепившийся от траулера. Он притянул его в грузовой отсек корабля при помощи гравитационного луча.";
                                string ItemDropString = GenerateItemDrop(AccountID); // Создаем DROP строку и выдаем предметы
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp + "\n\n<b>📦Предметы:</b>\n" + ItemDropString, 1);
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            }
                            else
                            {
                                MissionText = "Вы успешно вошли в атмосферу планеты.";
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            }
                            break;
                        case 3:
                            string AnalitikID = NPC.ProfessionExists(AccountID, 7);
                            if (AnalitikID != "")
                            {
                                string NPCName = NPC.GetDataByClanID(Convert.ToInt32(AnalitikID), "FirstName");
                                string NPCSurname = NPC.GetDataByClanID(Convert.ToInt32(AnalitikID), "LastName");

                                MissionText = "Подлетая к планете вы получили сигнал SOS от корабля в секторе неподалёку. Ваш аналитик " + NPCName + " " + NPCSurname + " в грубой форме остановил ваш порыв прийти на помощь неизвестному кораблю. Оказалось, сигнал подавал маяк давно уничтоженного транспортного корабля Флота Федерации. Возможно сегодня вы избежали серьезных проблем.";
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            }
                            else
                            {
                                MissionText = "Подлетая к планете вы получили сигнал SOS от корабля в секторе неподалёку. Вы поспешили на помощь команде крейсера застрявшего в гравитационной аномалии.";
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            }
                            break;
                    }
                }
                #endregion
                #region Хорошие события - локация "Космос"
                if (EventID == 2)
                {
                    Console.WriteLine("GenerateSituation - EventID == 2");
                    switch (GenerateChanceGood_EventID2) // Генерация события для города - хорошее
                    {
                        case 1:
                            Console.WriteLine("GenerateSituation - Case 1");
                            string MissionText = Message + "Вы решили скрасить перелет за чтением какой-нибудь хорошей книги. ";
                            if (Account.StatDiceCheck(AccountID, "Luck") == true)
                            {
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "Под руку попалась книга некого Джона Грея - Мужчины с Марса, женщины с Венеры. Дааа, автор даже приблизительно не угадал... Прочитав с десяток страниц, вы закрыли книгу и подумали, что неплохо было бы запихать её в Рекрафтер к чертовой матери.\nОтнося книгу в отсек с Рекрафтером, вы случайно выронили её из рук и обнаружили между страниц несколько пластиковых купюр галактических кредитов. Хоть какая то польза!" + "\n\n<b>Получено:</b> \n<b>Деньги:</b> 💳 " + RndmGold + "\n<b>Опыт:</b> ✨" + RndmExp, 1);
                                Account.AddData(AccountID, "Money", Convert.ToString(RndmGold));
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            }
                            else
                            {
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "Под руку попалась книга некого Джона Грея - Мужчины с Марса, женщины с Венеры. Дааа, автор даже приблизительно не угадал... Прочитав с десяток страниц, вы закрыли книгу и подумали, что неплохо было бы запихать её в Рекрафтер к чертовой матери." + "\n\n<b>Получено:</b>\n<b>Опыт:</b> ✨" + RndmExp, 1);
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            }
                            break;
                        case 2:
                            Console.WriteLine("GenerateSituation - Case 2");
                            string ItemDropString = GenerateItemDrop(AccountID); // Создаем DROP строку и выдаем предметы
                            MissionText = Message + "Ваша любимая тетушка живущая в далекой системе 4-8-15-ZETA прислала вам посылку. Курьер очень сильно настаивал, чтобы вы позвонили тетушке и сообщили, что с её посылкой всё в порядке. \n- И обязательно передайте мои огромные извинения за задержку в 15 минут! - сказал курьер и исчез в шлюзе. Странный ¯\\_(ツ)_/¯";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\n<b>Получено:</b> \n<b>Деньги:</b> 💳 " + RndmGold + "\n<b>Опыт:</b> ✨" + RndmExp + "\n\n<b>Предметы:</b> \n" + ItemDropString, 1);
                            Account.AddData(AccountID, "Money", Convert.ToString(RndmGold));
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        case 3:
                            Console.WriteLine("GenerateSituation - Case 2");
                            ItemDropString = GenerateItemDrop(AccountID); // Создаем DROP строку и выдаем предметы
                            MissionText = Message + "Во время перелета вы вдруг почувствовали острую необходимость прибраться в своей каюте. Взяв в руки тряпку из нановолокна и настроив автоматическую швабру вы с воодушевлением принялись за работу. ";
                            if (Account.StatDiceCheck(AccountID, "Luck") == true)
                            {
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "Под кроватью вы нашли неучтенные денежные средства, а также пару занятных вещичек." + "\n\n<b>Получено:</b> \n<b>Деньги:</b> 💳 " + RndmGold + "\n<b>Опыт:</b> ✨" + RndmExp + "\n\n<b>Предметы:</b> \n" + ItemDropString, 1);
                                Account.AddData(AccountID, "Money", Convert.ToString(RndmGold));
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            }
                            else
                            {
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "" + "\n\n<b>Получено:</b>\n<b>Опыт:</b> ✨" + RndmExp, 1);
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            }
                            break;
                        case 4:
                            if (RareChance == 5)
                            {
                                Console.WriteLine("GenerateSituation - Case 4 RARE");
                                MissionText = Message + "⭐️ <b>Легендарное событие</b> ⭐️\n\nЗабив на правила безопасности ближних космических перелетов вы завалились на диван в пультогостинной и уснули. Во сне, вы стояли посреди поля усыпанного одуванчивами и смотрели в небо. Там над облаками горели два ангельских крыла, словно сотканные из солнечного света. Не в силах отвести взгляд, вы продолжали смотреть пока ваши глаза не заслезились. Вы проснулись и осознали себя плачущим свернувшись в клубок на диванчике в пультогостинной.";
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\n<b>Получено:</b>" + "\n<b>Опыт:</b> ✨" + RndmExp, 1);
                                RndmExp = RndmExp * 3;
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                                break;
                            }
                            else
                            {
                                Console.WriteLine("GenerateSituation - Case 4");
                                MissionText = Message + "Вам на почтовый ящик пришло письмо с благодарностями от некого Кун'джах Киз. Вы вспомнили, что это гракс который оплатил доставку каких то семян в Сектор 24 системы Sol. Гракс рассыпался в комплиментах, хвалил вашу стойкость и ответственность как капитана и выражал восторг по поводу вашего корабля. В приложении к письму вы обнаружили прикрепленный электронный перевод с парой галакредитов. Приятно!";
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\n<b>Получено:</b> \n<b>Деньги:</b> 💳 " + RndmGold + "\n<b>Опыт:</b> ✨" + RndmExp, 1);
                                Account.AddData(AccountID, "Money", Convert.ToString(RndmGold));
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                                break;
                            }
                        default:
                            Console.WriteLine("GenerateSituation - Case Def");
                            break;
                    }
                }
                #endregion
                #region Хорошие события - локация "Астероид"
                if (EventID == 3)
                {
                    Console.WriteLine("GenerateSituation - EventID == 3");
                    switch (GenerateChanceGood_EventID3) // Генерация события для города - плохие
                    {
                        case 1:
                            string MissionText = "Вы увидели астероид. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        case 2:
                            string AnalitikID = NPC.ProfessionExists(AccountID, 7);
                            if (AnalitikID != "")
                            {
                                string NPCName = NPC.GetDataByClanID(Convert.ToInt32(AnalitikID), "FirstName");
                                string NPCSurname = NPC.GetDataByClanID(Convert.ToInt32(AnalitikID), "LastName");

                                MissionText = "Вы зависли возле астероида странной формы: совершенно гладкий, продолговатый, больше всего он напоминал допотопную торпеду. Ваш аналитик " + NPCName + " " + NPCSurname + " увидев его на экране визора, чуть не упал со стула и с безумными глазами принялся обьяснять вам что \"Это прекрасный образчик атомарного сжатия структур!\". Вы покивали головой, но наотрез отказались грузить астероид в трюм. Повздыхав и поохав, ваш аналитик просканировал астероид и погрузился в исследование полученных данных.";
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            }
                            else
                            {
                                MissionText = "Вы зависли возле астероида странной формы: совершенно гладкий, продолговатый, больше всего он напоминал допотопную торпеду. Вы пофотографировались на его фоне.";
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            }
                            break;
                        case 3:
                            string TacticID = NPC.ProfessionExists(AccountID, 5);
                            if (TacticID != "")
                            {
                                string NPCName = NPC.GetDataByClanID(Convert.ToInt32(TacticID), "FirstName");
                                string NPCSurname = NPC.GetDataByClanID(Convert.ToInt32(TacticID), "LastName");

                                MissionText = "Вы увидели как вокруг большого астероида копошится множество мелкий ботов-старателей. Красиво работают! Ваш тактик " + NPCName + " " + NPCSurname + " засек небольшое гравитационное возмущение на другой стороне астероида. Кажется, транспортный корабль только что отбыл с грузом.";
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            }
                            else
                            {
                                MissionText = "Вы увидели как вокруг большого астероида копошится множество мелкий ботов-старателей. Красиво работают!";
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            }
                            break;
                        default:
                            Console.WriteLine("GenerateSituation - Case Def");
                            MissionText = "Вы увидели астероид. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
                #region Хорошие события - локация "Искривление пространства"
                if (EventID == 4)
                {
                    Console.WriteLine("GenerateSituation - EventID == 4");
                    switch (GenerateChanceGood_EventID4) // Генерация события для города - плохие
                    {
                        case 1:
                            string MissionText = "Сенсоры корабля засекли искривление в пространстве. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        case 2:
                            string NavigatorID = NPC.ProfessionExists(AccountID, 3);
                            if (NavigatorID != "")
                            {
                                string NPCName = NPC.GetDataByClanID(Convert.ToInt32(NavigatorID), "FirstName");
                                string NPCSurname = NPC.GetDataByClanID(Convert.ToInt32(NavigatorID), "LastName");

                                MissionText = "Вы приблизились к гравитационной аномалии. Мимо вашего корабля пролетел идеально ровный, словно разрезанный бритвой, астероид. Опасная хрень.\n Ваш навигатор " + NPCName + " " + NPCSurname + " задумчиво созерцая пролетяющий кусок камня сказал:\n- Мда, а отец мне говорил, будто кто-то из умных лбов нашел способ использовать аномалии как врата для прыжка на многие десятки световых лет. Не хотел бы я оказаться сейчас на месте этого астероида...";
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            }
                            else
                            {
                                MissionText = "Вы приблизились к гравитационной аномалии. Мимо вашего корабля пролетел идеально ровный, словно разрезанный бритвой, астероид. Опасная хрень.";
                                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                                Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            }
                            break;
                        default:
                            Console.WriteLine("GenerateSituation - Case Def");
                            MissionText = "Сенсоры корабля засекли искривление в пространстве. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
                #region Хорошие события - локация "Космическая станция"
                if (EventID == 5)
                {
                    Console.WriteLine("GenerateSituation - EventID == 5");
                    switch (GenerateChanceGood_EventID5) // Генерация события для города - плохие
                    {
                        case 1:
                            string MissionText = "Вы получили разрешение на стыковку к космической станции. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        case 2:
                            MissionText = "Вы получили разрешение на стыковку к космической станции. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        default:
                            Console.WriteLine("GenerateSituation - Case Def");
                            MissionText = "Вы получили разрешение на стыковку к космической станции. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
            }
            else if (SituationCondition == "Bad")
            {
                Console.WriteLine("GenerateSituation - SituationCondition == Bad");
                #region Плохие события - локация "Планета"
                if (EventID == 1) // Планета
                {
                    switch (GenerateChanceBad_EventID1)
                    {
                        case 1:
                            string SupercargoID = NPC.ProfessionExists(AccountID, 2);
                            if (SupercargoID != "")
                            {
                                string MissionText = "После выхода на орбиту планеты, Корабль Федеральных Космических сил поймал вас в гравитационный захват и потребовал присоединить стыковочный шлюз для проверки груза, документов на груз, документов команды и документов на корабль. Проверка заняла несколько часов и выявила несколько незначительных нарушений. Если бы не ваш суперкарго, пришлось бы заплатить штраф. Он сумел договориться с сотрудниками и вы отделались лишь предупреждением.";
                                if (Convert.ToInt32(Account.GetSingleData(AccountID, "Money")) >= 10)
                                {
                                    Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПотеряно:\nДеньги: 💳 " + 0, 1);
                                }
                                else
                                {
                                    Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "" + "\n\nПотеряно:\nДеньги: 💳 " + 0, 1);
                                }
                            }
                            else
                            {
                                string MissionText = "После выхода на орбиту планеты, Корабль Федеральных Космических сил поймал вас в гравитационный захват и потребовал присоединить стыковочный шлюз для проверки груза, документов на груз, документов команды и документов на корабль. Проверка заняла несколько часов и выявила несколько незначительных нарушений. Придется заплатить штраф...";
                                if (Convert.ToInt32(Account.GetSingleData(AccountID, "Money")) >= 10)
                                {
                                    RndmGold = rndmzr.Next(10, ProcentResultCount(Convert.ToInt32(Account.GetSingleData(AccountID, "Money")), 5));
                                    int GoldTaken = Account.TakeGoldForced(AccountID, RndmGold);
                                    Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПотеряно:\nДеньги: 💳 " + GoldTaken, 1);
                                }
                                else
                                {
                                    RndmGold = Convert.ToInt32(Account.GetSingleData(AccountID, "Money"));
                                    int GoldTaken = Account.TakeGoldForced(AccountID, RndmGold);
                                    Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "" + "\n\nПотеряно:\nДеньги: 💳 " + GoldTaken, 1);
                                }
                            }
                            break;
                        case 2:
                            SupercargoID = NPC.ProfessionExists(AccountID, 2);
                            if (SupercargoID != "")
                            {
                                string MissionText = "После выхода на орбиту планеты, Корабль Федеральных Космических сил поймал вас в гравитационный захват и потребовал присоединить стыковочный шлюз для проверки груза, документов на груз, документов команды и документов на корабль. Проверка заняла несколько часов и выявила несколько незначительных нарушений. Если бы не ваш суперкарго, пришлось бы заплатить штраф. Он сумел договориться с сотрудниками и вы отделались лишь предупреждением.";
                                if (Convert.ToInt32(Account.GetSingleData(AccountID, "Money")) >= 10)
                                {
                                    Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПотеряно:\nДеньги: 💳 " + 0, 1);
                                }
                                else
                                {
                                    Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "" + "\n\nПотеряно:\nДеньги: 💳 " + 0, 1);
                                }
                            }
                            else
                            {
                                string MissionText = "После выхода на орбиту планеты, Корабль Федеральных Космических сил поймал вас в гравитационный захват и потребовал присоединить стыковочный шлюз для проверки груза, документов на груз, документов команды и документов на корабль. Проверка заняла несколько часов и выявила несколько незначительных нарушений. Придется заплатить штраф...";
                                if (Convert.ToInt32(Account.GetSingleData(AccountID, "Money")) >= 10)
                                {
                                    RndmGold = rndmzr.Next(10, ProcentResultCount(Convert.ToInt32(Account.GetSingleData(AccountID, "Money")), 5));
                                    int GoldTaken = Account.TakeGoldForced(AccountID, RndmGold);
                                    Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПотеряно:\nДеньги: 💳 " + GoldTaken, 1);
                                }
                                else
                                {
                                    RndmGold = Convert.ToInt32(Account.GetSingleData(AccountID, "Money"));
                                    int GoldTaken = Account.TakeGoldForced(AccountID, RndmGold);
                                    Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "" + "\n\nПотеряно:\nДеньги: 💳 " + GoldTaken, 1);
                                }
                            }
                            break;
                    }
                }
                #endregion
                #region Плохие события - локация "Космос"
                if (EventID == 2)
                {
                    Console.WriteLine("GenerateSituation - EventID == 2");
                    switch (GenerateChanceBad_EventID2) // Генерация события для космоса - плохие
                    {
                        case 1:
                            string DiceCheckString = string.Empty;
                            Console.WriteLine("GenerateSituation - Case 1");
                            if (Account.StatDiceCheck(AccountID, "Strength")) DiceCheckString = "Вы смогли поймать Жужелицу и практически не пострадали!";
                            else
                            {
                                int HealthToDel = rndmzr.Next(10, 25);
                                DiceCheckString = "Вы гонялись за Жужелицей несколько часов, с переменным успехом отбиваясь от её шипастого хвоста.\nПотеряно: ❤️ " + HealthToDel;
                                Account.DeleteHealth(AccountID, HealthToDel);
                            }
                            string MissionText = Message + "Во время осмотра грузового отсека вы обнаружили молодую марсианскую Жужелицу которая с аппетитом грызла обшивку и проводку. " + DiceCheckString;
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\n<b>Получено:</b> \n<b>Опыт:</b> ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        case 2:
                            DiceCheckString = string.Empty;
                            Console.WriteLine("GenerateSituation - Case 2");
                            if (Account.StatDiceCheck(AccountID, "Intelligence")) DiceCheckString = "Вам хватило благоразумия изменить курс и вы успешно избежали столкновения с потоком.";
                            else
                            {
                                int HealthToDel = rndmzr.Next(5, 10);
                                DiceCheckString = "Недооценив опасность вы решили продолжить полёт и столкнулись с несколькими небольшими метеоритами.\nПотеряно: ❤️ " + HealthToDel;
                                Ships.Ship.DelHealth(AccountID, HealthToDel);
                            }
                            MissionText = Message + "Сканер пространства показал плотный метеоритный поток прям по вашему курсу.\n" + DiceCheckString;
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\n<b>Получено:</b> \n<b>Опыт:</b> ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        default:
                            Console.WriteLine("GenerateSituation - Case Def");
                            break;
                    }
                }
                #endregion
                #region Плохие события - локация "Астероид"
                if (EventID == 3)
                {
                    Console.WriteLine("GenerateSituation - EventID == 3");
                    switch (GenerateChanceBad_EventID3) // Генерация события для города - плохие
                    {
                        case 1:
                            string MissionText = "Вы увидели астероид. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        case 2:
                            MissionText = "Вы увидели астероид. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        default:
                            Console.WriteLine("GenerateSituation - Case Def");
                            MissionText = "Вы увидели астероид. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
                #region Плохие события - локация "Искривление пространства"
                if (EventID == 4)
                {
                    Console.WriteLine("GenerateSituation - EventID == 4");
                    switch (GenerateChanceBad_EventID4) // Генерация события для города - плохие
                    {
                        case 1:
                            string MissionText = "Сенсоры корабля засекли искривление в пространственно-временном континууме. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        case 2:
                            MissionText = "Сенсоры корабля засекли искривление в пространственно-временном континууме. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        default:
                            Console.WriteLine("GenerateSituation - Case Def");
                            MissionText = "Сенсоры корабля засекли искривление в пространственно-временном континууме. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
                #region Плохие события - локация "Космическая станция"
                if (EventID == 5)
                {
                    Console.WriteLine("GenerateSituation - EventID == 5");
                    switch (GenerateChanceBad_EventID5) // Генерация события для города - плохие
                    {
                        case 1:
                            string MissionText = "Вы получили разрешение на стыковку к космической станции. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        case 2:
                            MissionText = "Вы получили разрешение на стыковку к космической станции. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        default:
                            Console.WriteLine("GenerateSituation - Case Def");
                            MissionText = "Вы получили разрешение на стыковку к космической станции. Больше ничего не произошло.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
            }
        }

        public static void GeneratePlanetSituation(int AccountID, int EventID, int LocationDanger, string SituationCondition, string Message)
        {
            int GenerateChanceGood_EventID1 = rndmzr.Next(1, 2); // Поселение - Количество запрограммированных событий + 1 (для хороших) 
            int GenerateChanceBad_EventID1 = rndmzr.Next(1, 2); // Поселение - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceGood_EventID2 = rndmzr.Next(1, 2); // Лес - Количество запрограммированных событий + 1 (для хороших)
            int GenerateChanceBad_EventID2 = rndmzr.Next(1, 2); // Лес - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceGood_EventID3 = rndmzr.Next(1, 2); // Горы - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceBad_EventID3 = rndmzr.Next(1, 2); // Горы - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceGood_EventID4 = rndmzr.Next(1, 2); // Лагерь - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceBad_EventID4 = rndmzr.Next(1, 2); // Лагерь - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceGood_EventID5 = rndmzr.Next(1, 2); // Дорога - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceBad_EventID5 = rndmzr.Next(1, 2); // Дорога - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceGood_EventID6 = rndmzr.Next(1, 2); // Здание - Количество запрограммированных событий + 1 (для плохих)
            int GenerateChanceBad_EventID6 = rndmzr.Next(1, 2); // Здание - Количество запрограммированных событий + 1 (для плохих)

            int RareChance = rndmzr.Next(1, 100); // Рандомизатор редких событий


            int RndmGold = rndmzr.Next(25, 76);
            int RndmExp = rndmzr.Next(25, 76);

            string RndmCharFirstName = Generator.Generate.BotsFirstNameFemale();
            string RndmCharLastName = Generator.Generate.BotsFirstNameMale();
            string RndmDanger = Generator.Generate.SituationDanger();
            //string RndmDangerDude = NameGenerator.Generate.DangerMeet();

            string RndmChar = RndmCharFirstName + " " + RndmCharLastName;
            // Типы локаций: 1 - Поселение, 2 - Лес, 3 - Горы, 4 - Лагерь, 5 - Дорога, 6 - Здание
            // Расчет наибольшего показателя профиля игрока, генерация строки положительного события
            int CaptainStrength = Convert.ToInt32(Account.GetSingleData(AccountID, "Strength"));
            int CaptainCharisma = Convert.ToInt32(Account.GetSingleData(AccountID, "Charisma"));
            int CaptainIntelligence = Convert.ToInt32(Account.GetSingleData(AccountID, "Intelligence"));
            int CaptainAgility = Convert.ToInt32(Account.GetSingleData(AccountID, "Agility"));
            int CaptainLuck = Convert.ToInt32(Account.GetSingleData(AccountID, "Luck"));
            string GoodCase1String = string.Empty;
            string GoodCase2String = string.Empty;
            // Профессии
            // Пустая - 0
            // Старпом - 1
            // Суперкарго - 2
            // Навигатор - 3
            // Техник-механик - 4 
            // Тактик - 5
            // Стрелок - 6
            // Аналитик - 7 
            // Медик - 8
            // Пилот - 9

            string X = Account.GetSingleData(AccountID, "X");
            string Y = Account.GetSingleData(AccountID, "Y");
            string pX = Account.GetSingleData(AccountID, "planetX");
            string pY = Account.GetSingleData(AccountID, "planetY");

            if (SituationCondition == "Good")
            {
                Console.WriteLine("GenerateSituation - SituationCondition == Good");
                #region Хорошие события - локация "Поселение"
                if (EventID == 1) // Поселение
                {
                    Console.WriteLine("GenerateSituation - EventID == 1");
                    #region Расчет денег и опыта
                    if (CaptainLuck <= 15 && CaptainLuck >= 5)
                    {
                        RndmGold = rndmzr.Next(30, 101);
                    }
                    else if (CaptainLuck <= 20 && CaptainLuck >= 16)
                    {
                        RndmGold = rndmzr.Next(50, 151);
                    }
                    else if (CaptainLuck >= 21)
                    {
                        RndmGold = rndmzr.Next(75, 201);
                    }

                    if (CaptainIntelligence <= 21 && CaptainIntelligence >= 5)
                    {
                        RndmExp = rndmzr.Next(30, 101);
                    }
                    else if (CaptainIntelligence <= 30 && CaptainIntelligence >= 22)
                    {
                        RndmExp = rndmzr.Next(50, 151);
                    }
                    else if (CaptainIntelligence >= 31)
                    {
                        RndmExp = rndmzr.Next(75, 201);
                    }
                    // Конец расчета
                    #endregion

                    string MissionText = string.Empty;
                    switch (GenerateChanceGood_EventID1) // Генерация события для поселения - хорошее
                    {

                        case 1:
                            MissionText = "Вы пришли в поселение.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
                #region Хорошие события - локация "Лес"
                if (EventID == 2)
                {
                    Console.WriteLine("GenerateSituation - EventID == 2");
                    switch (GenerateChanceGood_EventID2) // Генерация события для леса - хорошее
                    {
                        case 1:
                            string MissionText = "Вы пришли в лес.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;

                        default:
                            Console.WriteLine("GenerateSituation - Case Def");
                            break;
                    }
                }
                #endregion
                #region Хорошие события - локация "Горы"
                if (EventID == 3)
                {
                    Console.WriteLine("GenerateSituation - EventID == 3");
                    switch (GenerateChanceGood_EventID3) // Генерация события для гор - хорошие
                    {
                        case 1:
                            string MissionText = "Вы поднялись по горной тропе.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        default:
                            Console.WriteLine("GenerateSituation - Case Def");
                            MissionText = "Вы поднялись по горной тропе.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
                #region Хорошие события - локация "Лагерь"
                if (EventID == 4)
                {
                    Console.WriteLine("GenerateSituation - EventID == 4");
                    switch (GenerateChanceGood_EventID4) // Генерация события для лагеря - хорошие
                    {
                        case 1:
                            string MissionText = "Вы пришли в лагерь.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        default:
                            MissionText = "Вы пришли в лагерь.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
                #region Хорошие события - локация "Дороги"
                if (EventID == 5)
                {
                    Console.WriteLine("GenerateSituation - EventID == 5");
                    switch (GenerateChanceGood_EventID5) // Генерация события для дороги - хорошие
                    {
                        case 1:
                            string MissionText = "Вы вышли на дорогу.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        
                        default:
                            Console.WriteLine("GenerateSituation - Case Def");
                            MissionText = "Вы вышли на дорогу.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
                #region Хорошие события - локация "Здание"
                if (EventID == 6)
                {
                    Console.WriteLine("GenerateSituation - EventID == 6");
                    switch (GenerateChanceGood_EventID6) // Генерация события для здания - хорошие
                    {
                        case 1:
                            string MissionText = "Вы зашли в здание.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;

                        default:
                            Console.WriteLine("GenerateSituation - Case Def");
                            MissionText = "Вы зашли в здание.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
            }
            else if (SituationCondition == "Bad")
            {
                Console.WriteLine("GenerateSituation - SituationCondition == Bad");
                #region Плохие события - локация "Поселение"
                if (EventID == 1) // Поселение
                {
                    switch (GenerateChanceBad_EventID1)
                    {
                        case 1:
                            string MissionText = "Вы зашли в поселение.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        default:
                            MissionText = "Вы зашли в поселение.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
                #region Плохие события - локация "Лес"
                if (EventID == 2)
                {
                    Console.WriteLine("GenerateSituation - EventID == 2");
                    switch (GenerateChanceBad_EventID2) // Генерация события для леса - плохие
                    {
                        case 1:
                            string MissionText = "Вы зашли в лес.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        
                        default:
                            Console.WriteLine("GenerateSituation - Case Def");
                            MissionText = "Вы зашли в лес.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
                #region Плохие события - локация "Горы"
                if (EventID == 3)
                {
                    Console.WriteLine("GenerateSituation - EventID == 3");
                    switch (GenerateChanceBad_EventID3) // Генерация события для гор - плохие
                    {
                        case 1:
                            string MissionText = "Вы поднялись по горной тропе.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        
                        default:
                            MissionText = "Вы поднялись по горной тропе.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
                #region Плохие события - локация "Лагерь"
                if (EventID == 4)
                {
                    Console.WriteLine("GenerateSituation - EventID == 4");
                    switch (GenerateChanceBad_EventID4) // Генерация события для лагеря - плохие
                    {
                        case 1:
                            string MissionText = "Вы зашли в лагерь.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        
                        default:
                            MissionText = "Вы зашли в лагерь.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
                #region Плохие события - локация "Дорога"
                if (EventID == 5)
                {
                    Console.WriteLine("GenerateSituation - EventID == 5");
                    switch (GenerateChanceBad_EventID5) // Генерация события для дороги - плохие
                    {
                        case 1:
                            string MissionText = "Вы вышли на дорогу.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                        
                        default:
                            MissionText = "Вы вышли на дорогу.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
                #region Плохие события - локация "Здание"
                if (EventID == 6)
                {
                    Console.WriteLine("GenerateSituation - EventID == 6");
                    switch (GenerateChanceBad_EventID6) // Генерация события для здания - плохие
                    {
                        case 1:
                            string MissionText = "Вы зашли в здание.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;

                        default:
                            MissionText = "Вы зашли в здание.";
                            Telegram.Bot.Examples.WebHook.Message.Send(AccountID, MissionText + "\n\nПолучено: \nОпыт: ✨" + RndmExp, 1);
                            Account.AddData(AccountID, "Exp", Convert.ToString(RndmExp));
                            break;
                    }
                }
                #endregion
            }
        }


        public static void GenerateSituationAfterPlayerMove(int AccountID, int EventID, string Message)
        {
            Account.MinusData(AccountID, "Energy", "1");
            int X = Convert.ToInt32(Account.GetSingleData(AccountID, "X"));
            int Y = Convert.ToInt32(Account.GetSingleData(AccountID, "Y"));
            int pY = Convert.ToInt32(Account.GetSingleData(AccountID, "planetY"));
            int pX = Convert.ToInt32(Account.GetSingleData(AccountID, "planetX"));

            string isMapExists = IsPlanetMapCoordExists(Convert.ToInt32(X), Convert.ToInt32(Y), pX, pY);
            if (isMapExists != "") // Если координаты найдены
            {
                int LocationDanger = Convert.ToInt32(GetPlanetMapSingleData(X, Y, pX, pY, "LocationDanger"));
                int MapOnCooldown = Convert.ToInt32(GetPlanetMapSingleData(X, Y, pX, pY, "CooldownTime"));
                Console.WriteLine("GenerateSituationAfterShipMove - координаты существуют. LocationDanger: " + LocationDanger + " MapOnCooldown: " + MapOnCooldown);
                if (MapOnCooldown == 0) // Кулдаун с точки сброшен - если > 0 то точка недоступна для создания ситуации
                {

                    int GenerateChance = rndmzr.Next(1, 101);
                    if (LocationDanger == 0)
                    {
                        if (GenerateChance >= 1 && GenerateChance <= 98) // Положительный случай
                        {
                            GeneratePlanetSituation(AccountID, EventID, LocationDanger, "Good", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - GOOD");
                        }
                        else if (GenerateChance >= 99 && GenerateChance <= 100) // Отрицательный случай
                        {
                            GeneratePlanetSituation(AccountID, EventID, LocationDanger, "Bad", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - BAD");
                        }
                    }

                    else if (LocationDanger == 1)
                    {
                        if (GenerateChance >= 1 && GenerateChance <= 90) // Положительный случай
                        {

                            GeneratePlanetSituation(AccountID, EventID, LocationDanger, "Good", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - GOOD");
                        }
                        else if (GenerateChance >= 91 && GenerateChance <= 100) // Отрицательный случай
                        {
                            GeneratePlanetSituation(AccountID, EventID, LocationDanger, "Bad", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - BAD");
                        }
                    }
                    else if (LocationDanger == 2)
                    {
                        if (GenerateChance >= 1 && GenerateChance <= 80) // Положительный случай
                        {
                            GeneratePlanetSituation(AccountID, EventID, LocationDanger, "Good", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - GOOD");
                        }
                        else if (GenerateChance >= 81 && GenerateChance <= 100) // Отрицательный случай
                        {
                            GeneratePlanetSituation(AccountID, EventID, LocationDanger, "Bad", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - BAD");
                        }
                    }
                    else if (LocationDanger == 3)
                    {
                        if (GenerateChance >= 1 && GenerateChance <= 70) // Положительный случай
                        {
                            GeneratePlanetSituation(AccountID, EventID, LocationDanger, "Good", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - GOOD");
                        }
                        else if (GenerateChance >= 71 && GenerateChance <= 100) // Отрицательный случай
                        {
                            GeneratePlanetSituation(AccountID, EventID, LocationDanger, "Bad", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - BAD");
                        }
                    }
                    else if (LocationDanger == 4)
                    {
                        if (GenerateChance >= 1 && GenerateChance <= 60) // Положительный случай
                        {
                            GeneratePlanetSituation(AccountID, EventID, LocationDanger, "Good", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - GOOD");
                        }
                        else if (GenerateChance >= 61 && GenerateChance <= 100) // Отрицательный случай
                        {
                            GeneratePlanetSituation(AccountID, EventID, LocationDanger, "Bad", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - BAD");
                        }
                    }
                    else if (LocationDanger == 5)
                    {
                        if (GenerateChance >= 1 && GenerateChance <= 50) // Положительный случай
                        {
                            GeneratePlanetSituation(AccountID, EventID, LocationDanger, "Good", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - GOOD");
                        }
                        else if (GenerateChance >= 51 && GenerateChance <= 100) // Отрицательный случай
                        {
                            GeneratePlanetSituation(AccountID, EventID, LocationDanger, "Bad", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - BAD");
                        }
                    }

                    // Отправляем точку на кулдаун
                    int LocationType = Convert.ToInt32(GetPlanetMapSingleData(X, Y, pX, pY, "LocationType"));
                    if (LocationType == 1)
                    {
                        SetPlanetMapSingleData(X, Y, pX, pY, "CooldownTime", "12");
                    }
                    else if (LocationType == 2)
                    {
                        SetPlanetMapSingleData(X, Y, pX, pY, "CooldownTime", "1");
                    }
                    else if (LocationType == 3)
                    {
                        SetPlanetMapSingleData(X, Y, pX, pY, "CooldownTime", "12");
                    }
                    else if (LocationType == 4)
                    {
                        SetPlanetMapSingleData(X, Y, pX, pY, "CooldownTime", "12");
                    }
                    else if (LocationType == 5)
                    {
                        SetPlanetMapSingleData(X, Y, pX, pY, "CooldownTime", "6");
                    }
                    else if (LocationType == 6)
                    {
                        SetPlanetMapSingleData(X, Y, pX, pY, "CooldownTime", "6");
                    }

                    // Продолжить писать генерацию событий   
                }
                else
                {
                    Telegram.Bot.Examples.WebHook.Message.Send(Convert.ToInt32(AccountID), Message, 1);
                }
            }
            else
            {
                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, "ОШИБКА #30", 1);
            }

        }

        public static void GenerateSituationAfterShipMove(int AccountID, int EventID, string Message)
        {
            string ShipEnergyOverweight = Ships.Ship.GetSingleData(AccountID, "ShipEnergyOverweight");
            int ShipEnergyOverweightInt = 1 + Convert.ToInt32(ShipEnergyOverweight);
            Ships.Ship.MinusData(AccountID, "ShipEnergy", ShipEnergyOverweightInt.ToString());
            int X = Convert.ToInt32(Ships.Ship.GetSingleData(AccountID, "X"));
            int Y = Convert.ToInt32(Ships.Ship.GetSingleData(AccountID, "Y"));
            string isMapExists = IsMapCoordExists(Convert.ToInt32(X), Convert.ToInt32(Y));
            if (isMapExists != "") // Если координаты найдены
            {

                int LocationDanger = Convert.ToInt32(GetMapSingleData(X, Y, "LocationDanger"));
                int MapOnCooldown = Convert.ToInt32(GetMapSingleData(X, Y, "CooldownTime"));
                Console.WriteLine("GenerateSituationAfterShipMove - координаты существуют. LocationDanger: " + LocationDanger + " MapOnCooldown: " + MapOnCooldown);
                if (MapOnCooldown == 0) // Кулдаун с точки сброшен - если > 0 то точка недоступна для создания ситуации
                {

                    int GenerateChance = rndmzr.Next(1, 101);
                    if (LocationDanger == 0)
                    {
                        if (GenerateChance >= 1 && GenerateChance <= 98) // Положительный случай
                        {
                            GenerateSituation(AccountID, EventID, LocationDanger, "Good", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - GOOD");
                        }
                        else if (GenerateChance >= 99 && GenerateChance <= 100) // Отрицательный случай
                        {
                            GenerateSituation(AccountID, EventID, LocationDanger, "Bad", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - BAD");
                        }
                    }

                    else if (LocationDanger == 1)
                    {
                        if (GenerateChance >= 1 && GenerateChance <= 90) // Положительный случай
                        {

                            GenerateSituation(AccountID, EventID, LocationDanger, "Good", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - GOOD");
                        }
                        else if (GenerateChance >= 91 && GenerateChance <= 100) // Отрицательный случай
                        {
                            GenerateSituation(AccountID, EventID, LocationDanger, "Bad", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - BAD");
                        }
                    }
                    else if (LocationDanger == 2)
                    {
                        if (GenerateChance >= 1 && GenerateChance <= 80) // Положительный случай
                        {
                            GenerateSituation(AccountID, EventID, LocationDanger, "Good", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - GOOD");
                        }
                        else if (GenerateChance >= 81 && GenerateChance <= 100) // Отрицательный случай
                        {
                            GenerateSituation(AccountID, EventID, LocationDanger, "Bad", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - BAD");
                        }
                    }
                    else if (LocationDanger == 3)
                    {
                        if (GenerateChance >= 1 && GenerateChance <= 70) // Положительный случай
                        {
                            GenerateSituation(AccountID, EventID, LocationDanger, "Good", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - GOOD");
                        }
                        else if (GenerateChance >= 71 && GenerateChance <= 100) // Отрицательный случай
                        {
                            GenerateSituation(AccountID, EventID, LocationDanger, "Bad", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - BAD");
                        }
                    }
                    else if (LocationDanger == 4)
                    {
                        if (GenerateChance >= 1 && GenerateChance <= 60) // Положительный случай
                        {
                            GenerateSituation(AccountID, EventID, LocationDanger, "Good", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - GOOD");
                        }
                        else if (GenerateChance >= 61 && GenerateChance <= 100) // Отрицательный случай
                        {
                            GenerateSituation(AccountID, EventID, LocationDanger, "Bad", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - BAD");
                        }
                    }
                    else if (LocationDanger == 5)
                    {
                        if (GenerateChance >= 1 && GenerateChance <= 50) // Положительный случай
                        {
                            GenerateSituation(AccountID, EventID, LocationDanger, "Good", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - GOOD");
                        }
                        else if (GenerateChance >= 51 && GenerateChance <= 100) // Отрицательный случай
                        {
                            GenerateSituation(AccountID, EventID, LocationDanger, "Bad", Message);
                            Console.WriteLine("GenerateSituation - EventID: " + EventID + " LocationDanger: " + LocationDanger + " - BAD");
                        }
                    }

                    // Отправляем точку на кулдаун
                    int LocationType = Convert.ToInt32(GetMapSingleData(X, Y, "LocationType"));
                    if (LocationType == 1)
                    {
                        SetMapSingleData(X, Y, "CooldownTime", "12");
                    }
                    else if (LocationType == 2)
                    {
                        SetMapSingleData(X, Y, "CooldownTime", "1");
                    }
                    else if (LocationType == 3)
                    {
                        SetMapSingleData(X, Y, "CooldownTime", "12");
                    }
                    else if (LocationType == 4)
                    {
                        SetMapSingleData(X, Y, "CooldownTime", "12");
                    }
                    else if (LocationType == 5)
                    {
                        SetMapSingleData(X, Y, "CooldownTime", "6");
                    }

                    // Продолжить писать генерацию событий   
                }
                else
                {
                    Telegram.Bot.Examples.WebHook.Message.Send(Convert.ToInt32(AccountID), Message, 1);
                }
            }
            else
            {
                Telegram.Bot.Examples.WebHook.Message.Send(AccountID, "ОШИБКА #30", 1);
            }

        }

        public static void GenerateMap(int EventID, int X, int Y, string LocationOwnerID, string LocationOwnerName, string Message)
        {
            Console.WriteLine("Инициализация функции GenerateMap. ");
            // Типы локаций: 1 - планета, 2 - Космос, 3 - Астероид, 4 - Искривление пространства, 5 - Космическая станция
            int LocationDanger = rndmzr.Next(0, 6);
            Console.WriteLine("Получаем сложность локации в LocationDanger: " + LocationDanger);
            int GenerateChance = rndmzr.Next(1, 101);
            Console.WriteLine("Получаем шанс для генерации GenerateChance: " + GenerateChance);
            if (GenerateChance <= 75)
            {
                // Космос
                EventID = 2;
                SetMapData(X, Y, 2, "Космос", LocationOwnerID, LocationOwnerName, LocationDanger, 0, 0);
                GenerateSituationAfterShipMove(Convert.ToInt32(LocationOwnerID), EventID, Message);
                Console.WriteLine("Создаем запись для координат " + X + ":" + Y + " с типом " + EventID + " (Космос) " + " для владельца: " + LocationOwnerName + " с уровнем опасности локации " + LocationDanger);
                Console.WriteLine("Генерируем ситуация после движения корабля для локации: Космос");
            }
            else if (GenerateChance >= 76 && GenerateChance <= 90)
            {
                int NormalIventsChance = rndmzr.Next(1, 101);
                if (NormalIventsChance >= 1 && NormalIventsChance <= 50)
                {
                    // Астероид
                    EventID = 3;
                    SetMapData(X, Y, 3, "Астероид", LocationOwnerID, LocationOwnerName, LocationDanger, 0, 0);
                    GenerateSituationAfterShipMove(Convert.ToInt32(LocationOwnerID), EventID, Message);
                    Console.WriteLine("Создаем запись для координат " + X + ":" + Y + " с типом " + EventID + " (Астероид) " + " для владельца: " + LocationOwnerName + " с уровнем опасности локации " + LocationDanger);
                    Console.WriteLine("Генерируем ситуация после движения корабля для локации: Астероид");
                }
                else if ((NormalIventsChance >= 51 && NormalIventsChance <= 100))
                {
                    EventID = 5;
                    SetMapData(X, Y, 5, "Космическая станция", LocationOwnerID, LocationOwnerName, LocationDanger, 0, 0);
                    GenerateSituationAfterShipMove(Convert.ToInt32(LocationOwnerID), EventID, Message);
                    Console.WriteLine("Создаем запись для координат " + X + ":" + Y + " с типом " + EventID + " (Космическая станция) " + " для владельца: " + LocationOwnerName + " с уровнем опасности локации " + LocationDanger);
                    Console.WriteLine("Генерируем ситуация после движения корабля для локации: Космическая станция");
                }

            }
            else if (GenerateChance >= 91 && GenerateChance <= 100)
            {
                int RareIventsChance = rndmzr.Next(1, 21);
                if (RareIventsChance >= 1 && RareIventsChance <= 5)
                {
                    // Планета
                    EventID = 1;
                    SetMapData(X, Y, 1, "Планета", LocationOwnerID, LocationOwnerName, LocationDanger, 0, 0);
                    GenerateSituationAfterShipMove(Convert.ToInt32(LocationOwnerID), EventID, Message);
                    Console.WriteLine("Создаем запись для координат " + X + ":" + Y + " с типом " + EventID + " (Планета) " + " для владельца: " + LocationOwnerName + " с уровнем опасности локации " + LocationDanger);
                    Console.WriteLine("Генерируем ситуация после движения корабля для локации: Планета");
                }
                else if ((RareIventsChance >= 6 && RareIventsChance <= 20))
                {
                    // Искривление пространства
                    EventID = 4;
                    SetMapData(X, Y, 4, "Искривление пространства", LocationOwnerID, LocationOwnerName, LocationDanger, 0, 0); // Не успевает генерироваться карта походу, смотреть, исправлять, ошибка
                    GenerateSituationAfterShipMove(Convert.ToInt32(LocationOwnerID), EventID, Message);
                    Console.WriteLine("Создаем запись для координат " + X + ":" + Y + " с типом " + EventID + " (Искривление пространства) " + " для владельца: " + LocationOwnerName + " с уровнем опасности локации " + LocationDanger);
                    Console.WriteLine("Генерируем ситуация после движения корабля для локации: Искривление пространства");
                }
            }
        }

        public static void GeneratePlanetMap(int EventID, int X, int Y, int planetX, int planetY, string LocationOwnerID, string LocationOwnerName, string Message)
        {
            Console.WriteLine("Инициализация функции GenerateMap. ");
            // Типы локаций: 1 - Поселение, 2 - Лес, 3 - Горы, 4 - Лагерь, 5 - Дорога, 6 - Здание
            int LocationDanger = rndmzr.Next(0, 6);
            Console.WriteLine("Получаем сложность локации в LocationDanger: " + LocationDanger);
            int GenerateChance = rndmzr.Next(1, 101);
            Console.WriteLine("Получаем шанс для генерации GenerateChance: " + GenerateChance);
            if (GenerateChance <= 75)
            {
                int NormalIventsChance = rndmzr.Next(1, 101);
                if (NormalIventsChance >= 1 && NormalIventsChance <= 50)
                {
                    // Лес
                    EventID = 2;
                    SetPlanetMapData(X, Y, planetX, planetY, 2, "Лес", LocationOwnerID, LocationOwnerName, LocationDanger, 0, 0);
                    GenerateSituationAfterPlayerMove(Convert.ToInt32(LocationOwnerID), EventID, Message);
                    Console.WriteLine("Создаем запись для координат " + X + ":" + Y + " с типом " + EventID + " (Лес) " + " для владельца: " + LocationOwnerName + " с уровнем опасности локации " + LocationDanger);
                    Console.WriteLine("Генерируем ситуация после движения корабля для локации: Лес");
                }
                else if ((NormalIventsChance >= 51 && NormalIventsChance <= 100))
                {
                    // Горы
                    EventID = 3;
                    SetPlanetMapData(X, Y, planetX, planetY, 3, "Горы", LocationOwnerID, LocationOwnerName, LocationDanger, 0, 0);
                    GenerateSituationAfterPlayerMove(Convert.ToInt32(LocationOwnerID), EventID, Message);
                    Console.WriteLine("Создаем запись для координат " + X + ":" + Y + " с типом " + EventID + " (Горы) " + " для владельца: " + LocationOwnerName + " с уровнем опасности локации " + LocationDanger);
                    Console.WriteLine("Генерируем ситуация после движения корабля для локации: Горы");
                }
            }
            else if (GenerateChance >= 76 && GenerateChance <= 90)
            {
                int NormalIventsChance = rndmzr.Next(1, 101);
                if (NormalIventsChance >= 1 && NormalIventsChance <= 50)
                {
                    // Дорога
                    EventID = 5;
                    SetPlanetMapData(X, Y, planetX, planetY, 5, "Дорога", LocationOwnerID, LocationOwnerName, LocationDanger, 0, 0);
                    GenerateSituationAfterPlayerMove(Convert.ToInt32(LocationOwnerID), EventID, Message);
                    Console.WriteLine("Создаем запись для координат " + X + ":" + Y + " с типом " + EventID + " (Дорога) " + " для владельца: " + LocationOwnerName + " с уровнем опасности локации " + LocationDanger);
                    Console.WriteLine("Генерируем ситуация после движения корабля для локации: Дорога");
                }
                else if ((NormalIventsChance >= 51 && NormalIventsChance <= 100))
                {
                    // Здание
                    EventID = 6;
                    SetPlanetMapData(X, Y, planetX, planetY, 6, "Здание", LocationOwnerID, LocationOwnerName, LocationDanger, 0, 0);
                    GenerateSituationAfterPlayerMove(Convert.ToInt32(LocationOwnerID), EventID, Message);
                    Console.WriteLine("Создаем запись для координат " + X + ":" + Y + " с типом " + EventID + " (Здание) " + " для владельца: " + LocationOwnerName + " с уровнем опасности локации " + LocationDanger);
                    Console.WriteLine("Генерируем ситуация после движения корабля для локации: Здание");
                }

            }
            else if (GenerateChance >= 91 && GenerateChance <= 100)
            {
                int RareIventsChance = rndmzr.Next(1, 21);
                if (RareIventsChance >= 1 && RareIventsChance <= 5)
                {
                    // Поселение
                    EventID = 1;
                    SetPlanetMapData(X, Y, planetX, planetY, 1, "Поселение", LocationOwnerID, LocationOwnerName, LocationDanger, 0, 0);
                    GenerateSituationAfterPlayerMove(Convert.ToInt32(LocationOwnerID), EventID, Message);
                    Console.WriteLine("Создаем запись для координат " + X + ":" + Y + " с типом " + EventID + " (Поселение) " + " для владельца: " + LocationOwnerName + " с уровнем опасности локации " + LocationDanger);
                    Console.WriteLine("Генерируем ситуация после движения корабля для локации: Поселение");
                }
                else if ((RareIventsChance >= 6 && RareIventsChance <= 20))
                {
                    // Лагерь
                    EventID = 4;
                    SetPlanetMapData(X, Y, planetX, planetY, 4, "Лагерь", LocationOwnerID, LocationOwnerName, LocationDanger, 0, 0); // Не успевает генерироваться карта походу, смотреть, исправлять, ошибка
                    GenerateSituationAfterPlayerMove(Convert.ToInt32(LocationOwnerID), EventID, Message);
                    Console.WriteLine("Создаем запись для координат " + X + ":" + Y + " с типом " + EventID + " (Лагерь) " + " для владельца: " + LocationOwnerName + " с уровнем опасности локации " + LocationDanger);
                    Console.WriteLine("Генерируем ситуация после движения корабля для локации: Лагерь");
                }
            }
        }

        #region SendMapMoveShipEventResult - функция отправки результатов ивента. Здесь нужна генерация точек на карте, если точки пустые
        public static void SendMapMoveShipEventResult(string AccountID, string EventID)
        {
            string MessageToAll = string.Empty; // Переменная для сохранения текста о том, куда игрок переместился
            if (EventID == "1")
            {
                Ships.Ship.AddData(Convert.ToInt32(AccountID), "Y", "1");
            }
            else if (EventID == "2")
            {
                Ships.Ship.AddData(Convert.ToInt32(AccountID), "Y", "-1");
            }
            else if (EventID == "3")
            {
                Ships.Ship.AddData(Convert.ToInt32(AccountID), "X", "-1");
            }
            else if (EventID == "4")
            {
                Ships.Ship.AddData(Convert.ToInt32(AccountID), "X", "1");
            }

            string X = Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "X"); // Получение координат корабля
            string Y = Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "Y");
            string isMapExists = IsMapCoordExists(Convert.ToInt32(X), Convert.ToInt32(Y)); // Получаем ID записи с точкой координат
            Console.WriteLine("Параметр isMapExists отдал значение: " + isMapExists);
            if (isMapExists == "") // Если координат не существует 
            {
                Console.WriteLine("Координат не существует - инициализируем генерацию.");
                string PlayerName = Account.GetSingleData(Convert.ToInt32(AccountID), "CharName"); // Получаем имя игрока
                if (EventID == "1")
                {
                    MessageToAll = "<b>Вы переместились севернее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    //Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("EventID == 1");
                }
                if (EventID == "2")
                {
                    MessageToAll = "<b>Вы переместились южнее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    //Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("EventID == 2");
                }
                if (EventID == "3")
                {
                    MessageToAll = "<b>Вы переместились западнее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    //Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("EventID == 3");
                }
                if (EventID == "4")
                {
                    MessageToAll = "<b>Вы переместились восточнее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    //Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("EventID == 4");
                }
                GenerateMap(Convert.ToInt32(EventID), Convert.ToInt32(X), Convert.ToInt32(Y), AccountID, PlayerName, MessageToAll); // Генерируем запись для точки координат, т.к. у нас её не существует
                Console.WriteLine("Генерируем запись для точки координат. X: " + X + " Y: " + Y + "Выполняем GenerateMap");
                //string NewEventID = GetMapSingleData(Convert.ToInt32(X), Convert.ToInt32(Y), "LocationType");
                //if (NewEventID == "1")
                //{
                //    GenerateSituationAfterPlayerMove(Convert.ToInt32(AccountID), 1);
                //}
                //if (NewEventID == "2")
                //{
                //    GenerateSituationAfterPlayerMove(Convert.ToInt32(AccountID), 2);
                //}
                //   Message.SendToAllByShip(Convert.ToInt32(ShipID), MessageToAll);
            }
            else // Если координаты существуют
            {
                Console.WriteLine("Координаты существуют. Продолжаем.");
                if (EventID == "1")
                {
                    MessageToAll = "<b>Вы переместились севернее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    // Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("Выполняется условие If (EventID == 1)");
                }
                if (EventID == "2")
                {
                    MessageToAll = "<b>Вы переместились южнее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    // Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("Выполняется условие If (EventID == 2)");
                }
                if (EventID == "3")
                {
                    MessageToAll = "<b>Вы переместились западнее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    // Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("Выполняется условие If (EventID == 3)");
                }
                if (EventID == "4")
                {
                    MessageToAll = "<b>Вы переместились восточнее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    //Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("Выполняется условие If (EventID == 4)");
                }
                // Подключение локаций

                string NewEventID = GetMapSingleData(Convert.ToInt32(X), Convert.ToInt32(Y), "LocationType"); // Получаем тип локации в которой находимся в NewEventID.
                Console.WriteLine("Получаем тип локации в которой находимся.");
                Console.WriteLine("Тип локации в NewEventID: " + NewEventID);
                if (NewEventID == "1")
                {
                    Console.WriteLine("Выполняется условие If (NewEventID == 1)");
                    GenerateSituationAfterShipMove(Convert.ToInt32(AccountID), 1, MessageToAll);
                }
                if (NewEventID == "2")
                {
                    Console.WriteLine("Выполняется условие If (NewEventID == 2)");
                    GenerateSituationAfterShipMove(Convert.ToInt32(AccountID), 2, MessageToAll);
                }
                if (NewEventID == "3")
                {
                    Console.WriteLine("Выполняется условие If (NewEventID == 3)");
                    GenerateSituationAfterShipMove(Convert.ToInt32(AccountID), 3, MessageToAll);
                }
                if (NewEventID == "4")
                {
                    Console.WriteLine("Выполняется условие If (NewEventID == 4)");
                    GenerateSituationAfterShipMove(Convert.ToInt32(AccountID), 4, MessageToAll);
                }
                if (NewEventID == "5")
                {
                    Console.WriteLine("Выполняется условие If (NewEventID == 5)");
                    GenerateSituationAfterShipMove(Convert.ToInt32(AccountID), 5, MessageToAll);
                }

            }
        }
        #endregion

        public static void SendPlanetMapMoveEventResult(string AccountID, string EventID)
        {
            string MessageToAll = string.Empty; // Переменная для сохранения текста о том, куда игрок переместился
            if (EventID == "1")
            {
                Account.AddData(Convert.ToInt32(AccountID), "Y", "1");
            }
            else if (EventID == "2")
            {
                Account.AddData(Convert.ToInt32(AccountID), "Y", "-1");
            }
            else if (EventID == "3")
            {
                Account.AddData(Convert.ToInt32(AccountID), "X", "-1");
            }
            else if (EventID == "4")
            {
                Account.AddData(Convert.ToInt32(AccountID), "X", "1");
            }

            string X = Account.GetSingleData(Convert.ToInt32(AccountID), "X"); // Получение координат корабля
            string Y = Account.GetSingleData(Convert.ToInt32(AccountID), "Y");
            string pX = Account.GetSingleData(Convert.ToInt32(AccountID), "planetX");
            string pY = Account.GetSingleData(Convert.ToInt32(AccountID), "planetY");

            string isMapExists = IsPlanetMapCoordExists(Convert.ToInt32(X), Convert.ToInt32(Y), Convert.ToInt32(pX), Convert.ToInt32(pY));
            Console.WriteLine("Параметр isMapExists отдал значение: " + isMapExists);
            if (isMapExists == "") // Если координат не существует 
            {
                Console.WriteLine("Координат не существует - инициализируем генерацию.");
                string PlayerName = Account.GetSingleData(Convert.ToInt32(AccountID), "CharName"); // Получаем имя игрока
                if (EventID == "1")
                {
                    MessageToAll = "<b>Вы переместились севернее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    //Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("EventID == 1");
                }
                if (EventID == "2")
                {
                    MessageToAll = "<b>Вы переместились южнее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    //Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("EventID == 2");
                }
                if (EventID == "3")
                {
                    MessageToAll = "<b>Вы переместились западнее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    //Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("EventID == 3");
                }
                if (EventID == "4")
                {
                    MessageToAll = "<b>Вы переместились восточнее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    //Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("EventID == 4");
                }
                GeneratePlanetMap(Convert.ToInt32(EventID), Convert.ToInt32(X), Convert.ToInt32(Y), Convert.ToInt32(pX), Convert.ToInt32(pY), AccountID, PlayerName, MessageToAll); // Генерируем запись для точки координат, т.к. у нас её не существует
                Console.WriteLine("Генерируем запись для точки координат. X: " + X + " Y: " + Y + "Выполняем GenerateMap");
            }
            else // Если координаты существуют
            {
                Console.WriteLine("Координаты существуют. Продолжаем.");
                if (EventID == "1")
                {
                    MessageToAll = "<b>Вы переместились севернее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    // Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("Выполняется условие If (EventID == 1)");
                }
                if (EventID == "2")
                {
                    MessageToAll = "<b>Вы переместились южнее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    // Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("Выполняется условие If (EventID == 2)");
                }
                if (EventID == "3")
                {
                    MessageToAll = "<b>Вы переместились западнее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    // Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("Выполняется условие If (EventID == 3)");
                }
                if (EventID == "4")
                {
                    MessageToAll = "<b>Вы переместились восточнее.</b>\n\n";
                    Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    //Message.Send(Convert.ToInt32(AccountID), MessageToAll, 1);
                    Console.WriteLine("Выполняется условие If (EventID == 4)");
                }
                // Подключение локаций

                string NewEventID = GetPlanetMapSingleData(Convert.ToInt32(X), Convert.ToInt32(Y), Convert.ToInt32(pX), Convert.ToInt32(pY), "LocationType"); // Получаем тип локации в которой находимся в NewEventID.
                Console.WriteLine("Получаем тип локации в которой находимся.");
                Console.WriteLine("Тип локации в NewEventID: " + NewEventID);
                if (NewEventID == "1")
                {
                    Console.WriteLine("Выполняется условие If (NewEventID == 1)");
                    GenerateSituationAfterPlayerMove(Convert.ToInt32(AccountID), 1, MessageToAll);
                }
                if (NewEventID == "2")
                {
                    Console.WriteLine("Выполняется условие If (NewEventID == 2)");
                    GenerateSituationAfterPlayerMove(Convert.ToInt32(AccountID), 2, MessageToAll);
                }
                if (NewEventID == "3")
                {
                    Console.WriteLine("Выполняется условие If (NewEventID == 3)");
                    GenerateSituationAfterPlayerMove(Convert.ToInt32(AccountID), 3, MessageToAll);
                }
                if (NewEventID == "4")
                {
                    Console.WriteLine("Выполняется условие If (NewEventID == 4)");
                    GenerateSituationAfterPlayerMove(Convert.ToInt32(AccountID), 4, MessageToAll);
                }
                if (NewEventID == "5")
                {
                    Console.WriteLine("Выполняется условие If (NewEventID == 5)");
                    GenerateSituationAfterPlayerMove(Convert.ToInt32(AccountID), 5, MessageToAll);
                }
                if (NewEventID == "6")
                {
                    Console.WriteLine("Выполняется условие If (NewEventID == 6)");
                    GenerateSituationAfterPlayerMove(Convert.ToInt32(AccountID), 6, MessageToAll);
                }

            }
        }

        #region PlayerSendEventResult(string AccountID, string EventID) - Автоответчик для ивентов в зависимости от EventID
        public static void PlayerSendEventResult(string AccountID, string EventID, string Tech)
        {
            Console.WriteLine("AAAAAAAAAAA: PlayerSendEventResult");
            int Item1Chance = 70;
            int Item2Chance = 70;
            int Item3Chance = 60;
            int Item4Chance = 60;
            switch (Convert.ToInt32(EventID))
            {
                case 0: // Ивент Revive
                    if (Account.GetSingleData(Convert.ToInt32(AccountID), "Status") == "666")
                    {
                        string MaxHealthP = Account.GetSingleData(Convert.ToInt32(AccountID), "MaxHealth");
                        string MaxHealthS = Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "ShipMaxHealth");
                        Ships.Ship.SetSingleData(Convert.ToInt32(AccountID), "ShipHealth", MaxHealthS);
                        Account.SetSingleData(Convert.ToInt32(AccountID), "Health", MaxHealthP);
                        int ShipSlotConnect = rndmzr.Next(1, 35);
                        Message.Send(Convert.ToInt32(AccountID), "Вы очнулись в медицинском отсеке крейсера страховой компании. Добро пожаловать в мир живых! Ваш корабль ждёт вас у шлюза номер " + ShipSlotConnect + ".", 1);
                        Account.SetSingleData(Convert.ToInt32(AccountID), "Status", "0");
                        Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                        Ships.Ship.SetSingleData(Convert.ToInt32(AccountID), "X", "0");
                        Ships.Ship.SetSingleData(Convert.ToInt32(AccountID), "Y", "0");
                    }
                    break;

                case 1: // Ивент "Ремонт"
                    Console.WriteLine("AAAAAAAAAAA: Ivent Repairing...");
                    string MechanikID = NPC.ProfessionExists(Convert.ToInt32(AccountID), 4);
                    if (MechanikID != "")
                    {
                        Console.WriteLine("AAAAAAAAAAA: Meh found");
                        NPC.GetAllInfoByNPCID(Convert.ToInt32(MechanikID), out string ReturnFirstName, out string ReturnLastName, out string ReturnUpgradeFocus1, out string ReturnUpgradeFocus2,
                            out string ReturnStrength, out string ReturnEndurance, out string ReturnCharisma,
                            out string ReturnIntelligence, out string ReturnAgility, out string ReturnLuck,
                            out string ReturnExp, out string ReturnLevel,
                            out string ReturnApperception, out string ReturnCandor, out string ReturnVivacity,
                            out string ReturnCoordination, out string ReturnMeekness, out string ReturnHumility,
                            out string ReturnCruelty, out string ReturnSelfpreservation, out string ReturnPatience,
                            out string ReturnDecisiveness, out string ReturnImagination, out string ReturnCuriosity,
                            out string ReturnAggression, out string ReturnLoyalty, out string ReturnEmpathy,
                            out string ReturnTenacity, out string ReturnCourage, out string ReturnSensuality,
                            out string ReturnCharm, out string ReturnHumor, out string ReturnProfession);
                        Console.WriteLine("AAAAAAAAAAA: 1");
                        string ShipMaxHealth = Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "ShipMaxHealth");
                        int HPRestoreToShip = Convert.ToInt32(ReturnIntelligence) * 3;
                        Console.WriteLine("AAAAAAAAAAA: 2");
                        int AdditionalPercentHP2Restore = 0;
                        if (Convert.ToInt32(ReturnIntelligence) <= 5)
                        {
                            AdditionalPercentHP2Restore = AdditionalPercentHP2Restore + 3;
                        }
                        else if (Convert.ToInt32(ReturnIntelligence) > 5 && Convert.ToInt32(ReturnIntelligence) <= 10)
                        {
                            AdditionalPercentHP2Restore = AdditionalPercentHP2Restore + 6;
                        }
                        else if (Convert.ToInt32(ReturnIntelligence) > 10)
                        {
                            AdditionalPercentHP2Restore = AdditionalPercentHP2Restore + 10;
                        }
                        Console.WriteLine("AAAAAAAAAAA: 3");
                        int FullHP = ProcentResultCount(Convert.ToInt32(ShipMaxHealth), 15 + AdditionalPercentHP2Restore);
                        Console.WriteLine("AAAAAAAAAAA: 4");
                        int Health2Restore = Ships.Ship.AddHealth(Convert.ToInt32(AccountID), FullHP);
                        Console.WriteLine("AAAAAAAAAAA: 5");
                        Message.Send(Convert.ToInt32(AccountID), "Ваш 👨‍🚀 Техник-механик " + ReturnFirstName + " " + ReturnLastName + " закончил ремонт корабля. Восстановлено: ❤️ " + Health2Restore + ".", 1);
                        Account.SetSingleData(Convert.ToInt32(AccountID), "CurrentAction", "0");
                    }
                    else
                    {
                        Message.Send(Convert.ToInt32(AccountID), "Кажется, вашего Техника-механика выкинуло в открытый космос. Без скафандра.", 1);
                    }
                    break;

                case 2: // Ивент "Плакательная"

                    int RandomEvent = rndmzr.Next(1, 17);
                    switch (RandomEvent)
                    {
                        case 1:
                            Message.Send(Convert.ToInt32(AccountID), "Вы открыли дверь 💦Плакательной и хотели было зайти во внутрь, но увидели странную картину. За столом в каюте сидят Боцман и Томпинамбур. На столе несколько початых бутылок рома и кажется дорогой Сумрачный портвейн. Они опрокидывают в себя стакан за стаканом, занюхивают чем-то, подозрительно напоминающим лук-порей, только синего цвета. Вы в изумлении закрыли дверь, не решась побеспокоить странную компанию.", 1);
                            break;
                        case 2:
                            Random rndGold = new Random();
                            int rndGoldNext = rndmzr.Next(2, 6);
                            Account.SetGold(Convert.ToInt32(AccountID), rndGoldNext, false);
                            Message.Send(Convert.ToInt32(AccountID), "Вы вошли в 💦Плакательную, но подскользнулись и упали. Лежа на полу и созерцая первозданный смысл бытия, вы осознали всю ничтожность своих проблем в общем и себя в целом в частности. А еще нашли несколько монеток!\n\n Вы получили: \n💳Деньги: " + rndGoldNext, 1);
                            break;
                        case 3:
                            Message.Send(Convert.ToInt32(AccountID), "Вы вошли в 💦Плакательную, сели за стол и начали плакать. Не знаю, чего вы ожидали входя сюда.", 1);
                            break;
                        case 4:
                            Message.Send(Convert.ToInt32(AccountID), "Дверь в 💦Плакательную закрыта. Вы грустно подождали несколько минут возле неё. Пришел капитан, по-отечески приобнял вас и увел драить палубу. Хороший у нас капитан!", 1);
                            break;
                        case 5:
                            rndGold = new Random();
                            rndGoldNext = rndmzr.Next(5, 17);
                            Account.SetGold(Convert.ToInt32(AccountID), rndGoldNext, false);
                            Message.Send(Convert.ToInt32(AccountID), "Вы вошли в 💦Плакательную. В темноте помещения вы видите силуэт молодой девушки, которая сгорбилась над столом. Краем уха вы слышите приглушенные рыдания и всхлипывания. Вы кашлянули, попытавшись привлечь внимание девушки. Бесполезно. Вы подошли ближе. Волосы закрывают лицо девушки. На столе вы увидели мешочек с монетами и не стесняясь забрали его себе. Вдруг, корабль начало трясти в такт с её рыданиями. Вы в ужасе выбежали из 💦Плакательной захлопнув дверь и придавив ею извивающиеся щупальца. Вы больше не хотите плакать.\n\n Вы получили: \n💳Деньги: " + rndGoldNext, 1);
                            break;
                        case 6:
                            Message.Send(Convert.ToInt32(AccountID), "Зайдя в 💦Плакательную, ты в кромешной тьме споткнулся об чье-то лежащее на полу тело. Ты услышал как покатилась бутылка. Не успев подумать, что на корабле кто-то умер и испугаться, как снизу забормотали: \n- Сейчас...я на минуточку глаза закрою и все...хрр...досчитаю...хрр...осталось пять крыс...хррр...дежурство...калий...хррр. \nОсторожно, стараясь не разбудить лежащего на полу, ты вышел из плакательной.", 1);
                            break;
                        case 7:
                            Message.Send(Convert.ToInt32(AccountID), "В 💦Плакательной было темно и сыро. Ты сел на бочку в углу и только приготовился плакать, как из противоположного угла прозвучали гитарные аккорды. Ты как завороженный сидел и слушал, а потом приятный мужской голос запел. \nОн долго и красиво пел о любви, о битвах и потерях, о великих подвигах. Ты сам не заметил, как выплакал все глаза. Когда слез уже не осталось, ты молча встал и вышел.", 1);
                            break;
                        case 8:
                            Message.Send(Convert.ToInt32(AccountID), "Приоткрыв дверь 💦Плакательной ты поразился необычной обстановке. В посреди комнаты стоял массивный деревянный стол. На столе стояла старая масляная лампа, отбрасывающая уродливую сгорбленную тень на стену. \nЗа столом, заваленным свитками, сидела девушка. Внезапно, она с силой скомкала свиток, в котором еще секунду назад, что-то старательно писала и пробормотала себе под нос:\n- Все куплено, все через верха, продалась...У нас сроки горят, пора в печать, а сенат эту чушь не примет. Снова будут прилетать голуби с гневными отзывами...\nТут она повернулась и посмотрела прямо в твоё лицо. Тебя слегка испугали ее безумные глаза.\n- Знаешь, что мне недавно пришло с голубем? Вот, послушай!\nПорывшись в бумагах, она достала смятый лист, стряхнула с него голубиный помет, и прочитала:\n- Пафосное говно, отписка!\nВыкинув листы, девушка схватилась за голову и горестно вздохнула.\nПопятившись назад, ты молча закрыл за собой дверь. Поплакать так и не удалось.", 1);
                            break;
                        case 9:
                            Message.Send(Convert.ToInt32(AccountID), "Едва сдерживая слезы и ощущая, как пригорают штаны, ты ворвался в 💦Плакательную. Посреди комнаты на коврике сидел в позе лотоса красивый мужчина, в одних облегающем трико. Он жестом поманил тебя к себе. Когда ты сел рядом, мужчина ласково приобнял тебя и ты вдоволь выплакался на его огромных куполах...", 1);
                            break;
                        case 10:
                            Message.Send(Convert.ToInt32(AccountID), "Ощущая давящую тоску в области груди и непреодолимое желание порыдать, ты пошел в 💦Плакательную. Распахнув дверь ты застыл как вкопанный. \nПосреди комнаты стоял маленький жеребенок и пил воду. Тут и там лежали аккуратные кучки переработанного сена, а рядом с малышом стоял капитан и любовно начесывал ему гриву, приговаривая, что уж теперь то нашу атаку никому не перебить.\nСубординация не позволила тебе сказать всё, что ты думаешь. Ты закрыл дверь и вышел, решив поплакать в старом сундуке.", 1);
                            break;
                        case 11:
                            Message.Send(Convert.ToInt32(AccountID), "В 💦Плакательной, по древней пиратской традиции, царила полутьма. И когда ты вошёл, ты не сразу понял, что помещение кем-то занято. А когда увидел, было уже поздно. \nЗа столом сидел ОН, и неспешно точил свой Нарсил.\nГора мышц и суровый взгляд. Боевых шрамов больше, чем волос по всему телу (а их было ЗНАЧИТЕЛЬНОЕ количество). Под рубашкой пряталась татуировка оскаленной волчьей морды во всю грудь. Ты узнал человека сидящего за столом.Его боятся чужие, а свои уважают.\nОн заметил тебя и отложил меч. Мысленно составив завещание и отпустив сам себе все грехи, ты замер.Порывшись в стоящем рядом сундуке, он достал какой - то сверток и с коварной улыбкой кинул тебе.Каким чудом ты его поймал не знает никто.\n- Держи, боец, мой старый шлем, он не раз спасал мне жизнь. - Сказал человек с татуировкой. - И тебе он сослужит хорошую службу. \nПоблагодарив воина, ты, на ватных от гордости, восторга и страха ногах, вышел вон. Не до рыданий теперь с шапкой Триумфа.", 1);
                            break;
                        case 12:
                            Message.Send(Convert.ToInt32(AccountID), "Открыв дверь 💦Плакательной тебя чуть не сшиб с ног выводок хомячков. Оглушительный крик с отчетливыми скрипящими нотками КАКОЙ БЕС ТЕБЯ СЮДА ПРИТАЩИЛ, ЛОВИ ИХ БЫСТРО!! вывел тебя из ступора и гонка началась.\nСпустя два часа, когда все беглецы были пойманы, вы сели в 💦Плакательной и молча выпили рома из запасов Дроида.\nДроид появился на корабле после путешествия в Пустоши. Капитан выменял чуднОго металлического человека на бочонок отборного рома и двух молодых рабынь.\nОн оказался отличным воином, еды не требовал, только пил ром и регулярно просил купить ему машинное масло.Отсутствие необходимости набивать брюхо, а также повышенная, по сравнению с человеческой, скорость принятия решений, сделала Дроида идеальным кандидатом в хранители еды для пиратских питомцев.Он как раз пересчитывал и прятал запасы, когда ты вошел в 💦Плакательную.\nРаспив бочонок рома на двоих с Дроидом, ты отрубился, так и не поплакав.", 1);
                            break;
                        case 13:
                            Message.Send(Convert.ToInt32(AccountID), "Войдя в 💦Плакательную, ты услышал глухой стук. Обыскав всю комнату, ты остановился возле большого сундука, крышка которого была неплотно закрыта. \nРаспахнув сундук ты увидел женщину. Несанкционированная рыжая женщина на борту корабля возмутила тебя, а также вызвала какое - то доселе неизвестное ощущение.Не обращая внимания на ее ругань и брыкания, ты потащил её к команде чтоб решить, что с ней делать.\n\n- РЫЖУЮ ЗА БОРТ ОТ НЕЕ БУДУТ БЕДЫ!\n- НО ОНА ЖЕ КРАСИВАЯ, ЖАЛКО ЕЕ!\n- КРАСИВАЯ, КАК КОБЫЛА СИВАЯ, ЗА БОРТ, НА ***, НЕЧЕГО ЧУЖИМ ДЕЛАТЬ НА ПИРАТСКОМ КОРАБЛЕ!\n- ДА Я ЕЕ ЗНАЮ, ОТВАЛИТЕ, ОНА ХОРОШАЯ!\n\nНа крики и шум пришел капитан и быстро всех разогнал. Остался только ты, рыжая и капитан.\n\n- Иди, я с ней разберусь. - Сказал капитан и хитро улыбаясь увел рыжую в сторону своей каюты.\n\nА ты пошел в 💦Плакательную. Почему-то тебе стало еще грустнее.", 1);
                            break;
                        case 14:
                            Message.Send(Convert.ToInt32(AccountID), "В сильно расстроенных чувствах ты ворвался в 💦Плакательную, но получил в лицо женской рубашкой. Прямо перед тобой разворачивалась недвусмысленная сцена: мужчина и женщина страстно целуясь срывали друг с друга одежду. Воздух в 💦Плакательной был буквально наэлектризован. Умом ты понимал, что оставаться здесь неправильно и нужно уйти, но ты не мог пошевелиться и смотрел не отрываясь. \nМужчина страстно прижал женщину к стене, и ты увидел, как пара тентаклей обвилась вокруг его тела. А еще одна игриво щелкнула тебя по носу. \nТут у тебя сдали нервы и ты пошел драить палубу. Ибо нечего.", 1);
                            break;
                        case 15:
                            Message.Send(Convert.ToInt32(AccountID), "Ты решил сходить в плакательную перед серьезным боем, настроиться, так сказать. Открыв дверь тебя чуть не сшибло с ног каким то горшком. *Хоть бы не ночным* успел подумать ты, прежде чем невероятно милый голос, в котором ты признал одну из помощниц капитана, произнёс из темноты: \n- Ну почему опять битва ? Не хочу... \nЗаверив, что не выдашь место ее схрона ты пошел готовиться к битве.", 1);
                            break;
                        case 16:
                            Message.Send(Convert.ToInt32(AccountID), "Ты вошел в плакательную, устроился поудобнее и начал плакать. Вдруг резко распахнулась дверь. В проеме стояла светловолосая девушка-пират, из кармана у нее торчала хомячья голова, из небольшого мешка слегка рассыпалось зерно, а в руках она держала большой кусок мяса. \n- Дроид! ДРОИД! Ой, простите, а Дроида не видели? \nУслышав отрицательный ответ девушка потащила свой скарб дальше, в поисках Дроида. \nВспомнив, что у тебя в каюте есть кое-какие запасы ты решил отдать их, пока мясо не начало тухнуть.", 1);
                            break;
                    }
                    break;

                case 4: // Ивент "Изучение рецептов"
                    string RecipeID = Tech;
                    int RecipeLearnUpdate = 5;
                    bool RecipeLearnUpdated = Account.AddRecipeData(Convert.ToInt32(AccountID), RecipeID, "RecipeLevel", RecipeLearnUpdate.ToString());
                    if (RecipeLearnUpdated == true)
                    {
                        Message.Send(Convert.ToInt32(AccountID), "Фууух! Вы очень долго и нудно изучали содержимое чертежа и кое-что поняли.\nПолучено: + " + RecipeLearnUpdate + " к знанию чертежа.", 1);
                    }
                    else
                    {
                        Message.Send(Convert.ToInt32(AccountID), "Что-то пошло не так при изучении рецепта.", 1);
                    }
                    break;
                case 10: // Ивент "Посадка на планету"
                    Account.SetSingleData(Convert.ToInt32(AccountID), "onPlanet", "1");
                    string planetX = Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "X");
                    string planetY = Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "Y");
                    Account.SetSingleData(Convert.ToInt32(AccountID), "PlanetX", planetX);
                    Account.SetSingleData(Convert.ToInt32(AccountID), "PlanetY", planetY);
                    Message.Send(Convert.ToInt32(AccountID), "Вы совершили посадку на поверхность планеты.", 1);
                    break;

                case 20: // Ивент "Взлёт с планеты"
                    Account.SetSingleData(Convert.ToInt32(AccountID), "onPlanet", "0");
                    //planetX = Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "X");
                    //planetY = Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "Y");
                    //Account.SetSingleData(Convert.ToInt32(AccountID), "PlanetX", planetX);
                    //Account.SetSingleData(Convert.ToInt32(AccountID), "PlanetY", planetY);
                    Message.Send(Convert.ToInt32(AccountID), "Ваш корабль вышел на орбиту планеты.", 1);
                    break;
            }
        }
        #endregion

        #region SetEventTimer - Создание и настройка таймера для проверки окончания событий
        public static void SetEventTimer()
        {
            EventTimer = new System.Timers.Timer(30000);
            // Hook up the Elapsed event for the timer. 
            EventTimer.Elapsed += OnTimedEvent;
            EventTimer.AutoReset = true;
            EventTimer.Enabled = true;
        }
        #endregion

        public static void SetDonationTimer()
        {
            EventTimer = new System.Timers.Timer(3000);
            // Hook up the Elapsed event for the timer. 
            EventTimer.Elapsed += OnDonationTimer;
            EventTimer.AutoReset = true;
            EventTimer.Enabled = true;
        }

        public static void SetHealthRecoveryTimer()
        {
            HealthRecoveryTimer = new System.Timers.Timer(300000);
            // Hook up the Elapsed event for the timer. 
            HealthRecoveryTimer.Elapsed += OnAutoHealthRecovery;
            HealthRecoveryTimer.AutoReset = true;
            HealthRecoveryTimer.Enabled = true;
        }

        public static void SetMapMoveShipEventTimer()
        {
             MapMoveShipEventTimer = new System.Timers.Timer(1000);
            // Hook up the Elapsed event for the timer. 
            MapMoveShipEventTimer.Elapsed += OnMapMoveShipTimerEvent;
            MapMoveShipEventTimer.AutoReset = true;
            MapMoveShipEventTimer.Enabled = true;
        }

        public static void SetPlanetMapMoveEventTimer()
        {
            PlanetMapMoveEventTimer = new System.Timers.Timer(1000);
            // Hook up the Elapsed event for the timer. 
            PlanetMapMoveEventTimer.Elapsed += OnPlanetMapMoveTimerEvent;
            PlanetMapMoveEventTimer.AutoReset = true;
            PlanetMapMoveEventTimer.Enabled = true;
        }

        public static void SetWeatherTimer()
        {
            WeatherTimer = new System.Timers.Timer(43200000);
            // Hook up the Elapsed event for the timer. 
            WeatherTimer.Elapsed += OnWeatherTimerEvent;
            WeatherTimer.AutoReset = true;
            WeatherTimer.Enabled = true;
        }

        public static void SetBetweenMissionsTimer()
        {
            BetweenMissionsTimer = new System.Timers.Timer(10000);
            // Hook up the Elapsed event for the timer. 
            BetweenMissionsTimer.Elapsed += OnBetweenMissionsEvent;
            BetweenMissionsTimer.AutoReset = true;
            BetweenMissionsTimer.Enabled = true;
        }

        #region SetEnergyRecoveryTimer - Создание и настройка таймера для востановления энергии игрока
        public static void SetEnergyRecoveryTimer()
        {
            EnergyRecoveryTimer = new System.Timers.Timer(1800000);
            // Hook up the Elapsed event for the timer. 
            EnergyRecoveryTimer.Elapsed += OnEnergyRecoveryTimer;
            EnergyRecoveryTimer.AutoReset = true;
            EnergyRecoveryTimer.Enabled = true;
        }
        #endregion

        public static void SetNPCActionsTimer()
        {
            NPCActionsTimer = new System.Timers.Timer(600000);
            // Hook up the Elapsed event for the timer. 
            NPCActionsTimer.Elapsed += OnNPCActionsTimer;
            NPCActionsTimer.AutoReset = true;
            NPCActionsTimer.Enabled = true;
        }

        public static void SetNPCListRefreshTimer()
        {
            NPCListRefreshTimer = new System.Timers.Timer(3600000 * 24);
            // Hook up the Elapsed event for the timer. 
            NPCListRefreshTimer.Elapsed += OnNPCListRefreshTimer;
            NPCListRefreshTimer.AutoReset = true;
            NPCListRefreshTimer.Enabled = true;
        }

        #region SetEnergyRecoveryTimer - Создание и настройка таймера для востановления энергии корабля
        public static void SetShipEnergyRecoveryTimer()
        {
            ShipEnergyRecoveryTimer = new System.Timers.Timer(1800000);
            // Hook up the Elapsed event for the timer. 
            ShipEnergyRecoveryTimer.Elapsed += OnShipEnergyRecoveryTimer;
            ShipEnergyRecoveryTimer.AutoReset = true;
            ShipEnergyRecoveryTimer.Enabled = true;
        }
        #endregion

        #region MapCooldownTimer - Создание и настройка таймера для изменения значения кулдауна у точек на карте
        public static void SetMapCooldownTimer()
        {
            MapCooldownTimer = new System.Timers.Timer(600000);
            // Hook up the Elapsed event for the timer. 
            MapCooldownTimer.Elapsed += OnMapCooldownTimer;
            MapCooldownTimer.AutoReset = true;
            MapCooldownTimer.Enabled = true;
        }
        #endregion

        public static void SetPlanetMapCooldownTimer()
        {
            PlanetMapCooldownTimer = new System.Timers.Timer(600000);
            // Hook up the Elapsed event for the timer. 
            PlanetMapCooldownTimer.Elapsed += OnPlanetMapCooldownTimer;
            PlanetMapCooldownTimer.AutoReset = true;
            PlanetMapCooldownTimer.Enabled = true;
        }

        public static void SetAntiSpamTimer()
        {
            AntiSpamTimer = new System.Timers.Timer(1000);
            // Hook up the Elapsed event for the timer. 
            AntiSpamTimer.Elapsed += OnAntiSpamTimer;
            AntiSpamTimer.AutoReset = true;
            AntiSpamTimer.Enabled = true;
        }

        public static void SetBattleTimer()
        {
            BattleTimer = new System.Timers.Timer(1000);
            // Hook up the Elapsed event for the timer. 
            BattleTimer.Elapsed += OnBattleTimer;
            BattleTimer.AutoReset = true;
            BattleTimer.Enabled = true;
        }

        public static void SetLevelUpTimer()
        {
            LevelupTimer = new System.Timers.Timer(5000);
            // Hook up the Elapsed event for the timer. 
            LevelupTimer.Elapsed += OnLevelupTimer;
            LevelupTimer.AutoReset = false;
            LevelupTimer.Enabled = true;
        }

        #region OnTimedEvent - Если таймер срабатывает. В этой функции вызывается PlayerEventController(true)
        public static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("Event timer start works at {0:HH:mm:ss.fff}",
                            e.SignalTime);
            PlayerEventController(true);
        }
        #endregion

        public static void OnAutoHealthRecovery(Object source, ElapsedEventArgs e)
        {
            AutoHealthRecoveryController();
        }

        public static void OnNPCListRefreshTimer(Object source, ElapsedEventArgs e)
        {
            RefreshNPCListForAllPlayers();
        }

        public static void OnWeatherTimerEvent(Object source, ElapsedEventArgs e)
        {
            WeatherController();
        }

        public static void OnPlanetMapMoveTimerEvent(Object source, ElapsedEventArgs e)
        {
            PlanetMapMoveEventController();
        }

        public static void OnMapMoveShipTimerEvent(Object source, ElapsedEventArgs e)
        {
            MapMoveShipEventController();
        }

        public static void WeatherController()
        {
            int WeatherChance = rndmzr.Next(1, 101);
            if (WeatherChance >= 1 && WeatherChance <= 60)
            {
                GlobalWeather = rndmzr.Next(1, 4);
            }
            if (WeatherChance >= 61 && WeatherChance <= 90)
            {
                GlobalWeather = rndmzr.Next(4, 7);
            }
            if (WeatherChance >= 91 && WeatherChance <= 100)
            {
                GlobalWeather = rndmzr.Next(7, 10);
            }

            switch (GlobalWeather)
            {
                case 0:
                    GlobalWeatherName = "☀️ Солнечно";
                    break;
                case 1:
                    GlobalWeatherName = "☀️ Солнечно";
                    break;
                case 2:
                    GlobalWeatherName = "☁️ Облачно";
                    break;
                case 3:
                    GlobalWeatherName = "☁️ Облачно";
                    break;
                case 4:
                    GlobalWeatherName = "🌧 Дождь";
                    break;
                case 5:
                    GlobalWeatherName = "🌧 Ливень";
                    break;
                case 6:
                    GlobalWeatherName = "🌫 Туман";
                    break;
                case 7:
                    GlobalWeatherName = "⛈ Гроза";
                    break;
                case 8:
                    GlobalWeatherName = "🌊 Шторм";
                    break;
                case 9:
                    GlobalWeatherName = "🌪 Ураган";
                    break;
            }
        }

        public static object GetFile(string path)
        {
            throw new NotImplementedException();
        }

        #region OnEnergyRecoveryTimer - Если таймер на востановление энергии срабатывает.
        public static void OnEnergyRecoveryTimer(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("Energy recovery timer start works at {0:HH:mm:ss.fff}",
                              e.SignalTime);
            PlayerEnergyRecoveryController(true);
        }
        #endregion

        public static void OnNPCActionsTimer(Object source, ElapsedEventArgs e)
        {
            //   NPCActionsController();
        }

        public static void OnShipEnergyRecoveryTimer(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("Ship Energy recovery timer start works at {0:HH:mm:ss.fff}",
                              e.SignalTime);
            ShipEnergyRecoveryController(true);
        }

        #region OnMapCooldownTimer - Если таймер на востановление энергии срабатывает.
        public static void OnMapCooldownTimer(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("Map cooldown timer start works at {0:HH:mm:ss.fff}",
                              e.SignalTime);
            MapCooldownController();
        }

        #endregion

        public static void OnPlanetMapCooldownTimer(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("Map cooldown timer start works at {0:HH:mm:ss.fff}",
                              e.SignalTime);
            PlanetMapCooldownController();
        }

        public static void OnAntiSpamTimer(Object source, ElapsedEventArgs e)
        {
            AntiSpamController(true);
            EmissionEventController(true);

        }

        public static void OnBattleTimer(Object source, ElapsedEventArgs e)
        {
            // Console.WriteLine("Battle timer start works at {0:HH:mm:ss.fff}", e.SignalTime);
            BattleController();
        }

        public static void OnDonationTimer(Object source, ElapsedEventArgs e)
        {
            DonationController(true);
        }

        public static void OnBetweenMissionsEvent(Object source, ElapsedEventArgs e)
        {
            BetweenMissionsController(true);
        }

        public static void OnLevelupTimer(Object source, ElapsedEventArgs e)
        {
            LevelupController(true);
        }

        public static bool BetweenMissionsController(bool F)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID, EventID, StartTime, EndTime FROM on_between_events", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            while (thisReader.Read())
            {

                string AccountID = string.Empty;
                string StartTime = string.Empty;
                string EndTime = string.Empty;
                string EventID = string.Empty;
                AccountID += thisReader["AccountID"];
                StartTime += thisReader["StartTime"];
                EndTime += thisReader["EndTime"];
                EventID += thisReader["EventID"];
                // Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - BetweenMissionsController] For id: " + AccountID + " - from " + StartTime + " to " + EndTime);
                int CompareResult = DateTime.Compare((Convert.ToDateTime(EndTime)), DateTime.Now);
                if (CompareResult <= 0)
                {
                    PlayerDeleteBetweenEvent(Convert.ToInt32(AccountID));
                    // ЗДЕСЬ ДОЛЖНА БЫТЬ ОТПРАВКА НА МИССИЮ
                    PlayerSetEvent(Convert.ToInt32(AccountID), Convert.ToInt32(EventID), 5, 2);
                }

            }
            myConnection.Close();
            
            thisReader.Close();
            return F;
        }

        public static void MapCooldownController()
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT X, Y FROM map WHERE CooldownTime>0", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            while (thisReader.Read())
            {
                string X = string.Empty;
                string Y = string.Empty;
                X += thisReader["X"];
                Y += thisReader["Y"];

                myConnection = new MySqlConnection(Connect);
                MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
                string sql;

                sql = "UPDATE map SET CooldownTime = CooldownTime - 1 " + "WHERE X = " + X + " AND Y = " + Y + ";";
                MySqlScript script = new MySqlScript(myConnection, sql);
                myConnection.Open();
                
                script.Query = sql;
                script.Connection = myConnection;
                try
                {
                    script.Execute();

                }
                catch (MySqlException sqlEx)
                {
                    Console.WriteLine("ОШИБКА!: " + sqlEx);
                    myConnection.Close();
                    
                }
                
                myConnection.Close(); //Обязательно закрываем соединение!
            }
            
            myConnection.Close();
            thisReader.Close();
        }

        public static void PlanetMapCooldownController()
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT X, Y FROM map_planet WHERE CooldownTime>0", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            while (thisReader.Read())
            {
                string X = string.Empty;
                string Y = string.Empty;
                X += thisReader["X"];
                Y += thisReader["Y"];

                myConnection = new MySqlConnection(Connect);
                MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
                string sql;

                sql = "UPDATE map_planet SET CooldownTime = CooldownTime - 1 " + "WHERE X = " + X + " AND Y = " + Y + ";";
                MySqlScript script = new MySqlScript(myConnection, sql);
                myConnection.Open();

                script.Query = sql;
                script.Connection = myConnection;
                try
                {
                    script.Execute();

                }
                catch (MySqlException sqlEx)
                {
                    Console.WriteLine("ОШИБКА!: " + sqlEx);
                    myConnection.Close();

                }

                myConnection.Close(); //Обязательно закрываем соединение!
            }

            myConnection.Close();
            thisReader.Close();
        }

        public static void AutoHealthRecoveryController()
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID, Health, MaxHealth FROM users WHERE Health<MaxHealth", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            while (thisReader.Read())
            {
                string AccountID = string.Empty;
                string Health = string.Empty;
                string MaxHealth = string.Empty;
                AccountID += thisReader["AccountID"];
                Health += thisReader["Health"];
                MaxHealth += thisReader["MaxHealth"];
                string PlayerEndurance = Account.GetSingleData(Convert.ToInt32(AccountID), "Endurance");
                float HealthToRecovery = Convert.ToInt32(PlayerEndurance) * 0.85f;
                Console.WriteLine("DEBUG: HealthToRecovery = " + Convert.ToInt32(HealthToRecovery));
                int HealthToRecoveryFinal = Convert.ToInt32(HealthToRecovery);
                HealthToRecoveryFinal = HealthToRecoveryFinal + 3;
                Console.WriteLine("DEBUG: Add player health = " + HealthToRecoveryFinal);
                Account.AddHealth(Convert.ToInt32(AccountID), HealthToRecoveryFinal);
            }
            
            myConnection.Close();
            thisReader.Close();
        }

        //private static void NPCActionsController()
        //{
        //    // Пустая - 0
        //    // Старпом - 1
        //    // Суперкарго - 2
        //    // Навигатор - 3
        //    // Техник-механик - 4 
        //    // Тактик - 5
        //    // Стрелок - 6
        //    // Аналитик - 7 
        //    // Медик - 8
        //    // Пилот - 9
        //    string HireString = string.Empty;
        //    MySqlConnection myConnection = new MySqlConnection(Connect);
        //    myConnection.Open();
        //    MySqlCommand sql_commmand = new MySqlCommand("SELECT ID, FounderID, Profession FROM npc_data WHERE Hired = 1 AND (Profession = 4 OR Profession = 8)", myConnection);
        //    MySqlDataReader thisReader = sql_commmand.ExecuteReader();

        //    while (thisReader.Read())
        //    {
        //        string TempString = string.Empty;
        //        string ID = string.Empty;
        //        string FounderID = string.Empty;
        //        string Profession = string.Empty;
        //        ID += thisReader["ID"];
        //        FounderID += thisReader["FounderID"];
        //        Profession += thisReader["Profession"];

        //        if (Profession == "4")
        //        {
        //            NPC.GetAllInfoByNPCID(Convert.ToInt32(ID), out string ReturnFirstName, out string ReturnLastName, out string ReturnUpgradeFocus1, out string ReturnUpgradeFocus2,
        //                out string ReturnStrength, out string ReturnEndurance, out string ReturnCharisma,
        //                out string ReturnIntelligence, out string ReturnAgility, out string ReturnLuck,
        //                out string ReturnExp, out string ReturnLevel,
        //                out string ReturnApperception, out string ReturnCandor, out string ReturnVivacity,
        //                out string ReturnCoordination, out string ReturnMeekness, out string ReturnHumility,
        //                out string ReturnCruelty, out string ReturnSelfpreservation, out string ReturnPatience,
        //                out string ReturnDecisiveness, out string ReturnImagination, out string ReturnCuriosity,
        //                out string ReturnAggression, out string ReturnLoyalty, out string ReturnEmpathy,
        //                out string ReturnTenacity, out string ReturnCourage, out string ReturnSensuality,
        //                out string ReturnCharm, out string ReturnHumor, out string ReturnProfession);

        //            int HealthToRevive = Convert.ToInt32(Convert.ToInt32(ReturnIntelligence) * 0.8) +
        //                                 Convert.ToInt32(ReturnAgility);
        //            Ships.Ship.AddHealth(Convert.ToInt32(FounderID), HealthToRevive);
        //        }
        //        //TODO: Лечение капитана медиком

        //    }
        //    thisReader.Close();
        //    myConnection.Close();
        //}

        #region PlayerEnergyRecoveryController - контроллер востановления энергии. Вызывается по таймеру раз в час.
        public static bool PlayerEnergyRecoveryController(bool Finished)
        {

            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID, Energy, MaxEnergy FROM users WHERE Energy<MaxEnergy", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            while (thisReader.Read())
            {
                string AccountID = string.Empty;
                string Energy = string.Empty;
                string MaxEnergy = string.Empty;
                AccountID += thisReader["AccountID"];
                Energy += thisReader["Energy"];
                MaxEnergy += thisReader["MaxEnergy"];

                myConnection = new MySqlConnection(Connect);
                MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
                string sql;

                int NewEnergy = Convert.ToInt32(Energy) + 1;

                sql = "UPDATE users SET Energy =" + @"""" + NewEnergy.ToString() + @"""" + "WHERE AccountID = " + AccountID + ";";
                Console.WriteLine("[PlayerEnergyRecoveryController] Запрос к базе для аккаунта: " + AccountID.ToString() + " query - " + sql);
                MySqlScript script = new MySqlScript(myConnection, sql);
                myConnection.Open();
                
                script.Query = sql;
                script.Connection = myConnection;
                try
                {
                    script.Execute();

                }
                catch (MySqlException sqlEx)
                {
                    Console.WriteLine("ОШИБКА!: " + sqlEx);
                    myConnection.Close();
                    
                }
                if (Energy == MaxEnergy)
                {
                    Message.Send(Convert.ToInt32(AccountID), "Энергия восстановлена! Вы полны сил и готовы к приключениям!", 1);
                }
                
                myConnection.Close(); //Обязательно закрываем соединение!
            }
            
            myConnection.Close();
            thisReader.Close();
            return Finished;
        }
        #endregion

        public static bool ShipEnergyRecoveryController(bool Finished)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ShipID, ShipEnergy, ShipMaxEnergy FROM ships WHERE ShipEnergy<ShipMaxEnergy", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            while (thisReader.Read())
            {
                string ShipID = string.Empty;
                string ShipEnergy = string.Empty;
                string ShipMaxEnergy = string.Empty;
                ShipID += thisReader["ShipID"];
                ShipEnergy += thisReader["ShipEnergy"];
                ShipMaxEnergy += thisReader["ShipMaxEnergy"];

                myConnection = new MySqlConnection(Connect);
                MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
                string sql;

                int NewEnergy = Convert.ToInt32(ShipEnergy) + 1;

                sql = "UPDATE ships SET ShipEnergy =" + @"""" + NewEnergy.ToString() + @"""" + "WHERE ShipID = " + ShipID + ";";
                Console.WriteLine("[ShipEnergyRecoveryController] Запрос к базе для корабля: " + ShipID.ToString() + " query - " + sql);
                MySqlScript script = new MySqlScript(myConnection, sql);
                myConnection.Open();
                
                script.Query = sql;
                script.Connection = myConnection;
                try
                {
                    script.Execute();
                }
                catch (MySqlException sqlEx)
                {
                    Console.WriteLine("ОШИБКА!: " + sqlEx);
                    myConnection.Close();
                    
                }
                if (ShipEnergy == ShipMaxEnergy)
                {
                    Message.Send(Convert.ToInt32(ShipID), "<b>Уведомление:</b>\nЭнергетическое ядро корабля полностью заряжено.", 1);
                }
                
                myConnection.Close(); //Обязательно закрываем соединение!
            }
            
            myConnection.Close();
            thisReader.Close();
            return Finished;
        }

        public static bool LevelupController(bool Finished) // Хрень, переделать! #ИЗМЕНИТЬ #ДОДЕЛАТЬ
        {
            LevelupTimer.Enabled = false;
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID, Level, Exp FROM users", myConnection);
            MySqlDataReader levelReader = sql_commmand.ExecuteReader();

            while (levelReader.Read())
            {
                string Level = string.Empty;
                string Exp = string.Empty;
                string AccountID = string.Empty;
                Level += levelReader["Level"];
                Exp += levelReader["Exp"];
                AccountID += levelReader["AccountID"];
                if (Convert.ToInt32(Exp) > 1000 && Convert.ToInt32(Exp) <= 2000 && Convert.ToInt32(Level) == 1)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 2000 && Convert.ToInt32(Exp) <= 3000 && Convert.ToInt32(Level) == 2)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 3000 && Convert.ToInt32(Exp) <= 4000 && Convert.ToInt32(Level) == 3)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 4000 && Convert.ToInt32(Exp) <= 5000 && Convert.ToInt32(Level) == 4)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 5000 && Convert.ToInt32(Exp) <= 6000 && Convert.ToInt32(Level) == 5)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 6000 && Convert.ToInt32(Exp) <= 7000 && Convert.ToInt32(Level) == 6)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 7000 && Convert.ToInt32(Exp) <= 8000 && Convert.ToInt32(Level) == 7)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 8000 && Convert.ToInt32(Exp) <= 9000 && Convert.ToInt32(Level) == 8)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 9000 && Convert.ToInt32(Exp) <= 10000 && Convert.ToInt32(Level) == 9)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 10000 && Convert.ToInt32(Exp) <= 11000 && Convert.ToInt32(Level) == 10)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 11000 && Convert.ToInt32(Exp) <= 12000 && Convert.ToInt32(Level) == 11)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 12000 && Convert.ToInt32(Exp) <= 13000 && Convert.ToInt32(Level) == 12)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 13000 && Convert.ToInt32(Exp) <= 14000 && Convert.ToInt32(Level) == 13)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 14000 && Convert.ToInt32(Exp) <= 15000 && Convert.ToInt32(Level) == 14)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 15000 && Convert.ToInt32(Exp) <= 16000 && Convert.ToInt32(Level) == 15)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 16000 && Convert.ToInt32(Exp) <= 17000 && Convert.ToInt32(Level) == 16)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 17000 && Convert.ToInt32(Exp) <= 18000 && Convert.ToInt32(Level) == 17)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 18000 && Convert.ToInt32(Exp) <= 19000 && Convert.ToInt32(Level) == 18)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 19000 && Convert.ToInt32(Exp) <= 20000 && Convert.ToInt32(Level) == 19)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 20000 && Convert.ToInt32(Exp) <= 22000 && Convert.ToInt32(Level) == 20)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 22000 && Convert.ToInt32(Exp) <= 24000 && Convert.ToInt32(Level) == 21)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 24000 && Convert.ToInt32(Exp) <= 26000 && Convert.ToInt32(Level) == 22)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 26000 && Convert.ToInt32(Exp) <= 28000 && Convert.ToInt32(Level) == 23)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 28000 && Convert.ToInt32(Exp) <= 30000 && Convert.ToInt32(Level) == 24)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 30000 && Convert.ToInt32(Exp) <= 32000 && Convert.ToInt32(Level) == 25)
                {
                    Console.WriteLine("LEVEL DEBUG: Exp - " + Exp + " - 30000-32000");
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 32000 && Convert.ToInt32(Exp) <= 34000 && Convert.ToInt32(Level) == 26)
                {
                    Console.WriteLine("LEVEL DEBUG: Exp - " + Exp + " - 32000-34000");
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 34000 && Convert.ToInt32(Exp) <= 36000 && Convert.ToInt32(Level) == 27)
                {
                    Console.WriteLine("LEVEL DEBUG: Exp - " + Exp + " - 36000-38000");
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 36000 && Convert.ToInt32(Exp) <= 38000 && Convert.ToInt32(Level) == 28)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 38000 && Convert.ToInt32(Exp) <= 40000 && Convert.ToInt32(Level) == 29)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 40000 && Convert.ToInt32(Exp) <= 43000 && Convert.ToInt32(Level) == 30)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 43000 && Convert.ToInt32(Exp) <= 46000 && Convert.ToInt32(Level) == 31)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 46000 && Convert.ToInt32(Exp) <= 49000 && Convert.ToInt32(Level) == 32)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 49000 && Convert.ToInt32(Exp) <= 52000 && Convert.ToInt32(Level) == 33)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 52000 && Convert.ToInt32(Exp) <= 55000 && Convert.ToInt32(Level) == 34)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 55000 && Convert.ToInt32(Exp) <= 58000 && Convert.ToInt32(Level) == 35)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 58000 && Convert.ToInt32(Exp) <= 61000 && Convert.ToInt32(Level) == 36)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 61000 && Convert.ToInt32(Exp) <= 64000 && Convert.ToInt32(Level) == 37)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 64000 && Convert.ToInt32(Exp) <= 67000 && Convert.ToInt32(Level) == 38)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 67000 && Convert.ToInt32(Exp) <= 70000 && Convert.ToInt32(Level) == 39)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 70000 && Convert.ToInt32(Exp) <= 75000 && Convert.ToInt32(Level) == 40)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 75000 && Convert.ToInt32(Exp) <= 80000 && Convert.ToInt32(Level) == 41)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 80000 && Convert.ToInt32(Exp) <= 85000 && Convert.ToInt32(Level) == 42)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 85000 && Convert.ToInt32(Exp) <= 90000 && Convert.ToInt32(Level) == 43)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 90000 && Convert.ToInt32(Exp) <= 95000 && Convert.ToInt32(Level) == 44)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 95000 && Convert.ToInt32(Exp) <= 100000 && Convert.ToInt32(Level) == 45)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 100000 && Convert.ToInt32(Exp) <= 105000 && Convert.ToInt32(Level) == 46)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 105000 && Convert.ToInt32(Exp) <= 110000 && Convert.ToInt32(Level) == 47)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 110000 && Convert.ToInt32(Exp) <= 115000 && Convert.ToInt32(Level) == 48)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 115000 && Convert.ToInt32(Exp) <= 120000 && Convert.ToInt32(Level) == 49)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 120000 && Convert.ToInt32(Exp) <= 125000 && Convert.ToInt32(Level) == 50)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 125000 && Convert.ToInt32(Exp) <= 140000 && Convert.ToInt32(Level) == 51)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 140000 && Convert.ToInt32(Exp) <= 160000 && Convert.ToInt32(Level) == 52)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 160000 && Convert.ToInt32(Exp) <= 180000 && Convert.ToInt32(Level) == 53)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 180000 && Convert.ToInt32(Exp) <= 200000 && Convert.ToInt32(Level) == 54)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
                else if (Convert.ToInt32(Exp) > 200000 && Convert.ToInt32(Exp) <= 220000 && Convert.ToInt32(Level) == 55)
                {
                    Account.SetLevelUp(Convert.ToInt32(AccountID));
                }
            }
            LevelupTimer.Enabled = true;
            myConnection.Close(); //Обязательно закрываем соединение!
            
            levelReader.Close();
            return Finished;
        }

        public static bool AntiSpamController(bool Finished)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID, SpamCount FROM spam WHERE SpamCount>0", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            while (thisReader.Read())
            {
                string AccountID = string.Empty;
                string SpamCount = string.Empty;
                AccountID += thisReader["AccountID"];
                SpamCount += thisReader["SpamCount"];

                myConnection = new MySqlConnection(Connect);
                MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
                string sql;

                if (Convert.ToInt32(SpamCount) > 0)
                {
                    int NewSpamCount = Convert.ToInt32(SpamCount) - 1;
                    sql = "UPDATE spam SET SpamCount =" + @"""" + NewSpamCount.ToString() + @"""" + "WHERE AccountID = " + AccountID + ";";
                    MySqlScript script = new MySqlScript(myConnection, sql);
                    myConnection.Open();
                    script.Query = sql;
                    script.Connection = myConnection;
                    try
                    {
                        script.Execute();
                    }
                    catch (MySqlException sqlEx)
                    {
                        Console.WriteLine("ОШИБКА!: " + sqlEx);
                        myConnection.Close();
                        
                    }
                }
                
                myConnection.Close(); //Обязательно закрываем соединение!
            }
            
            myConnection.Close();
            thisReader.Close();
            return Finished;
        }

        public static string GetRandomDropItemNameByID(int ItemID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT Name FROM items WHERE ID = @ItemID", myConnection);
            sql_commmand.Parameters.AddWithValue("@ItemID", ItemID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string ItemName = string.Empty;

            while (thisReader.Read())
            {
                ItemName += thisReader["Name"];
            }
            thisReader.Close();
            myConnection.Close();
            
            return ItemName;
        }

        public static string RefreshNPCListForAllPlayers()
        {
            string HireString = string.Empty;
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID FROM users", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            string NPCList = string.Empty;

            while (thisReader.Read())
            {
                string TempString = string.Empty;
                string AccountID = string.Empty;
                AccountID += thisReader["AccountID"];
                NPC.DeleteAllUnemployed(Convert.ToInt32(AccountID));
                NPC.Create(Convert.ToInt32(AccountID), "0");
                NPC.Create(Convert.ToInt32(AccountID), "0");
                NPC.Create(Convert.ToInt32(AccountID), "0");
                NPC.Create(Convert.ToInt32(AccountID), "0");
                NPC.Create(Convert.ToInt32(AccountID), "0");
                NPC.Create(Convert.ToInt32(AccountID), "0");
            }
            thisReader.Close();
            myConnection.Close();
            return NPCList;
        }

        public static void GetReportAllData(int AccountID, out string rTargetX, out string rTargetY, out string rResult, out string rRewardMoney, out string rRewardItems, out string rRewardExp)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_command = new MySqlCommand("SELECT TargetX, TargetY, Result, RewardMoney, RewardItems, RewardExp FROM reports WHERE AccountID = @AccountID", myConnection);
            sql_command.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_command.ExecuteReader();
            string TargetX = string.Empty;
            string TargetY = string.Empty;
            string Result = string.Empty;
            string RewardMoney = string.Empty;
            string RewardItems = string.Empty;
            string RewardExp = string.Empty;

            while (thisReader.Read())
            {
                TargetX += thisReader["TargetX"];
                TargetY += thisReader["TargetY"];
                Result += thisReader["Result"];
                RewardMoney += thisReader["RewardMoney"];
                RewardItems += thisReader["RewardItems"];
                RewardExp += thisReader["RewardExp"];
            }
            thisReader.Close();
            myConnection.Close();
            
            rTargetX = TargetX;
            rTargetY = TargetY;
            rResult = Result;
            rRewardMoney = RewardMoney;
            rRewardItems = RewardItems;
            rRewardExp = RewardExp;
        }

        public static void GetRandomItemDrop(Random rndmzr, out int Random_Items_Id_Drop, out int Random_Items_Count)
        {
            //
            Random_Items_Id_Drop = 1;
            Random_Items_Count = 1;
            int index = 0;
            int Random_Items_Chance = rndmzr.Next(1, 101); // 1-100 - шанс на дроп
            if (Random_Items_Chance >= 1 && Random_Items_Chance <= 51)
            {
                index = rndmzr.Next(very_high_chance_drop.Count);
                Random_Items_Id_Drop = very_high_chance_drop[index];
                Random_Items_Count = rndmzr.Next(1, Convert.ToInt32(Inventory.Items.Item.GetSingleData(Random_Items_Id_Drop, "DropMaxCount")));
            }
            else if (Random_Items_Chance >= 50 && Random_Items_Chance <= 70)
            {
                index = rndmzr.Next(high_chance_drop.Count);
                Random_Items_Id_Drop = high_chance_drop[index];
                Random_Items_Count = rndmzr.Next(1, Convert.ToInt32(Inventory.Items.Item.GetSingleData(Random_Items_Id_Drop, "DropMaxCount")));
            }
            else if (Random_Items_Chance >= 71 && Random_Items_Chance <= 85)
            {
                index = rndmzr.Next(medium_chance_drop.Count);
                Random_Items_Id_Drop = medium_chance_drop[index];
                Random_Items_Count = rndmzr.Next(1, Convert.ToInt32(Inventory.Items.Item.GetSingleData(Random_Items_Id_Drop, "DropMaxCount")));
            }
            else if (Random_Items_Chance >= 86 && Random_Items_Chance <= 95)
            {
                index = rndmzr.Next(low_chance_drop.Count);
                Random_Items_Id_Drop = low_chance_drop[index];
                Random_Items_Count = rndmzr.Next(1, Convert.ToInt32(Inventory.Items.Item.GetSingleData(Random_Items_Id_Drop, "DropMaxCount")));
            }
            else if (Random_Items_Chance >= 96 && Random_Items_Chance <= 101)
            {
                index = rndmzr.Next(very_low_chance_drop.Count);
                Random_Items_Id_Drop = very_low_chance_drop[index];
                Random_Items_Count = rndmzr.Next(1, Convert.ToInt32(Inventory.Items.Item.GetSingleData(Random_Items_Id_Drop, "DropMaxCount")));
            }
        }

        public static int GetRandomItemDropCount(int AccountID)
        {
            int Random_Items_Drop_Count = 1;
            if (Account.StatDiceCheck(AccountID, "Luck") == true)
            {
                Random_Items_Drop_Count++;
                if (Account.StatDiceCheck(AccountID, "Luck") == true)
                {
                    Random_Items_Drop_Count++;
                    if (Account.StatDiceCheck(AccountID, "Luck") == true)
                    {
                        Random_Items_Drop_Count++;
                        if (Account.StatDiceCheck(AccountID, "Luck") == true)
                        {
                            Random_Items_Drop_Count++;
                            if (Account.StatDiceCheck(AccountID, "Luck") == true)
                            {
                                Random_Items_Drop_Count++;
                            }
                            else return Random_Items_Drop_Count;
                        }
                        else return Random_Items_Drop_Count;
                    }
                    else return Random_Items_Drop_Count;
                }
                else return Random_Items_Drop_Count;
            }
            return Random_Items_Drop_Count;
        }

        public static string GetGlobalSingleData(string DataBlockName) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM global", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["" + DataBlockName + ""];
            }
            thisReader.Close();
            myConnection.Close();
            
            // Convert.ToInt64(DataBlockResult);
            return DataBlockResult;
        }

        public static string GetModuleIDByID(int ShipID, int ID) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ModuleID FROM ships_modules WHERE ShipID = @ShipID AND ID = @ID", myConnection);
            sql_commmand.Parameters.AddWithValue("@ShipID", ShipID);
            sql_commmand.Parameters.AddWithValue("@ID", ID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["ModuleID"];
            }
            thisReader.Close();
            myConnection.Close();
            
            // Convert.ToInt64(DataBlockResult);
            return DataBlockResult;
        }

        public static string GetPlayerBaseBattleSingleData(int AccountID, string DataBlockName) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM players_battle WHERE ShipID = @AccountID", myConnection);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["" + DataBlockName + ""];
            }
            thisReader.Close();
            myConnection.Close();
            
            // Convert.ToInt64(DataBlockResult);
            return DataBlockResult;
        }

        public static string GetPlayerBaseSingleData(string DataBlockName, string X, string Y) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM players_bases WHERE X = @X AND Y = @Y", myConnection);
            sql_commmand.Parameters.AddWithValue("@X", X);
            sql_commmand.Parameters.AddWithValue("@Y", Y);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["" + DataBlockName + ""];
            }
            thisReader.Close();
            myConnection.Close();
            
            // Convert.ToInt64(DataBlockResult);
            return DataBlockResult;
        }

        public static string GetBotSingleData(int ID, string DataBlockName) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM ship_bots WHERE ID = @ID", myConnection);
            sql_commmand.Parameters.AddWithValue("@ID", ID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["" + DataBlockName + ""];
            }
            thisReader.Close();
            myConnection.Close();
            
            // Convert.ToInt64(DataBlockResult);
            return DataBlockResult;
        }

        #region GetMapSingleData(int X, int Y, string DataBlockName) - Функция позволяет получить блок данных из MySQL таблицы Map по координатам
        public static string GetMapSingleData(int X, int Y, string DataBlockName) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM map WHERE X = @X AND Y = @Y", myConnection);
            sql_commmand.Parameters.AddWithValue("@X", X);
            sql_commmand.Parameters.AddWithValue("@Y", Y);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["" + DataBlockName + ""];
            }
            thisReader.Close();
            myConnection.Close();
            
            // Convert.ToInt64(DataBlockResult);
            return DataBlockResult;
        }
        #endregion

        public static string GetPlanetMapSingleData(int X, int Y, int planetX, int planetY, string DataBlockName) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM map_planet WHERE X = @X AND Y = @Y AND planetX = @pX AND planetY = @pY", myConnection);
            sql_commmand.Parameters.AddWithValue("@X", X);
            sql_commmand.Parameters.AddWithValue("@Y", Y);
            sql_commmand.Parameters.AddWithValue("@pX", planetX);
            sql_commmand.Parameters.AddWithValue("@pY", planetY);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["" + DataBlockName + ""];
            }
            thisReader.Close();
            myConnection.Close();

            // Convert.ToInt64(DataBlockResult);
            return DataBlockResult;
        }

        public static bool SetMapSingleData(int X, int Y, string DataBlockName, string NewParameter)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;

            sql = "UPDATE map SET " + DataBlockName + " =" + @"""" + NewParameter + @"""" + " WHERE X = " + X.ToString() + " AND Y = " + Y.ToString() + ";";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - SetMapSingleData] Запрос к базе для координат: " + X + " " + Y + " datablock " + DataBlockName + " query - " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();
                
                Finished = false;
                return Finished;
            }
            
            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static bool SetPlanetMapSingleData(int X, int Y, int planetX, int planetY, string DataBlockName, string NewParameter)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);

            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            string sql;
            bool Finished = false;

            sql = "UPDATE map_planet SET " + DataBlockName + " =" + @"""" + NewParameter + @"""" + " WHERE X = " + X.ToString() + " AND Y = " + Y.ToString() + " AND planetX = " + planetX.ToString() + " AND planetY = " + planetY.ToString() + ";";
            Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - SetPlanetMapSingleData] Запрос к базе для координат: " + X + " " + Y + " datablock " + DataBlockName + " query - " + sql);
            MySqlScript script = new MySqlScript(myConnection, sql);
            myConnection.Open();
            script.Query = sql;
            script.Connection = myConnection;
            try
            {
                script.Execute();
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("ОШИБКА!: " + sqlEx);
                myConnection.Close();

                Finished = false;
                return Finished;
            }

            myConnection.Close(); //Обязательно закрываем соединение!
            Finished = true;
            return Finished;
        }

        public static void GetMapData(int X, int Y, out int LocationType, out string LocationName, out string LocationOwnerShipID, out string LocationOwnerCaptainName, out int LocationDanger, out int IsCooldown, out int CooldownTime)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_command = new MySqlCommand("SELECT LocationType, LocationName, LocationOwnerShipID, LocationOwnerCaptainName, LocationDanger, IsCooldown, CooldownTime FROM map WHERE X = @X AND Y = @Y", myConnection);
            sql_command.Parameters.AddWithValue("@X", X);
            sql_command.Parameters.AddWithValue("@Y", Y);
            MySqlDataReader thisReader = sql_command.ExecuteReader();

            string LocationTypeString = string.Empty;
            string LocationNameString = string.Empty;
            string LocationOwnerShipIDString = string.Empty;
            string LocationOwnerCaptainNameString = string.Empty;
            string LocationDangerString = string.Empty;
            string IsCooldownString = string.Empty;
            string CooldownTimeString = string.Empty;

            while (thisReader.Read())
            {
                LocationTypeString += thisReader["LocationType"];
                LocationNameString += thisReader["LocationName"];
                LocationOwnerShipIDString += thisReader["LocationOwnerShipID"];
                LocationOwnerCaptainNameString += thisReader["LocationOwnerCaptainName"];
                LocationDangerString += thisReader["LocationDanger"];
                IsCooldownString += thisReader["IsCooldown"];
                CooldownTimeString += thisReader["CooldownTime"];
            }
            myConnection.Close();
            
            thisReader.Close();
            LocationType = Convert.ToInt32(LocationTypeString);
            LocationName = LocationNameString;
            LocationOwnerShipID = LocationOwnerShipIDString;
            LocationOwnerCaptainName = LocationOwnerCaptainNameString;
            LocationDanger = Convert.ToInt32(LocationDangerString);
            IsCooldown = Convert.ToInt32(IsCooldownString);
            CooldownTime = Convert.ToInt32(CooldownTimeString);
        }

        public static bool IsReportAlreadyInBase(int AccountID, string ForwardDate, string UserMessageID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            
            MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID, MessageID FROM reports WHERE ReportTime = @ReportTime", myConnection);
            sql_commmand.Parameters.AddWithValue("@ReportTime", ForwardDate);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string ReportInBase = string.Empty;
            string MessageID = string.Empty;
            while (thisReader.Read())
            {
                MessageID += thisReader["MessageID"];
                ReportInBase += thisReader["AccountID"];
            }
            thisReader.Close();
            myConnection.Close();
            
            bool AlreadyInBase;
            Console.WriteLine("Get bool AlreadyInBase");
            if (ReportInBase != "" && MessageID != UserMessageID) // Если репорт есть в базе
            {
                AlreadyInBase = true;
                Console.WriteLine("IsReportAlreadyInBase = true");
            }
            else { AlreadyInBase = false; Console.WriteLine("IsReportAlreadyInBase = false"); }
            return AlreadyInBase;
        }

        //public static async void SendStickerMessage(int ChatID, string StickerID)
        //{
        //    var keyboard = new ReplyKeyboardMarkup(new[]
        //            {
        //                new [] // first row
        //                {
        //                    new KeyboardButton("⚔️ Атака"),
        //                    new KeyboardButton("✊🏻 Действия"),
        //                    new KeyboardButton("🚀 Корабль"),
        //                },
        //                new [] // last row
        //                {
        //                    new KeyboardButton("🛡 Защита"),
        //                    new KeyboardButton("📖 Функции"),
        //                    new KeyboardButton("👤 Персонаж"),
        //                }
        //                }, true);
        //    string file = @"" + StickerID;
        //    var fts = new FileToSend(file);
        //    await Bot.Api.SendStickerAsync(ChatID, fts, replyMarkup: keyboard);
        //}

        #region BotOnChosenInlineResultReceived
        private static void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs chosenInlineResultEventArgs)
        {
            Console.WriteLine($"Received choosen inline result: {chosenInlineResultEventArgs.ChosenInlineResult.ResultId}");
        }
        #endregion

        #region BotOnInlineQueryReceived
        private static async void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs inlineQueryEventArgs)
        {
            //InlineQueryResult[] results = {
            //    new InlineQueryResultLocation
            //    {
            //        Id = "1",
            //        Latitude = 40.7058316f, // displayed result
            //        Longitude = -74.2581888f,
            //        Title = "New York",
            //        InputMessageContent = new InputLocationMessageContent // message if result is selected
            //        {
            //            Latitude = 40.7058316f,
            //            Longitude = -74.2581888f,
            //        }
            //    },

            //    new InlineQueryResultLocation
            //    {
            //        Id = "2",
            //        Longitude = 52.507629f, // displayed result
            //        Latitude = 13.1449577f,
            //        Title = "Berlin",
            //        InputMessageContent = new InputLocationMessageContent // message if result is selected
            //        {
            //            Longitude = 52.507629f,
            //            Latitude = 13.1449577f
            //        }
            //    }
            //};

            //await Bot.Api.AnswerInlineQueryAsync(inlineQueryEventArgs.InlineQuery.Id, results, isPersonal: true, cacheTime: 0);
        }
        #endregion
    }

    public class Callbacks
    {
        #region BotOnReceiveError - если бот вызывает ошибку
        public static void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            Console.WriteLine("Debugger error: " + receiveErrorEventArgs.ToString());
            //Debugger.Break();

        }
        #endregion
    }

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var configuration = new HttpConfiguration();

            configuration.Routes.MapHttpRoute("WebHook", "{controller}");

            app.UseWebApi(configuration);
        }
    }

    public class WebHookController : ApiController
    {
        public async Task<IHttpActionResult> Post(Update update)
        {
            var message = update.Message;

            Console.WriteLine(message.From.Id + " - " + message.Text);
            Console.WriteLine(">>>> Message ID: " + message.MessageId);

            MySqlConnection AntispamConnection = new MySqlConnection(DataStorage.Connect);
            MySqlCommand AntispamCommand = new MySqlCommand(CommandText, AntispamConnection);
            AntispamConnection.Open();
            

            MySqlCommand antispam_commmand = new MySqlCommand("SELECT SpamCount FROM spam WHERE AccountID = @AccountID", AntispamConnection);
            antispam_commmand.Parameters.AddWithValue("@AccountID", message.From.Id);
            MySqlDataReader antispamReader = antispam_commmand.ExecuteReader();
            string SpamCount = string.Empty;
            while (antispamReader.Read())
            {
                SpamCount += antispamReader["SpamCount"];
            }
            antispamReader.Close();
            Console.WriteLine("SpamCount: " + SpamCount);
            // Convert.ToInt64(DataBlockResult);

            if (SpamCount == "")
            {
                string spamsql;
                spamsql = "INSERT INTO spam (AccountID, SpamCount) VALUES (" + message.From.Id + ", 0" + "); ";
                MySqlScript antispam_script = new MySqlScript(AntispamConnection, spamsql);
                antispam_script = new MySqlScript(AntispamConnection, spamsql);
                antispam_script.Query = spamsql;
                antispam_script.Connection = AntispamConnection;
                try
                {
                    antispam_script.Execute();
                }
                catch (MySqlException sqlEx)
                {
                    Console.WriteLine("ОШИБКА!: " + sqlEx);
                }
                finally
                {
                    AntispamConnection.Close();
                    
                }
                AntispamConnection.Close();
            }
            else
            {
                if (Convert.ToInt32(SpamCount) > 3)
                {
                    
                }
                else
                {
                    string sql;
                    sql = "UPDATE spam SET SpamCount = SpamCount + 2 WHERE AccountID = " + message.From.Id + ";";
                    MySqlScript antispam_script = new MySqlScript(AntispamConnection, sql);
                    antispam_script.Query = sql;
                    antispam_script.Connection = AntispamConnection;
                    try
                    {
                        antispam_script.Execute();
                    }
                    catch (MySqlException sqlEx)
                    {
                        Console.WriteLine("ОШИБКА!: " + sqlEx);
                    }
                    finally
                    {
                        AntispamConnection.Close();
                        
                    }
                    AntispamConnection.Close(); //Обязательно закрываем соединение!
                }
            }
            AntispamConnection.Close();

            if (message == null || message.Type != MessageType.Text) return InternalServerError();

            else if (message.Text == "/block")
            {

            }

            else if (message.Text == "/start")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                           // new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                           // new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                Console.WriteLine("Command from: " + message.Chat.Id);
                try
                {
                    Telegram.Bot.Examples.WebHook.Account.Create(message.From.Id, message.From.Username, message.From.FirstName, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 10, 10, 0, 1, 50, 100);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception: " + ex);
                }

                try
                {
                    Ships.Ship.Create(message.From.Id, 1, message.From.Id, 100, 0, 0, Program.BaseShipList[1].Energy, 0, Program.BaseShipList[1].HP, Program.BaseShipList[1].Def, Program.BaseShipList[1].Attack, Program.BaseShipList[1].MaxCrew, Program.BaseShipList[1].MaxModules, Program.BaseShipList[1].Speed, Program.BaseShipList[1].Maneuv, Program.BaseShipList[1].HP, Program.BaseShipList[1].Energy, 0);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception: " + ex);
                }
            }

            else if (message.Text == "/reader")
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                MySqlCommand sql_commmand = new MySqlCommand("SELECT BattleTime FROM global", myConnection);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();

                while (thisReader.Read())
                {

                }
            }


            else if (message.Text == "/report")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                if (UserAccountID != "" && Program.IsReportExists(message.From.Id) == true)
                {
                    Program.GetReportAllData(message.From.Id, out string rTargetX, out string rTargetY, out string rResult, out string rRewardMoney, out string rRewardItems, out string rRewardExp);
                    Console.WriteLine("DEBUG: X: " + rTargetX + " Y: " + rTargetY);
                    string LocationName = Program.GetMapSingleData(Convert.ToInt32(rTargetX), Convert.ToInt32(rTargetY), "LocationName");
                    string LocationTypeID = Program.GetMapSingleData(Convert.ToInt32(rTargetX), Convert.ToInt32(rTargetY), "LocationType");
                    string LocationIcon = ""; // Иконка локации
                    string BattleResultString = ""; // Строка с результатом битвы
                    switch (LocationTypeID)
                    {
                        case "1": // Планета
                            LocationIcon = "🌎";
                            break;

                        case "2": // Космос
                            LocationIcon = "⬛️";
                            break;

                        case "3": // Астероид
                            LocationIcon = "🌑";
                            break;

                        case "4": // Искривление пространства
                            LocationIcon = "🌀";
                            break;

                        case "5": // Космическая станция
                            LocationIcon = "⚙️";
                            break;

                        default:
                            LocationIcon = "";
                            break;
                    }
                    switch (rResult)
                    {
                        case "BattleAward_Win_Hard":
                            BattleResultString = "🔥 В ходе ожесточенного сражения ваша команда одержала победу над противником!";
                            break;

                        case "BattleAward_Win_Medium":
                            BattleResultString = "Ваша команда одержала победу над противником!";
                            break;

                        case "BattleAward_Win_Easy":
                            BattleResultString = "Ваша команда смогла легко разбить ряды противника!";
                            break;

                        case "BattleAward_Loss_Hard":
                            BattleResultString = "🔥 Ваша команда не смогла победить в этом сражении.";
                            break;

                        case "BattleAward_Loss_Medium":
                            BattleResultString = "Ваша команда не смогла победить в этом сражении.";
                            break;

                        case "BattleAward_Loss_Easy":
                            BattleResultString = "У вас были все шансы, но ваша команда не смогла победить в этом сражении.";
                            break;

                        case "BattleAward_Draw":
                            BattleResultString = "Силы вашей команды и команды противника были равны.";
                            break;

                        default:
                            break;
                    }
                    Message.Send(message.From.Id, "<b>Отчёт о бое:</b>\nЛокация: " + LocationIcon + " " + LocationName + "\n\n" + BattleResultString + "\n✨ Опыт: +" + rRewardExp + "\n💳 Деньги: +" + rRewardMoney + "\n" + rRewardItems, 1);
                }
            }

            else if (message.Text == "/home")
            {
                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                if (PlayerOnEvent != message.From.Id.ToString())
                {
                    Program.PlayerDeleteBetweenEvent(Convert.ToInt32(message.From.Id));
                    Message.Send(message.From.Id, "Вы закончили приключение.", 1);
                }
                else
                {
                    Program.PlayerDeleteBetweenEvent(Convert.ToInt32(message.From.Id));
                    Program.PlayerDeleteEvent(Convert.ToInt32(message.From.Id));
                    Message.Send(message.From.Id, "Вы закончили приключение.", 1);
                }
            }

            else if (message.Text == "🏞 Задания")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("🏘 В город"),
                            new KeyboardButton("🏡 В пригород"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🏜 На пляж"),
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);

                await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Куда вы хотите пойти?", replyMarkup: keyboard);
            }

            else if (message.Text == "📖 Функции")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("💳 Донат"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Выберите направление:", replyMarkup: keyboard);
            }

            else if (message.Text == "💳 Донат" || message.Text == "/donate")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                if (UserAccountID != "")
                {

                    string successURL = "";
                    string link1Yandex = "https://money.yandex.ru/quickpay/button-widget?targets=" + message.From.Id + "&default-sum=75&button-text=Пожертвовать&yamoney-payment-type=on&button-size=m&button-color=orange&successURL= " + successURL + "&quickpay=small&account=41001249364560&label=" + message.From.Id;
                    string link1Card = "https://money.yandex.ru/quickpay/button-widget?targets=" + message.From.Id + "&default-sum=75&button-text=Пожертвовать&any-card-payment-type=on&button-size=m&button-color=orange&successURL= " + successURL + "&quickpay=small&account=41001249364560&label=" + message.From.Id;
                    string link2Yandex = "https://money.yandex.ru/quickpay/button-widget?targets=" + message.From.Id + "&default-sum=199&button-text=Пожертвовать&yamoney-payment-type=on&button-size=m&button-color=orange&successURL= " + successURL + "&quickpay=small&account=41001249364560&label=" + message.From.Id;
                    string link2Card = "https://money.yandex.ru/quickpay/button-widget?targets=" + message.From.Id + "&default-sum=199&button-text=Пожертвовать&any-card-payment-type=on&button-size=m&button-color=orange&successURL= " + successURL + "&quickpay=small&account=41001249364560&label=" + message.From.Id;
                    string link3Yandex = "https://money.yandex.ru/quickpay/button-widget?targets=" + message.From.Id + "&default-sum=349&button-text=Пожертвовать&yamoney-payment-type=on&button-size=m&button-color=orange&successURL= " + successURL + "&quickpay=small&account=41001249364560&label=" + message.From.Id;
                    string link3Card = "https://money.yandex.ru/quickpay/button-widget?targets=" + message.From.Id + "&default-sum=349&button-text=Пожертвовать&any-card-payment-type=on&button-size=m&button-color=orange&successURL= " + successURL + "&quickpay=small&account=41001249364560&label=" + message.From.Id;
                    string link4Yandex = "https://money.yandex.ru/quickpay/button-widget?targets=" + message.From.Id + "&default-sum=699&button-text=Пожертвовать&yamoney-payment-type=on&button-size=m&button-color=orange&successURL= " + successURL + "&quickpay=small&account=41001249364560&label=" + message.From.Id;
                    string link4Card = "https://money.yandex.ru/quickpay/button-widget?targets=" + message.From.Id + "&default-sum=699&button-text=Пожертвовать&any-card-payment-type=on&button-size=m&button-color=orange&successURL= " + successURL + "&quickpay=small&account=41001249364560&label=" + message.From.Id;
                    string link5Yandex = "https://money.yandex.ru/quickpay/button-widget?targets=" + message.From.Id + "&default-sum=1190&button-text=Пожертвовать&yamoney-payment-type=on&button-size=m&button-color=orange&successURL= " + successURL + "&quickpay=small&account=41001249364560&label=" + message.From.Id;
                    string link5Card = "https://money.yandex.ru/quickpay/button-widget?targets=" + message.From.Id + "&default-sum=1190&button-text=Пожертвовать&any-card-payment-type=on&button-size=m&button-color=orange&successURL= " + successURL + "&quickpay=small&account=41001249364560&label=" + message.From.Id;
                    string link6Yandex = "https://money.yandex.ru/quickpay/button-widget?targets=" + message.From.Id + "&default-sum=3490&button-text=Пожертвовать&yamoney-payment-type=on&button-size=m&button-color=orange&successURL= " + successURL + "&quickpay=small&account=41001249364560&label=" + message.From.Id;
                    string link6Card = "https://money.yandex.ru/quickpay/button-widget?targets=" + message.From.Id + "&default-sum=3490&button-text=Пожертвовать&any-card-payment-type=on&button-size=m&button-color=orange&successURL= " + successURL + "&quickpay=small&account=41001249364560&label=" + message.From.Id;
                    var reply = "<b>Пожертвуйте на работу сервера и получите специальные предметы в подарок!</b>\n\n"
            + "💠 <b>30 кристаллов этериума</b> за 💳 75 руб.: \n<a href=\"" + link1Card + "\">Visa/MasterCard</a> \n<a href=\"" + link1Yandex + "\">Yandex.Деньги</a>\n\n"
            + "💠 <b>100 кристаллов этериума</b> за 💳 199 руб.: \n<a href=\"" + link2Card + "\">Visa/MasterCard</a> \n<a href=\"" + link2Yandex + "\">Yandex.Деньги</a>\n\n"
            + "💠 <b>200 кристаллов этериума</b> за 💳 349 руб.: \n<a href=\"" + link3Card + "\">Visa/MasterCard</a> \n<a href=\"" + link3Yandex + "\">Yandex.Деньги</a>\n\n"
            + "💠 <b>500 кристаллов этериума</b> за 💳 699 руб.: \n<a href=\"" + link4Card + "\">Visa/MasterCard</a> \n<a href=\"" + link4Yandex + "\">Yandex.Деньги</a>\n\n"
            + "💠 <b>1000 кристаллов этериума</b> за 💳 1190 руб.: \n<a href=\"" + link5Card + "\">Visa/MasterCard</a> \n<a href=\"" + link5Yandex + "\">Yandex.Деньги</a>\n\n"
            + "💠 <b>3000 кристаллов этериума</b> за 💳 3490 руб.: \n<a href=\"" + link6Card + "\">Visa/MasterCard</a> \n<a href=\"" + link6Yandex + "\">Yandex.Деньги</a>\n\n";
                    Message.Send(message.From.Id, reply, 1);
                }

            }

            else if (message.Text == "/setweather" && message.From.Id == 65530966)
            {
                Program.WeatherController();
            }

            else if (message.Text == "/strength")
            {
                string TalantPoints = Account.GetSingleData(message.From.Id, "TalantPoints");
                if (Convert.ToInt32(TalantPoints) > 0)
                {
                    Program.DefAttackCalculation(message.From.Id);
                    int NewTalantPoints = Convert.ToInt32(TalantPoints) - 1;
                    Account.SetSingleData(message.From.Id, "TalantPoints", Convert.ToString(NewTalantPoints));

                    string SingleData = Account.GetSingleData(message.From.Id, "Strength");
                    int NewSingleData = Convert.ToInt32(SingleData) + 1;
                    Account.SetSingleData(message.From.Id, "Strength", Convert.ToString(NewSingleData));

                    SingleData = Account.GetSingleData(message.From.Id, "TalantPointSpend");
                    NewSingleData = Convert.ToInt32(SingleData) + 1;
                    Account.SetSingleData(message.From.Id, "TalantPointSpend", Convert.ToString(NewSingleData));
                    Message.SendProfileMessage(message.From.Id, "Ваша Сила увеличена!");
                }
                else Message.Send(message.From.Id, "У вас не хватает 🔰 Очков талантов!", 1);

            }
            else if (message.Text == "/endurance")
            {
                string TalantPoints = Account.GetSingleData(message.From.Id, "TalantPoints");
                if (Convert.ToInt32(TalantPoints) > 0)
                {
                    Program.DefAttackCalculation(message.From.Id);
                    int NewTalantPoints = Convert.ToInt32(TalantPoints) - 1;
                    Account.SetSingleData(message.From.Id, "TalantPoints", Convert.ToString(NewTalantPoints));

                    string SingleData = Account.GetSingleData(message.From.Id, "Endurance");
                    int NewSingleData = Convert.ToInt32(SingleData) + 1;
                    Account.SetSingleData(message.From.Id, "Endurance", Convert.ToString(NewSingleData));

                    SingleData = Account.GetSingleData(message.From.Id, "TalantPointSpend");
                    NewSingleData = Convert.ToInt32(SingleData) + 1;
                    Account.SetSingleData(message.From.Id, "TalantPointSpend", Convert.ToString(NewSingleData));
                    Message.SendProfileMessage(message.From.Id, "Ваша Выносливость увеличена!");
                }
                else Message.Send(message.From.Id, "У вас не хватает 🔰 Очков талантов!", 1);

            }
            else if (message.Text == "/charisma")
            {
                string TalantPoints = Account.GetSingleData(message.From.Id, "TalantPoints");
                if (Convert.ToInt32(TalantPoints) > 0)
                {
                    Program.DefAttackCalculation(message.From.Id);
                    int NewTalantPoints = Convert.ToInt32(TalantPoints) - 1;
                    Account.SetSingleData(message.From.Id, "TalantPoints", Convert.ToString(NewTalantPoints));

                    string SingleData = Account.GetSingleData(message.From.Id, "Charisma");
                    int NewSingleData = Convert.ToInt32(SingleData) + 1;
                    Account.SetSingleData(message.From.Id, "Charisma", Convert.ToString(NewSingleData));

                    SingleData = Account.GetSingleData(message.From.Id, "TalantPointSpend");
                    NewSingleData = Convert.ToInt32(SingleData) + 1;
                    Account.SetSingleData(message.From.Id, "TalantPointSpend", Convert.ToString(NewSingleData));
                    Message.SendProfileMessage(message.From.Id, "Ваша Харизма увеличена!");
                }
                else Message.Send(message.From.Id, "У вас не хватает 🔰 Очков талантов!", 1);

            }
            else if (message.Text == "/intelligence")
            {
                string TalantPoints = Account.GetSingleData(message.From.Id, "TalantPoints");
                if (Convert.ToInt32(TalantPoints) > 0)
                {
                    Program.DefAttackCalculation(message.From.Id);
                    int NewTalantPoints = Convert.ToInt32(TalantPoints) - 1;
                    Account.SetSingleData(message.From.Id, "TalantPoints", Convert.ToString(NewTalantPoints));

                    string SingleData = Account.GetSingleData(message.From.Id, "Intelligence");
                    int NewSingleData = Convert.ToInt32(SingleData) + 1;
                    Account.SetSingleData(message.From.Id, "Intelligence", Convert.ToString(NewSingleData));

                    SingleData = Account.GetSingleData(message.From.Id, "TalantPointSpend");
                    NewSingleData = Convert.ToInt32(SingleData) + 1;
                    Account.SetSingleData(message.From.Id, "TalantPointSpend", Convert.ToString(NewSingleData));
                    Message.SendProfileMessage(message.From.Id, "Ваш Интеллект увеличен!");
                }
                else Message.Send(message.From.Id, "У вас не хватает 🔰 Очков талантов!", 1);

            }
            else if (message.Text == "/agility")
            {
                string TalantPoints = Account.GetSingleData(message.From.Id, "TalantPoints");
                if (Convert.ToInt32(TalantPoints) > 0)
                {
                    Program.DefAttackCalculation(message.From.Id);
                    int NewTalantPoints = Convert.ToInt32(TalantPoints) - 1;
                    Account.SetSingleData(message.From.Id, "TalantPoints", Convert.ToString(NewTalantPoints));

                    string SingleData = Account.GetSingleData(message.From.Id, "Agility");
                    int NewSingleData = Convert.ToInt32(SingleData) + 1;
                    Account.SetSingleData(message.From.Id, "Agility", Convert.ToString(NewSingleData));

                    SingleData = Account.GetSingleData(message.From.Id, "TalantPointSpend");
                    NewSingleData = Convert.ToInt32(SingleData) + 1;
                    Account.SetSingleData(message.From.Id, "TalantPointSpend", Convert.ToString(NewSingleData));
                    Message.SendProfileMessage(message.From.Id, "Ваша Ловкость увеличена!");
                }
                else Message.Send(message.From.Id, "У вас не хватает 🔰 Очков талантов!", 1);

            }
            else if (message.Text == "/luck")
            {
                string TalantPoints = Account.GetSingleData(message.From.Id, "TalantPoints");
                if (Convert.ToInt32(TalantPoints) > 0)
                {
                    Program.DefAttackCalculation(message.From.Id);
                    int NewTalantPoints = Convert.ToInt32(TalantPoints) - 1;
                    Account.SetSingleData(message.From.Id, "TalantPoints", Convert.ToString(NewTalantPoints));

                    string SingleData = Account.GetSingleData(message.From.Id, "Luck");
                    int NewSingleData = Convert.ToInt32(SingleData) + 1;
                    Account.SetSingleData(message.From.Id, "Luck", Convert.ToString(NewSingleData));

                    SingleData = Account.GetSingleData(message.From.Id, "TalantPointSpend");
                    NewSingleData = Convert.ToInt32(SingleData) + 1;
                    Account.SetSingleData(message.From.Id, "TalantPointSpend", Convert.ToString(NewSingleData));
                    Message.SendProfileMessage(message.From.Id, "Ваша Удача увеличена!");
                }
                else Message.Send(message.From.Id, "У вас не хватает 🔰 Очков талантов!", 1);
            }

            else if (message.Text == "👤 Персонаж")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                if (UserAccountID != "")
                {
                    #region keyboard
                    var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("📦 Инвентарь"),
                            new KeyboardButton("🎩 Экипировка"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🔮 Характеристики"),
                            new KeyboardButton("👨‍👦 Клан"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                    #endregion
                    if (Account.IsInClan(message.From.Id) == true && Clans.GetDataByOwnerID(message.From.Id, "Active") == "1")
                    {
                        #region keyboard
                        keyboard = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton("📦 Инвентарь"),
                            new KeyboardButton("🎩 Экипировка"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🔮 Характеристики"),
                            new KeyboardButton("👨‍👦 Клан"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🌎 Перемещение"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                        #endregion
                    }
                    else
                    {
                        #region keyboard
                        keyboard = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton("📦 Инвентарь"),
                            new KeyboardButton("🎩 Экипировка"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🔮 Характеристики"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🌎 Перемещение"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                        #endregion
                    }
                    Program.DefAttackCalculation(message.From.Id);

                    string BattleTime = Program.GetGlobalSingleData("BattleTime");
                    // Расчет времени битвы
                    DateTime BattleOldDate = Convert.ToDateTime(BattleTime);
                    DateTime BattleNewDate = DateTime.Now;
                    TimeSpan Battle_ts = BattleOldDate - BattleNewDate;
                    int BattleDifferenceInMinutes = Battle_ts.Minutes;
                    int BattleDifferenceInHours = Battle_ts.Hours;

                    MySqlConnection myConnection = new MySqlConnection(Connect);
                    myConnection.Open();
                    MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID, UserName, CharName, Attack, Radiation, Resistance, Def, Money, Level, Exp, Strength, Endurance, Charisma, Intelligence, Agility, Luck, TalantPoints, Energy, MaxEnergy, MedalBH, MedalBeta, MainhandSlot, OffhandSlot, HeadSlot, NeckSlot, ChestSlot, PantsSlot, FeetSlot, Faction, Health, MaxHealth, Credits, CurrentAction, X, Y FROM users WHERE AccountID = @AccountID", myConnection);
                    sql_commmand.Parameters.AddWithValue("@AccountID", message.Chat.Id);
                    MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                    string AccountID = string.Empty;
                    string UserName = string.Empty;
                    string CharName = string.Empty;
                    string Attack = string.Empty;
                    string Def = string.Empty;
                    string Radiation = string.Empty;
                    string Resistance = string.Empty;
                    string Money = string.Empty;
                    string Level = string.Empty;
                    string Exp = string.Empty;
                    string Strength = string.Empty;
                    string Endurance = string.Empty;
                    string Charisma = string.Empty;
                    string Intelligence = string.Empty;
                    string Agility = string.Empty;
                    string Luck = string.Empty;
                    string TalantPoints = string.Empty;
                    string Energy = string.Empty;
                    string MaxEnergy = string.Empty;
                    string MedalBH = string.Empty;
                    string MedalBeta = string.Empty;
                    string MainhandSlot = string.Empty;
                    string OffhandSlot = string.Empty;
                    string HeadSlot = string.Empty;
                    string NeckSlot = string.Empty;
                    string ChestSlot = string.Empty;
                    string PantsSlot = string.Empty;
                    string FeetSlot = string.Empty;
                    string Faction = string.Empty;
                    string Health = string.Empty;
                    string MaxHealth = string.Empty;
                    string Credits = string.Empty;
                    string CurrentAction = string.Empty;
                    string X = string.Empty;
                    string Y = string.Empty;

                    while (thisReader.Read())
                    {
                        AccountID += thisReader["AccountID"];
                        CharName += thisReader["CharName"];
                        UserName += thisReader["UserName"];
                        Attack += thisReader["Attack"];
                        Radiation += thisReader["Radiation"];
                        Resistance += thisReader["Resistance"];
                        Def += thisReader["Def"];
                        Level += thisReader["Level"];
                        Money += thisReader["Money"];
                        Exp += thisReader["Exp"];
                        Strength += thisReader["Strength"];
                        Endurance += thisReader["Endurance"];
                        Charisma += thisReader["Charisma"];
                        Intelligence += thisReader["Intelligence"];
                        Agility += thisReader["Agility"];
                        Luck += thisReader["Luck"];
                        TalantPoints += thisReader["TalantPoints"];
                        Energy += thisReader["Energy"];
                        MaxEnergy += thisReader["MaxEnergy"];
                        MedalBH += thisReader["MedalBH"];
                        MedalBeta += thisReader["MedalBeta"];
                        MainhandSlot += thisReader["MainhandSlot"];
                        OffhandSlot += thisReader["OffhandSlot"];
                        HeadSlot += thisReader["HeadSlot"];
                        NeckSlot += thisReader["NeckSlot"];
                        ChestSlot += thisReader["ChestSlot"];
                        PantsSlot += thisReader["PantsSlot"];
                        FeetSlot += thisReader["FeetSlot"];
                        Faction += thisReader["Faction"];
                        Health += thisReader["Health"];
                        MaxHealth += thisReader["MaxHealth"];
                        Credits += thisReader["Credits"];
                        CurrentAction += thisReader["CurrentAction"];
                    }
                    thisReader.Close();
                    myConnection.Close();

                    X = Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "X");
                    Y = Ships.Ship.GetSingleData(Convert.ToInt32(AccountID), "Y");
                    string FactionString = "Свободный пилот";
                    string HaveTalantPointString = string.Empty;
                    string StrengthButton = string.Empty;
                    string EnduranceButton = string.Empty;
                    string CharismaButton = string.Empty;
                    string IntelligenceButton = string.Empty;
                    string AgilityButton = string.Empty;
                    string LuckButton = string.Empty;
                    string ShipNameString = string.Empty;
                    string CurrentActionString = string.Empty;


                    string MedalBH_string = string.Empty;
                    string MedalBeta_string = string.Empty;

                    if (MedalBH == "1")
                    {
                        MedalBH_string = "🎖";
                    }
                    if (MedalBeta == "1")
                    {
                        MedalBeta_string = "🏅";
                    }

                    string NextExp = "~"; // Переменная для хранения необходимого для левелапа опыта

                    switch (Convert.ToInt32(Level))
                    {
                        case 1:
                            NextExp = "1000";
                            break;
                        case 2:
                            NextExp = "2000";
                            break;
                        case 3:
                            NextExp = "3000";
                            break;
                        case 4:
                            NextExp = "4000";
                            break;
                        case 5:
                            NextExp = "5000";
                            break;
                        case 6:
                            NextExp = "6000";
                            break;
                        case 7:
                            NextExp = "7000";
                            break;
                        case 8:
                            NextExp = "8000";
                            break;
                        case 9:
                            NextExp = "9000";
                            break;
                        case 10:
                            NextExp = "10000";
                            break;
                        case 11:
                            NextExp = "11000";
                            break;
                        case 12:
                            NextExp = "12000";
                            break;
                        case 13:
                            NextExp = "13000";
                            break;
                        case 14:
                            NextExp = "14000";
                            break;
                        case 15:
                            NextExp = "15000";
                            break;
                        case 16:
                            NextExp = "16000";
                            break;
                        case 17:
                            NextExp = "17000";
                            break;
                        case 18:
                            NextExp = "18000";
                            break;
                        case 19:
                            NextExp = "19000";
                            break;
                        case 20:
                            NextExp = "20000";
                            break;
                        case 21:
                            NextExp = "22000";
                            break;
                        case 22:
                            NextExp = "24000";
                            break;
                        case 23:
                            NextExp = "26000";
                            break;
                        case 24:
                            NextExp = "28000";
                            break;
                        case 25:
                            NextExp = "30000";
                            break;
                        case 26:
                            NextExp = "32000";
                            break;
                        case 27:
                            NextExp = "34000";
                            break;
                        case 28:
                            NextExp = "36000";
                            break;
                        case 29:
                            NextExp = "38000";
                            break;
                        case 30:
                            NextExp = "40000";
                            break;
                        case 31:
                            NextExp = "43000";
                            break;
                        case 32:
                            NextExp = "46000";
                            break;
                        case 33:
                            NextExp = "49000";
                            break;
                        case 34:
                            NextExp = "52000";
                            break;
                        case 35:
                            NextExp = "55000";
                            break;
                        case 36:
                            NextExp = "58000";
                            break;
                        case 37:
                            NextExp = "61000";
                            break;
                        case 38:
                            NextExp = "64000";
                            break;
                        case 39:
                            NextExp = "67000";
                            break;
                        case 40:
                            NextExp = "70000";
                            break;
                        case 41:
                            NextExp = "75000";
                            break;
                        case 42:
                            NextExp = "80000";
                            break;
                        case 43:
                            NextExp = "85000";
                            break;
                        case 44:
                            NextExp = "90000";
                            break;
                        case 45:
                            NextExp = "95000";
                            break;
                        case 46:
                            NextExp = "100000";
                            break;
                        case 47:
                            NextExp = "105000";
                            break;
                        case 48:
                            NextExp = "110000";
                            break;
                        case 49:
                            NextExp = "115000";
                            break;
                        case 50:
                            NextExp = "120000";
                            break;
                        case 51:
                            NextExp = "125000";
                            break;
                        case 52:
                            NextExp = "140000";
                            break;
                        case 53:
                            NextExp = "160000";
                            break;
                        case 54:
                            NextExp = "180000";
                            break;
                        case 55:
                            NextExp = "200000";
                            break;
                    }

                    switch (Faction) // Список фракций
                    {
                        case "0":
                            FactionString = "🇲🇭 Федерация";
                            break;
                        case "1":
                            FactionString = "🇵🇬 Империя";
                            break;
                        case "2":
                            FactionString = "🇨🇩 Содружество";
                            break;
                        case "3":
                            FactionString = "🇹🇹 Наемники";
                            break;
                        case "4":
                            FactionString = "🇱🇨 Наблюдатели";
                            break;
                    }

                    switch (CurrentAction) // Список действий
                    {
                        case "0":
                            CurrentActionString = "Где-то в космосе";
                            break;
                        case "1":
                            CurrentActionString = "Передвигаетесь на север";
                            break;
                        case "2":
                            CurrentActionString = "Передвигаетесь на юг";
                            break;
                        case "3":
                            CurrentActionString = "Передвигаетесь на запад";
                            break;
                        case "4":
                            CurrentActionString = "Передвигаетесь на восток";
                            break;
                        case "10":
                            CurrentActionString = "🛡 Защищаете точку 🛡";
                            break;
                        case "11":
                            CurrentActionString = "🔧 Ремонт корабля 🔧";
                            break;
                        case "12":
                            CurrentActionString = "❤️ В процессе оживления ❤️";
                            break;
                        case "13":
                            CurrentActionString = "";
                            break;
                        case "14":
                            CurrentActionString = "";
                            break;

                        case "15":
                            CurrentActionString = "⚔️ Атакуете точку ⚔️";
                            break;
                        case "16":
                            CurrentActionString = "";
                            break;
                        case "17":
                            CurrentActionString = "";
                            break;
                        case "18":
                            CurrentActionString = "";
                            break;
                        case "19":
                            CurrentActionString = "";
                            break;
                        case "666":
                            CurrentActionString = "☠️ Без сознания ☠️ (/revive)";
                            break;
                    }

                    string Equip = string.Empty;
                    if (MainhandSlot == "0" && OffhandSlot == "0")
                    {
                        Equip = "\n🎩 Экипировка (Пусто)";
                    }
                    else Equip = "\n🎩 Экипировка (/equip)";

                    if (Convert.ToInt32(TalantPoints) > 0)
                    {
                        HaveTalantPointString = "🔰 Очков талантов: " + TalantPoints + " - /stats";
                    }
                    else HaveTalantPointString = "\n";

                    string BattleString = "⚔️ До битвы: ⏱ " + BattleDifferenceInHours + " час. " + BattleDifferenceInMinutes + " мин.";
                    if (Program.GetGlobalSingleData("OnBattle") == "true")
                    {
                        BattleString = "⚔️ Идёт битва ⚔️";
                    }

                    var MeInfo = @"" + BattleString + "\n" + "\n👤 " + CharName + " (" + Level + " ур.)" + MedalBH_string + MedalBeta_string + "\n" + "🌐 " + CurrentActionString + "\n" + "🖲 Координаты: " + X + ":" + Y + "\n" + FactionString + "\n" + "❤️ " + Health + "/" + MaxHealth + "\n" + "⚔️ " + Attack + " 🛡 " + Def + "\n" +
                    "💳 Деньги: " + Money + "\n" +
                    "💠 Этериум: " + Credits + " (/donate)\n" +
                    "✨ Опыт: " + Exp + "/" + NextExp + "\n" +
                    "🔋 Энергия: " + Energy + "/" + MaxEnergy + "\n" +
                    "\n" + HaveTalantPointString;
                    try
                    {
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, MeInfo, replyMarkup: keyboard);
                    }
                    catch (Exception MyEx)
                    {
                        Console.WriteLine(MyEx);
                    }
                }
                else
                {
                    Message.Send(message.From.Id, "У вас нет игрового профиля! Нажмите /start ", 1);
                }

            }

            else if (message.Text == "/equip" || message.Text == "🎩 Экипировка")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("🔮 Характеристики"),
                            new KeyboardButton("🎩 Экипировка"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("📦 Инвентарь"),
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                Account.ShowInventory(message.From.Id);

            }

            else if (message.Text == "/stats" || message.Text == "🔮 Характеристики")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("🔮 Характеристики"),
                            new KeyboardButton("🎩 Экипировка"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("📦 Инвентарь"),
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                var keyboard2 = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton("🌐 Загрузка матрицы"),
                        }
                        }, true);
                string PlayerStatus = Account.GetSingleData(Convert.ToInt32(message.From.Id), "Status");
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                
                MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID, CharName, Level, Strength, Endurance, Charisma, Intelligence, Agility, Luck, TalantPoints, MedalBH, MedalBeta, Faction FROM users WHERE AccountID = @AccountID", myConnection);
                sql_commmand.Parameters.AddWithValue("@AccountID", message.Chat.Id);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                string AccountID = string.Empty;
                string CharName = string.Empty;
                string Level = string.Empty;
                string Strength = string.Empty;
                string Endurance = string.Empty;
                string Charisma = string.Empty;
                string Intelligence = string.Empty;
                string Agility = string.Empty;
                string Luck = string.Empty;
                string TalantPoints = string.Empty;
                string MedalBH = string.Empty;
                string MedalBeta = string.Empty;
                string Faction = string.Empty;

                while (thisReader.Read())
                {
                    AccountID += thisReader["AccountID"];
                    CharName += thisReader["CharName"];
                    Level += thisReader["Level"];
                    Strength += thisReader["Strength"];
                    Endurance += thisReader["Endurance"];
                    Charisma += thisReader["Charisma"];
                    Intelligence += thisReader["Intelligence"];
                    Agility += thisReader["Agility"];
                    Luck += thisReader["Luck"];
                    TalantPoints += thisReader["TalantPoints"];
                    MedalBH += thisReader["MedalBH"];
                    MedalBeta += thisReader["MedalBeta"];
                    Faction += thisReader["Faction"];
                }
                string HaveTalantPointString = string.Empty;
                string StrengthButton = string.Empty;
                string EnduranceButton = string.Empty;
                string CharismaButton = string.Empty;
                string IntelligenceButton = string.Empty;
                string AgilityButton = string.Empty;
                string LuckButton = string.Empty;

                string FactionString = string.Empty;
                string MedalBH_string = string.Empty;
                string MedalBeta_string = string.Empty;

                switch (Faction) // Список фракций
                {
                    case "0":
                        FactionString = "🇲🇭";
                        break;
                    case "1":
                        FactionString = "🇵🇬";
                        break;
                    case "2":
                        FactionString = "🇨🇩";
                        break;
                    case "3":
                        FactionString = "🇹🇹";
                        break;
                    case "4":
                        FactionString = "🇱🇨";
                        break;
                }

                if (MedalBH == "1")
                {
                    MedalBH_string = "🎖";
                }
                if (MedalBeta == "1")
                {
                    MedalBeta_string = "🏅";
                }
                if (Convert.ToInt32(TalantPoints) > 0)
                {
                    StrengthButton = "/strength+";
                    EnduranceButton = "/endurance+";
                    CharismaButton = "/charisma+";
                    IntelligenceButton = "/intelligence+";
                    AgilityButton = "/agility+";
                    LuckButton = "/luck+";
                    HaveTalantPointString = "🔰 Очков талантов: " + TalantPoints + "\n";
                }
                else
                {
                    StrengthButton = string.Empty;
                    EnduranceButton = string.Empty;
                    CharismaButton = string.Empty;
                    IntelligenceButton = string.Empty;
                    AgilityButton = string.Empty;
                    LuckButton = string.Empty;
                    HaveTalantPointString = string.Empty;
                }
                myConnection.Close();
                
                var MeInfo = @"" + FactionString + " " + CharName + " (" + Level + " ур.)" + MedalBH_string + MedalBeta_string + "\n🔮 Характеристики: " + "\n" + HaveTalantPointString +
                "  " + Strength + " - Сила  " + StrengthButton + "\n" +
                "  " + Endurance + " - Выносливость  " + EnduranceButton + "\n" +
                "  " + Charisma + " - Харизма  " + CharismaButton + "\n" +
                "  " + Intelligence + " - Интеллект  " + IntelligenceButton + "\n" +
                "  " + Agility + " - Ловкость  " + AgilityButton + "\n" +
                "  " + Luck + " - Удача  " + LuckButton;
                if (PlayerStatus == "1")
                {
                    try
                    {
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, MeInfo, replyMarkup: keyboard2);
                    }
                    catch (Exception MyEx)
                    {
                        Console.WriteLine(MyEx);
                    }
                }
                else if (PlayerStatus == "0")
                {
                    try
                    {
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, MeInfo, replyMarkup: keyboard);
                    }
                    catch (Exception MyEx)
                    {
                        Console.WriteLine(MyEx);
                    }
                }
            }

            else if (message.Text == "🚪 Назад в город" || message.Text == "/city" || message.Text == "🏘 В город" || message.Text == "🔙 Назад в город")
            {
                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                Program.PlayerDeleteBetweenEvent(message.From.Id);
                if (PlayerOnEvent != message.From.Id.ToString())
                {
                    var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                    new [] // first row
                    {
                        new KeyboardButton("🏤 Ратуша"),
                        new KeyboardButton("🍺 Таверна"),
                    },
                    new [] // last row
                    {
                        new KeyboardButton("⛩ Рынок"),
                        new KeyboardButton("💳 Банк"),
                    },
                    new [] // last row
                    {
                        new KeyboardButton("🏫 Академия"),
                        new KeyboardButton("👣 Покинуть город"),
                    }
                    }, true);
                    try
                    {
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Вы находитесь на городской площади. Куда вы хотите пойти?",
                                         replyMarkup: keyboard);
                    }
                    catch (Exception MyEx)
                    {
                        Console.WriteLine(MyEx);
                    }

                }
                else Message.Send((int)message.Chat.Id, "Вы сейчас заняты другим делом.", 1);
            }

            else if (message.Text.StartsWith("/givemedal_bh") && message.From.Id == 65530966)
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                string PlayerID = message.Text.After("/givemedal_bh ");
                PlayerID = PlayerID.Trim();

                try
                {
                    Account.SetSingleData(Convert.ToInt32(PlayerID), "MedalBH", "1");
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Медаль выдана для ID: " + PlayerID, replyMarkup: keyboard);
                    await Bot.Api.SendTextMessageAsync(PlayerID, "🎉 ПОЗДРАВЛЯЕМ! 🎉 \n\nВы награждены 🏅 Медалью: \nЗа заслуги перед сообществом.\nНосите её с честью!", replyMarkup: keyboard);
                }
                catch
                {

                }
            }

            else if (message.Text.StartsWith("/givemedal_beta") && message.From.Id == 65530966)
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                string PlayerID = message.Text.After("/givemedal_beta ");
                PlayerID = PlayerID.Trim();

                try
                {
                    Account.SetSingleData(Convert.ToInt32(PlayerID), "MedalBeta", "1");
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Медаль выдана для ID: " + PlayerID, replyMarkup: keyboard);
                    await Bot.Api.SendTextMessageAsync(PlayerID, "🎉 ПОЗДРАВЛЯЕМ! 🎉 \n\nВы награждены 🎖 Медалью: \nЗа участие в Первой Военной Кампании!\nНосите её с честью!", replyMarkup: keyboard);
                }
                catch
                {

                }
            }

            else if (message.Text == "🏡 В пригород")
            {
                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                if (PlayerOnEvent != message.From.Id.ToString())
                {
                    var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                    new [] // first row
                    {
                        new KeyboardButton("🌳 В лес"),
                    },
                    new [] // first row
                    {
                        new KeyboardButton("🌋 К вулкану"),
                    },
                    new [] // first row
                    {
                        new KeyboardButton("🚪 Назад"),
                    }
                    }, true);
                    try
                    {
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Вы вышли из городских ворот. Втянув свежий, с едва уловимым запахом сирени, воздух, вы огляделись вокруг себя. Куда вы хотите пойти?\n\n🌳 Лес - Стоимость: ⚡️1\nМесто, где поют птички, по раскидистым деревьям шныряют белочки, а в глубине чащи притаились разбойники... Сможешь ли ты выйти живым?\nВозможные противники:\n🐺🦅🐍🐅🐁🐿\n\n🌋 Вулкан - Стоимость: ⚡️2\n Древний потухший вулкан напоминает жителям города о скоротечности их жизней. Здесь очень много различных ресурсов, но любопытных и невнимательных здесь ждёт гибель.\nВозможные противники:\n🐍🦂🐗🐺🦇🦍🐉",
                                              replyMarkup: keyboard);
                    }
                    catch (Exception MyEx)
                    {
                        Console.WriteLine(MyEx);
                    }

                }
                else Message.Send((int)message.Chat.Id, "Вы сейчас заняты другим делом.", 1);

            }

            else if (message.Text == "🏫 Академия")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                {
                    new [] // first row
                    {
                        new KeyboardButton("📜 Обучение"),
                    },
                    new [] // first row
                    {
                        new KeyboardButton("🚪 Назад в город"),
                    }
                }, true);
                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Вы вошли в здание 🏫 Академии. Удивительно, но тут есть жизнь. Туда-сюда снуют молодые люди, мудрые преподаватели с умным видом обсуждают глобальные проблемы. Чем вы хотите заняться?",
                                         replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

            }

            else if (message.Text == "MH")
            {
                string MechanikExists = NPC.ProfessionExists(message.From.Id, 4);
                Message.Send(message.From.Id, "Рез: " + MechanikExists, 1);
            }

            else if (message.Text == "🌐 Посадка")
            {
                string X_Position = Ships.Ship.GetSingleData(message.From.Id, "X");
                string Y_Position = Ships.Ship.GetSingleData(message.From.Id, "Y");
                string CurrentPositionLocationType = Program.GetMapSingleData(Convert.ToInt32(X_Position), Convert.ToInt32(Y_Position), "LocationType");
                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != "") && (CurrentPositionLocationType == "1")) //На орбите планеты
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        Program.PlayerDeleteBetweenEvent(message.From.Id);
                        Program.PlayerSetEvent((int)message.From.Id, 10, 5, 0); // Посадка на планету
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);
                }
                else Message.Send(message.From.Id, "Действие недоступно в данный момент.", 1);
            }

            else if (message.Text == "🚀 Взлёт")
            {
                string X_Position = Ships.Ship.GetSingleData(message.From.Id, "X");
                string Y_Position = Ships.Ship.GetSingleData(message.From.Id, "Y");
                string onPlanet = Account.GetSingleData(message.From.Id, "onPlanet");
                string CurrentPositionLocationType = Program.GetMapSingleData(Convert.ToInt32(X_Position), Convert.ToInt32(Y_Position), "LocationType");
                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != "") && (CurrentPositionLocationType == "1")) // Корабль игрока на планете
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        if (onPlanet == "1")
                        {
                            string X = Account.GetSingleData(message.From.Id, "X");
                            string Y = Account.GetSingleData(message.From.Id, "Y");
                            string pX = Account.GetSingleData(message.From.Id, "planetX");
                            string pY = Account.GetSingleData(message.From.Id, "planetY");
                            string PlanetMapPointName = Program.GetPlanetMapSingleData(Convert.ToInt32(X), Convert.ToInt32(Y), Convert.ToInt32(pX), Convert.ToInt32(pY), "LocationName");

                            if (PlanetMapPointName == "Космодром")
                            {
                                Program.PlayerDeleteBetweenEvent(message.From.Id);
                                Program.PlayerSetEvent((int)message.From.Id, 20, 5, 0); // Ивент "Взлёт"
                            }
                            else Message.Send(message.From.Id, "Требуется находиться на космодроме.", 1);
                        }
                        else Message.Send(message.From.Id, "Вы не находитесь на поверхности планеты.", 1);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);
                }
                else Message.Send(message.From.Id, "Действие недоступно в данный момент.", 1);
            }

            else if (message.Text == "🔧 Ремонт")
            {
                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                string X_Position = Ships.Ship.GetSingleData(message.From.Id, "X");
                string Y_Position = Ships.Ship.GetSingleData(message.From.Id, "Y");
                string CurrentPositionLocationType = Program.GetMapSingleData(Convert.ToInt32(X_Position), Convert.ToInt32(Y_Position), "LocationType");

                string MechanikString = "";
                string NanobotsString = "";

                string MechanikHelp = "";
                string NanobotsHelp = "";

                string MechanikExists = NPC.ProfessionExists(message.From.Id, 4);
                bool PlayerHasLittleNanobots = Account.isHasItem(message.From.Id, 60, 1);
                bool PlayerHasBigNanobots = Account.isHasItem(message.From.Id, 61, 1);

                if (MechanikExists != "")
                {
                    MechanikString = "🔧 Механик";
                    MechanikHelp =
                        "🔧 Механик - Отдайте приказ вашему механику начать ремонт корабля без использования наноботов. Скорость и качество ремонта напрямую зависит от навыков вашего механика.\n\n";
                }

                if (PlayerHasLittleNanobots == true || PlayerHasBigNanobots == true)
                {
                    NanobotsString = "🔧 Наноботы";
                    NanobotsHelp =
                        "🔧 Наноботы - Используйте наноботов для ремонта вашего корабля. После использования наноботы приходят в негодность и выкидываются.\n\n";
                }

                if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != "") && (CurrentPositionLocationType == "2")) // В открытом космосе
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                                new [] // first row
                                {
                                    new KeyboardButton(MechanikString),
                                    new KeyboardButton(""),
                                },
                                new [] // last row
                                {
                                    new KeyboardButton(NanobotsString),
                                    new KeyboardButton(""),
                                },
                                new [] // last row
                                {
                                    new KeyboardButton("🚪 Назад"),
                                }
                            }, true);
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Выберите команду:\n" + MechanikHelp + NanobotsHelp,
                            replyMarkup: keyboard);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                }
                else
                {
                    Message.Send(message.From.Id, "Действие недоступно в данный момент.", 1);
                }
            }

            else if (message.Text == "👨🏼 Персонажи")
            {

                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                string UserAccountID = Account.GetAccountExists(message.From.Id);

                string X_Position = Ships.Ship.GetSingleData(message.From.Id, "X");
                string Y_Position = Ships.Ship.GetSingleData(message.From.Id, "Y");
                string CurrentPositionLocationType = Program.GetMapSingleData(Convert.ToInt32(X_Position), Convert.ToInt32(Y_Position), "LocationType");
                if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != "") && (CurrentPositionLocationType == "5")) // У космической станции
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                            new [] // first row
                            {
                                new KeyboardButton(""),
                                new KeyboardButton(""),
                            },
                            new [] // last row
                            {
                                new KeyboardButton(""),
                                new KeyboardButton(""),
                            },
                            new [] // last row
                            {
                                new KeyboardButton("🚪 Назад"),
                            }
                        }, true);
                        string NPCList = NPC.Friend.GetNPCList(Convert.ToInt32(X_Position), Convert.ToInt32(Y_Position));
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, NPCList, replyMarkup: keyboard, parseMode: ParseMode.Html);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);
                }
                else
                {
                    Message.Send(message.From.Id, "Действие недоступно в данный момент.", 1);
                }
            }

            else if (message.Text == "✊🏻 Действия")
            {
                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                string UserAccountID = Account.GetAccountExists(message.From.Id);

                string X_Position = Ships.Ship.GetSingleData(message.From.Id, "X");
                string Y_Position = Ships.Ship.GetSingleData(message.From.Id, "Y");
                string CurrentPositionLocationType = Program.GetMapSingleData(Convert.ToInt32(X_Position), Convert.ToInt32(Y_Position), "LocationType");
                string onPlanet = Account.GetSingleData(message.From.Id, "onPlanet");

                if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != "") && (CurrentPositionLocationType == "2")) // В открытом космосе
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton("🔧 Ремонт"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🌐 Сканер"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Выберите ✊🏻 Действие:\n\n🔧 Ремонт - Осталось мало здоровья после стычки с пиратами? Не беда! При наличии у вас Комплекта наноботов вы сможете починить корабль! Также возможна починка при помощи вашего корабельного Механика.\n\n🌐 Сканер - Используйте Сканер, чтобы найти ближайшие к вам обьекты и корабли.\n\n",
                                            replyMarkup: keyboard);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                }

                else if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != "") && (CurrentPositionLocationType == "1")) // На орбите планеты
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        if (onPlanet == "0")
                        {
                            var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🌐 Посадка"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                            await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Выберите ✊🏻 Действие:\n\n🌐 Посадка - Осуществите посадку на поверхность планеты! Исследуйте местность, флору и фауну планеты, посетите города и знакомьтесь с культурой аборигенов!\n\n",
                                            replyMarkup: keyboard);
                        }
                        else if (onPlanet == "1")
                        {
                            var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚀 Взлёт"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                            await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Выберите ✊🏻 Действие:\n\n🚀 Взлёт - Взлетите на своём корабле в темную бесконечность космоса (и далее)!\n\n",
                                            replyMarkup: keyboard);
                        }   
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                }
                else if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != "") && (CurrentPositionLocationType == "5")) // У космической станции
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                            new [] // first row
                            {
                                new KeyboardButton("👨🏼 Персонажи"),
                                new KeyboardButton(""),
                            },
                            new [] // last row
                            {
                                new KeyboardButton(""),
                                new KeyboardButton(""),
                            },
                            new [] // last row
                            {
                                new KeyboardButton("🚪 Назад"),
                            }
                        }, true);
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Выберите ✊🏻 Действие:\n\n👨🏼 Персонажи - пообщайтесь с формами жизни в данной локации, узнайте последние новости, возьмите задания, продайте ненужный хлам и всё в таком духе.\n\n",
                            replyMarkup: keyboard);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);
                }
                else
                {
                    Message.Send(message.From.Id, "Действие недоступно в данный момент.", 1);
                }
            }

            else if (message.Text == "🌐 Сканер")
            {
                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                string UserAccountID = Account.GetAccountExists(message.From.Id);

                string X_Position = Ships.Ship.GetSingleData(message.From.Id, "X");
                string Y_Position = Ships.Ship.GetSingleData(message.From.Id, "Y");
                string CurrentPositionLocationType = Program.GetMapSingleData(Convert.ToInt32(X_Position), Convert.ToInt32(Y_Position), "LocationType");

                if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != "") && (CurrentPositionLocationType == "2")) // В открытом космосе
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton("🌐 Сканировать"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("📗 Игроки"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Выберите ✊🏻 Действие:\n\n🌐 Сканировать - Использовать сканер для поиска кораблей.\n\n📗 Игроки - Корабли принадлежащие игрокам.",
                                            replyMarkup: keyboard);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                }
                else
                {
                    Message.Send(message.From.Id, "Вы не можете сделать это здесь.", 1);
                }
            }

            else if (message.Text == "🌐 Сканировать")
            {
                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                string UserAccountID = Account.GetAccountExists(message.From.Id);

                string X_Position = Ships.Ship.GetSingleData(message.From.Id, "X");
                string Y_Position = Ships.Ship.GetSingleData(message.From.Id, "Y");
                string CurrentPositionLocationType = Program.GetMapSingleData(Convert.ToInt32(X_Position), Convert.ToInt32(Y_Position), "LocationType");

                if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != "") && (CurrentPositionLocationType == "2")) // В открытом космосе
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                            new [] // first row
                            {
                                new KeyboardButton("⚔️ Атаковать"),
                            },
                            new [] // last row
                            {
                                new KeyboardButton("🌐 Сканировать"),
                            },
                            new [] // last row
                            {
                                new KeyboardButton("🚪 Назад"),
                            },
                        }, true);
                        string NPCEnemy = NPC.Enemy.Generate(message.From.Id);
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "🌐 <b>Обнаружен противник:</b>\n\n" + NPCEnemy,
                                            replyMarkup: keyboard, parseMode: ParseMode.Html);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                }
                else
                {
                    Message.Send(message.From.Id, "Вы не можете сделать это здесь.", 1);
                }
            }

            else if (message.Text == "📜 Обучение")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                {
                    new [] // first row
                    {
                        new KeyboardButton("🚪 Назад в город"),
                    },
                }, true);
                int CharUpgradePrice = 1000;
                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Здесь вы можете потратить ваш ✨Опыт на повышение одной из 🔮Характеристик!\n Обучение: ✨" + CharUpgradePrice + " опыта за одну единицу.",
                                         replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

            }

            else if (message.Text == "👣 Покинуть город")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Вы вышли из города через главные ворота. Теперь стоите на пристани и наслаждаетесь солёным запахом ветра, что веет с моря.",
                                     replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

            }

            else if (message.Text == "🏤 Ратуша")
            {
                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                if (PlayerOnEvent != message.From.Id.ToString())
                {
                    var keyboard = new ReplyKeyboardMarkup(new[]
                {
                    new [] // first row
                    {
                        new KeyboardButton("📜 Отдел лицензий"),
                    },
                    new [] // first row
                    {
                        new KeyboardButton("🚪 Назад в город"),
                    }
                }, true);
                    try
                    {
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Вы вошли в здание 🏤 Ратуши. Все двери закрыты, кроме одной. Вы подходите и читаете надпись на табличке Отдел Капитанских Лицензий.",
                         replyMarkup: keyboard);
                    }
                    catch (Exception MyEx)
                    {
                        Console.WriteLine(MyEx);
                    }

                }
                else Message.Send((int)message.Chat.Id, "Вы сейчас заняты другим делом.", 1);
            }

            else if (message.Text == "💳 Банк")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                {
                    new [] // first row
                    {
                        new KeyboardButton("🚪 Назад в город"),
                    },
                }, true);
                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Вы вошли в здание 💳 Банка. Такое ощущение, что Кольцов ввел еще 2 обновы и теперь тут произошел экономический коллапс. Вы видите как перекати поле перекатывается по просторному холлу банка.",
                     replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

            }

            else if (message.Text == "🛡 Защита")
            {
                if (Account.GetSingleData(message.From.Id, "Status") != "666" && Account.GetSingleData(message.From.Id, "Status") != "")
                {
                    // Типы локаций: 1 - Планета, 2 - Космос, 3 - Астероид, 4 - Искривление пространства, 5 - Космическая станция
                    string X_Position = Ships.Ship.GetSingleData(message.From.Id, "X");
                    string Y_Position = Ships.Ship.GetSingleData(message.From.Id, "Y");
                    string LocationTypeID = Program.GetMapSingleData(Convert.ToInt32(X_Position), Convert.ToInt32(Y_Position), "LocationType");
                    if (LocationTypeID != "")
                    {
                        switch (LocationTypeID)
                        {
                            case "1": // Планета
                                Program.SetPlayerBattleAction(message.From.Id, X_Position, Y_Position, "Def", "0"); // ClanID - 0, пока не сделано
                                break;

                            case "2": // Космос
                                Program.SetPlayerBattleAction(message.From.Id, X_Position, Y_Position, "Def", "0"); // ClanID - 0, пока не сделано
                                break;

                            case "3": // Астероид
                                Program.SetPlayerBattleAction(message.From.Id, X_Position, Y_Position, "Def", "0"); // ClanID - 0, пока не сделано
                                break;

                            case "4": // Искривление пространства
                                Program.SetPlayerBattleAction(message.From.Id, X_Position, Y_Position, "Def", "0"); // ClanID - 0, пока не сделано
                                break;

                            case "5": // Космическая станция
                                Program.SetPlayerBattleAction(message.From.Id, X_Position, Y_Position, "Def", "0"); // ClanID - 0, пока не сделано
                                break;

                            default:
                                Message.Send(message.From.Id, "Ошибка определения типа локации! #4487", 1);
                                break;
                        }
                    }
                    else Message.Send(message.From.Id, "Ошибка номер 953! Ваше местоположение не обнаружено.", 1);
                }
                else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);
            }

            else if (message.Text == "⚔️ Атака")
            {
                if (Account.GetSingleData(message.From.Id, "Status") != "666" && Account.GetSingleData(message.From.Id, "Status") != "")
                {
                    // Типы локаций: 1 - Планета, 2 - Космос, 3 - Астероид, 4 - Искривление пространства, 5 - Космическая станция
                    string X_Position = Ships.Ship.GetSingleData(message.From.Id, "X");
                    string Y_Position = Ships.Ship.GetSingleData(message.From.Id, "Y");
                    string LocationTypeID = Program.GetMapSingleData(Convert.ToInt32(X_Position), Convert.ToInt32(Y_Position), "LocationType");
                    if (LocationTypeID != "")
                    {
                        switch (LocationTypeID)
                        {
                            case "1": // Планета
                                Program.SetPlayerBattleAction(message.From.Id, X_Position, Y_Position, "Attack", "0"); // ClanID - 0, пока не сделано
                                break;

                            case "2": // Космос
                                Program.SetPlayerBattleAction(message.From.Id, X_Position, Y_Position, "Attack", "0"); // ClanID - 0, пока не сделано
                                break;

                            case "3": // Астероид
                                Program.SetPlayerBattleAction(message.From.Id, X_Position, Y_Position, "Attack", "0"); // ClanID - 0, пока не сделано
                                break;

                            case "4": // Искривление пространства
                                Program.SetPlayerBattleAction(message.From.Id, X_Position, Y_Position, "Attack", "0"); // ClanID - 0, пока не сделано
                                break;

                            case "5": // Космическая станция
                                Program.SetPlayerBattleAction(message.From.Id, X_Position, Y_Position, "Attack", "0"); // ClanID - 0, пока не сделано
                                break;

                            default:
                                Message.Send(message.From.Id, "Ошибка определения типа локации! #4488", 1);
                                break;
                        }
                    }
                    else Message.Send(message.From.Id, "Ошибка номер 954! Ваше местоположение не обнаружено.", 1);
                }
                else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                //string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                //string UserAccountID = Account.GetAccountExists(message.From.Id);
                //if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != ""))
                //{
                //    string X_Position = Account.GetSingleData(message.From.Id, "X");
                //    Console.WriteLine("Debug X_Position: " + X_Position);
                //    string Y_Position = Account.GetSingleData(message.From.Id, "Y");
                //    Console.WriteLine("Debug Y_Position: " + Y_Position);
                //    string BaseID = GetPlayerBaseSingleData("ID", X_Position, Y_Position);
                //    if (BaseID != "")
                //    {
                //        string TargetBaseID = GetMapSingleData(Convert.ToInt32(X_Position), Convert.ToInt32(Y_Position), "LocationType");
                //        if (TargetBaseID != "")
                //        {
                //            SetPlayerBaseBattleAction(message.From.Id, Convert.ToInt32(TargetBaseID), 1);
                //        }
                //        else
                //        {
                //            Message.Send(message.From.Id, "Нужно находиться на территории базы группировки, чтобы атаковать её.", 1);
                //        }
                //    }
                //    else
                //    {
                //        Message.Send(message.From.Id, "Нужно находиться на территории базы группировки, чтобы атаковать её.", 1);
                //    }
                //}
                //else
                //{
                //    Message.Send(message.From.Id, "Вы не можете этого сделать сейчас.", 1);
                //}
            }

            else if (message.Text.StartsWith("/craft"))
            {
                string Module2 = string.Empty;
                if (Ships.Ship.isHasModuleByItemID(message.From.Id, 41) == true)
                {
                    Module2 = "📚 Библиотека";
                }
                var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                            new [] // first row
                        {
                            new KeyboardButton("🔳 Доступные чертежи"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                            new [] // last row
                        {
                            new KeyboardButton(Module2),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);

                string[] cmds = message.Text.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                string RecipeID = string.Empty;
                string ItemsCount = string.Empty;
                try
                {
                    RecipeID = cmds[1];
                    ItemsCount = cmds[2];
                }
                catch
                {
                    Console.WriteLine("Error on RecipeID ItemsCount = cmds[]");
                }
                RecipeID = RecipeID.OnlyNumbers();
                ItemsCount = ItemsCount.OnlyNumbers();

                if (RecipeID != "" && ItemsCount != "")
                {
                    string CraftResult = Program.CraftItemToPlayer(message.From.Id, Convert.ToInt32(RecipeID), Convert.ToInt32(ItemsCount));
                    Message.Send(message.From.Id, CraftResult, 6);
                }
                else await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Ошибка №3884!", replyMarkup: keyboard, parseMode: ParseMode.Html);
            }

            else if (message.Text.StartsWith("/einf"))
            {
                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                string UserAccountID = Account.GetAccountExists(message.From.Id);

                string X_Position = Ships.Ship.GetSingleData(message.From.Id, "X");
                string Y_Position = Ships.Ship.GetSingleData(message.From.Id, "Y");
                string CurrentPositionLocationType = Program.GetMapSingleData(Convert.ToInt32(X_Position), Convert.ToInt32(Y_Position), "LocationType");

                if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != "") && (CurrentPositionLocationType == "2")) // В открытом космосе
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton(""),
                        }
                        }, true);
                        string NPCEnemy_ID = string.Empty;
                        string[] cmds;
                        if (message.Text == "/einf" || message.Text == "/einf_")
                        {
                            return NotFound();
                        }
                        else
                        {
                            cmds = message.Text.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                            NPCEnemy_ID = cmds[1];
                            NPCEnemy_ID = NPCEnemy_ID.OnlyNumbers();
                        }

                        if (NPCEnemy_ID != "")
                        {

                            // await Bot.Api.SendTextMessageAsync(message.Chat.Id, "<b>🌐 Данные сканера:</b> " + "\n\n" + NPCEnemy_List, replyMarkup: keyboard, parseMode: ParseMode.Html);
                        }
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                }
                else
                {
                    Message.Send(message.From.Id, "Вы не можете сделать это здесь.", 1);
                }
            }

            else if (message.Text.StartsWith("/plans"))
            {
                string Module2 = string.Empty;
                if (Ships.Ship.isHasModuleByItemID(message.From.Id, 41) == true)
                {
                    Module2 = "📚 Библиотека";
                }
                var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                            new [] // first row
                        {
                            new KeyboardButton("🔳 Доступные чертежи"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                            new [] // last row
                        {
                            new KeyboardButton(Module2),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);
                string PageID = string.Empty;
                string[] cmds;
                if (message.Text == "/plans" || message.Text == "/plans_")
                {
                    PageID = "1";
                }
                else
                {
                    cmds = message.Text.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                    PageID = cmds[1];
                    PageID = PageID.OnlyNumbers();
                }

                if (PageID != "")
                {
                    string RecipesList = Recipes.GetListForPlayer(message.From.Id, Convert.ToInt32(PageID));
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "<b>Доступные чертежи:</b>\n" + RecipesList, replyMarkup: keyboard, parseMode: ParseMode.Html);
                }

            }

            else if (message.Text.StartsWith("/bibl"))
            {
                if (Ships.Ship.isHasModuleByItemID(message.From.Id, 41) == true)
                {
                    var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                            new [] // first row
                        {
                            new KeyboardButton("🔳 Изучение чертежей"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                            new [] // last row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);
                    string[] cmds = message.Text.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                    string Page = string.Empty;
                    try
                    {
                        Page = cmds[1];
                    }
                    catch
                    {
                        Console.WriteLine("Error on PageID = cmds[]");
                    }
                    Page = Page.OnlyNumbers();
                    if (Page != "" && Page != "0")
                    {
                        string UnlearnedRecipes = Recipes.GetUnlearnedListForPlayer(message.From.Id, Convert.ToInt32(Page));
                        Message.Send(message.From.Id, "📚 <b>Библиотека</b>\n" + "Страница " + Page + "\n" + UnlearnedRecipes, 1);
                    }
                    else await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Ошибка синтаксиса команды!", replyMarkup: keyboard, parseMode: ParseMode.Html);
                }
                else Message.Send(message.From.Id, "На вашем корабле не установлен указанный модуль.", 1);

            }

            else if (message.Text.StartsWith("/sells"))
            {

                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);


                string[] cmds = message.Text.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                string Page = string.Empty;
                try
                {
                    Page = cmds[1];
                }
                catch
                {
                    Console.WriteLine("Error on PageID = cmds[]");
                }

                string X = Ships.Ship.GetSingleData(message.From.Id, "X");
                string Y = Ships.Ship.GetSingleData(message.From.Id, "Y");
                bool isNPCVendorInPlayerPosition = NPC.Friend.IsExistsInMapPointByType(Convert.ToInt32(X), Convert.ToInt32(Y), 0);

                if (isNPCVendorInPlayerPosition == true)
                {
                    if (Page != "" && Page != "0")
                    {
                        Page = Page.OnlyNumbers();
                        string SellList = Inventory.GetSellList(message.From.Id, Convert.ToInt32(Page));
                        Message.Send(message.From.Id, "Страница " + Page + "\n" + SellList, 1);
                    }
                    else await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Ошибка синтаксиса команды!", replyMarkup: keyboard, parseMode: ParseMode.Html);
                }
                else await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Вы не можете продавать вещи в этой локации.", replyMarkup: keyboard, parseMode: ParseMode.Html);

            }

            else if (message.Text.StartsWith("/sell"))
            {
                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                string UserAccountID = Account.GetAccountExists(message.From.Id);

                if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != "")) // В открытом космосе
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);

                        string[] cmds = message.Text.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                        string ItemID = string.Empty;
                        string ItemCount = string.Empty;
                        bool NoCount = false;

                        try
                        {
                            ItemID = cmds[1];
                            ItemCount = cmds[2];
                        }
                        catch
                        {
                            Console.WriteLine("Error on RecipeID ItemsCount = cmds[]");
                        }

                        ItemID = ItemID.OnlyNumbers();
                        ItemCount = ItemCount.OnlyNumbers();

                        string X = Ships.Ship.GetSingleData(message.From.Id, "X");
                        string Y = Ships.Ship.GetSingleData(message.From.Id, "Y");
                        bool isNPCVendorInPlayerPosition = NPC.Friend.IsExistsInMapPointByType(Convert.ToInt32(X), Convert.ToInt32(Y), 0);
                        if (ItemID != "" && ItemCount != "" && ItemCount != "0" && ItemID != "0" && isNPCVendorInPlayerPosition == true)
                        {
                            bool hasItem = isHasItem(message.From.Id, Convert.ToInt32(ItemID), Convert.ToInt32(ItemCount));
                            if (hasItem == true)
                            {
                                Inventory.Items.Item.GetInfo(Convert.ToInt32(ItemID), out string ItemName, out string ItemUsable, out string ItemWearable, out string ItemWearSlot, out string ItemPrice, out string ItemType, out string ItemPower);
                                float ItemFinalPrice = Program.ProcentResultCount(Convert.ToInt32(ItemPrice), 50);
                                int ItemFinalPriceInt = (int)Math.Round(ItemFinalPrice);
                                int PriceSum = Convert.ToInt32(ItemCount) * ItemFinalPriceInt;
                                bool DeleteStatus = DeleteItem(message.From.Id, ItemID, ItemCount);
                                if (DeleteStatus == true)
                                {
                                    bool SetGoldStatus = Account.SetGold(message.From.Id, PriceSum, false);
                                    if (SetGoldStatus == true)
                                    {
                                        Message.Send(message.From.Id, "Вы продали " + ItemCount + " 📦 " + ItemName + " общей стоимостью 💳 " + PriceSum.ToString() + ".", 1);
                                    }
                                    else Message.Send(message.From.Id, "Ошибка начисления оплаты! Обратитесь к разработчику.", 1);
                                }
                                else Message.Send(message.From.Id, "Ошибка! Возможно у вас нет данного предмета!", 1);
                            }
                            else Message.Send(message.From.Id, "У вас нет данного предмета!", 1);
                        }
                        //else if (ItemID != "" && ItemID != "0")
                        //{
                        //    bool hasItem = isHasItem(message.From.Id, Convert.ToInt32(ItemID), Convert.ToInt32(ItemCount));
                        //    if (hasItem == true)
                        //    {
                        //        Inventory.Items.Item.GetInfo(Convert.ToInt32(ItemID), out string ItemName, out string ItemUsable, out string ItemWearable, out string ItemWearSlot, out string ItemPrice, out string ItemType, out string ItemPower);
                        //        string PlayerItemsCount = Inventory.GetItemCount(message.From.Id, ItemID);
                        //        int PriceSum = Convert.ToInt32(PlayerItemsCount) * Convert.ToInt32(ItemPrice);
                        //        Message.Send(message.From.Id, "📦 " + ItemName + "\n💳 Стоимость за шт.: " + ItemPrice + "\n💳 Общая стоимость: " + PriceSum + "\n\nПродать 1: /sell_" + ItemID + "_1" + "\nПродать всё: /sell_" + ItemID + "_" + PlayerItemsCount, 1);
                        //    }
                        //    else Message.Send(message.From.Id, "У вас нет данного предмета!", 1);
                        //}
                        else await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Ошибка синтаксиса команды!", replyMarkup: keyboard, parseMode: ParseMode.Html);

                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                }
                else
                {
                    Message.Send(message.From.Id, "Сейчас вы не можете сделать это.", 1);
                }

                
            }

            else if (message.Text.StartsWith("/rlearn"))
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                                {
                            new [] // first row
                        {
                            new KeyboardButton("🔳 Изучение чертежей"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                            new [] // last row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);
                string[] cmds = message.Text.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                string RecipeID = string.Empty;
                try
                {
                    RecipeID = cmds[1];
                }
                catch
                {
                    Console.WriteLine("RecipeID = cmds[1] error!");
                }
                Console.WriteLine("DEBUG1: RecipeID = " + RecipeID);
                Regex REG = new Regex("[0-9]+");
                // ищем совпадения в исходной строке
                MatchCollection mc = REG.Matches(RecipeID);
                StringBuilder sb = new StringBuilder();
                // создаем строку результата
                foreach (Match matc in mc)
                    sb.Append(matc.Value);
                // а это твой результат - только цифры
                RecipeID = sb.ToString();
                char[] charsToTrim = { '*', ' ', '\'', '№', '@', '!', '#', '$', '%', '^', '&', '(', ')', '"', ';', ':', '?', '/' }; // Обрезаем символы
                RecipeID = RecipeID.Replace('(', '@');
                RecipeID = RecipeID.Replace(')', '@');
                RecipeID = RecipeID.Trim(charsToTrim);
                if (RecipeID != "" && RecipeID != " ")
                {
                    if (Ships.Ship.isHasModuleByItemID(message.From.Id, 41) == true)
                    {
                        if (Account.isHasRecipe(message.From.Id, Convert.ToInt32(RecipeID)))
                        {
                            string RecipesInfoString = string.Empty;
                            Recipes.GetAllInfo(Convert.ToInt32(RecipeID), out string RecipeName, out string RecipePrice, out string RecipeCraftedItemID, out string RecipeCraftedItemPower, out string RecipeMaterial1, out string RecipeMaterial2, out string RecipeMaterial3, out string RecipeMaterial4, out string RecipeMaterial5, out string RecipeMaterial6, out string RecipeMaterial1_Count, out string RecipeMaterial2_Count, out string RecipeMaterial3_Count, out string RecipeMaterial4_Count, out string RecipeMaterial5_Count, out string RecipeMaterial6_Count, out string RecipeCraftPriceMoney, out string RecipeCraftPriceEnergy);
                            Console.WriteLine("Test 1");
                            if (RecipeMaterial1 != "0")
                            {
                                Inventory.Items.Item.GetInfo(Convert.ToInt32(RecipeMaterial1), out string ItemName, out string ItemUsable, out string ItemWearable, out string ItemWearSlot, out string ItemPrice, out string ItemType, out string ItemPower);
                                RecipesInfoString += "📦 " + ItemName + " (" + RecipeMaterial1_Count + ")\n";
                            }
                            else if (RecipeMaterial2 != "0")
                            {
                                Inventory.Items.Item.GetInfo(Convert.ToInt32(RecipeMaterial2), out string ItemName, out string ItemUsable, out string ItemWearable, out string ItemWearSlot, out string ItemPrice, out string ItemType, out string ItemPower);
                                RecipesInfoString += "📦 " + ItemName + " (" + RecipeMaterial2_Count + ")\n";
                            }
                            else if (RecipeMaterial3 != "0")
                            {
                                Inventory.Items.Item.GetInfo(Convert.ToInt32(RecipeMaterial3), out string ItemName, out string ItemUsable, out string ItemWearable, out string ItemWearSlot, out string ItemPrice, out string ItemType, out string ItemPower);
                                RecipesInfoString += "📦 " + ItemName + " (" + RecipeMaterial3_Count + ")\n";
                            }
                            else if (RecipeMaterial4 != "0")
                            {
                                Inventory.Items.Item.GetInfo(Convert.ToInt32(RecipeMaterial4), out string ItemName, out string ItemUsable, out string ItemWearable, out string ItemWearSlot, out string ItemPrice, out string ItemType, out string ItemPower);
                                RecipesInfoString += "📦 " + ItemName + " (" + RecipeMaterial4_Count + ")\n";
                            }
                            else if (RecipeMaterial5 != "0")
                            {
                                Inventory.Items.Item.GetInfo(Convert.ToInt32(RecipeMaterial5), out string ItemName, out string ItemUsable, out string ItemWearable, out string ItemWearSlot, out string ItemPrice, out string ItemType, out string ItemPower);
                                RecipesInfoString += "📦 " + ItemName + " (" + RecipeMaterial5_Count + ")\n";
                            }
                            else if (RecipeMaterial6 != "0")
                            {
                                Inventory.Items.Item.GetInfo(Convert.ToInt32(RecipeMaterial6), out string ItemName, out string ItemUsable, out string ItemWearable, out string ItemWearSlot, out string ItemPrice, out string ItemType, out string ItemPower);
                                RecipesInfoString += "📦 " + ItemName + " (" + RecipeMaterial6_Count + ")\n";
                            }
                            Console.WriteLine("Test 2");
                            string CurrentRecipeLevel = Account.GetRecipeDataByID(message.From.Id, Convert.ToInt32(RecipeID), "RecipeLevel");
                            Console.WriteLine("Test 3");
                            Message.Send(message.From.Id, "🔳 <b>" + RecipeName + "</b>\n\n<b>Компоненты:</b>\n" + RecipesInfoString + "\n📚 <b>Изучен на:</b>\n" + CurrentRecipeLevel + "/1000" + "\n\n<b>Продолжить изучение:</b>\n/startlearn_" + RecipeID, 6);
                            Console.WriteLine("Test 4");
                        }
                        else
                        {
                            Console.WriteLine("Hest 0");
                            if (Recipes.isRecipeInDatabase(Convert.ToInt32(RecipeID)) == true)
                            {
                                Console.WriteLine("Hest 1");
                                bool RecipeAdded = Account.AddRecipe(message.From.Id, RecipeID, "0", "1000");
                                if (RecipeAdded == true)
                                {
                                    Console.WriteLine("Hest 2");
                                    string RecipesInfoString = string.Empty;
                                    Recipes.GetAllInfo(Convert.ToInt32(RecipeID), out string RecipeName, out string RecipePrice, out string RecipeCraftedItemID, out string RecipeCraftedItemPower, out string RecipeMaterial1, out string RecipeMaterial2, out string RecipeMaterial3, out string RecipeMaterial4, out string RecipeMaterial5, out string RecipeMaterial6, out string RecipeMaterial1_Count, out string RecipeMaterial2_Count, out string RecipeMaterial3_Count, out string RecipeMaterial4_Count, out string RecipeMaterial5_Count, out string RecipeMaterial6_Count, out string RecipeCraftPriceMoney, out string RecipeCraftPriceEnergy);
                                    Console.WriteLine("Hest 3");
                                    if (RecipeMaterial1 != "0")
                                    {
                                        Inventory.Items.Item.GetInfo(Convert.ToInt32(RecipeMaterial1), out string ItemName, out string ItemUsable, out string ItemWearable, out string ItemWearSlot, out string ItemPrice, out string ItemType, out string ItemPower);
                                        RecipesInfoString += "📦 " + ItemName + " (" + RecipeMaterial1_Count + ")\n";
                                    }
                                    else if (RecipeMaterial2 != "0")
                                    {
                                        Inventory.Items.Item.GetInfo(Convert.ToInt32(RecipeMaterial2), out string ItemName, out string ItemUsable, out string ItemWearable, out string ItemWearSlot, out string ItemPrice, out string ItemType, out string ItemPower);
                                        RecipesInfoString += "📦 " + ItemName + " (" + RecipeMaterial2_Count + ")\n";
                                    }
                                    else if (RecipeMaterial3 != "0")
                                    {
                                        Inventory.Items.Item.GetInfo(Convert.ToInt32(RecipeMaterial3), out string ItemName, out string ItemUsable, out string ItemWearable, out string ItemWearSlot, out string ItemPrice, out string ItemType, out string ItemPower);
                                        RecipesInfoString += "📦 " + ItemName + " (" + RecipeMaterial3_Count + ")\n";
                                    }
                                    else if (RecipeMaterial4 != "0")
                                    {
                                        Inventory.Items.Item.GetInfo(Convert.ToInt32(RecipeMaterial4), out string ItemName, out string ItemUsable, out string ItemWearable, out string ItemWearSlot, out string ItemPrice, out string ItemType, out string ItemPower);
                                        RecipesInfoString += "📦 " + ItemName + " (" + RecipeMaterial4_Count + ")\n";
                                    }
                                    else if (RecipeMaterial5 != "0")
                                    {
                                        Inventory.Items.Item.GetInfo(Convert.ToInt32(RecipeMaterial5), out string ItemName, out string ItemUsable, out string ItemWearable, out string ItemWearSlot, out string ItemPrice, out string ItemType, out string ItemPower);
                                        RecipesInfoString += "📦 " + ItemName + " (" + RecipeMaterial5_Count + ")\n";
                                    }
                                    else if (RecipeMaterial6 != "0")
                                    {
                                        Inventory.Items.Item.GetInfo(Convert.ToInt32(RecipeMaterial6), out string ItemName, out string ItemUsable, out string ItemWearable, out string ItemWearSlot, out string ItemPrice, out string ItemType, out string ItemPower);
                                        RecipesInfoString += "📦 " + ItemName + " (" + RecipeMaterial6_Count + ")\n";
                                    }
                                    Console.WriteLine("Hest 4");
                                    string CurrentRecipeLevel = Account.GetRecipeDataByID(message.From.Id, Convert.ToInt32(RecipeID), "RecipeLevel");
                                    Console.WriteLine("Hest 5");
                                    Message.Send(message.From.Id, "🔳 <b>" + RecipeName + "</b>\n\n<b>Компоненты:</b>\n" + RecipesInfoString + "\n📚 <b>Изучен на:</b>\n" + CurrentRecipeLevel + "/1000" + "\n\n<b>Продолжить изучение:</b>\n/startlearn_" + RecipeID, 6);
                                    Console.WriteLine("Hest 6");
                                }
                                else
                                {
                                    Message.Send(message.From.Id, "Ошибка добавления рецепта!", 1);
                                }

                            }
                            else
                            {
                                Message.Send(message.From.Id, "Неправильный синтаксис команды!", 1);
                            }
                        }
                    }
                    else Message.Send(message.From.Id, "Нет установленного модуля!", 1);
                }
                else
                {
                    Console.WriteLine("DEBUG: RecipeID = " + RecipeID);
                    Message.Send(message.From.Id, "Ошибка #1430", 1);
                }
            }

            else if (message.Text.StartsWith("/startlearn"))
            {
                string[] cmds = message.Text.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                string RecipeID = string.Empty;
                try
                {
                    RecipeID = cmds[1];
                }
                catch
                {
                    Console.WriteLine("RecipeID = cmds[1] error!");
                }
                Console.WriteLine("DEBUG1: RecipeID = " + RecipeID);
                Regex REG = new Regex("[0-9]+");
                // ищем совпадения в исходной строке
                MatchCollection mc = REG.Matches(RecipeID);
                StringBuilder sb = new StringBuilder();
                // создаем строку результата
                foreach (Match matc in mc)
                    sb.Append(matc.Value);
                // а это твой результат - только цифры
                RecipeID = sb.ToString();
                char[] charsToTrim = { '*', ' ', '\'', '№', '@', '!', '#', '$', '%', '^', '&', '(', ')', '"', ';', ':', '?', '/' }; // Обрезаем символы
                RecipeID = RecipeID.Replace('(', '@');
                RecipeID = RecipeID.Replace(')', '@');
                RecipeID = RecipeID.Trim(charsToTrim);
                if (RecipeID != "" && RecipeID != " ")
                {
                    if (Ships.Ship.isHasModuleByItemID(message.From.Id, 41) == true)
                    {
                        if (Account.isHasRecipe(message.From.Id, Convert.ToInt32(RecipeID)))
                        {
                            string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                            string UserAccountID = Account.GetAccountExists(message.From.Id);
                            if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != ""))
                            {
                                if (Account.GetSingleData(message.From.Id, "Status") != "666")
                                {
                                    Program.PlayerDeleteBetweenEvent(message.From.Id);
                                    Program.PlayerSetEvent((int)message.From.Id, 4, 5, Convert.ToInt32(RecipeID)); // Ремонт корабля
                                }
                                else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);
                            }
                            else Message.Send(message.From.Id, "Вы сейчас заняты.", 1);
                        }
                        else
                        {
                            Message.Send(message.From.Id, "Ошибка #4390", 1);
                        }
                    }
                    else Message.Send(message.From.Id, "Нет установленного модуля!", 1);
                }
                else Message.Send(message.From.Id, "Ошибка #1530", 1);
            }

            else if (message.Text.StartsWith("/rshow"))
            {
                string[] cmds = message.Text.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                string RecipeID = string.Empty;
                try
                {
                    RecipeID = cmds[1];
                }
                catch
                {
                    Console.WriteLine("RecipeID = cmds[1] error!");
                }
                Console.WriteLine("DEBUG1: RecipeID = " + RecipeID);
                Regex REG = new Regex("[0-9]+");
                // ищем совпадения в исходной строке
                MatchCollection mc = REG.Matches(RecipeID);
                StringBuilder sb = new StringBuilder();
                // создаем строку результата
                foreach (Match matc in mc)
                    sb.Append(matc.Value);
                // а это твой результат - только цифры
                RecipeID = sb.ToString();
                char[] charsToTrim = { '*', ' ', '\'', '№', '@', '!', '#', '$', '%', '^', '&', '(', ')', '"', ';', ':', '?', '/' }; // Обрезаем символы
                RecipeID = RecipeID.Replace('(', '@');
                RecipeID = RecipeID.Replace(')', '@');
                RecipeID = RecipeID.Trim(charsToTrim);
                if (RecipeID != "" && RecipeID != " ")
                {
                    if (Account.isHasRecipe(message.From.Id, Convert.ToInt32(RecipeID)))
                    {
                        string MaterialsToCraft = string.Empty;
                        Recipes.GetAllInfo(Convert.ToInt32(RecipeID), out string RecipeName, out string RecipePrice, out string RecipeCraftedItemID, out string RecipeCraftedItemPower, out string RecipeMaterial1, out string RecipeMaterial2, out string RecipeMaterial3, out string RecipeMaterial4, out string RecipeMaterial5, out string RecipeMaterial6, out string RecipeMaterial1_Count, out string RecipeMaterial2_Count, out string RecipeMaterial3_Count, out string RecipeMaterial4_Count, out string RecipeMaterial5_Count, out string RecipeMaterial6_Count, out string RecipeCraftPriceMoney, out string RecipeCraftPriceEnergy);
                        string CraftedItemName = Inventory.Items.Item.GetSingleData(Convert.ToInt32(RecipeCraftedItemID), "Name");
                        if (Convert.ToInt32(RecipeMaterial1) > 0 && RecipeMaterial1 != "")
                        {
                            string TempName = Inventory.Items.Item.GetSingleData(Convert.ToInt32(RecipeMaterial1), "Name");
                            bool HasItem = Account.isHasItem(Convert.ToInt32(message.From.Id), Convert.ToInt32(RecipeMaterial1), Convert.ToInt32(RecipeMaterial1_Count));
                            string PlayerHasItemIndicator = string.Empty;
                            if (HasItem == true)
                            {
                                PlayerHasItemIndicator = " ✅";
                            }
                            else PlayerHasItemIndicator = " ❌";
                            MaterialsToCraft = MaterialsToCraft + "📦 " + TempName + " (" + RecipeMaterial1_Count + ")" + PlayerHasItemIndicator + "\n";

                        }
                        if (Convert.ToInt32(RecipeMaterial2) > 0 && RecipeMaterial2 != "")
                        {
                            string TempName = Inventory.Items.Item.GetSingleData(Convert.ToInt32(RecipeMaterial2), "Name");
                            bool HasItem = Account.isHasItem(Convert.ToInt32(message.From.Id), Convert.ToInt32(RecipeMaterial2), Convert.ToInt32(RecipeMaterial2_Count));
                            string PlayerHasItemIndicator = string.Empty;
                            if (HasItem == true)
                            {
                                PlayerHasItemIndicator = " ✅";
                            }
                            else PlayerHasItemIndicator = " ❌";
                            MaterialsToCraft = MaterialsToCraft + "📦 " + TempName + " (" + RecipeMaterial2_Count + ")" + PlayerHasItemIndicator + "\n";
                        }
                        if (Convert.ToInt32(RecipeMaterial3) > 0 && RecipeMaterial3 != "")
                        {
                            string TempName = Inventory.Items.Item.GetSingleData(Convert.ToInt32(RecipeMaterial3), "Name");
                            bool HasItem = Account.isHasItem(Convert.ToInt32(message.From.Id), Convert.ToInt32(RecipeMaterial3), Convert.ToInt32(RecipeMaterial3_Count));
                            string PlayerHasItemIndicator = string.Empty;
                            if (HasItem == true)
                            {
                                PlayerHasItemIndicator = " ✅";
                            }
                            else PlayerHasItemIndicator = " ❌";
                            MaterialsToCraft = MaterialsToCraft + "📦 " + TempName + " (" + RecipeMaterial3_Count + ")" + PlayerHasItemIndicator + "\n";
                        }
                        if (Convert.ToInt32(RecipeMaterial4) > 0 && RecipeMaterial4 != "")
                        {
                            string TempName = Inventory.Items.Item.GetSingleData(Convert.ToInt32(RecipeMaterial4), "Name");
                            bool HasItem = Account.isHasItem(Convert.ToInt32(message.From.Id), Convert.ToInt32(RecipeMaterial4), Convert.ToInt32(RecipeMaterial4_Count));
                            string PlayerHasItemIndicator = string.Empty;
                            if (HasItem == true)
                            {
                                PlayerHasItemIndicator = " ✅";
                            }
                            else PlayerHasItemIndicator = " ❌";
                            MaterialsToCraft = MaterialsToCraft + "📦 " + TempName + " (" + RecipeMaterial4_Count + ")" + PlayerHasItemIndicator + "\n";
                        }
                        if (Convert.ToInt32(RecipeMaterial5) > 0 && RecipeMaterial5 != "")
                        {
                            string TempName = Inventory.Items.Item.GetSingleData(Convert.ToInt32(RecipeMaterial5), "Name");
                            bool HasItem = Account.isHasItem(Convert.ToInt32(message.From.Id), Convert.ToInt32(RecipeMaterial5), Convert.ToInt32(RecipeMaterial5_Count));
                            string PlayerHasItemIndicator = string.Empty;
                            if (HasItem == true)
                            {
                                PlayerHasItemIndicator = " ✅";
                            }
                            else PlayerHasItemIndicator = " ❌";
                            MaterialsToCraft = MaterialsToCraft + "📦 " + TempName + " (" + RecipeMaterial5_Count + ")" + PlayerHasItemIndicator + "\n";
                        }
                        if (Convert.ToInt32(RecipeMaterial6) > 0 && RecipeMaterial6 != "")
                        {
                            string TempName = Inventory.Items.Item.GetSingleData(Convert.ToInt32(RecipeMaterial6), "Name");
                            bool HasItem = Account.isHasItem(Convert.ToInt32(message.From.Id), Convert.ToInt32(RecipeMaterial6), Convert.ToInt32(RecipeMaterial6_Count));
                            string PlayerHasItemIndicator = string.Empty;
                            if (HasItem == true)
                            {
                                PlayerHasItemIndicator = " ✅";
                            }
                            else PlayerHasItemIndicator = " ❌";
                            MaterialsToCraft = MaterialsToCraft + "📦 " + TempName + " (" + RecipeMaterial6_Count + ")" + PlayerHasItemIndicator + "\n";
                        }
                        string Module2 = string.Empty;
                        if (Ships.Ship.isHasModuleByItemID(message.From.Id, 41) == true)
                        {
                            Module2 = "📚 Библиотека";
                        }
                        var keyboard = new ReplyKeyboardMarkup(new[]
                                {
                            new [] // first row
                        {
                            new KeyboardButton("🔳 Доступные чертежи"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                            new [] // last row
                        {
                            new KeyboardButton(Module2),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "<b>📃 Чертеж: " + RecipeName + "</b>\n\nСоздает предмет: \n📦 " + CraftedItemName + " (1)\n\n<b>Материалы для создания:</b>\n" + MaterialsToCraft + "\n<b>Создать предмет:</b> /craft_" + RecipeID + "_1\n<b>Создать 5 предметов:</b> /craft_" + RecipeID + "_5", replyMarkup: keyboard, parseMode: ParseMode.Html);
                    }
                    else
                    {
                        Message.Send(message.From.Id, "У вас нет данного чертежа!", 6);
                    }
                }
                else
                {
                    Console.WriteLine("DEBUG: RecipeID = " + RecipeID);
                    Message.Send(message.From.Id, "Ошибка #1330", 1);
                }

            }

            else if (message.Text == "/clan_pay")
            {
                if (Account.IsInClan(message.From.Id) == true)
                {
                    string ClanID = Clans.GetDataByAccountID(message.From.Id, "ClanID");
                    if (Clans.GetDataByClanID(Convert.ToInt32(ClanID), "Active") == "0")
                    {
                        string ClanName = Clans.GetDataByClanID(Convert.ToInt32(ClanID), "ClanName");
                        string ClanTag = Clans.GetDataByClanID(Convert.ToInt32(ClanID), "ClanTag");

                        if (ClanName != "" && ClanTag != "")
                        {
                            if (Account.TakeGold(message.From.Id, ClanCreationPrice) == true)
                            {
                                Clans.SetClanData(Convert.ToInt32(ClanID), "Active", "1");
                                Message.Send(message.From.Id, "<b>Создан новый клан:\n</b>[" + ClanTag + "] " + ClanName + "\n<b>Уровень:</b> 1\n<b>Макс. членов:</b> 5", 7);
                            }
                            else Message.Send(message.From.Id, "У вас не хватает денег для создания клана!\nСтоимость: 💳 " + ClanCreationPrice, 1);
                        }
                        else
                        {
                            string PlayerStatus = Account.GetSingleData(message.From.Id, "Status");
                            if (PlayerStatus == "4500")
                            {
                                Message.Send(message.From.Id, "Вы не указали название клана!\nВведите название вашего клана:\n(Не более 25 и не менее 3 букв)", 1);
                            }
                            else if (PlayerStatus == "4501")
                            {
                                Message.Send(message.From.Id, "Вы не указали тэг клана!\nВведите тэг вашего клана:\n(Не более 4 и не менее 2 букв)", 1);
                            }
                            else Message.Send(message.From.Id, "Неожиданная ошибка. Попробуйте использовать /clan_create", 1);
                        }
                    }
                    else Message.Send(message.From.Id, "Оплата не требуется.", 1);
                }
                else Message.Send(message.From.Id, "У вас нет клана! Нажмите /clan_create чтобы создать собственный!\nСтоимость: 💳 " + ClanCreationPrice, 1);
            }

            else if (message.Text == "🚻 Члены клана")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🚻 Члены клана"),
                            new KeyboardButton("⚙️ Управление кланом"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚀 Ангар клана"),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);
                if (Account.IsInActiveClan(message.From.Id) == true)
                {
                    string ClanID = Clans.GetDataByAccountID(message.From.Id, "ClanID");
                    string ClanTag = Clans.GetDataByClanID(Convert.ToInt32(ClanID), "ClanTag");
                    string CLanList = Clans.GetPlayersList(Convert.ToInt32(ClanID));
                    await Bot.Api.SendTextMessageAsync(message.From.Id, "<b>Члены клана [" + ClanTag + "]:</b>\n" + CLanList, replyMarkup: keyboard, parseMode: ParseMode.Html);
                }
                else await Bot.Api.SendTextMessageAsync(message.From.Id, "Вы не состоите в клане или ваш клан не активен.\nХотите создать собственный клан? Используйте /clan_create", replyMarkup: keyboard, parseMode: ParseMode.Html);
            }

            else if (message.Text == "🚀 Ангар клана")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🚻 Члены клана"),
                            new KeyboardButton("⚙️ Управление кланом"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚀 Ангар клана"),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);
                if (Account.IsInActiveClan(message.From.Id) == true)
                {
                    string ClanID = Clans.GetDataByAccountID(message.From.Id, "ClanID");
                    string ClanTag = Clans.GetDataByClanID(Convert.ToInt32(ClanID), "ClanTag");
                    string CLanList = Clans.GetShipsList(Convert.ToInt32(ClanID));
                    await Bot.Api.SendTextMessageAsync(message.From.Id, "🚀 <b>Ангар клана [" + ClanTag + "]:</b>\n" + CLanList, replyMarkup: keyboard, parseMode: ParseMode.Html);
                }
                else await Bot.Api.SendTextMessageAsync(message.From.Id, "Вы не состоите в клане или ваш клан не активен.\nХотите создать собственный клан? Используйте /clan_create", replyMarkup: keyboard, parseMode: ParseMode.Html);
            }

            else if (message.Text == "👨‍👦 Клан")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🚻 Члены клана"),
                            new KeyboardButton("⚙️ Управление кланом"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚀 Ангар клана"),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);
                if (Account.IsInActiveClan(message.From.Id) == true)
                {
                    string ClanID = Clans.GetDataByAccountID(message.From.Id, "ClanID");
                    Clans.GetAllInfo(Convert.ToInt32(ClanID), out string ReturnClanOwner, out string ReturnClanName, out string ReturnClanTag, out string ReturnClanLevel, out string ReturnClanPlayersCount, out string ReturnClanMaxPlayersCount, out string ReturnActive);
                    await Bot.Api.SendTextMessageAsync(message.From.Id, "<b>👥 [" + ReturnClanTag + "] " + ReturnClanName + "</b> [⭐️ " + ReturnClanLevel + "]\n👨‍👦 Члены клана: " + ReturnClanPlayersCount + @"\" + ReturnClanMaxPlayersCount, replyMarkup: keyboard, parseMode: ParseMode.Html);
                }
                else await Bot.Api.SendTextMessageAsync(message.From.Id, "Вы не состоите в клане или ваш клан не активен.\nХотите создать собственный клан? Используйте /clan_create", replyMarkup: keyboard, parseMode: ParseMode.Html);
            }

            else if (message.Text == "/clan_create")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🚻 Члены клана"),
                            new KeyboardButton("⚙️ Управление кланом"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚀 Ангар клана"),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);
                string PlayerStatus = Account.GetSingleData(Convert.ToInt32(message.From.Id), "Status"); // 4500 - Выбор имени клана, 4501 - выбор тэга клана, 0 - обычное состояние
                if (PlayerStatus == "0" || PlayerStatus == "4500" || PlayerStatus == "4501")
                {
                    if (Clans.IsExistsByClanOwner(message.From.Id) == false)
                    {
                        int ClanID = Clans.Create(message.From.Id);
                        if (ClanID != 0)
                        {
                            Account.InviteToClan(message.From.Id, ClanID, 9);
                            Account.SetSingleData(Convert.ToInt32(message.From.Id), "Status", "4500");
                            await Bot.Api.SendTextMessageAsync(message.From.Id, "<b>Создание клана:</b>\n<b>Стоимость:</b> 💳 " + ClanCreationPrice + "\n\n<b>Введите название вашего клана:</b>\n(не менее 4 и не более 25 букв)", replyMarkup: keyboard, parseMode: ParseMode.Html);
                        }
                        else
                        {
                            Message.Send(message.From.Id, "Ошибка создания клана!", 1);
                        }
                    }
                    else Message.Send(message.From.Id, "У вас уже есть клан.", 7);
                }
            }

            else if (message.Text == "/attack" || message.Text == "⚔️ Атаковать")
            {
                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                string UserAccountID = Account.GetAccountExists(message.From.Id);

                string X_Position = Ships.Ship.GetSingleData(message.From.Id, "X");
                string Y_Position = Ships.Ship.GetSingleData(message.From.Id, "Y");

                if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != "")) // Стандартные проверки
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        if (NPC.Enemy.IsExists(message.From.Id) == true)
                        {
                            string EnemyID = string.Empty;

                            NPC.Enemy.GetAllInfoByAccountID(message.From.Id, out string NPCID, out string EnemyName, out string EnemyLevel, out string EnemyHP, out string EnemyMaxHP, out string EnemyNpcType, out string EnemyShipType, out string EnemyMoney, out string EnemyCargoType, out string EnemyAgression, out string EnemyX, out string EnemyY, out string EnemyInAction, out string EnemyAttack, out string EnemyDef, out string EnemyDanger);
                            var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                            {
                                new KeyboardButton("🌐 Сканировать"),
                                new KeyboardButton(""),
                            },
                            new [] // last row
                            {
                                new KeyboardButton("📗 Игроки"),
                                new KeyboardButton(""),
                            },
                            new [] // last row
                            {
                                new KeyboardButton("🚪 Назад"),
                            }
                            }, true);
                            if (X_Position == EnemyX && Y_Position == EnemyY)
                            {
                                Program.DeleteBaseBattleData(message.From.Id); // Удаляем запись об участии в глобальной битве.
                                                                       // Продолжить
                                                                       // Расчитывать опыт от уровня угрозы!
                                string BattleLog = "";
                                string BattleWinner = "";

                                int PlayerShipHealth = Convert.ToInt32(Ships.Ship.GetSingleData(message.From.Id, "ShipHealth"));
                                int EnemyShipHealth = Convert.ToInt32(EnemyHP);
                                int EnemyShipAttack = Convert.ToInt32(EnemyAttack);
                                int EnemyShipDef = Convert.ToInt32(EnemyDef);

                                int CriticalDamagePercent = 5; // Базовый шанс нанесения критических повреждений игроку
                                int EvasionPercent = 5; // Базовый шанс уклонения от атаки игрока

                                int PlayerCriticalDamagePercent = 10;
                                int PlayerEvasionPercent = 10;

                                int PlayerShipAttack = Convert.ToInt32(Ships.Ship.GetSingleData(message.From.Id, "ShipAttack"));
                                int PlayerShipDef = Convert.ToInt32(Ships.Ship.GetSingleData(message.From.Id, "ShipDef"));
                                int WinExp = 20;

                                int CriticalChance = 0;
                                #region Расчет базовых характеристик противника в зависимости от опасности
                                // Расчет базовых характеристик противника в зависимости от опасности
                                if (EnemyDanger == "0")
                                {
                                    CriticalDamagePercent += 5;
                                    EvasionPercent += 5;
                                    WinExp = rndmzr.Next(40, 65);
                                }
                                else if (EnemyDanger == "1")
                                {
                                    CriticalDamagePercent += 10;
                                    EvasionPercent += 10;
                                    WinExp = rndmzr.Next(90, 140);
                                }
                                else if (EnemyDanger == "2")
                                {
                                    CriticalDamagePercent += 15;
                                    EvasionPercent += 15;
                                    WinExp = rndmzr.Next(140, 205);
                                }
                                #endregion
                                #region Расчет базовых характеристик противника в зависимости от агрессивности
                                // Расчет базовых характеристик противника в зависимости от агрессивности
                                if (EnemyAgression == "0")
                                {
                                    CriticalDamagePercent -= 10;
                                    EvasionPercent += 10;
                                }
                                else if (EnemyAgression == "1")
                                {
                                    CriticalDamagePercent += 10;
                                    EvasionPercent -= 10;
                                }
                                #endregion

                                #region Расчет базовых характеристик игрока
                                bool LuckCritCheck = Account.StatDiceCheck(message.From.Id, "Luck");
                                if (LuckCritCheck == true)
                                {
                                    PlayerCriticalDamagePercent += 10;
                                }
                                bool IntelligenceCritCheck = Account.StatDiceCheck(message.From.Id, "Intelligence");
                                if (IntelligenceCritCheck == true)
                                {
                                    PlayerCriticalDamagePercent += 10;
                                }
                                bool AgilityCritCheck = Account.StatDiceCheck(message.From.Id, "Agility");
                                if (AgilityCritCheck == true)
                                {
                                    PlayerCriticalDamagePercent += 10;
                                }
                                #endregion

                                Console.WriteLine("Start while do");
                                bool FinishBattle = false;
                                do
                                {
                                    // Атака игрока
                                    CriticalChance = rndmzr.Next(1, 101); // Крит или нет?
                                    if (Convert.ToInt32(Ships.Ship.GetSingleData(message.From.Id, "ShipEnergy")) > 1)
                                    {
                                        Ships.Ship.MinusData(message.From.Id, "ShipEnergy", "1");
                                        if (CriticalChance >= 1 && CriticalChance <= PlayerCriticalDamagePercent) // Крит
                                        {
                                            PlayerShipAttack = rndmzr.Next(PlayerShipAttack - Program.ProcentResultCount(PlayerShipAttack, 15), PlayerShipAttack + Program.ProcentResultCount(PlayerShipAttack, 15));
                                            PlayerShipAttack *= 2;
                                            BattleLog += "💥 Вы атаковали " + EnemyName + " на " + PlayerShipAttack + " ❤️\n";
                                            NPC.Enemy.MinusData(message.From.Id, "HP", PlayerShipAttack.ToString());
                                            EnemyShipHealth -= PlayerShipAttack;
                                        }
                                        else // Не крит
                                        {
                                            PlayerShipAttack = rndmzr.Next(PlayerShipAttack - Program.ProcentResultCount(PlayerShipAttack, 15), PlayerShipAttack + Program.ProcentResultCount(PlayerShipAttack, 15));
                                            BattleLog += "Вы атаковали " + EnemyName + " на " + PlayerShipAttack + " ❤️\n";
                                            NPC.Enemy.MinusData(message.From.Id, "HP", PlayerShipAttack.ToString());
                                            EnemyShipHealth -= PlayerShipAttack;
                                        }
                                    }
                                    else
                                    {
                                        BattleLog += "⚡️ Не хватает энергии для атаки!";
                                    }


                                    // Проверка не помер ли противник
                                    if (EnemyShipHealth < 1)
                                    {
                                        BattleWinner = "Player";
                                        string PlayerShipEnergy = Ships.Ship.GetSingleData(message.From.Id, "ShipEnergy");
                                        BattleLog += "\n🎉 Вы одержали победу! 🎉\n<b>Получено:</b>\n<b>Деньги:</b> 💳 " + EnemyMoney + "\n<b>Опыт:</b> ✨ 53\n" + "\nПрочность корабля: ❤️ " + PlayerShipHealth + "\nОсталось энергии: ⚡️ " + PlayerShipEnergy;

                                        FinishBattle = true;
                                        break;
                                    }

                                    // Атака NPC enemy
                                    CriticalChance = rndmzr.Next(1, 101);
                                    if (CriticalChance >= 1 && CriticalChance <= CriticalDamagePercent) // Крит
                                    {
                                        EnemyShipAttack = rndmzr.Next(EnemyShipAttack - Program.ProcentResultCount(EnemyShipAttack, 20), EnemyShipAttack + Program.ProcentResultCount(EnemyShipAttack, 10));
                                        EnemyShipAttack *= 2;
                                        BattleLog += "💥 " + EnemyName + " атаковал вас на " + EnemyShipAttack + " ❤️\n";
                                        Ships.Ship.DelHealth(message.From.Id, EnemyShipAttack);
                                        PlayerShipHealth -= EnemyShipAttack;
                                    }
                                    else // Не крит
                                    {
                                        EnemyShipAttack = rndmzr.Next(EnemyShipAttack - Program.ProcentResultCount(EnemyShipAttack, 25), EnemyShipAttack + Program.ProcentResultCount(EnemyShipAttack, 5));
                                        BattleLog += "" + EnemyName + " атаковал вас на " + EnemyShipAttack + " ❤️\n";
                                        Ships.Ship.DelHealth(message.From.Id, EnemyShipAttack);
                                        PlayerShipHealth -= EnemyShipAttack;
                                    }


                                    if (PlayerShipHealth < 1)
                                    {
                                        BattleWinner = "Enemy";
                                        string PlayerShipEnergy = Ships.Ship.GetSingleData(message.From.Id, "ShipEnergy");
                                        BattleLog += "\n☠️ <b>Вы проиграли!</b> ☠️\n" + "\nПрочность корабля: ❤️ " + "0" + "\nОсталось энергии: ⚡️ " + PlayerShipEnergy;

                                        FinishBattle = true;
                                        break;
                                    }

                                }
                                while (FinishBattle == false);

                                Console.WriteLine("Finish while do");

                                // Расчет наград
                                if (FinishBattle == true && BattleWinner == "Player")
                                {
                                    Message.Send(message.From.Id, "🎖 <b>Результаты битвы:</b>\n" + BattleLog, 11);
                                    Account.AddData(message.From.Id, "Exp", WinExp.ToString());
                                    Account.AddData(message.From.Id, "Money", EnemyMoney);
                                    NPC.Enemy.Delete(message.From.Id);
                                }
                                else if (FinishBattle == true && BattleWinner == "Enemy")
                                {
                                    Message.Send(message.From.Id, "🎖 <b>Результаты битвы:</b>\n" + BattleLog, 1);
                                    NPC.Enemy.Delete(message.From.Id);
                                }
                            }
                            else Message.Send(message.From.Id, "Ошибка! Неправильный ID противника!", 1);
                        }
                        else Message.Send(message.From.Id, "Нажмите 🌐 Сканировать для поиска кораблей!", 10);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                }
                else
                {
                    Message.Send(message.From.Id, "Вы выполняете другое действие.", 1);
                }
            }

            else if (message.Text.StartsWith("/char_info"))
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🚻 Текущий список"),
                            new KeyboardButton("🔄 Обновить"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        }
                        }, true);
                string[] cmds = message.Text.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                string NPCID = string.Empty;
                try
                {
                    NPCID = cmds[2];
                }
                catch
                {
                    Console.WriteLine("NPCID = cmds[2] error");
                }
                Console.WriteLine("DEBUG1: NPCID = " + NPCID);
                Regex REG = new Regex("[0-9]+");
                // ищем совпадения в исходной строке
                MatchCollection mc = REG.Matches(NPCID);
                StringBuilder sb = new StringBuilder();
                // создаем строку результата
                foreach (Match matc in mc)
                    sb.Append(matc.Value);
                // а это твой результат - только цифры
                NPCID = sb.ToString();
                char[] charsToTrim = { '*', ' ', '\'', '№', '@', '!', '#', '$', '%', '^', '&', '(', ')', '"', ';', ':', '?', '/' }; // Обрезаем символы
                NPCID = NPCID.Replace('(', '@');
                NPCID = NPCID.Replace(')', '@');
                NPCID = NPCID.Trim(charsToTrim);

                if (NPCID != "" && NPCID != " " && NPC.IsExists(message.From.Id, Convert.ToInt32(NPCID)) == true)
                {
                    NPC.GetAllInfoByNPCID(Convert.ToInt32(NPCID), out string ReturnFirstName, out string ReturnLastName, out string ReturnUpgradeFocus1, out string ReturnUpgradeFocus2, out string ReturnStrength, out string ReturnEndurance, out string ReturnCharisma, out string ReturnIntelligence, out string ReturnAgility, out string ReturnLuck, out string ReturnExp, out string ReturnLevel,
                    out string ReturnApperception, out string ReturnCandor, out string ReturnVivacity,
                    out string ReturnCoordination, out string ReturnMeekness, out string ReturnHumility,
                    out string ReturnCruelty, out string ReturnSelfpreservation, out string ReturnPatience,
                    out string ReturnDecisiveness, out string ReturnImagination, out string ReturnCuriosity,
                    out string ReturnAggression, out string ReturnLoyalty, out string ReturnEmpathy,
                    out string ReturnTenacity, out string ReturnCourage, out string ReturnSensuality,
                    out string ReturnCharm, out string ReturnHumor, out string ReturnProfession);

                    string UpgradeFocusString = "⚠️ Нет данных ⚠️"; // Предрасположенности
                    string UpgradeFocusTemp1, UpgradeFocusTemp2 = string.Empty;
                    string QualityRating = "⚠️ Нет данных ⚠️";
                    string CharExpString = "⚠️ Нет данных ⚠️"; // Опыт
                    string CharAttributesString = "⚠️ Нет данных ⚠️"; // Спецатрибуты

                    if (Ships.Ship.isHasModuleByItemID(message.From.Id, 44) == true || Ships.Ship.isHasModuleByItemID(message.From.Id, 45) == true || Ships.Ship.isHasModuleByItemID(message.From.Id, 46) == true) // Проверка становленного информационного модуля уровня 1
                    {
                        CharExpString = ReturnExp; // Опыт
                    }

                    if (Ships.Ship.isHasModuleByItemID(message.From.Id, 45) == true || Ships.Ship.isHasModuleByItemID(message.From.Id, 46) == true) // Проверка становленного информационного модуля уровня 2
                    {
                        switch (ReturnUpgradeFocus1)
                        {
                            case "1":
                                UpgradeFocusTemp1 = "Сила";
                                break;
                            case "2":
                                UpgradeFocusTemp1 = "Выносливость";
                                break;
                            case "3":
                                UpgradeFocusTemp1 = "Харизма";
                                break;
                            case "4":
                                UpgradeFocusTemp1 = "Интеллект";
                                break;
                            case "5":
                                UpgradeFocusTemp1 = "Ловкость";
                                break;
                            case "6":
                                UpgradeFocusTemp1 = "Удача";
                                break;
                            default:
                                UpgradeFocusTemp1 = "????";
                                break;
                        }
                        switch (ReturnUpgradeFocus2)
                        {
                            case "1":
                                UpgradeFocusTemp2 = "Сила";
                                break;
                            case "2":
                                UpgradeFocusTemp2 = "Выносливость";
                                break;
                            case "3":
                                UpgradeFocusTemp2 = "Харизма";
                                break;
                            case "4":
                                UpgradeFocusTemp2 = "Интеллект";
                                break;
                            case "5":
                                UpgradeFocusTemp2 = "Ловкость";
                                break;
                            case "6":
                                UpgradeFocusTemp2 = "Удача";
                                break;
                            default:
                                UpgradeFocusTemp1 = "????";
                                break;
                        }
                        UpgradeFocusString = "- " + UpgradeFocusTemp1 + "\n" + "- " + UpgradeFocusTemp2; // Предрасположенности
                    }

                    if (Ships.Ship.isHasModuleByItemID(message.From.Id, 46) == true) // Проверка установленного информационного модуля уровня 3
                    {
                        CharAttributesString = "<b>Анализ поведения:</b>\n"
                        + "Самоосознание: " + ReturnApperception + "\n"
                        + "Искренность: " + ReturnCandor + "\n"
                        + "Живость: " + ReturnVivacity + "\n"
                        + "Координация: " + ReturnCoordination + "\n"
                        + "Мягкосердечность: " + ReturnMeekness + "\n"
                        + "Покорность: " + ReturnHumility + "\n"
                        + "Жестокость: " + ReturnCruelty + "\n"
                        + "Самосохранение: " + ReturnSelfpreservation + "\n"
                        + "Терпение: " + ReturnPatience + "\n"
                        + "Решительность: " + ReturnDecisiveness + "\n"
                        + "Воображение: " + ReturnImagination + "\n"
                        + "Любопытство: " + ReturnCuriosity + "\n"
                        + "Агрессия: " + ReturnAggression + "\n"
                        + "Преданность: " + ReturnLoyalty + "\n"
                        + "Сопереживание: " + ReturnEmpathy + "\n"
                        + "Упорство: " + ReturnTenacity + "\n"
                        + "Храбрость: " + ReturnCourage + "\n"
                        + "Чувственность: " + ReturnSensuality + "\n"
                        + "Очарование: " + ReturnCharm + "\n"
                        + "Чувство юмора: " + ReturnHumor;

                        int NPC_QualityPoints_Main = NPC.GetQualityPointsCountMain(Convert.ToInt32(NPCID));
                        if (NPC_QualityPoints_Main > 0 && NPC_QualityPoints_Main <= 10)
                        {
                            QualityRating = "💟 Оценка: 💛";
                        }
                        if (NPC_QualityPoints_Main > 10 && NPC_QualityPoints_Main <= 20)
                        {
                            QualityRating = "💟 Оценка: 💙";
                        }
                        else if (NPC_QualityPoints_Main > 20 && NPC_QualityPoints_Main <= 40)
                        {
                            QualityRating = "💟 Оценка: 💜";
                        }
                        else if (NPC_QualityPoints_Main > 40 && NPC_QualityPoints_Main <= 60)
                        {
                            QualityRating = "💟 Оценка: 🖤";
                        }
                    }

                    string AvailableCommands = string.Empty;
                    string CommandHire = "Нанять: /hire_char_" + NPCID;
                    string CommandDismiss = "Уволить: /dismiss_char_" + NPCID;

                    if (NPC.isHired(message.From.Id, Convert.ToInt32(NPCID)) == true)
                    {
                        AvailableCommands = AvailableCommands + CommandDismiss;
                    }
                    else
                    {
                        int HirePrice = NPC.GetHirePrice(Convert.ToInt32(NPCID));
                        AvailableCommands = AvailableCommands + "Цена контракта: 💳 " + HirePrice + "\n" + CommandHire;
                    }

                    string CharProfession = "⚒ " + Program.ProfessionsList[Convert.ToInt32(ReturnProfession)].Profession.ToString();

                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "<b>Информация о персонаже:</b> \n" + ReturnFirstName + " " + ReturnLastName + "\n"
                    + CharProfession + "\n"
                    + "⭐️ Уровень: " + ReturnLevel + "\n" + "✨ Опыт: " + CharExpString + "\n" + QualityRating + "\n\n"
                    + "🈁 <b>Характеристики:</b> " + "\n"
                    + "Сила: " + ReturnStrength + "\n"
                    + "Выносливость: " + ReturnEndurance + "\n"
                    + "Харизма: " + ReturnCharisma + "\n"
                    + "Интеллект: " + ReturnIntelligence + "\n"
                    + "Ловкость: " + ReturnAgility + "\n"
                    + "Удача: " + ReturnLuck + "\n\n"
                    + "↗️ <b>Предрасположенности: </b>" + "\n" + UpgradeFocusString + "\n\n"
                    + CharAttributesString + "\n\n" + AvailableCommands,
                   replyMarkup: keyboard, parseMode: ParseMode.Html);
                }
                else
                {
                    Console.WriteLine("DEBUG: NPCID = " + NPCID);
                    Message.Send(message.From.Id, "Ошибка #330", 1);
                }
            }

            else if (message.Text.StartsWith("/hire_char"))
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🚻 Текущий список"),
                            new KeyboardButton("🔄 Обновить"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        }
                        }, true);
                string[] cmds = message.Text.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                string NPCID = string.Empty;
                try
                {
                    NPCID = cmds[2];
                }
                catch
                {
                    Console.WriteLine("NPCID = cmds[2] error");
                }
                Regex REG = new Regex("[0-9]+");
                // ищем совпадения в исходной строке
                MatchCollection mc = REG.Matches(NPCID);
                StringBuilder sb = new StringBuilder();
                // создаем строку результата
                foreach (Match matc in mc)
                    sb.Append(matc.Value);
                // а это твой результат - только цифры
                NPCID = sb.ToString();
                char[] charsToTrim = { '*', ' ', '\'', '№', '@', '!', '#', '$', '%', '^', '&', '(', ')', '"', ';', ':', '?', '/' }; // Обрезаем символы
                NPCID = NPCID.Replace('(', '@');
                NPCID = NPCID.Replace(')', '@');
                NPCID = NPCID.Trim(charsToTrim);
                if (NPCID != "" && NPCID != " " && NPC.IsExists(message.From.Id, Convert.ToInt32(NPCID)))
                {
                    string BotsMax = Ships.Ship.GetSingleData(message.From.Id, "ShipBotsSlots");
                    int BotsCurrent = Ships.Ship.GetNPCCount(message.From.Id);
                    if (BotsCurrent < Convert.ToInt32(BotsMax))
                    {
                        string NoHired = NPC.GetDataByClanID(Convert.ToInt32(NPCID), "Hired");
                        string FounderID = NPC.GetDataByClanID(Convert.ToInt32(NPCID), "FounderID");
                        if (NoHired == "0" && FounderID == message.From.Id.ToString())
                        {
                            // Расчет стоимости найма NPC

                            int HirePrice = NPC.GetHirePrice(Convert.ToInt32(NPCID));
                            string PlayerMoney = Account.GetSingleData(message.From.Id, "Money");
                            if (HirePrice <= Convert.ToInt32(PlayerMoney))
                            {
                                bool NPCHiredFinish = NPC.SetDataForPlayerHired(message.From.Id, Convert.ToInt32(NPCID), "Hired", "1", "0");
                                Account.TakeGoldForced(message.From.Id, HirePrice);
                                if (NPCHiredFinish == true)
                                {
                                    Program.NPCStatsCalculation(message.From.Id, Convert.ToInt32(NPCID));
                                    Message.Send(message.From.Id, "Новый член команды прибыл на борт корабля!\nВы заплатили: 💳 " + HirePrice, 1);
                                }
                                else
                                {
                                    Message.Send(message.From.Id, "Ошибка №335|" + HirePrice + "д", 1);
                                    Account.SetGold(message.From.Id, HirePrice, false);
                                }
                            }
                            else Message.Send(message.From.Id, "У вас недостаточно денег для найма сотрудника!", 1);
                        }
                        else Message.Send(message.From.Id, "Невозможно нанять данного члена экипажа!", 1);
                    }
                    else Message.Send(message.From.Id, "На вашем корабле нет места для новых членов экипажа!", 1);
                }
                else Message.Send(message.From.Id, "Ошибка #331", 1);
            }

            else if (message.Text.StartsWith("/dismiss_char"))
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🚻 Текущий список"),
                            new KeyboardButton("🔄 Обновить"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        }
                        }, true);
                string[] cmds = message.Text.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                string NPCID = string.Empty;
                try
                {
                    NPCID = cmds[2];
                }
                catch
                {
                    Console.WriteLine("NPCID = cmds[2] error");
                }
                Regex REG = new Regex("[0-9]+");
                // ищем совпадения в исходной строке
                MatchCollection mc = REG.Matches(NPCID);
                StringBuilder sb = new StringBuilder();
                // создаем строку результата
                foreach (Match matc in mc)
                    sb.Append(matc.Value);
                // а это твой результат - только цифры
                NPCID = sb.ToString();
                char[] charsToTrim = { '*', ' ', '\'', '№', '@', '!', '#', '$', '%', '^', '&', '(', ')', '"', ';', ':', '?', '/' }; // Обрезаем символы
                NPCID = NPCID.Replace('(', '@');
                NPCID = NPCID.Replace(')', '@');
                NPCID = NPCID.Trim(charsToTrim);
                if (NPCID != "" && NPCID != " ")
                {
                    string Hired = NPC.GetDataByClanID(Convert.ToInt32(NPCID), "Hired");
                    string FounderID = NPC.GetDataByClanID(Convert.ToInt32(NPCID), "FounderID");
                    if (Hired == "1" && FounderID == message.From.Id.ToString())
                    {
                        // Расчет стоимости найма NPC
                        bool NPCDismissFinish = NPC.SetDataForPlayerHired(message.From.Id, Convert.ToInt32(NPCID), "Hired", "0", "1");
                        if (NPCDismissFinish == true)
                        {
                            Program.NPCStatsCalculation(message.From.Id, Convert.ToInt32(NPCID), 1);
                            Message.Send(message.From.Id, "Член команды уволен.", 1);
                        }
                        else Message.Send(message.From.Id, "Ошибка №335.1", 1);
                    }
                    else Message.Send(message.From.Id, "Невозможно уволить данного члена экипажа!", 1);
                }
                else Message.Send(message.From.Id, "Ошибка #331.1", 1);
            }

            else if (message.Text == "🚻 Текущий список")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🚻 Текущий список"),
                            new KeyboardButton("🔄 Обновить"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        }
                        }, true);
                //NPC.Create(message.From.Id, "0");
                //NPC.Create(message.From.Id, "0");
                //NPC.Create(message.From.Id, "0");
                string HireList = Ships.Ship.GetNPCHireList(message.From.Id);
                await Bot.Api.SendTextMessageAsync(message.Chat.Id, "<b>Доступные персонажи:</b>\n" + HireList,
                   replyMarkup: keyboard, parseMode: ParseMode.Html);
            }

            else if (message.Text == "🛏 Найм команды")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🚻 Текущий список"),
                            new KeyboardButton("🔄 Обновить"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        }
                        }, true);
                await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Вы воспользовались Межгалактическим Терминалом Поиска Сотрудников. На экране терминала появляется текст:\n\n 🚻 Текущий список - выводит список членов экипажа доступных для найма. Автоматическое обновление каждые 24 часа.\n\n🔄 Обновить - обновляет текущий список персонажей для найма.\nСтоимость обновления: 💳 1000.\n\n<b>Не видите полной информации о персонаже?</b>\nПолучите доступ ко всей информации установив один из Информационных модулей!",
                   replyMarkup: keyboard, parseMode: ParseMode.Html);
            }

            else if (message.Text == "🔄 Обновить")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🚻 Текущий список"),
                            new KeyboardButton("🔄 Обновить"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        }
                        }, true);
                string PlayerMoney = Account.GetSingleData(message.From.Id, "Money");
                if (Convert.ToInt32(PlayerMoney) >= 1000)
                {
                    bool GoldTaken = Account.TakeGold(message.From.Id, 1000);
                    if (GoldTaken == true)
                    {
                        bool RefreshFinished = NPC.DeleteAllUnemployed(message.From.Id);
                        if (RefreshFinished == true)
                        {
                            NPC.Create(message.From.Id, "0");
                            NPC.Create(message.From.Id, "0");
                            NPC.Create(message.From.Id, "0");
                            NPC.Create(message.From.Id, "0");
                            NPC.Create(message.From.Id, "0");
                            Message.Send(message.From.Id, "Оплата принята. Список успешно обновлен.", 9);
                        }
                        else
                        {
                            Message.Send(message.From.Id, "Не могу обновить список. Возврат денежных средств...", 9);
                            Account.SetGold(message.From.Id, 1000, false);
                        }
                    }
                    else Message.Send(message.From.Id, "У вас не хватает денег для обновления списка.\nСтоимость: 💳 1000", 9);
                }
                else
                {
                    Message.Send(message.From.Id, "У вас не хватает денег для обновления списка.\nСтоимость: 💳 1000", 9);
                }
            }

            else if (message.Text == "⚒ Рекрафтер")
            {
                string Module2 = string.Empty;
                if (Ships.Ship.isHasModuleByItemID(message.From.Id, 41) == true)
                {
                    Module2 = "📚 Библиотека";
                }
                var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                            new [] // first row
                        {
                            new KeyboardButton("🔳 Доступные чертежи"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                            new [] // last row
                        {
                            new KeyboardButton(Module2),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);

                await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Используйте ⚒ Рекрафтер, чтобы создавать предметы. Выберите направление:",
                replyMarkup: keyboard);
            }

            else if (message.Text == "📚 Библиотека")
            {
                if (Ships.Ship.isHasModuleByItemID(message.From.Id, 41) == true)
                {
                    var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                            new [] // first row
                        {
                            new KeyboardButton("🔳 Изучение чертежей"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                            new [] // last row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "<b>🔳 Изучение чертежей</b> - здесь вы можете научиться создавать новые предметы различного уровня.\n\n",
                replyMarkup: keyboard, parseMode: ParseMode.Html);
                }
                else Message.Send(message.From.Id, "На вашем корабле не установлен указанный модуль.", 1);
            }

            else if (message.Text == "🔳 Изучение чертежей")
            {
                if (Ships.Ship.isHasModuleByItemID(message.From.Id, 41) == true)
                {
                    var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                            new [] // first row
                        {
                            new KeyboardButton("🔳 Изучение чертежей"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                            new [] // last row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);
                    string UnlearnedRecipes = Recipes.GetUnlearnedListForPlayer(message.From.Id, 1);
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "📚 <b>Библиотека</b>\n\n" + UnlearnedRecipes,
                replyMarkup: keyboard, parseMode: ParseMode.Html);
                }
                else Message.Send(message.From.Id, "На вашем корабле не установлен указанный модуль.", 1);
            }

            else if (message.Text == "🔳 Доступные чертежи")
            {
                string Module2 = string.Empty;
                if (Ships.Ship.isHasModuleByItemID(message.From.Id, 41) == true)
                {
                    Module2 = "📚 Библиотека";
                }
                var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                            new [] // first row
                        {
                            new KeyboardButton("🔳 Доступные чертежи"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                            new [] // last row
                        {
                            new KeyboardButton(Module2),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton(""),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);
                string RecipesList = Recipes.GetListForPlayer(message.From.Id, 1);
                await Bot.Api.SendTextMessageAsync(message.Chat.Id, "<b>Доступные чертежи:</b>\n" + RecipesList,
                replyMarkup: keyboard, parseMode: ParseMode.Html);
            }

            else if (message.Text == "🚀 Кап. мостик")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🛌 Команда"),
                            new KeyboardButton("🛏 Найм команды"),
                            new KeyboardButton(""),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🖲 Статус корабля"),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);

                await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Выберите направление:",
                replyMarkup: keyboard);
            }

            else if (message.Text == "/bag" || message.Text == "📦 Инвентарь")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("🔪 Оружие"),
                            new KeyboardButton("👕 Одежда"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🍞 Еда, вода"),
                            new KeyboardButton("💎 Модули"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("💊 Медикаменты"),
                            new KeyboardButton("🔩 Амуниция"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🀄️ Разное"),
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                Account.ShowInventory(message.From.Id);
            }

            else if (message.Text == "🔪 Оружие")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("🔪 Оружие"),
                            new KeyboardButton("👕 Одежда"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🍞 Еда, вода"),
                            new KeyboardButton("💎 Модули"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("💊 Медикаменты"),
                            new KeyboardButton("🔩 Амуниция"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🀄️ Разное"),
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                Console.WriteLine("---------------------------");
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ItemID, ItemCount FROM inventory WHERE AccountID = @AccountID AND ItemType = 1", myConnection);
                sql_commmand.Parameters.AddWithValue("@AccountID", message.From.Id);
                Console.WriteLine("Запрос инвентаря от ID: " + message.From.Id + " запрос - " + sql_commmand.CommandText);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();

                string InventoryString = string.Empty;
                string InventoryTempString = string.Empty;
                string UsableString = string.Empty;
                string WearableString = string.Empty;
                string PriceString = string.Empty;

                while (thisReader.Read())
                {
                    string AttackDefString = string.Empty;
                    string ItemID = string.Empty;
                    string ItemType = string.Empty;
                    string ItemCount = string.Empty;
                    string ItemName = string.Empty;
                    string ItemUsable = string.Empty;
                    string ItemWearable = string.Empty;
                    string ItemWearSlot = string.Empty;
                    string ItemPrice = string.Empty;
                    string ItemPower = string.Empty;
                    UsableString = string.Empty;
                    WearableString = string.Empty;
                    PriceString = string.Empty;
                    ItemID += thisReader["ItemID"];
                    ItemCount += thisReader["ItemCount"];

                    Inventory.Items.Item.GetAllInfo(Convert.ToInt32(ItemID), out string rItemName, out string rItemUsable, out string rItemWearable, out string rItemWearSlot, out string rItemPrice, out string rItemType, out string rItemStrength, out string rItemCharisma, out string rItemIntelligence, out string rItemAgility, out string rItemLuck, out string rItemAttack, out string rItemDef, out string rItemPower, out string rItemText);
                    AttackDefString = "⚔️" + rItemAttack + " 🛡" + rItemDef;

                    Inventory.Items.Item.GetInfo(Convert.ToInt32(ItemID), out ItemName, out ItemUsable, out ItemWearable, out ItemWearSlot, out ItemPrice, out ItemType, out ItemPower);
                    if (ItemUsable == "1")
                    {
                        UsableString = " /use_" + ItemID;
                    }
                    if (ItemWearable == "1")
                    {
                        UsableString = " /wear_" + ItemID;
                    }
                    //+ (Convert.ToInt32(ItemCount) * Convert.ToInt32(ItemPrice))
                    InventoryTempString = AttackDefString + " | " + ItemName + " (" + ItemCount + " шт.) " + UsableString + WearableString;
                    InventoryString += "\n" + InventoryTempString;
                }
                thisReader.Close();

                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "🔪 Оружие:\n" + InventoryString,
                    replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

                Console.WriteLine("---------------------------");
                myConnection.Close();
                
            }

            else if (message.Text == "👕 Одежда")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("🔪 Оружие"),
                            new KeyboardButton("👕 Одежда"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🍞 Еда, вода"),
                            new KeyboardButton("💎 Модули"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("💊 Медикаменты"),
                            new KeyboardButton("🔩 Амуниция"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🀄️ Разное"),
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                Console.WriteLine("---------------------------");
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ItemID, ItemCount FROM inventory WHERE AccountID = @AccountID AND ItemType = 2", myConnection);
                sql_commmand.Parameters.AddWithValue("@AccountID", message.From.Id);
                Console.WriteLine("Запрос инвентаря от ID: " + message.From.Id + " запрос - " + sql_commmand.CommandText);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();

                string InventoryString = string.Empty;
                string InventoryTempString = string.Empty;
                string UsableString = string.Empty;
                string WearableString = string.Empty;
                string PriceString = string.Empty;

                while (thisReader.Read())
                {
                    string AttackDefString = string.Empty;
                    string ItemID = string.Empty;
                    string ItemType = string.Empty;
                    string ItemCount = string.Empty;
                    string ItemName = string.Empty;
                    string ItemUsable = string.Empty;
                    string ItemWearable = string.Empty;
                    string ItemWearSlot = string.Empty;
                    string ItemPrice = string.Empty;
                    string ItemPower = string.Empty;
                    UsableString = string.Empty;
                    WearableString = string.Empty;
                    PriceString = string.Empty;
                    ItemID += thisReader["ItemID"];
                    ItemCount += thisReader["ItemCount"];

                    Inventory.Items.Item.GetAllInfo(Convert.ToInt32(ItemID), out string rItemName, out string rItemUsable, out string rItemWearable, out string rItemWearSlot, out string rItemPrice, out string rItemType, out string rItemStrength, out string rItemCharisma, out string rItemIntelligence, out string rItemAgility, out string rItemLuck, out string rItemAttack, out string rItemDef, out string rItemPower, out string rItemText);
                    AttackDefString = "⚔️" + rItemAttack + " 🛡" + rItemDef;

                    Inventory.Items.Item.GetInfo(Convert.ToInt32(ItemID), out ItemName, out ItemUsable, out ItemWearable, out ItemWearSlot, out ItemPrice, out ItemType, out ItemPower);
                    if (ItemUsable == "1")
                    {
                        UsableString = " /use_" + ItemID;
                    }
                    if (ItemWearable == "1")
                    {
                        UsableString = " /wear_" + ItemID;
                    }
                    //+ (Convert.ToInt32(ItemCount) * Convert.ToInt32(ItemPrice))
                    InventoryTempString = AttackDefString + " | " + ItemName + " (" + ItemCount + " шт.) " + UsableString + WearableString;
                    InventoryString += "\n" + InventoryTempString;
                }
                thisReader.Close();

                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "👕 Одежда:\n" + InventoryString,
                    replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

                Console.WriteLine("---------------------------");
                myConnection.Close();
                
            }

            else if (message.Text == "🍞 Еда, вода")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("🔪 Оружие"),
                            new KeyboardButton("👕 Одежда"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🍞 Еда, вода"),
                            new KeyboardButton("💎 Модули"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("💊 Медикаменты"),
                            new KeyboardButton("🔩 Амуниция"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🀄️ Разное"),
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                Console.WriteLine("---------------------------");
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ItemID, ItemCount FROM inventory WHERE AccountID = @AccountID AND ItemType = 4", myConnection);
                sql_commmand.Parameters.AddWithValue("@AccountID", message.From.Id);
                Console.WriteLine("Запрос инвентаря от ID: " + message.From.Id + " запрос - " + sql_commmand.CommandText);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();

                string InventoryString = string.Empty;
                string InventoryTempString = string.Empty;
                string UsableString = string.Empty;
                string WearableString = string.Empty;
                string PriceString = string.Empty;

                while (thisReader.Read())
                {
                    string ItemID = string.Empty;
                    string ItemType = string.Empty;
                    string ItemCount = string.Empty;
                    string ItemName = string.Empty;
                    string ItemUsable = string.Empty;
                    string ItemWearable = string.Empty;
                    string ItemWearSlot = string.Empty;
                    string ItemPrice = string.Empty;
                    string ItemPower = string.Empty;
                    UsableString = string.Empty;
                    WearableString = string.Empty;
                    PriceString = string.Empty;
                    ItemID += thisReader["ItemID"];
                    ItemCount += thisReader["ItemCount"];

                    Inventory.Items.Item.GetInfo(Convert.ToInt32(ItemID), out ItemName, out ItemUsable, out ItemWearable, out ItemWearSlot, out ItemPrice, out ItemType, out ItemPower);
                    if (ItemUsable == "1")
                    {
                        UsableString = " /use_" + ItemID;
                    }
                    if (ItemWearable == "1")
                    {
                        UsableString = " /wear_" + ItemID;
                    }
                    //+ (Convert.ToInt32(ItemCount) * Convert.ToInt32(ItemPrice))
                    InventoryTempString = ItemName + " (" + ItemCount + " шт.) " + UsableString + WearableString;
                    InventoryString += "\n" + InventoryTempString;
                }
                thisReader.Close();

                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "🍞 Еда, вода:\n" + InventoryString,
                    replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

                Console.WriteLine("---------------------------");
                myConnection.Close();
                
            }

            else if (message.Text == "💎 Модули")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("🔪 Оружие"),
                            new KeyboardButton("👕 Одежда"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🍞 Еда, вода"),
                            new KeyboardButton("💎 Модули"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("💊 Медикаменты"),
                            new KeyboardButton("🔩 Амуниция"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🀄️ Разное"),
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                Console.WriteLine("---------------------------");
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ItemID, ItemCount FROM inventory WHERE AccountID = @AccountID AND ItemType = 3", myConnection);
                sql_commmand.Parameters.AddWithValue("@AccountID", message.From.Id);
                Console.WriteLine("Запрос инвентаря от ID: " + message.From.Id + " запрос - " + sql_commmand.CommandText);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();

                string InventoryString = string.Empty;
                string InventoryTempString = string.Empty;
                string UsableString = string.Empty;
                string WearableString = string.Empty;
                string PriceString = string.Empty;

                while (thisReader.Read())
                {
                    string ItemID = string.Empty;
                    string ItemType = string.Empty;
                    string ItemCount = string.Empty;
                    string ItemName = string.Empty;
                    string ItemUsable = string.Empty;
                    string ItemWearable = string.Empty;
                    string ItemWearSlot = string.Empty;
                    string ItemPrice = string.Empty;
                    string ItemPower = string.Empty;
                    UsableString = string.Empty;
                    WearableString = string.Empty;
                    PriceString = string.Empty;
                    ItemID += thisReader["ItemID"];
                    ItemCount += thisReader["ItemCount"];

                    Inventory.Items.Item.GetInfo(Convert.ToInt32(ItemID), out ItemName, out ItemUsable, out ItemWearable, out ItemWearSlot, out ItemPrice, out ItemType, out ItemPower);
                    if (ItemUsable == "1")
                    {
                        UsableString = " /use_" + ItemID;
                    }
                    if (ItemWearable == "1")
                    {
                        UsableString = " /wear_" + ItemID;
                    }
                    //+ (Convert.ToInt32(ItemCount) * Convert.ToInt32(ItemPrice))
                    InventoryTempString = ItemName + " (" + ItemCount + " шт.) " + UsableString + WearableString;
                    InventoryString += "\n" + InventoryTempString;
                }
                thisReader.Close();

                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "💎 Модули:\n" + InventoryString,
                    replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

                Console.WriteLine("---------------------------");
                myConnection.Close();
                
            }

            else if (message.Text == "💊 Медикаменты")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("🔪 Оружие"),
                            new KeyboardButton("👕 Одежда"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🍞 Еда, вода"),
                            new KeyboardButton("💎 Модули"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("💊 Медикаменты"),
                            new KeyboardButton("🔩 Амуниция"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🀄️ Разное"),
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                Console.WriteLine("---------------------------");
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ItemID, ItemCount FROM inventory WHERE AccountID = @AccountID AND ItemType = 5", myConnection);
                sql_commmand.Parameters.AddWithValue("@AccountID", message.From.Id);
                Console.WriteLine("Запрос инвентаря от ID: " + message.From.Id + " запрос - " + sql_commmand.CommandText);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();

                string InventoryString = string.Empty;
                string InventoryTempString = string.Empty;
                string UsableString = string.Empty;
                string WearableString = string.Empty;
                string PriceString = string.Empty;

                while (thisReader.Read())
                {
                    string ItemID = string.Empty;
                    string ItemType = string.Empty;
                    string ItemCount = string.Empty;
                    string ItemName = string.Empty;
                    string ItemUsable = string.Empty;
                    string ItemWearable = string.Empty;
                    string ItemWearSlot = string.Empty;
                    string ItemPrice = string.Empty;
                    string ItemPower = string.Empty;
                    UsableString = string.Empty;
                    WearableString = string.Empty;
                    PriceString = string.Empty;
                    ItemID += thisReader["ItemID"];
                    ItemCount += thisReader["ItemCount"];

                    Inventory.Items.Item.GetInfo(Convert.ToInt32(ItemID), out ItemName, out ItemUsable, out ItemWearable, out ItemWearSlot, out ItemPrice, out ItemType, out ItemPower);
                    if (ItemUsable == "1")
                    {
                        UsableString = " /use_" + ItemID;
                    }
                    if (ItemWearable == "1")
                    {
                        UsableString = " /wear_" + ItemID;
                    }
                    //+ (Convert.ToInt32(ItemCount) * Convert.ToInt32(ItemPrice))
                    InventoryTempString = ItemName + " (" + ItemCount + " шт.) " + UsableString + WearableString;
                    InventoryString += "\n" + InventoryTempString;
                }
                thisReader.Close();

                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "💊 Медикаменты:\n" + InventoryString,
                    replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

                Console.WriteLine("---------------------------");
                myConnection.Close();
                
            }

            else if (message.Text == "🔩 Амуниция")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("🔪 Оружие"),
                            new KeyboardButton("👕 Одежда"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🍞 Еда, вода"),
                            new KeyboardButton("💎 Модули"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("💊 Медикаменты"),
                            new KeyboardButton("🔩 Амуниция"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🀄️ Разное"),
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                Console.WriteLine("---------------------------");
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ItemID, ItemCount FROM inventory WHERE AccountID = @AccountID AND ItemType = 7", myConnection);
                sql_commmand.Parameters.AddWithValue("@AccountID", message.From.Id);
                Console.WriteLine("Запрос инвентаря от ID: " + message.From.Id + " запрос - " + sql_commmand.CommandText);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();

                string InventoryString = string.Empty;
                string InventoryTempString = string.Empty;
                string UsableString = string.Empty;
                string WearableString = string.Empty;
                string PriceString = string.Empty;

                while (thisReader.Read())
                {
                    string ItemID = string.Empty;
                    string ItemType = string.Empty;
                    string ItemCount = string.Empty;
                    string ItemName = string.Empty;
                    string ItemUsable = string.Empty;
                    string ItemWearable = string.Empty;
                    string ItemWearSlot = string.Empty;
                    string ItemPrice = string.Empty;
                    string ItemPower = string.Empty;
                    UsableString = string.Empty;
                    WearableString = string.Empty;
                    PriceString = string.Empty;
                    ItemID += thisReader["ItemID"];
                    ItemCount += thisReader["ItemCount"];

                    Inventory.Items.Item.GetInfo(Convert.ToInt32(ItemID), out ItemName, out ItemUsable, out ItemWearable, out ItemWearSlot, out ItemPrice, out ItemType, out ItemPower);
                    if (ItemUsable == "1")
                    {
                        UsableString = " /use_" + ItemID;
                    }
                    if (ItemWearable == "1")
                    {
                        UsableString = " /wear_" + ItemID;
                    }
                    //+ (Convert.ToInt32(ItemCount) * Convert.ToInt32(ItemPrice))
                    InventoryTempString = ItemName + " (" + ItemCount + " шт.) " + UsableString + WearableString;
                    InventoryString += "\n" + InventoryTempString;
                }
                thisReader.Close();

                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "🔩 Амуниция:\n" + InventoryString,
                    replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

                Console.WriteLine("---------------------------");
                myConnection.Close();
                
            }

            else if (message.Text == "🀄️ Разное")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("🔪 Оружие"),
                            new KeyboardButton("👕 Одежда"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🍞 Еда, вода"),
                            new KeyboardButton("💎 Модули"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("💊 Медикаменты"),
                            new KeyboardButton("🔩 Амуниция"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🀄️ Разное"),
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                Console.WriteLine("---------------------------");
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ItemID, ItemCount FROM inventory WHERE AccountID = @AccountID AND ItemType = 6", myConnection);
                sql_commmand.Parameters.AddWithValue("@AccountID", message.From.Id);
                Console.WriteLine("Запрос инвентаря от ID: " + message.From.Id + " запрос - " + sql_commmand.CommandText);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();

                string InventoryString = string.Empty;
                string InventoryTempString = string.Empty;
                string UsableString = string.Empty;
                string WearableString = string.Empty;
                string PriceString = string.Empty;

                while (thisReader.Read())
                {
                    string ItemID = string.Empty;
                    string ItemType = string.Empty;
                    string ItemCount = string.Empty;
                    string ItemName = string.Empty;
                    string ItemUsable = string.Empty;
                    string ItemWearable = string.Empty;
                    string ItemWearSlot = string.Empty;
                    string ItemPrice = string.Empty;
                    string ItemPower = string.Empty;
                    UsableString = string.Empty;
                    WearableString = string.Empty;
                    PriceString = string.Empty;
                    ItemID += thisReader["ItemID"];
                    ItemCount += thisReader["ItemCount"];

                    Inventory.Items.Item.GetInfo(Convert.ToInt32(ItemID), out ItemName, out ItemUsable, out ItemWearable, out ItemWearSlot, out ItemPrice, out ItemType, out ItemPower);
                    if (ItemUsable == "1")
                    {
                        UsableString = " /use_" + ItemID;
                    }
                    if (ItemWearable == "1")
                    {
                        UsableString = " /wear_" + ItemID;
                    }
                    //+ (Convert.ToInt32(ItemCount) * Convert.ToInt32(ItemPrice))
                    InventoryTempString = ItemName + " (" + ItemCount + " шт.) " + UsableString + WearableString + " /drop_" + ItemID + "_1";
                    InventoryString += "\n" + InventoryTempString;
                }
                thisReader.Close();

                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "🀄️ Разное:\n" + InventoryString,
                    replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

                Console.WriteLine("---------------------------");
                myConnection.Close();
                
            }

            else if (message.Text == "/modules" || message.Text == "🔧 Модули")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🚀 Курс"),
                            new KeyboardButton("🕋 Отсеки"),
                            new KeyboardButton("🔧 Модули"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                            new KeyboardButton(""),
                        }
                        }, true);
                Console.WriteLine("---------------------------");
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ID, ModuleID FROM ships_modules WHERE ShipID = @ShipID", myConnection);
                sql_commmand.Parameters.AddWithValue("@ShipID", message.From.Id);
                Console.WriteLine("Запрос инвентаря от ID: " + message.From.Id + " запрос - " + sql_commmand.CommandText);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();

                string ItemName = string.Empty;
                string ModulesTempString = string.Empty;
                string ModulesString = string.Empty;

                while (thisReader.Read())
                {
                    string ID = string.Empty;
                    string ModuleID = string.Empty;
                    ID += thisReader["ID"];
                    ModuleID += thisReader["ModuleID"];
                    ItemName = Inventory.Items.Item.GetName(Convert.ToInt32(ModuleID));

                    ModulesTempString = ItemName + "";
                    ModulesString += "\n" + ModulesTempString + " (Снять: /uninstall_" + ID + " )";
                }
                thisReader.Close();

                string ShipModulesSlots = Ships.Ship.GetSingleData(message.From.Id, "ShipModulesSlots");
                string CurrentModulesCount = Ships.Ship.GetModulesCount(message.From.Id);
                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "<b>Установленные модули:</b> " + CurrentModulesCount + "/" + ShipModulesSlots + "\n" + ModulesString, replyMarkup: keyboard, parseMode: ParseMode.Html);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

                Console.WriteLine("---------------------------");
                myConnection.Close();
                
            }

            else if (message.Text.StartsWith("/uninstall_"))
            {
                string ItemID = message.Text.After("/uninstall_");
                Regex REG = new Regex("[0-9]+");
                // ищем совпадения в исходной строке
                MatchCollection mc = REG.Matches(ItemID);
                StringBuilder sb = new StringBuilder();
                // создаем строку результата
                foreach (Match matc in mc)
                    sb.Append(matc.Value);
                // а это твой результат - только цифры
                string MyItemID = sb.ToString();
                char[] charsToTrim = { '*', ' ', '\'', '№', '@', '!', '#', '$', '%', '^', '&', '(', ')', '"', ';', ':', '?', '/' }; // Обрезаем символы
                MyItemID = MyItemID.Replace('(', '@');
                MyItemID = MyItemID.Replace(')', '@');
                MyItemID = MyItemID.Trim(charsToTrim);
                string X = Ships.Ship.GetSingleData(message.From.Id, "X");
                string Y = Ships.Ship.GetSingleData(message.From.Id, "Y");
                string MapType = Program.GetMapSingleData(Convert.ToInt32(X), Convert.ToInt32(Y), "LocationType");
                if (MapType == "1" || MapType == "5")
                {
                    if (MyItemID != "" && MyItemID != "0")
                    {
                        if (Ships.Ship.isHasModule(message.From.Id, Convert.ToInt32(MyItemID)) == true)
                        {
                            string ModuleItemID = Program.GetModuleIDByID(message.From.Id, Convert.ToInt32(MyItemID));
                            Account.GiveItem(message.From.Id, ModuleItemID, "1");
                            Program.DeleteShipModule(Convert.ToInt32(MyItemID));
                            Program.ShipModulesStatsCalculation(message.From.Id);
                            Message.Send(message.From.Id, "Модуль деинсталирован.", 1);
                        }
                        else Message.Send(message.From.Id, "Ошибка деинсталяции модуля.", 1);
                    }
                    else Message.Send(message.From.Id, "Ошибка деинсталяции модуля.", 1);
                }
                else Message.Send(message.From.Id, "Деинсталировать модули вы можете только на планетах и космических станциях.", 1);
            }

            else if (message.Text.StartsWith("/install_"))
            {
                string ItemID = message.Text.After("/install_");
                Regex REG = new Regex("[0-9]+");
                // ищем совпадения в исходной строке
                MatchCollection mc = REG.Matches(ItemID);
                StringBuilder sb = new StringBuilder();
                // создаем строку результата
                foreach (Match matc in mc)
                    sb.Append(matc.Value);
                // а это твой результат - только цифры
                string MyItemID = sb.ToString();
                char[] charsToTrim = { '*', ' ', '\'', '№', '@', '!', '#', '$', '%', '^', '&', '(', ')', '"', ';', ':', '?', '/' }; // Обрезаем символы
                MyItemID = MyItemID.Replace('(', '@');
                MyItemID = MyItemID.Replace(')', '@');
                MyItemID = MyItemID.Trim(charsToTrim);
                string X = Ships.Ship.GetSingleData(message.From.Id, "X");
                string Y = Ships.Ship.GetSingleData(message.From.Id, "Y");
                string MapType = Program.GetMapSingleData(Convert.ToInt32(X), Convert.ToInt32(Y), "LocationType");
                if (MapType == "1" || MapType == "5")
                {
                    if (MyItemID != "" && MyItemID != "0")
                    {
                        Inventory.Items.Item.GetAllInfo(Convert.ToInt32(MyItemID), out string ReturnItemName, out string ReturnItemUsable, out string ReturnItemWearable, out string ReturnItemWearSlot, out string ReturnItemPrice, out string ReturnItemType, out string rItemStrength, out string rItemCharisma, out string rItemIntelligence, out string rItemAgility, out string rItemLuck, out string rItemAttack, out string rItemDef, out string ReturnItemPower, out string ReturnItemText);
                        bool PlayerHasItem = Account.HasItem(message.From.Id, MyItemID, "1"); // Есть ли у игрока модуль
                        string ShipID = Ships.Ship.GetSingleData(message.From.Id, "ShipID");
                        string InstalledModulesCount = string.Empty;
                        string ShipModulesSlotsCount = string.Empty;
                        if (ShipID != "")
                        {
                            InstalledModulesCount = Ships.Ship.GetModulesCount(Convert.ToInt32(ShipID));
                            ShipModulesSlotsCount = Ships.Ship.GetSingleData(Convert.ToInt32(ShipID), "ShipModulesSlots");
                        }

                        if (PlayerHasItem == true && ReturnItemType == "3" && ShipID != "" && Convert.ToInt32(InstalledModulesCount) < Convert.ToInt32(ShipModulesSlotsCount))
                        {
                            Program.AddShipModule(Convert.ToInt32(ShipID), MyItemID);
                            Account.DeleteItem(message.From.Id, MyItemID, "1");
                            Program.ShipModulesStatsCalculation(message.From.Id);
                            Message.Send(message.From.Id, "<b>Установлен модуль:</b>\n" + ReturnItemName, 1);
                        }
                        else Message.Send(message.From.Id, "Ошибка установки модуля.", 1);
                    }
                    else Message.Send(message.From.Id, "Ошибка установки модуля.", 1);
                }
                else Message.Send(message.From.Id, "Устанавливать модули можно только на планетах и космических станциях.", 1);
            }

            else if (message.Text.StartsWith("/drop_"))
            {
                char delimiter = '_';

                string ItemParameters = message.Text.After("/drop_");
                string[] substrings = ItemParameters.Split(delimiter);

                string ItemCount = string.Empty;
                string ItemID = string.Empty;

                try { ItemID = substrings[0]; }
                catch {; }

                try { ItemCount = substrings[1]; }
                catch {; }

                string MyItemID = ItemID.OnlyNumbers();
                ItemCount = ItemCount.OnlyNumbers();
                string MyItemCount = ItemCount;

                bool PlayerHasItem = false;

                if (ItemCount != "" && ItemID != "" && ItemCount != "0" && ItemID != "0")
                {
                    if (MyItemCount == "0")
                    {
                        Console.WriteLine("MyItemCount is null");
                        MyItemCount = "1";
                    }
                    if (MyItemCount == "")
                    {
                        PlayerHasItem = Account.HasItem(message.From.Id, MyItemID, "1");
                        MyItemCount = "1";
                    }
                    else PlayerHasItem = Account.HasItem(message.From.Id, MyItemID, MyItemCount);

                    Console.WriteLine("MyItemID = " + MyItemID);
                    Console.WriteLine("MyItemCount = " + MyItemCount);
                    Console.WriteLine("MyItemCount bool = " + PlayerHasItem);

                    if (MyItemID != "" && MyItemID != null)
                    {
                        if (PlayerHasItem == true)
                        {
                            Account.DeleteItem(message.From.Id, MyItemID, MyItemCount);
                            Message.Send(message.From.Id, "Предмет успешно уничтожен.", 5);
                            Program.OverweightCalculation(message.From.Id);
                        }
                        else Message.Send(message.From.Id, "У вас нет такого количество предметов!", 5);
                    }
                    else Message.Send(message.From.Id, "Ошибка уничтожения предмета!", 5);
                }
            }

            else if (message.Text.StartsWith("/talk_"))
            {
                string X = Ships.Ship.GetSingleData(message.From.Id, "X");
                string Y = Ships.Ship.GetSingleData(message.From.Id, "Y");
                bool NPCExists = NPC.Friend.IsExistsInMapPoint(Convert.ToInt32(X), Convert.ToInt32(Y));
                string NPCID = message.Text.After("/talk_");
                Regex REG = new Regex("[0-9]+");
                // ищем совпадения в исходной строке
                MatchCollection mc = REG.Matches(NPCID);
                StringBuilder sb = new StringBuilder();
                // создаем строку результата
                foreach (Match matc in mc)
                    sb.Append(matc.Value);
                // а это твой результат - только цифры
                string TargetNPCID = sb.ToString();
                char[] charsToTrim = { '*', ' ', '\'', '№', '@', '!', '#', '$', '%', '^', '&', '(', ')', '"', ';', ':', '?', '/' }; // Обрезаем символы
                TargetNPCID = TargetNPCID.Replace('(', '@');
                TargetNPCID = TargetNPCID.Replace(')', '@');
                TargetNPCID = TargetNPCID.Trim(charsToTrim);
                if (TargetNPCID != "" && TargetNPCID != "0" && NPCExists == true)
                {
                    if (NPC.Friend.GetIDInMapPoint(Convert.ToInt32(X), Convert.ToInt32(Y)).ToString() == TargetNPCID && NPC.Friend.GetIDInMapPoint(Convert.ToInt32(X), Convert.ToInt32(Y)).ToString() != "")
                    {
                        Console.WriteLine("Отработали первые условия");
                        string JSONTalkString = NPC.Friend.GetData(Convert.ToInt32(TargetNPCID), "TalkTextJSON");
                        string NPCName = NPC.Friend.GetData(Convert.ToInt32(TargetNPCID), "Name");
                        string Sex = NPC.Friend.GetData(Convert.ToInt32(TargetNPCID), "Sex");
                        string Type = NPC.Friend.GetData(Convert.ToInt32(TargetNPCID), "Type");
                        dynamic JSONTalk = JObject.Parse(JSONTalkString);

                        string Talk1 = JSONTalk.Talk.Text1;
                        string Talk2 = JSONTalk.Talk.Text2;
                        string Talk3 = JSONTalk.Talk.Text3;
                        int RndmTalk = rndmzr.Next(1, 4);
                        string TalkText = string.Empty;

                        if (RndmTalk == 1 && Talk1 != "")
                        {
                            TalkText = Talk1;
                        }
                        else if (RndmTalk == 2 && Talk2 != "")
                        {
                            TalkText = Talk2;
                        }
                        else if (RndmTalk == 3 && Talk3 != "")
                        {
                            TalkText = Talk3;
                        }
                        else
                        {
                            TalkText = Talk1;
                        }
                        string CharIcon = string.Empty;
                        string CharProf = string.Empty;
                        Console.WriteLine("Блок 2");
                        if (Sex == "0") // Мужчина
                        {
                            switch (Convert.ToInt32(Type))
                            {
                                case 0: // Продавец
                                    CharProf = "💰";
                                    CharIcon = "👨🏻‍💼";
                                    break;
                                case 1: // Механик
                                    CharProf = "🔧";
                                    CharIcon = "👨🏼‍🔧";
                                    break;
                                case 2: // Доктор
                                    CharProf = "💉";
                                    CharIcon = "👨🏼‍⚕️";
                                    break;
                            }
                        }
                        else if (Sex == "1") // Женщина
                        {
                            switch (Convert.ToInt32(Type))
                            {
                                case 0: // Продавец
                                    CharProf = "💰";
                                    CharIcon = "👩🏽‍💼";
                                    break;
                                case 1: // Механик
                                    CharProf = "🔧";
                                    CharIcon = "👩🏻‍🏭";
                                    break;
                                case 2: // Доктор
                                    CharProf = "💉";
                                    CharIcon = "👩🏼‍⚕️";
                                    break;
                            }
                        }
                        else if (Sex == "2") // Инопланетянин
                        {
                            Console.WriteLine("Sex 2");
                        }
                        Console.WriteLine("Блок 3");
                        if (Sex == "")
                        {
                            Console.WriteLine("Sex _");
                            Message.Send(message.From.Id, "Неправильная команда.", 1);
                        }
                        else
                        {
                            Console.WriteLine("Блок 4");
                            switch (Convert.ToInt32(Type))
                            {
                                case 0: // Продавец
                                    Console.WriteLine("Блок 5");
                                    string SellList = Inventory.GetSellList(message.From.Id, 1);
                                    Message.Send(message.From.Id, "<b>" + CharIcon + NPCName + " говорит:</b>\n" + "- " + TalkText + "\n\n" + SellList, 1);
                                    break;
                                case 1: // Механик
                                    
                                    break;
                                case 2: // Доктор
                                    
                                    break;
                            }
                            Console.WriteLine("Else");
                            
                        }
                    }
                    else Message.Send(message.From.Id, "Неправильная команда.", 1);
                }
                else Message.Send(message.From.Id, "Неправильная команда.", 1);
            }

            else if (message.Text.StartsWith("/use_"))
            {
                string ItemID = message.Text.After("/use_");
                Regex REG = new Regex("[0-9]+");
                // ищем совпадения в исходной строке
                MatchCollection mc = REG.Matches(ItemID);
                StringBuilder sb = new StringBuilder();
                // создаем строку результата
                foreach (Match matc in mc)
                    sb.Append(matc.Value);
                // а это твой результат - только цифры
                string MyItemID = sb.ToString();
                char[] charsToTrim = { '*', ' ', '\'', '№', '@', '!', '#', '$', '%', '^', '&', '(', ')', '"', ';', ':', '?', '/' }; // Обрезаем символы
                MyItemID = MyItemID.Replace('(', '@');
                MyItemID = MyItemID.Replace(')', '@');
                MyItemID = MyItemID.Trim(charsToTrim);
                if (MyItemID != "" && MyItemID != "0")
                {
                    bool PlayerHasItem = Account.HasItem(message.From.Id, MyItemID, "1");
                    Inventory.Items.Item.GetAllInfo(Convert.ToInt32(MyItemID), out string ReturnItemName, out string ReturnItemUsable, out string ReturnItemWearable, out string ReturnItemWearSlot, out string ReturnItemPrice, out string ReturnItemType, out string rItemStrength, out string rItemCharisma, out string rItemIntelligence, out string rItemAgility, out string rItemLuck, out string rItemAttack, out string rItemDef, out string ReturnItemPower, out string ReturnItemText);
                    string PlayerHealth = Account.GetSingleData(message.From.Id, "Health");
                    if (PlayerHasItem == true && ReturnItemUsable == "1" && Convert.ToInt32(PlayerHealth) > 0)
                    {
                        int HealthToRestore = 0;
                        string StringItemPower = string.Empty;
                        string StringItemAttack = string.Empty;
                        string StringItemDef = string.Empty;
                        if (ReturnItemPower != "" || ReturnItemPower != "0")
                        {
                            StringItemPower = "<b>Коэф. усиления:</b> " + ReturnItemPower;
                        }
                        if (rItemAttack != "" || rItemAttack != "0")
                        {
                            StringItemAttack = "⚔️ Урон: +" + rItemAttack;
                        }
                        if (rItemDef != "" || rItemDef != "0")
                        {
                            StringItemAttack = "🛡 Защита: +" + rItemDef;
                        }

                        if (ReturnItemType == "3")
                        {
                            Message.Send(message.From.Id, "<b>" + ReturnItemName + "</b>\n<b>Описание:</b> " + ReturnItemText + "\n\n<b>Установить:</b> /install_" + MyItemID + "\n<b>Управление модулями:</b> /modules", 1);
                        }
                        else switch (Convert.ToInt32(MyItemID))
                            {
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 18:

                                    HealthToRestore = Account.AddHealth(message.From.Id, Convert.ToInt32(ReturnItemPower));
                                    if (HealthToRestore > 0)
                                    {
                                        Message.Send(message.From.Id, "Вы использовали: " + ReturnItemName + "\nВосстановлено: ❤️ " + ReturnItemPower, 1);
                                        Account.DeleteItem(message.From.Id, MyItemID.ToString(), "1");
                                    }
                                    else
                                    {
                                        Message.Send(message.From.Id, "Невозможно использовать! Вы полностью здоровы!", 1);
                                    }
                                    break;

                                case 19:
                                case 20:
                                case 21:
                                    HealthToRestore = Account.AddHealth(message.From.Id, Convert.ToInt32(ReturnItemPower));
                                    if (HealthToRestore > 0)
                                    {
                                        Message.Send(message.From.Id, "Вы съели: " + ReturnItemName + "\nВосстановлено: ❤️ " + ReturnItemPower, 1);
                                        Account.DeleteItem(message.From.Id, MyItemID.ToString(), "1");
                                    }
                                    else
                                    {
                                        Message.Send(message.From.Id, "Невозможно съесть. Вы не голодны.", 1);
                                    }
                                    break;
                                default:
                                    Message.Send(message.From.Id, "Невозможно использовать.", 1);
                                    break;
                            }
                    }
                    else Message.Send(message.From.Id, "У вас нет данного предмета!", 1);
                }
                else Message.Send(message.From.Id, "Невозможно использовать.", 1);
            }

            else if (message.Text.StartsWith("/wear_"))
            {
                string ItemID = message.Text.After("/wear_");
                Regex REG = new Regex("[0-9]+");
                // ищем совпадения в исходной строке
                MatchCollection mc = REG.Matches(ItemID);
                StringBuilder sb = new StringBuilder();
                // создаем строку результата
                foreach (Match matc in mc)
                    sb.Append(matc.Value);
                // а это твой результат - только цифры
                string MyItemID = sb.ToString();
                char[] charsToTrim = { '*', ' ', '\'', '№', '@', '!', '#', '$', '%', '^', '&', '(', ')', '"', ';', ':', '?', '/' }; // Обрезаем символы
                MyItemID = MyItemID.Replace('(', '@');
                MyItemID = MyItemID.Replace(')', '@');
                MyItemID = MyItemID.Trim(charsToTrim);
                bool PlayerHasItem = Account.HasItem(message.From.Id, MyItemID, "1");
                if (MyItemID != "" && MyItemID != "0")
                {
                    if (PlayerHasItem == true)
                    {
                        Console.WriteLine("Account.SetSingleData = " + MyItemID);
                        Inventory.Items.Item.GetInfo(Convert.ToInt32(MyItemID), out string ReturnItemName, out string ReturnItemUsable, out string ReturnItemWearable, out string ReturnItemWearSlot, out string ReturnItemPrice, out string ReturnItemType, out string ReturnItemPower);
                        if (ReturnItemWearable == "1")
                        {
                            string OffhandSlot = string.Empty;
                            string MainhandSlot = string.Empty;
                            string HeadSlot = string.Empty;
                            string NeckSlot = string.Empty;
                            string ChestSlot = string.Empty;
                            string PantsSlot = string.Empty;
                            string FeetSlot = string.Empty;

                            if (ReturnItemWearSlot == "1")
                            {
                                Console.WriteLine("------- Игрок: " + message.From.Id + " пытается одеть предмет с ID " + MyItemID + ". Слот: " + ReturnItemWearSlot);
                                OffhandSlot = Account.GetSingleData(message.From.Id, "OffhandSlot");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] У игрока: " + message.From.Id + " в слоте " + ReturnItemWearSlot + " находится предмет c ID " + OffhandSlot);
                                Account.GiveItem(message.From.Id, OffhandSlot, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Добавляем предмет " + OffhandSlot + " в инвентарь игрока " + message.From.Id);
                                Account.DeleteItem(message.From.Id, MyItemID, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Удаляем предмет " + MyItemID + " из инвентаря игрока " + message.From.Id);
                                Account.SetSingleData(message.From.Id, "OffhandSlot", MyItemID);
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Надеваем предмет " + MyItemID + " в активный слот игрока " + message.From.Id);
                                Program.DefAttackCalculation(message.From.Id);
                                Message.Send(message.From.Id, "Вы надели предмет: " + ReturnItemName, 5);
                            }
                            if (ReturnItemWearSlot == "2")
                            {
                                Console.WriteLine("------- Игрок: " + message.From.Id + " пытается одеть предмет с ID " + MyItemID + ". Слот: " + ReturnItemWearSlot);
                                MainhandSlot = Account.GetSingleData(message.From.Id, "MainhandSlot");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] У игрока: " + message.From.Id + " в слоте " + ReturnItemWearSlot + " находится предмет c ID " + MainhandSlot);
                                Account.GiveItem(message.From.Id, MainhandSlot, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Добавляем предмет " + MainhandSlot + " в инвентарь игрока " + message.From.Id);
                                Account.DeleteItem(message.From.Id, MyItemID, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Удаляем предмет " + MyItemID + " из инвентаря игрока " + message.From.Id);
                                Account.SetSingleData(message.From.Id, "MainhandSlot", MyItemID);
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Надеваем предмет " + MyItemID + " в активный слот игрока " + message.From.Id);
                                Program.DefAttackCalculation(message.From.Id);
                                Message.Send(message.From.Id, "Вы надели предмет: " + ReturnItemName, 5);
                            }
                            if (ReturnItemWearSlot == "3")
                            {
                                Console.WriteLine("------- Игрок: " + message.From.Id + " пытается одеть предмет с ID " + MyItemID + ". Слот: " + ReturnItemWearSlot);
                                HeadSlot = Account.GetSingleData(message.From.Id, "HeadSlot");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] У игрока: " + message.From.Id + " в слоте " + ReturnItemWearSlot + " находится предмет c ID " + HeadSlot);
                                Account.GiveItem(message.From.Id, HeadSlot, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Добавляем предмет " + HeadSlot + " в инвентарь игрока " + message.From.Id);
                                Account.DeleteItem(message.From.Id, MyItemID, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Удаляем предмет " + MyItemID + " из инвентаря игрока " + message.From.Id);
                                Account.SetSingleData(message.From.Id, "HeadSlot", MyItemID);
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Надеваем предмет " + MyItemID + " в активный слот игрока " + message.From.Id);
                                Program.DefAttackCalculation(message.From.Id);
                                Message.Send(message.From.Id, "Вы надели предмет: " + ReturnItemName, 5);
                            }
                            if (ReturnItemWearSlot == "4")
                            {
                                Console.WriteLine("------- Игрок: " + message.From.Id + " пытается одеть предмет с ID " + MyItemID + ". Слот: " + ReturnItemWearSlot);
                                NeckSlot = Account.GetSingleData(message.From.Id, "NeckSlot");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] У игрока: " + message.From.Id + " в слоте " + ReturnItemWearSlot + " находится предмет c ID " + NeckSlot);
                                Account.GiveItem(message.From.Id, NeckSlot, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Добавляем предмет " + NeckSlot + " в инвентарь игрока " + message.From.Id);
                                Account.DeleteItem(message.From.Id, MyItemID, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Удаляем предмет " + MyItemID + " из инвентаря игрока " + message.From.Id);
                                Account.SetSingleData(message.From.Id, "NeckSlot", MyItemID);
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Надеваем предмет " + MyItemID + " в активный слот игрока " + message.From.Id);
                                Program.DefAttackCalculation(message.From.Id);
                                Message.Send(message.From.Id, "Вы надели предмет: " + ReturnItemName, 5);
                            }
                            if (ReturnItemWearSlot == "5")
                            {
                                Console.WriteLine("------- Игрок: " + message.From.Id + " пытается одеть предмет с ID " + MyItemID + ". Слот: " + ReturnItemWearSlot);
                                ChestSlot = Account.GetSingleData(message.From.Id, "ChestSlot");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] У игрока: " + message.From.Id + " в слоте " + ReturnItemWearSlot + " находится предмет c ID " + ChestSlot);
                                Account.GiveItem(message.From.Id, ChestSlot, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Добавляем предмет " + ChestSlot + " в инвентарь игрока " + message.From.Id);
                                Account.DeleteItem(message.From.Id, MyItemID, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Удаляем предмет " + MyItemID + " из инвентаря игрока " + message.From.Id);
                                Account.SetSingleData(message.From.Id, "ChestSlot", MyItemID);
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Надеваем предмет " + MyItemID + " в активный слот игрока " + message.From.Id);
                                Program.DefAttackCalculation(message.From.Id);
                                Message.Send(message.From.Id, "Вы надели предмет: " + ReturnItemName, 5);
                            }
                            if (ReturnItemWearSlot == "6")
                            {
                                Console.WriteLine("------- Игрок: " + message.From.Id + " пытается одеть предмет с ID " + MyItemID + ". Слот: " + ReturnItemWearSlot);
                                PantsSlot = Account.GetSingleData(message.From.Id, "PantsSlot");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] У игрока: " + message.From.Id + " в слоте " + ReturnItemWearSlot + " находится предмет c ID " + PantsSlot);
                                Account.GiveItem(message.From.Id, PantsSlot, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Добавляем предмет " + PantsSlot + " в инвентарь игрока " + message.From.Id);
                                Account.DeleteItem(message.From.Id, MyItemID, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Удаляем предмет " + MyItemID + " из инвентаря игрока " + message.From.Id);
                                Account.SetSingleData(message.From.Id, "PantsSlot", MyItemID);
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Надеваем предмет " + MyItemID + " в активный слот игрока " + message.From.Id);
                                Program.DefAttackCalculation(message.From.Id);
                                Message.Send(message.From.Id, "Вы надели предмет: " + ReturnItemName, 5);
                            }
                            if (ReturnItemWearSlot == "7")
                            {
                                Console.WriteLine("------- Игрок: " + message.From.Id + " пытается одеть предмет с ID " + MyItemID + ". Слот: " + ReturnItemWearSlot);
                                FeetSlot = Account.GetSingleData(message.From.Id, "FeetSlot");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] У игрока: " + message.From.Id + " в слоте " + ReturnItemWearSlot + " находится предмет c ID " + FeetSlot);
                                Account.GiveItem(message.From.Id, FeetSlot, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Добавляем предмет " + FeetSlot + " в инвентарь игрока " + message.From.Id);
                                Account.DeleteItem(message.From.Id, MyItemID, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Удаляем предмет " + MyItemID + " из инвентаря игрока " + message.From.Id);
                                Account.SetSingleData(message.From.Id, "FeetSlot", MyItemID);
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Надеваем предмет " + MyItemID + " в активный слот игрока " + message.From.Id);
                                Program.DefAttackCalculation(message.From.Id);
                                Message.Send(message.From.Id, "Вы надели предмет: " + ReturnItemName, 5);
                            }
                            Account.ShowInventory(message.From.Id);
                        }
                        else Message.Send(message.From.Id, "Этот предмет нельзя надеть!", 1);
                    }
                    else Message.Send(message.From.Id, "У вас нет данного предмета!", 1);
                }
                else Message.Send(message.From.Id, "Ошибка надевания предмета!", 1);
            }

            else if (message.Text.StartsWith("/off_"))
            {
                string ItemID = message.Text.After("/off_");
                Regex REG = new Regex("[0-9]+");
                // ищем совпадения в исходной строке
                MatchCollection mc = REG.Matches(ItemID);
                StringBuilder sb = new StringBuilder();
                // создаем строку результата
                foreach (Match matc in mc)
                    sb.Append(matc.Value);
                // а это твой результат - только цифры
                string MyItemID = sb.ToString();
                string PlayerHasItem = string.Empty;
                char[] charsToTrim = { '*', ' ', '\'', '№', '@', '!', '#', '$', '%', '^', '&', '(', ')', '"', ';', ':', '?', '/' }; // Обрезаем символы
                MyItemID = MyItemID.Replace('(', '@');
                MyItemID = MyItemID.Replace(')', '@');
                MyItemID = MyItemID.Trim(charsToTrim);
                if (MyItemID != "" && MyItemID != "0")
                {
                    Inventory.Items.Item.GetInfo(Convert.ToInt32(MyItemID), out string ReturnItemName, out string ReturnItemUsable, out string ReturnItemWearable, out string ReturnItemWearSlot, out string ReturnItemPrice, out string ReturnItemType, out string ReturnItemPower);
                    switch (ReturnItemWearSlot)
                    {
                        case "1":
                            PlayerHasItem = Account.GetSingleData(message.From.Id, "OffhandSlot");
                            break;
                        case "2":
                            PlayerHasItem = Account.GetSingleData(message.From.Id, "MainhandSlot");
                            break;
                        case "3":
                            PlayerHasItem = Account.GetSingleData(message.From.Id, "HeadSlot");
                            break;
                        case "4":
                            PlayerHasItem = Account.GetSingleData(message.From.Id, "NeckSlot");
                            break;
                        case "5":
                            PlayerHasItem = Account.GetSingleData(message.From.Id, "ChestSlot");
                            break;
                        case "6":
                            PlayerHasItem = Account.GetSingleData(message.From.Id, "PantsSlot");
                            break;
                        case "7":
                            PlayerHasItem = Account.GetSingleData(message.From.Id, "FeetSlot");
                            break;
                        default:
                            PlayerHasItem = "0";
                            break;
                    }


                    if (PlayerHasItem != "" && PlayerHasItem != "0")
                    {
                        if (ReturnItemWearable == "1")
                        {
                            string OffhandSlot = string.Empty;
                            string MainhandSlot = string.Empty;
                            string HeadSlot = string.Empty;
                            string NeckSlot = string.Empty;
                            string ChestSlot = string.Empty;
                            string PantsSlot = string.Empty;
                            string FeetSlot = string.Empty;

                            if (ReturnItemWearSlot == "1")
                            {
                                Console.WriteLine("------- Игрок: " + message.From.Id + " пытается снять предмет с ID " + MyItemID + ". Слот: " + ReturnItemWearSlot);
                                Account.GiveItem(message.From.Id, MyItemID, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Добавляем предмет " + MyItemID + " в инвентарь игрока " + message.From.Id);
                                Account.SetSingleData(message.From.Id, "OffhandSlot", "0");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Снимаем предмет " + MyItemID + " из слота игрока " + message.From.Id);
                                Program.DefAttackCalculation(message.From.Id);
                                Message.Send(message.From.Id, "Вы сняли предмет: " + ReturnItemName, 5);
                            }
                            if (ReturnItemWearSlot == "2")
                            {
                                Console.WriteLine("------- Игрок: " + message.From.Id + " пытается снять предмет с ID " + MyItemID + ". Слот: " + ReturnItemWearSlot);
                                Account.GiveItem(message.From.Id, MyItemID, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Добавляем предмет " + MyItemID + " в инвентарь игрока " + message.From.Id);
                                Account.SetSingleData(message.From.Id, "MainhandSlot", "0");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Снимаем предмет " + MyItemID + " из слота игрока " + message.From.Id);
                                Program.DefAttackCalculation(message.From.Id);
                                Message.Send(message.From.Id, "Вы сняли предмет: " + ReturnItemName, 5);
                            }
                            if (ReturnItemWearSlot == "3")
                            {
                                Console.WriteLine("------- Игрок: " + message.From.Id + " пытается снять предмет с ID " + MyItemID + ". Слот: " + ReturnItemWearSlot);
                                Account.GiveItem(message.From.Id, MyItemID, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Добавляем предмет " + MyItemID + " в инвентарь игрока " + message.From.Id);
                                Account.SetSingleData(message.From.Id, "HeadSlot", "0");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Снимаем предмет " + MyItemID + " из слота игрока " + message.From.Id);
                                Program.DefAttackCalculation(message.From.Id);
                                Message.Send(message.From.Id, "Вы сняли предмет: " + ReturnItemName, 5);
                            }
                            if (ReturnItemWearSlot == "4")
                            {
                                Console.WriteLine("------- Игрок: " + message.From.Id + " пытается снять предмет с ID " + MyItemID + ". Слот: " + ReturnItemWearSlot);
                                Account.GiveItem(message.From.Id, MyItemID, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Добавляем предмет " + MyItemID + " в инвентарь игрока " + message.From.Id);
                                Account.SetSingleData(message.From.Id, "NeckSlot", "0");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Снимаем предмет " + MyItemID + " из слота игрока " + message.From.Id);
                                Program.DefAttackCalculation(message.From.Id);
                                Message.Send(message.From.Id, "Вы сняли предмет: " + ReturnItemName, 5);
                            }
                            if (ReturnItemWearSlot == "5")
                            {
                                Console.WriteLine("------- Игрок: " + message.From.Id + " пытается снять предмет с ID " + MyItemID + ". Слот: " + ReturnItemWearSlot);
                                Account.GiveItem(message.From.Id, MyItemID, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Добавляем предмет " + MyItemID + " в инвентарь игрока " + message.From.Id);
                                Account.SetSingleData(message.From.Id, "ChestSlot", "0");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Снимаем предмет " + MyItemID + " из слота игрока " + message.From.Id);
                                Program.DefAttackCalculation(message.From.Id);
                                Message.Send(message.From.Id, "Вы сняли предмет: " + ReturnItemName, 5);
                            }
                            if (ReturnItemWearSlot == "6")
                            {
                                Console.WriteLine("------- Игрок: " + message.From.Id + " пытается снять предмет с ID " + MyItemID + ". Слот: " + ReturnItemWearSlot);
                                Account.GiveItem(message.From.Id, MyItemID, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Добавляем предмет " + MyItemID + " в инвентарь игрока " + message.From.Id);
                                Account.SetSingleData(message.From.Id, "PantsSlot", "0");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Снимаем предмет " + MyItemID + " из слота игрока " + message.From.Id);
                                Program.DefAttackCalculation(message.From.Id);
                                Message.Send(message.From.Id, "Вы сняли предмет: " + ReturnItemName, 5);
                            }
                            if (ReturnItemWearSlot == "7")
                            {
                                Console.WriteLine("------- Игрок: " + message.From.Id + " пытается снять предмет с ID " + MyItemID + ". Слот: " + ReturnItemWearSlot);
                                Account.GiveItem(message.From.Id, MyItemID, "1");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Добавляем предмет " + MyItemID + " в инвентарь игрока " + message.From.Id);
                                Account.SetSingleData(message.From.Id, "FeetSlot", "0");
                                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "] Снимаем предмет " + MyItemID + " из слота игрока " + message.From.Id);
                                Program.DefAttackCalculation(message.From.Id);
                                Message.Send(message.From.Id, "Вы сняли предмет: " + ReturnItemName, 5);
                            }
                            Account.ShowInventory(message.From.Id);
                        }
                        else Message.Send(message.From.Id, "Этот предмет нельзя надеть!", 1);
                    }
                    else Message.Send(message.From.Id, "У вас нет данного предмета!", 1);
                }
                else Message.Send(message.From.Id, "Ошибка снятия предмета!", 1);
            }

            //else if (message.Text == "🍺 Заказать выпивку")
            //{
            //    PlayerDeleteBetweenEvent(message.From.Id);
            //    var keyboard = new ReplyKeyboardMarkup(new[]
            //        {
            //            new [] // first row
            //            {
            //                new KeyboardButton("⚔️ Атака"),
            //                new KeyboardButton("✊🏻 Действия"),
            //                new KeyboardButton("🚀 Корабль"),
            //            },
            //            new [] // last row
            //            {
            //                new KeyboardButton("🛡 Защита"),
            //                new KeyboardButton("📖 Функции"),
            //                new KeyboardButton("👤 Персонаж"),
            //            }
            //            }, true);
            //    Console.WriteLine("Заказ выпивки: Количество золота у игрока = " + Account.GetGold(message.From.Id));
            //    if (Convert.ToInt32(Account.GetGold(message.From.Id)) >= 5)
            //    {
            //        PlayerSetEvent((int)message.From.Id, 1, 5, 1);
            //    }
            //    else
            //    {
            //        try
            //        {
            //            await Bot.Api.SendTextMessageAsync(message.Chat.Id, "У тебя недостаточно денег. Иди работай! *Вас вышвырнули из таверны*",
            //         replyMarkup: keyboard);
            //        }
            //        catch (Exception MyEx)
            //        {
            //            Console.WriteLine(MyEx);
            //        }

            //    }
            //}

            //else if (message.Text == "🌳 В лес")
            //{
            //    PlayerDeleteBetweenEvent(message.From.Id);
            //    string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
            //    if (PlayerOnEvent != message.From.Id.ToString())
            //    {
            //        var keyboard = new ReplyKeyboardMarkup(new[]
            //        {
            //            new [] // first row
            //            {
            //                new KeyboardButton("⚔️ Атака"),
            //                new KeyboardButton("✊🏻 Действия"),
            //                new KeyboardButton("🚀 Корабль"),
            //            },
            //            new [] // last row
            //            {
            //                new KeyboardButton("🛡 Защита"),
            //                new KeyboardButton("📖 Функции"),
            //                new KeyboardButton("👤 Персонаж"),
            //            }
            //            }, true);
            //        string CurrentPlayerEnergy = Account.GetSingleData(message.From.Id, "Energy");
            //        Console.WriteLine("В лес: Количество энергии у игрока = " + CurrentPlayerEnergy);
            //        if (Convert.ToInt32(CurrentPlayerEnergy) >= 1)
            //        {
            //            PlayerSetEvent((int)message.From.Id, 3, 5, 1);
            //        }
            //        else
            //        {
            //            try
            //            {
            //                await Bot.Api.SendTextMessageAsync(message.Chat.Id, "У вас недостаточно энергии!",
            //             replyMarkup: keyboard);
            //            }
            //            catch (Exception MyEx)
            //            {
            //                Console.WriteLine(MyEx);
            //            }

            //        }
            //    }
            //    else Message.Send((int)message.Chat.Id, "Вы сейчас заняты другим делом.", 1);

            //}

            else if (message.Text == "🔧 Механик")
            {
                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                string X_Position = Ships.Ship.GetSingleData(message.From.Id, "X");
                string Y_Position = Ships.Ship.GetSingleData(message.From.Id, "Y");
                string CurrentPositionLocationType = Program.GetMapSingleData(Convert.ToInt32(X_Position), Convert.ToInt32(Y_Position), "LocationType");
                string MechanikExists = NPC.ProfessionExists(message.From.Id, 4);

                if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != "") && (CurrentPositionLocationType == "2")) // В открытом космосе
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        if (MechanikExists != "")
                        {
                            var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                                new [] // first row
                                {
                                    new KeyboardButton("⚔️ Атака"),
                                    new KeyboardButton("✊🏻 Действия"),
                                    new KeyboardButton("🚀 Корабль"),
                                },
                                new [] // last row
                                {
                                    new KeyboardButton("🛡 Защита"),
                                    new KeyboardButton("📖 Функции"),
                                    new KeyboardButton("👤 Персонаж"),
                                }
                            }, true);

                            Program.PlayerDeleteBetweenEvent(message.From.Id);
                            Program.PlayerSetEvent((int)message.From.Id, 1, 5, Convert.ToInt32(MechanikExists)); // Ремонт корабля
                        }
                        else Message.Send(message.From.Id, "В экипаже корабля нет Механика!", 1);

                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                }
                else
                {
                    Message.Send(message.From.Id, "Вы не можете сделать это здесь.", 1);
                }
            }

            else if (message.Text == "💦 Плакательная")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                if (UserAccountID != "" && Ships.Ship.isHasModuleByItemID(message.From.Id, 40) == true)
                {
                    var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                    Program.PlayerDeleteBetweenEvent(message.From.Id);
                    Console.WriteLine("Игрок вошел в плакательную: ");
                    Program.PlayerSetEvent((int)message.From.Id, 2, 15, 1);
                }
            }

            else if (message.Text == "🉑 Нейроинтерфейс")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                if (UserAccountID != "" && Ships.Ship.isHasModuleByItemID(message.From.Id, 47) == true)
                {
                    var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                    }, true);
                    string BotsMax = Ships.Ship.GetSingleData(message.From.Id, "ShipBotsSlots");
                    int BotsCurrent = Ships.Ship.GetNPCCount(message.From.Id);
                    string NPCList = Ships.Ship.GetNPCList(message.From.Id);
                    Message.Send(message.From.Id, "<b>Список членов команды (" + BotsCurrent.ToString() + "/" + BotsMax + ")" + ":</b>\n" + NPCList, 1);
                }
            }

            //else if (message.Text == "💬 Общаться с людьми" || message.Text == "🛎 Говорить с барменом")
            //{
            //    var keyboard = new ReplyKeyboardMarkup(new[]
            //    {
            //        new [] // first row
            //        {
            //            new KeyboardButton("🚪 Назад в город"),
            //        },
            //    }, true);
            //    try
            //    {
            //        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Вы с удивлением обнаруживаете, что не можете ни с кем говорить. Магия лени разработчика!",
            //         replyMarkup: keyboard);
            //    }
            //    catch (Exception MyEx)
            //    {
            //        Console.WriteLine(MyEx);
            //    }

            //}
            //else if (message.Text == "🍺 Таверна")
            //{
            //    var keyboard = new ReplyKeyboardMarkup(new[]
            //    {
            //        new [] // first row
            //        {
            //            new KeyboardButton("🍺 Заказать выпивку"),
            //            new KeyboardButton("💬 Общаться с людьми"),
            //        },
            //        new [] // last row
            //        {
            //            new KeyboardButton("🛎 Говорить с барменом"),
            //            new KeyboardButton("🚪 Назад в город"),
            //        }
            //    }, true);
            //    try
            //    {
            //        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Вы вошли в таверну. Кругом пьют, играют в кости, пьяные моряки орут песни и бьют друг другу лица. Чем хотите заняться?",
            //         replyMarkup: keyboard);
            //    }
            //    catch (Exception MyEx)
            //    {
            //        Console.WriteLine(MyEx);
            //    }

            //}


            else if (message.Text == "/giveme")
            {
                Account.GiveItem(message.From.Id, "2", "10");
            }

            else if (message.Text == "/deleteme")
            {
                Account.DeleteItem(message.From.Id, "2", "5");
            }


            else if (message.Text == "🗡 Оружейник")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                {
                    new [] // first row
                    {
                        new KeyboardButton("🚪 Назад в город"),
                    },
                }, true);
                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "На двери лавки оружейника висит табличка Закрыто. Когда-нибудь лавка заработает.",
                     replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

            }
            else if (message.Text == "🛡 Бронник")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                {
                    new [] // first row
                    {
                        new KeyboardButton("🚪 Назад в город"),
                    },
                }, true);
                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "На двери лавки торговца броней висит табличка Закрыто. Когда-нибудь лавка заработает.",
                     replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }
            }
            else if (message.Text == "/market" || message.Text == "🔙 Выйти из магазина" || message.Text == "⛩ Рынок")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                {
                    new [] // first row
                    {
                        new KeyboardButton("🎩 Портной"),
                        new KeyboardButton("⛵️ Корабельщик"),
                    },
                    new [] // last row
                    {
                        new KeyboardButton("🗡 Оружейник"),
                        new KeyboardButton("🛡 Бронник"),
                        new KeyboardButton("🚪 Назад в город"),
                    }
                }, true);
                ///////////
                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Вы пришли на рынок. Вокруг вас множество лавок, все они открыты и предлагают разные товары. Куда вы хотите пойти?",
                     replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

            }
            else if (message.Text == "/nick")
            {
                string PlayerStatus = Account.GetSingleData(Convert.ToInt32(message.From.Id), "Status");
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                var keyboard2 = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton("🌐 Протокол наименования"),
                        }
                        }, true);
                if (PlayerStatus == "1") // Игрок на начальной клавиатуре
                {
                    try
                    {
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "<i>Возмущенный писк</i>\n\n *Используйте команду /nick ИМЯ", parseMode: ParseMode.Html, replyMarkup: keyboard2);
                    }
                    catch (Exception MyEx)
                    {
                        Console.WriteLine(MyEx);
                    }
                }
                else if (PlayerStatus == "0")
                {
                    try
                    {
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "<i>Возмущенный писк</i>\n\n *Используйте команду /nick ИМЯ", parseMode: ParseMode.Html, replyMarkup: keyboard);
                    }
                    catch (Exception MyEx)
                    {
                        Console.WriteLine(MyEx);
                    }
                }


            }

            else if (message.Text == "/revive")
            {
                string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                string UserAccountID = Account.GetAccountExists(message.From.Id);

                if ((PlayerOnEvent != message.From.Id.ToString()) && (UserAccountID != "") && (Account.GetSingleData(message.From.Id, "Status") == "666")) // В открытом космосе
                {
                    Program.PlayerDeleteBetweenEvent(message.From.Id);
                    Program.PlayerSetEvent((int)message.From.Id, 0, 5, 0); // Ивент Revive
                }
                else Message.Send(message.From.Id, "В данный момент, команда /revive недоступна.", 1);
                //if (Account.GetSingleData(message.From.Id, "Status") == "666")
                //{
                //    string MaxHealthP = Account.GetSingleData(message.From.Id, "MaxHealth");
                //    string MaxHealthS = Ships.Ship.GetSingleData(message.From.Id, "ShipMaxHealth");
                //    Ships.Ship.SetSingleData(message.From.Id, "ShipHealth", MaxHealthS);
                //    Account.SetSingleData(message.From.Id, "Health", MaxHealthP);
                //    int ShipSlotConnect = rndmzr.Next(1, 35);
                //    Message.Send(message.From.Id, "Вы очнулись в медицинском отсеке крейсера страховой компании. Добро пожаловать в мир живых! Ваш корабль ждёт вас у шлюза номер " + ShipSlotConnect + ".", 1);
                //    Account.SetSingleData(message.From.Id, "Status", "0");
                //    Account.SetSingleData(message.From.Id, "CurrentAction", "0");
                //    Ships.Ship.SetSingleData(message.From.Id, "X", "0");
                //    Ships.Ship.SetSingleData(message.From.Id, "Y", "0");
                //}
            }

            else if (message.Text == "/test_damage")
            {
                Ships.Ship.DelHealth(message.From.Id, 30);
            }

            else if (message.Text == "/test_em")
            {
                Program.SetGlobalSingleData("EmissionTime", Convert.ToString(DateTime.Now.AddMinutes(60)));
            }

            else if (message.Text == "/test_giveitem")
            {
                Account.GiveItem(message.From.Id, "41", "1");
            }

            else if (message.Text == "🌐 Загрузка матрицы") // Попытаться вспомнить
            {
                Message.Send(message.From.Id, "<i>Одобрительный писк</i>\n\n*Загрузка матрицы сознания - 100%\n", 24);
                Account.SetSingleData(message.From.Id, "Status", "1");
            }
            else if (message.Text == "🌐 Протокол наименования")
            {
                Message.Send(message.From.Id, "<i>Вопросительный писк</i>\n\n*Требуется действие пользователя:\n исп. <b>/nick ВАШЕ_ИМЯ</b> для активации протокола наименования!", 24);
                Account.SetSingleData(message.From.Id, "Status", "1");
            }

            else if (message.Text == "/test_delitem")
            {
                bool Deleted = Account.DeleteItem(message.From.Id, "24", "3");
                if (Deleted == true)
                {
                    Message.Send(message.From.Id, "Удалено.", 1);
                }
                else
                {
                    Message.Send(message.From.Id, "Ошибка.", 1);
                }
            }

            else if (message.Text.StartsWith("/nick "))
            {
                int keyboard = 1;
                string PlayerStatus = Account.GetSingleData(Convert.ToInt32(message.From.Id), "Status");
                if (PlayerStatus == "1") // Игрок на начальной клавиатуре
                {
                    keyboard = 24;
                }
                else if (PlayerStatus == "0")
                {
                    keyboard = 1;
                }
                string DroppedNickname = message.Text.After("/nick ");
                string CharNickname;
                CharNickname = DroppedNickname;
                if (CharNickname.Length > 1 && CharNickname.Length < 26)
                {
                    bool NickChanged = Account.ChangeNick(message.From.Id, CharNickname);
                    if (NickChanged == true)
                    {
                        Message.Send(message.From.Id, "<i>Одобрительный писк</i>\n\n*Личностная матрица установлена...\n*Активация бионического профиля... Готово.\nДобро пожаловать!", 1);
                        Account.SetSingleData(message.From.Id, "Status", "0");
                    }
                    else Message.Send(message.From.Id, "<i>Возмущенный писк</i>\n\n*Ошибка протокола смены ника. Пожалуйста, обратитесь к администратору.", keyboard);
                }
                else Message.Send(message.From.Id, "<i>Возмущенный писк</i>\n\n*Длина имени не соответствует протоколам. Спецификация: Имя от 2 до 26 символов.", keyboard);
            }
            else if (message.Text.StartsWith("/shipname "))
            {
                string DroppedNickname = message.Text.After("/shipname ");
                string CharNickname;
                CharNickname = DroppedNickname.OnlyLetters();
                string CaptainID = Ships.Ship.GetSingleData(message.From.Id, "CaptainID");
                if (CaptainID != "" && CaptainID != "0")
                {
                    if (CharNickname.Length > 2 && CharNickname.Length < 25)
                    {
                        Ships.Ship.ChangeName((int)message.Chat.Id, CharNickname);
                    }
                    else Message.Send((int)message.Chat.Id, "Корабль должен иметь грозное и длинное имя! От 2 до 25 символов.", 1);
                }
                else Message.Send((int)message.Chat.Id, "Вы не капитан корабля!", 1);
            }

            else if (message.Text.Contains("invite#"))
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);

                string InviteKeyToDecrypt = message.Text.Between("invite#", "#");
                Console.WriteLine("DEBUG MESSAGE TO DECRYPT = " + InviteKeyToDecrypt);
                string DecryptedKey = StringCipher.Decrypt(InviteKeyToDecrypt, "VsratPiratesTheBest");
                Console.WriteLine("DEBUG DECRYPTED KEY = " + DecryptedKey);
                string ShipID = string.Empty;
                ShipID = Ships.Ship.GetSingleData(Convert.ToInt32(DecryptedKey), "ShipID");
                string ShipName = Ships.Ship.GetSingleData(Convert.ToInt32(ShipID), "ShipName");

                string Captain = Ships.Ship.GetSingleData(message.From.Id, "CaptainID");
                if (ShipID != "0" || ShipID != "")
                {
                    if (Captain != ShipID)
                    {
                        Account.SetSingleData(message.From.Id, "ShipID", ShipID);
                        try
                        {
                            await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Вы присоединились к команде корабля под названием: " + ShipName + "! Добро пожаловать на борт!", replyMarkup: keyboard);
                        }
                        catch (Exception MyEx)
                        {
                            Console.WriteLine(MyEx);
                        }

                    }
                    else
                    {
                        try
                        {
                            await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Вы уже капитан корабля и не можете присоединиться к команде!", replyMarkup: keyboard);
                        }
                        catch (Exception MyEx)
                        {
                            Console.WriteLine(MyEx);
                        }
                    }

                }
                else
                {
                    try
                    {
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Что-то пошло не так...", replyMarkup: keyboard);
                    }
                    catch (Exception MyEx)
                    {
                        Console.WriteLine(MyEx);
                    }

                    Console.WriteLine("DEBUG ERROR - SHIP ID = " + ShipID);
                    Console.WriteLine("DEBUG DECRYPTED KEY = " + DecryptedKey);
                    Console.WriteLine("DEBUG MESSAGE TO DECRYPT = " + InviteKeyToDecrypt);
                }
            }
            else if (message.Text == "🛎 Нанять пирата")
            {
                string CaptainID = Ships.Ship.GetSingleData(message.From.Id, "ShipID");
                if (CaptainID == Convert.ToString(message.From.Id))
                {
                    string ShipName = Ships.Ship.GetSingleData(Convert.ToInt32(CaptainID), "ShipName");
                    var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                       new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                    Message.Send(Convert.ToInt32(CaptainID), "Перешлите следующее сообщение игроку для его приглашения на борт:", 1);
                    string EncryptedKey = StringCipher.Encrypt(CaptainID, "VsratPiratesTheBest");
                    try
                    {
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Перешлите данное сообщение игровому боту @" + Program.GameBotName + ", чтобы присоединиться к команде корабля под названием: " + ShipName + ".\n\n" + "invite#" + EncryptedKey + "#" + "\n\nПредложение действительно 5 минут.", replyMarkup: keyboard);
                    }
                    catch (Exception MyEx)
                    {
                        Console.WriteLine(MyEx);
                    }

                }
                else Message.Send(message.From.Id, "Вы не можете использовать эту команду!", 1);
            }
            else if (message.Text == "🚪 Каюта капитана")
            {
                string CaptainID = Ships.Ship.GetSingleData(message.From.Id, "ShipID");
                if (CaptainID == Convert.ToString(message.From.Id))
                {
                    var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("🛎 Нанять пирата"),
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                    try
                    {
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Добро пожаловать в вашу каюту капитан! Здесь вы можете:\n\n🛎 Нанять пирата\nПолучите специальное сообщение и перешлите его любому игроку на ваш выбор. Как только игрок перешлет это сообщение игровому боту, он автоматически присоединится к вашей пиратской команде!", replyMarkup: keyboard);
                    }
                    catch (Exception MyEx)
                    {
                        Console.WriteLine(MyEx);
                    }

                }
                else Message.Send(message.From.Id, "Вы долго стояли и стучали в закрытую дверь. Кажется, капитан чем-то очень занят.", 1);
            }

            else if (message.Text == "🚪 Назад")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Выберите действие:", replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

            }

            else if (message.Text == "⬆️ На север")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);

                if (UserAccountID != "")
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                       if (Account.GetSingleData(message.From.Id, "onPlanet") == "0")
                        {
                            string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                            int OverweightBonusEnergy = Program.OverweightCalculation(message.From.Id);
                            if (PlayerOnEvent != message.From.Id.ToString())
                            {
                                var keyboard = new ReplyKeyboardMarkup(new[]
                                {
                            new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                                int PlayerShipEnergy = Convert.ToInt32(Ships.Ship.GetSingleData(message.From.Id, "ShipEnergy"));

                                if (PlayerShipEnergy > 0 + OverweightBonusEnergy)
                                {
                                    Account.MapMove(Convert.ToInt32(message.From.Id), "UP");
                                }
                                else Message.Send((int)message.Chat.Id, "Недостаточно энергии для перемещения корабля!", 1);

                            }
                            else Message.Send((int)message.Chat.Id, "Вы сейчас заняты. Попробуйте позднее.", 1);
                        }
                       else Message.Send(message.From.Id, "Ваш корабль находится на поверхности планеты. Управление невозможно. Осуществите взлёт!", 1);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                }
                else
                {
                    Message.Send(message.From.Id, "У вас нет аккаунта. Нажмите /start", 1);
                }
            }

            else if (message.Text == "⬇️ На юг")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                if (UserAccountID != "")
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        if (Account.GetSingleData(message.From.Id, "onPlanet") == "0")
                        {
                            string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                            int OverweightBonusEnergy = Program.OverweightCalculation(message.From.Id);
                            if (PlayerOnEvent != message.From.Id.ToString())
                            {
                                var keyboard = new ReplyKeyboardMarkup(new[]
                                {
                            new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                                int PlayerShipEnergy = Convert.ToInt32(Ships.Ship.GetSingleData(message.From.Id, "ShipEnergy"));
                                if (PlayerShipEnergy > 0 + OverweightBonusEnergy)
                                {
                                    Account.MapMove(Convert.ToInt32(message.From.Id), "DOWN");
                                }
                                else Message.Send((int)message.Chat.Id, "Недостаточно энергии для перемещения корабля!", 1);
                            }
                            else Message.Send((int)message.Chat.Id, "Вы сейчас заняты. Попробуйте позднее.", 1);
                        }
                        else Message.Send(message.From.Id, "Ваш корабль находится на поверхности планеты. Управление невозможно. Осуществите взлёт!", 1);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);
                }
                else
                {
                    Message.Send(message.From.Id, "У вас нет аккаунта. Нажмите /start", 1);
                }
            }

            else if (message.Text == "⬅️ На запад")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                if (UserAccountID != "")
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        if (Account.GetSingleData(message.From.Id, "onPlanet") == "0")
                        {
                            string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                            int OverweightBonusEnergy = Program.OverweightCalculation(message.From.Id);
                            if (PlayerOnEvent != message.From.Id.ToString())
                            {
                                var keyboard = new ReplyKeyboardMarkup(new[]
                                {
                            new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                                int PlayerShipEnergy = Convert.ToInt32(Ships.Ship.GetSingleData(message.From.Id, "ShipEnergy"));
                                if (PlayerShipEnergy > 0 + OverweightBonusEnergy)
                                {
                                    Account.MapMove(Convert.ToInt32(message.From.Id), "LEFT");
                                }
                                else Message.Send((int)message.Chat.Id, "Недостаточно энергии для перемещения корабля!", 1);
                            }
                            else Message.Send((int)message.Chat.Id, "Вы сейчас заняты. Попробуйте позднее.", 1);
                        }
                        else Message.Send(message.From.Id, "Ваш корабль находится на поверхности планеты. Управление невозможно. Осуществите взлёт!", 1);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                }
                else
                {
                    Message.Send(message.From.Id, "У вас нет аккаунта. Нажмите /start", 1);
                }
            }

            else if (message.Text == "➡️ На восток")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                if (UserAccountID != "")
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        if (Account.GetSingleData(message.From.Id, "onPlanet") == "0")
                        {
                            string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                            int OverweightBonusEnergy = Program.OverweightCalculation(message.From.Id);
                            if (PlayerOnEvent != message.From.Id.ToString())
                            {
                                var keyboard = new ReplyKeyboardMarkup(new[]
                                {
                            new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                                int PlayerShipEnergy = Convert.ToInt32(Ships.Ship.GetSingleData(message.From.Id, "ShipEnergy"));
                                if (PlayerShipEnergy > 0 + OverweightBonusEnergy)
                                {
                                    Account.MapMove(Convert.ToInt32(message.From.Id), "RIGHT");
                                }
                                else Message.Send((int)message.Chat.Id, "Недостаточно энергии для перемещения корабля!", 1);
                            }
                            else Message.Send((int)message.Chat.Id, "Вы сейчас заняты. Попробуйте позднее.", 1);
                        }
                        else Message.Send(message.From.Id, "Ваш корабль находится на поверхности планеты. Управление невозможно. Осуществите взлёт!", 1);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);
                }
                else
                {
                    Message.Send(message.From.Id, "У вас нет аккаунта. Нажмите /start", 1);
                }
            }

            else if (message.Text == "🔼 На север")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);

                if (UserAccountID != "")
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        if (Account.GetSingleData(message.From.Id, "onPlanet") == "1")
                        {
                            string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                            //int OverweightBonusEnergy = Program.OverweightCalculation(message.From.Id);
                            if (PlayerOnEvent != message.From.Id.ToString())
                            {
                                var keyboard = new ReplyKeyboardMarkup(new[]
                                {
                            new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                                int PlayerEnergy = Convert.ToInt32(Account.GetSingleData(message.From.Id, "Energy"));
                                //if (PlayerEnergy > 0 + OverweightBonusEnergy)
                                if (PlayerEnergy > 0)
                                {
                                    Account.PlanetMapMove(Convert.ToInt32(message.From.Id), "UP");
                                }
                                else Message.Send((int)message.Chat.Id, "Недостаточно энергии для перемещения корабля!", 1);

                            }
                            else Message.Send((int)message.Chat.Id, "Вы сейчас заняты. Попробуйте позднее.", 1);
                        }
                        else Message.Send(message.From.Id, "Вы не можете перемещаться по планете, т.к. ваш корабль на орбите.", 1);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                }
                else
                {
                    Message.Send(message.From.Id, "У вас нет аккаунта. Нажмите /start", 1);
                }
            }

            else if (message.Text == "🔽 На юг")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);

                if (UserAccountID != "")
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        if (Account.GetSingleData(message.From.Id, "onPlanet") == "1")
                        {
                            string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                            //int OverweightBonusEnergy = Program.OverweightCalculation(message.From.Id);
                            if (PlayerOnEvent != message.From.Id.ToString())
                            {
                                var keyboard = new ReplyKeyboardMarkup(new[]
                                {
                            new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                                int PlayerEnergy = Convert.ToInt32(Account.GetSingleData(message.From.Id, "Energy"));
                                //if (PlayerEnergy > 0 + OverweightBonusEnergy)
                                if (PlayerEnergy > 0)
                                {
                                    Account.PlanetMapMove(Convert.ToInt32(message.From.Id), "DOWN");
                                }
                                else Message.Send((int)message.Chat.Id, "Недостаточно энергии для перемещения корабля!", 1);

                            }
                            else Message.Send((int)message.Chat.Id, "Вы сейчас заняты. Попробуйте позднее.", 1);
                        }
                        else Message.Send(message.From.Id, "Вы не можете перемещаться по планете, т.к. ваш корабль на орбите.", 1);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                }
                else
                {
                    Message.Send(message.From.Id, "У вас нет аккаунта. Нажмите /start", 1);
                }
            }

            else if (message.Text == "◀️ На запад")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);

                if (UserAccountID != "")
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        if (Account.GetSingleData(message.From.Id, "onPlanet") == "1")
                        {
                            string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                            //int OverweightBonusEnergy = Program.OverweightCalculation(message.From.Id);
                            if (PlayerOnEvent != message.From.Id.ToString())
                            {
                                var keyboard = new ReplyKeyboardMarkup(new[]
                                {
                            new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                                int PlayerEnergy = Convert.ToInt32(Account.GetSingleData(message.From.Id, "Energy"));
                                //if (PlayerEnergy > 0 + OverweightBonusEnergy)
                                if (PlayerEnergy > 0)
                                {
                                    Account.PlanetMapMove(Convert.ToInt32(message.From.Id), "LEFT");
                                }
                                else Message.Send((int)message.Chat.Id, "Недостаточно энергии для перемещения корабля!", 1);

                            }
                            else Message.Send((int)message.Chat.Id, "Вы сейчас заняты. Попробуйте позднее.", 1);
                        }
                        else Message.Send(message.From.Id, "Вы не можете перемещаться по планете, т.к. ваш корабль на орбите.", 1);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                }
                else
                {
                    Message.Send(message.From.Id, "У вас нет аккаунта. Нажмите /start", 1);
                }
            }

            else if (message.Text == "▶️ На восток")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);

                if (UserAccountID != "")
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        if (Account.GetSingleData(message.From.Id, "onPlanet") == "1")
                        {
                            string PlayerOnEvent = Account.CheckOnEvent(message.From.Id);
                            //int OverweightBonusEnergy = Program.OverweightCalculation(message.From.Id);
                            if (PlayerOnEvent != message.From.Id.ToString())
                            {
                                var keyboard = new ReplyKeyboardMarkup(new[]
                                {
                            new [] // first row
                        {
                            new KeyboardButton("⚔️ Атака"),
                            new KeyboardButton("✊🏻 Действия"),
                            new KeyboardButton("🚀 Корабль"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🛡 Защита"),
                            new KeyboardButton("📖 Функции"),
                            new KeyboardButton("👤 Персонаж"),
                        }
                        }, true);
                                int PlayerEnergy = Convert.ToInt32(Account.GetSingleData(message.From.Id, "Energy"));
                                //if (PlayerEnergy > 0 + OverweightBonusEnergy)
                                if (PlayerEnergy > 0)
                                {
                                    Account.PlanetMapMove(Convert.ToInt32(message.From.Id), "RIGHT");
                                }
                                else Message.Send((int)message.Chat.Id, "Недостаточно энергии для перемещения корабля!", 1);

                            }
                            else Message.Send((int)message.Chat.Id, "Вы сейчас заняты. Попробуйте позднее.", 1);
                        }
                        else Message.Send(message.From.Id, "Вы не можете перемещаться по планете, т.к. ваш корабль на орбите.", 1);
                    }
                    else Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);

                }
                else
                {
                    Message.Send(message.From.Id, "У вас нет аккаунта. Нажмите /start", 1);
                }
            }

            else if (message.Text == "🕋 Отсеки")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                if (UserAccountID != "")
                {
                    string Module1 = string.Empty;
                    string Module2 = string.Empty;
                    string Module3 = string.Empty;

                    if (Ships.Ship.isHasModuleByItemID(message.From.Id, 40) == true)
                    {
                        Console.WriteLine("Module 1");
                        Module1 = "💦 Плакательная";
                    }
                    if (Ships.Ship.isHasModuleByItemID(message.From.Id, 41) == true)
                    {
                        Console.WriteLine("Module 2");
                        Module2 = "📚 Библиотека";
                    }
                    if (Ships.Ship.isHasModuleByItemID(message.From.Id, 47) == true)
                    {
                        Console.WriteLine("Module 3");
                        Module2 = "🉑 Нейроинтерфейс";
                    }

                    var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton(Module1),
                            new KeyboardButton(Module2),
                            new KeyboardButton(Module3),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚀 Кап. мостик"),
                            new KeyboardButton("⚒ Рекрафтер"),
                            new KeyboardButton(""),
                        },
                        new []
                        {
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Вы вошли в жилые отсеки:", replyMarkup: keyboard, parseMode: ParseMode.Html);
                }
            }

            else if (message.Text == "🚀 Корабль")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                if (UserAccountID != "")
                {
                    var keyboard = new ReplyKeyboardMarkup(new[]
                            {
                            new [] // first row
                        {
                            new KeyboardButton("🚀 Курс"),
                            new KeyboardButton("🕋 Отсеки"),
                            new KeyboardButton("🔧 Модули"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("👨‍🚀 Команда"),
                            new KeyboardButton("🚪 Назад"),
                            new KeyboardButton(""),
                        }
                        }, true);
                    MySqlConnection myConnection = new MySqlConnection(Connect);
                    myConnection.Open();
                    
                    MySqlCommand sql_commmand = new MySqlCommand("SELECT ShipType, ShipName, ShipCargoMax, X, Y, ShipEnergy, ShipHealth, ShipMaxHealth, ShipDef, ShipAttack, ShipManeuv, ShipSpeed, ShipBotsSlots, ShipModulesSlots FROM ships WHERE ShipID = @AccountID", myConnection);
                    sql_commmand.Parameters.AddWithValue("@AccountID", message.Chat.Id);
                    MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                    string ShipType = string.Empty;
                    string ShipName = string.Empty;
                    string ShipCargoMax = string.Empty;
                    string X = string.Empty;
                    string Y = string.Empty;
                    string ShipEnergy = string.Empty;
                    string ShipHealth = string.Empty;
                    string ShipMaxHealth = string.Empty;
                    string ShipAttack = string.Empty;
                    string ShipBotsSlots = string.Empty;
                    string ShipModulesSlots = string.Empty;
                    string ShipTypeName = string.Empty;
                    string ShipDef = string.Empty;
                    string ShipManeuv = string.Empty;
                    string ShipSpeed = string.Empty;

                    while (thisReader.Read())
                    {
                        ShipType += thisReader["ShipType"];
                        ShipName += thisReader["ShipName"];
                        ShipCargoMax += thisReader["ShipCargoMax"];
                        X += thisReader["X"];
                        Y += thisReader["Y"];
                        ShipEnergy += thisReader["ShipEnergy"];
                        ShipHealth += thisReader["ShipHealth"];
                        ShipMaxHealth += thisReader["ShipMaxHealth"];
                        ShipDef += thisReader["ShipDef"];
                        ShipAttack += thisReader["ShipAttack"];
                        ShipBotsSlots += thisReader["ShipBotsSlots"];
                        ShipModulesSlots += thisReader["ShipModulesSlots"];
                        ShipManeuv += thisReader["ShipManeuv"];
                        ShipSpeed += thisReader["ShipSpeed"];
                    }
                    thisReader.Close();
                    myConnection.Close();

                    switch (Convert.ToInt32(ShipType)) // Классы кораблей, названия кораблей
                    {
                        case 1:
                            ShipTypeName = "Легкий перехватчик";
                            break;
                        case 2:
                            ShipTypeName = "Средний перехватчик";
                            break;
                        case 3:
                            ShipTypeName = "Тяжелый перехватчик";
                            break;
                        case 4:
                            ShipTypeName = "Бомбардировщик";
                            break;
                        case 5:
                            ShipTypeName = "Тяжелый бомбардировщик";
                            break;
                        case 6:
                            ShipTypeName = "Фрегат";
                            break;
                        case 7:
                            ShipTypeName = "Тяжелый фрегат";
                            break;
                        case 8:
                            ShipTypeName = "Эсминец";
                            break;
                        case 9:
                            ShipTypeName = "Крейсер";
                            break;
                        case 10:
                            ShipTypeName = "Тяжелый крейсер";
                            break;
                        case 11:
                            ShipTypeName = "Линкор";
                            break;
                        case 12:
                            ShipTypeName = "Ударный линкор";
                            break;
                        case 13:
                            ShipTypeName = "Трансорбитальный линкор";
                            break;
                        case 14:
                            ShipTypeName = "Атлант";
                            break;
                        case 15:
                            ShipTypeName = "Материнский корабль";
                            break;
                    }

                    string OverweightStringWarning = string.Empty;

                    int ShipCargoMaximum = Convert.ToInt32(ShipCargoMax);
                    int CurrentItemsCount = Program.InventoryCalculation(message.From.Id);
                    Console.WriteLine("DEBUG: CurrentItemsCount = " + CurrentItemsCount);
                    Ships.Ship.SetSingleData(message.From.Id, "ShipEnergyOverweight", "0");
                    if (ShipCargoMaximum < CurrentItemsCount)
                    {
                        OverweightStringWarning = "<b>Превышен максимальный вес!</b>\n";
                    }

                    int CurrentModulesCount = Program.ModulesCalculation(message.From.Id);
                    string BotsMax = Ships.Ship.GetSingleData(message.From.Id, "ShipBotsSlots");
                    int BotsCurrent = Ships.Ship.GetNPCCount(message.From.Id);

                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "<b>" + ShipName + "</b>\n🚀 " + ShipTypeName + "\n❤️ " + ShipHealth + @"\" + ShipMaxHealth + " ⚔️ " + ShipAttack + " 🛡 " + ShipDef + "\n⚡️ " + ShipEnergy + " 🌀 " + ShipManeuv + " 🏁 " + ShipSpeed + "\n📡 " + X + ":" + Y + "\n📦 " + CurrentItemsCount + "/" + ShipCargoMax + " (/bag)\n" + OverweightStringWarning + "👨‍🚀 " + BotsCurrent + "/" + BotsMax + " (/crew)\n🔧 " + CurrentModulesCount + "/" + ShipModulesSlots + " (/modules)\n", replyMarkup: keyboard, parseMode: ParseMode.Html);
                }
            }

            else if (message.Text == "/crew" || message.Text == "👨‍🚀 Команда")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                if (UserAccountID != "")
                {
                    string BotsMax = Ships.Ship.GetSingleData(message.From.Id, "ShipBotsSlots");
                    int BotsCurrent = Ships.Ship.GetNPCCount(message.From.Id);
                    string NPCList = Ships.Ship.GetNPCList(message.From.Id);
                    Message.Send(message.From.Id, "<b>Список членов команды (" + BotsCurrent.ToString() + "/" + BotsMax + ")" + ":</b>\n" + NPCList, 9);
                }
                else
                {
                    Message.Send(message.From.Id, "Аккаунта не существует. Нажмите /start", 9);
                }

            }

            else if (message.Text == "🌎 Перемещение")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                if (UserAccountID != "")
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton("🔼 На север"),
                            new KeyboardButton("🔽 На юг"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("◀️ На запад"),
                            new KeyboardButton("▶️ На восток"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);

                        string onPlanet = Account.GetSingleData(message.From.Id, "onPlanet");
                        if (onPlanet == "0")
                        {
                            Message.Send(message.From.Id, "Вы должны совершить посадку на планету.", 1);
                        }
                        else if (onPlanet == "1")
                        {
                            int planetX = Convert.ToInt32(Account.GetSingleData(message.From.Id, "planetX"));
                            int planetY = Convert.ToInt32(Account.GetSingleData(message.From.Id, "planetY"));
                            Program.ShowPlanetMapForPlayer(message.From.Id, 12, planetX, planetY);
                        }
                        else Message.Send(message.From.Id, "Ошибка идентификации планеты.", 1);
                    }
                    else
                    {
                        Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);
                    }

                }
                else
                {
                    Message.Send(message.From.Id, "Аккаунта не существует. Нажмите /start", 1);
                }

            }

            else if (message.Text == "🚀 Курс")
            {
                string UserAccountID = Account.GetAccountExists(message.From.Id);
                if (UserAccountID != "")
                {
                    if (Account.GetSingleData(message.From.Id, "Status") != "666")
                    {
                        var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton("⬆️ На север"),
                            new KeyboardButton("⬇️ На юг"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("⬅️ На запад"),
                            new KeyboardButton("➡️ На восток"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                        Program.ShowMapForPlayer(message.From.Id, 2);
                    }
                    else
                    {
                        Message.Send(message.From.Id, "Вы без сознания.\nНажмите /revive чтобы очнуться.", 1);
                    }

                }
                else
                {
                    Message.Send(message.From.Id, "Аккаунта не существует. Нажмите /start", 1);
                }

            }

            else if (message.Text == "🛠 Улучшения")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            new KeyboardButton("🗺 В плавание"),
                            new KeyboardButton("🛠 Улучшения"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("📦 В трюм"),
                            new KeyboardButton("💦 Плакательная"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Назад"),
                        }
                       }, true);
                try
                {
                    await Bot.Api.SendTextMessageAsync(message.Chat.Id, "⛵️ Магазин корабельщика еще не открылся. Будете лобзиком новые мачты выпиливать?", replyMarkup: keyboard);
                }
                catch (Exception MyEx)
                {
                    Console.WriteLine(MyEx);
                }

            }

            else if (message.Text == "📦 В трюм")
            {
                var keyboard = new ReplyKeyboardMarkup(new[]
                        {
                        new [] // first row
                        {
                            new KeyboardButton("📦 В трюм"),
                            new KeyboardButton("💦 Плакательная"),
                        },
                        new [] // last row
                        {
                            new KeyboardButton("🚪 Каюта капитана"),
                            new KeyboardButton("🚪 Назад"),
                        }
                        }, true);
                string PlayerShipID = Account.GetSingleData(message.From.Id, "ShipID");
                if (Convert.ToInt32(PlayerShipID) != 0)
                {
                    string GoldOnShip = Ships.Ship.GetSingleData(Convert.ToInt32(PlayerShipID), "Money");
                    string RobbedOnShip = Ships.Ship.GetSingleData(Convert.ToInt32(PlayerShipID), "Robbed");
                    try
                    {
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "Вы спустились в 📦Трюм! \n\nВ корабельном сундуке: 💳" + GoldOnShip + " золота! \n" + "(Исп. /donate СУММА чтобы закинуть ваше Деньги в сундук)\nНаграблено: 📦" + RobbedOnShip, replyMarkup: keyboard);
                    }
                    catch (Exception MyEx)
                    {
                        Console.WriteLine(MyEx);
                    }

                }
                else
                {
                    try
                    {
                        await Bot.Api.SendTextMessageAsync(message.Chat.Id, "В вашей рыбацкой лодке нет трюма. Найдите себе корабль!", replyMarkup: keyboard);
                    }
                    catch (Exception MyEx)
                    {
                        Console.WriteLine(MyEx);
                    }

                }

            }


            else if (Account.GetAccountExists(message.From.Id) != "")
            {
                string PlayerStatus = Account.GetSingleData(message.From.Id, "Status");
                if (PlayerStatus == "0") // Обычный статус
                {

                }
                else if (PlayerStatus == "4500") // Создание клана: выбор имени
                {
                    if (Account.IsInClan(message.From.Id) == true)
                    {
                        string ClanName = message.Text.ToString().OnlyLetters();
                        Console.WriteLine("ClanName: " + ClanName);
                        if (ClanName != "" && ClanName != " ")
                        {
                            Console.WriteLine("ClanName: " + ClanName);
                            if (ClanName.Length >= 3 && ClanName.Length < 26)
                            {
                                if (Clans.IsExists(ClanName) == false)
                                {
                                    string ClanID = Clans.GetDataByAccountID(message.From.Id, "ClanID");
                                    Clans.SetClanData(Convert.ToInt32(ClanID), "ClanName", ClanName);
                                    Message.Send(message.From.Id, "<b>Создание клана:</b>\nВыберите тэг для клана:\n(макс. 4 буквы, мин. 2 буквы)", 7);
                                    Account.SetSingleData(message.From.Id, "Status", "4501");
                                }
                                else Message.Send(message.From.Id, "Введенное имя уже используется другим кланом!\n\nВведите название вашего клана:", 7);
                            }
                            else Message.Send(message.From.Id, "Ограничение на количество букв в имени!\n(Не более 25 и не менее 3)\n\nВведите название вашего клана:", 7);
                        }
                        else Message.Send(message.From.Id, "Неправильный формат данных!\n (Пример: Red Star Squad)\n\nВведите название вашего клана:", 7);
                    }
                    else
                    {
                        Message.Send(message.From.Id, "У вас нет клана! Изменение статуса...", 1);
                        Account.SetSingleData(message.From.Id, "Status", "0");
                    }
                }
                else if (PlayerStatus == "4501") // Создание клана: выбор тэга
                {
                    if (Account.IsInClan(message.From.Id) == true)
                    {
                        string ClanTag = message.Text.ToString().OnlyLetters();
                        if (ClanTag != "" && ClanTag != " ")
                        {
                            if (ClanTag.Length >= 2 && ClanTag.Length < 5)
                            {
                                if (Clans.IsExistsByClanTag(ClanTag) == false)
                                {
                                    string ClanID = Clans.GetDataByAccountID(message.From.Id, "ClanID");
                                    Clans.SetClanData(Convert.ToInt32(ClanID), "ClanTag", ClanTag);
                                    Message.Send(message.From.Id, "<b>Создание клана:</b>\nНажмите /clan_pay чтобы заплатить за создание клана и завершить процедуру.", 7);
                                    Account.SetSingleData(message.From.Id, "Status", "0");
                                }
                                else Message.Send(message.From.Id, "Введенный тэг уже используется другим кланом!\n\nВведите тэг вашего клана:", 7);
                            }
                            else Message.Send(message.From.Id, "Ограничение на количество букв в тэге!\n(Не более 4 и не менее 2)\n\nВведите тэг вашего клана:", 7);
                        }
                        else Message.Send(message.From.Id, "Неправильный формат данных!\n (Пример: RSS)\n\nВведите тэг вашего клана:", 7);
                    }
                    else
                    {
                        Message.Send(message.From.Id, "У вас нет клана! Изменение статуса...", 1);
                        Account.SetSingleData(message.From.Id, "Status", "0");
                    }
                }
            }

            return Ok();
        }
    }
}
