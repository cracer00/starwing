using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Telegram.Bot.Examples.WebHook.DataStorage;
using static Telegram.Bot.Examples.WebHook.Message;

namespace Telegram.Bot.Examples.WebHook
{
    public class Recipes
    {
        public static string GetList()
        {
            string HireString = string.Empty;
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID, Name FROM recipes", myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            string RecipeList = string.Empty;

            while (thisReader.Read())
            {
                string TempString = string.Empty;
                string ID = string.Empty;
                string Name = string.Empty;
                ID += thisReader["ID"];
                Name += thisReader["Name"];

                TempString = Name + " (/rshow_" + ID + ")\n";

                RecipeList = RecipeList + TempString;
            }
            thisReader.Close();
            myConnection.Close();
            return RecipeList;
        }

        public static bool IsLearnedByPlayer(int AccountID, int RecipeID)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM players_recipes WHERE AccountID = @AccountID AND RecipeID = @RecipeID AND RecipeLevel = RecipeLearnLevel", myConnection);
            Console.WriteLine(sql_commmand.CommandText);
            sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
            sql_commmand.Parameters.AddWithValue("@RecipeID", RecipeID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string RecipeExists = string.Empty;
            while (thisReader.Read())
            {
                RecipeExists += thisReader["ID"];
            }
            thisReader.Close();
            myConnection.Close();
            if (RecipeExists != "" && RecipeExists != " ")
            {
                return true;
            }
            else return false;
        }

        public static string GetListForPlayer(int PlayerID)
        {
            string HireString = string.Empty;
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            MySqlCommand sql_commmand = new MySqlCommand("SELECT ID, RecipeID FROM players_recipes WHERE AccountID = " + PlayerID, myConnection);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            string RecipeList = string.Empty;

            while (thisReader.Read())
            {
                string TempString = string.Empty;
                string ID = string.Empty;
                string RecipeID = string.Empty;
                ID += thisReader["ID"];
                RecipeID += thisReader["RecipeID"];
                string RecipeName = GetDataByID(Convert.ToInt32(RecipeID), "Name");

                TempString = RecipeName + " (/rshow_" + RecipeID + ")\n";

                RecipeList = RecipeList + TempString;
            }
            thisReader.Close();
            myConnection.Close();
            return RecipeList;
        }

        public static string GetDataByID(int RecipeID, string DataBlockName) // Функция позволяет получить блок данных из MySQL таблицы recipes по ID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM recipes WHERE ID = @RecipeID", myConnection);
            sql_commmand.Parameters.AddWithValue("@RecipeID", RecipeID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["" + DataBlockName + ""];
            }
            thisReader.Close();
            myConnection.Close();

            // Convert.ToInt64(DataBlockResult);
            return DataBlockResult;
        }

        public static bool isRecipeInDatabase(int RecipeID) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_commmand = new MySqlCommand("SELECT Name FROM recipes WHERE ID = @RecipeID", myConnection);
            sql_commmand.Parameters.AddWithValue("@RecipeID", RecipeID);
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();
            string DataBlockResult = string.Empty;
            while (thisReader.Read())
            {
                DataBlockResult += thisReader["Name"];
            }
            thisReader.Close();
            myConnection.Close();

            if (DataBlockResult != "")
            {
                return true;
            }
            else return false;
        }

        public static string GetListForPlayer(int PlayerID, int Page)
        {
            string HireString = string.Empty;
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand sql_commmand;
            myConnection.Open();
            switch (Page)
            {
                case 1:
                    sql_commmand = new MySqlCommand("SELECT ID, RecipeID FROM players_recipes WHERE AccountID = " + PlayerID + " AND RecipeLevel >= RecipeLearnLevel ORDER BY ID ASC limit 0, 10", myConnection);
                    break;
                case 2:
                    sql_commmand = new MySqlCommand("SELECT ID, RecipeID FROM players_recipes WHERE AccountID = " + PlayerID + " AND RecipeLevel >= RecipeLearnLevel ORDER BY ID ASC limit 10, 10", myConnection);
                    break;
                case 3:
                    sql_commmand = new MySqlCommand("SELECT ID, RecipeID FROM players_recipes WHERE AccountID = " + PlayerID + " AND RecipeLevel >= RecipeLearnLevel ORDER BY ID ASC limit 20, 10", myConnection);
                    break;
                case 4:
                    sql_commmand = new MySqlCommand("SELECT ID, RecipeID FROM players_recipes WHERE AccountID = " + PlayerID + " AND RecipeLevel >= RecipeLearnLevel ORDER BY ID ASC limit 30, 10", myConnection);
                    break;
                case 5:
                    sql_commmand = new MySqlCommand("SELECT ID, RecipeID FROM players_recipes WHERE AccountID = " + PlayerID + " AND RecipeLevel >= RecipeLearnLevel ORDER BY ID ASC limit 40, 10", myConnection);
                    break;
                case 6:
                    sql_commmand = new MySqlCommand("SELECT ID, RecipeID FROM players_recipes WHERE AccountID = " + PlayerID + " AND RecipeLevel >= RecipeLearnLevel ORDER BY ID ASC limit 50, 10", myConnection);
                    break;
                case 7:
                    sql_commmand = new MySqlCommand("SELECT ID, RecipeID FROM players_recipes WHERE AccountID = " + PlayerID + " AND RecipeLevel >= RecipeLearnLevel ORDER BY ID ASC limit 60, 10", myConnection);
                    break;
                default:
                    sql_commmand = new MySqlCommand("SELECT ID, RecipeID FROM players_recipes WHERE AccountID = " + PlayerID + " AND RecipeLevel >= RecipeLearnLevel ORDER BY ID ASC limit 0, 10", myConnection);
                    break;
            }
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            string RecipeList = string.Empty;

            while (thisReader.Read())
            {
                string TempString = string.Empty;
                string ID = string.Empty;
                string RecipeID = string.Empty;
                ID += thisReader["ID"];
                RecipeID += thisReader["RecipeID"];
                string CraftedItemID = GetDataByID(Convert.ToInt32(RecipeID), "CraftedItemID");
                string ItemType = Inventory.Items.Item.GetSingleData(Convert.ToInt32(CraftedItemID), "ItemType");
                string RecipeName = GetDataByID(Convert.ToInt32(RecipeID), "Name");
                string ItemIcon = "📦 ";
                switch (Convert.ToInt32(ItemType))
                {
                    case 7:
                        ItemIcon = "🔩 ";
                        break;
                    case 6:
                        ItemIcon = "📦 ";
                        break;
                    case 5:
                        ItemIcon = "💊 ";
                        break;
                    case 4:
                        ItemIcon = "🍞 ";
                        break;
                    case 3:
                        ItemIcon = "💎 ";
                        break;
                    case 2:
                        ItemIcon = "👕 ";
                        break;
                    case 1:
                        ItemIcon = "🔪 ";
                        break;
                    default:
                        ItemIcon = "📦 ";
                        break;
                }
                

                TempString = ItemIcon + RecipeName + " (/rshow_" + RecipeID + ")\n";

                RecipeList = RecipeList + TempString;
            }
            string NavigationCurrentPage = "Страница " + Page;
            string NavigationBack = "Назад: /plans_1";
            string NavigationForward = "Вперед: /plans_2";
            string Navigation = NavigationCurrentPage + "\n" + NavigationBack + "\n" + NavigationForward;
            if (Page > 1)
            {
                NavigationBack = "Назад: /plans_" + (Page - 1);
                NavigationForward = "Вперед: /plans_" + (Page + 1);
                Navigation = NavigationBack + "\n" + NavigationForward;
            }
            RecipeList = RecipeList + "\n" + Navigation;
            thisReader.Close();
            myConnection.Close();
            return RecipeList;
        }

        public static string GetUnlearnedListForPlayer(int PlayerID, int Page)
        {
            string HireString = string.Empty;
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand sql_commmand;
            myConnection.Open();
            switch (Page)
            {
                case 1:
                    sql_commmand = new MySqlCommand("SELECT ID, Name, CraftedItemID FROM recipes ORDER BY ID ASC limit 0, 10", myConnection);
                    break;
                case 2:
                    sql_commmand = new MySqlCommand("SELECT ID, Name, CraftedItemID FROM recipes ORDER BY ID ASC limit 10, 10", myConnection);
                    break;
                case 3:
                    sql_commmand = new MySqlCommand("SELECT ID, Name, CraftedItemID FROM recipes ORDER BY ID ASC limit 20, 10", myConnection);
                    break;
                case 4:
                    sql_commmand = new MySqlCommand("SELECT ID, Name, CraftedItemID FROM recipes ORDER BY ID ASC limit 30, 10", myConnection);
                    break;
                case 5:
                    sql_commmand = new MySqlCommand("SELECT ID, Name, CraftedItemID FROM recipes ORDER BY ID ASC limit 40, 10", myConnection);
                    break;
                case 6:
                    sql_commmand = new MySqlCommand("SELECT ID, Name, CraftedItemID FROM recipes ORDER BY ID ASC limit 50, 10", myConnection);
                    break;
                case 7:
                    sql_commmand = new MySqlCommand("SELECT ID, Name, CraftedItemID FROM recipes ORDER BY ID ASC limit 60, 10", myConnection);
                    break;
                default:
                    sql_commmand = new MySqlCommand("SELECT ID, Name, CraftedItemID FROM recipes ORDER BY ID ASC limit 0, 10", myConnection);
                    break;
            }
            MySqlDataReader thisReader = sql_commmand.ExecuteReader();

            string RecipeList = string.Empty;

            while (thisReader.Read())
            {
                string TempString = string.Empty;
                string ID = string.Empty;
                string Name = string.Empty;
                string CraftedItemID = string.Empty;
                ID += thisReader["ID"];
                Name += thisReader["Name"];
                CraftedItemID += thisReader["CraftedItemID"];

                string ItemType = Inventory.Items.Item.GetSingleData(Convert.ToInt32(CraftedItemID), "ItemType");
                
                string ItemIcon = "📦 ";
                switch (Convert.ToInt32(ItemType))
                {
                    case 7:
                        ItemIcon = "🔩 ";
                        break;
                    case 6:
                        ItemIcon = "📦 ";
                        break;
                    case 5:
                        ItemIcon = "💊 ";
                        break;
                    case 4:
                        ItemIcon = "🍞 ";
                        break;
                    case 3:
                        ItemIcon = "💎 ";
                        break;
                    case 2:
                        ItemIcon = "👕 ";
                        break;
                    case 1:
                        ItemIcon = "🔪 ";
                        break;
                    default:
                        ItemIcon = "📦 ";
                        break;
                }
                Console.WriteLine("Start of is");
                if (IsLearnedByPlayer(PlayerID, Convert.ToInt32(ID)) == true)
                {
                    TempString = "✅ " + Name + "\n";
                    Console.WriteLine("1");
                    RecipeList = RecipeList + TempString;
                }
                else
                {
                    TempString = ItemIcon + Name + " (/rlearn_" + ID + ")\n";
                    Console.WriteLine("2");
                    RecipeList = RecipeList + TempString;
                }
                Console.WriteLine("End of is");
            }
            string NavigationCurrentPage = "Страница " + Page;
            string NavigationBack = "Назад: /bibl_1";
            string NavigationForward = "Вперед: /bibl_2";
            string Navigation = NavigationCurrentPage + "\n" + NavigationBack + "\n" + NavigationForward;
            if (Page > 1)
            {
                NavigationBack = "Назад: /bibl_" + (Page - 1);
                NavigationForward = "Вперед: /bibl_" + (Page + 1);
                Navigation = NavigationBack + "\n" + NavigationForward;
            }
            RecipeList = RecipeList + "\n" + Navigation;
            thisReader.Close();
            myConnection.Close();
            return RecipeList;
        }

        public static void GetAllInfo(int RecipeID, out string ReturnName, out string ReturnPrice, out string ReturnCraftedItemID, out string ReturnCraftedItemPower, out string ReturnMaterial1, out string ReturnMaterial2, out string ReturnMaterial3, out string ReturnMaterial4, out string ReturnMaterial5, out string ReturnMaterial6, out string ReturnMaterial1_Count, out string ReturnMaterial2_Count, out string ReturnMaterial3_Count, out string ReturnMaterial4_Count, out string ReturnMaterial5_Count, out string ReturnMaterial6_Count, out string ReturnCraftPriceMoney, out string ReturnCraftPriceEnergy)
        {
            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();

            MySqlCommand sql_command = new MySqlCommand("SELECT Name, Price, CraftedItemID, CraftedItemPower, Material_1, Material_2, Material_3, Material_4, Material_5, Material_6, Material_1_Count, Material_2_Count, Material_3_Count, Material_4_Count, Material_5_Count, Material_6_Count, Craft_Price_Money, Craft_Price_Energy FROM recipes WHERE ID = @RecipeID", myConnection);
            sql_command.Parameters.AddWithValue("@RecipeID", RecipeID);
            MySqlDataReader thisReader = sql_command.ExecuteReader();
            string Name = string.Empty;
            string Price = string.Empty;
            string CraftedItemID = string.Empty;
            string CraftedItemPower = string.Empty;
            string Material_1 = string.Empty;
            string Material_2 = string.Empty;
            string Material_3 = string.Empty;
            string Material_4 = string.Empty;
            string Material_5 = string.Empty;
            string Material_6 = string.Empty;
            string Material_1_Count = string.Empty;
            string Material_2_Count = string.Empty;
            string Material_3_Count = string.Empty;
            string Material_4_Count = string.Empty;
            string Material_5_Count = string.Empty;
            string Material_6_Count = string.Empty;
            string CraftPriceMoney = string.Empty;
            string CraftPriceEnergy = string.Empty;


            while (thisReader.Read())
            {
                Name += thisReader["Name"];
                Price += thisReader["Price"];
                CraftedItemID += thisReader["CraftedItemID"];
                CraftedItemPower += thisReader["CraftedItemPower"];
                Material_1 += thisReader["Material_1"];
                Material_2 += thisReader["Material_2"];
                Material_3 += thisReader["Material_3"];
                Material_4 += thisReader["Material_4"];
                Material_5 += thisReader["Material_5"];
                Material_6 += thisReader["Material_6"];
                Material_1_Count += thisReader["Material_1_Count"];
                Material_2_Count += thisReader["Material_2_Count"];
                Material_3_Count += thisReader["Material_3_Count"];
                Material_4_Count += thisReader["Material_4_Count"];
                Material_5_Count += thisReader["Material_5_Count"];
                Material_6_Count += thisReader["Material_6_Count"];
                CraftPriceMoney += thisReader["Craft_Price_Money"];
                CraftPriceEnergy += thisReader["Craft_Price_Energy"];
            }
            thisReader.Close();
            myConnection.Close();

            ReturnName = Name;
            ReturnPrice = Price;
            ReturnCraftedItemID = CraftedItemID;
            ReturnCraftedItemPower = CraftedItemPower;
            ReturnMaterial1 = Material_1;
            ReturnMaterial2 = Material_2;
            ReturnMaterial3 = Material_3;
            ReturnMaterial4 = Material_4;
            ReturnMaterial5 = Material_5;
            ReturnMaterial6 = Material_6;
            ReturnMaterial1_Count = Material_1_Count;
            ReturnMaterial2_Count = Material_2_Count;
            ReturnMaterial3_Count = Material_3_Count;
            ReturnMaterial4_Count = Material_4_Count;
            ReturnMaterial5_Count = Material_5_Count;
            ReturnMaterial6_Count = Material_6_Count;
            ReturnCraftPriceMoney = CraftPriceMoney;
            ReturnCraftPriceEnergy = CraftPriceEnergy;
            //  return;
        }
    }
}
