using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Telegram.Bot.Examples.WebHook.DataStorage;

namespace Telegram.Bot.Examples.WebHook
{
    public class Ships
    {
        public class BaseShips
        {
            public string Name { get; set; }
            public int HP { get; set; }
            public int Attack { get; set; }
            public int Def { get; set; }
            public int Maneuv { get; set; }
            public int Speed { get; set; }
            public int Energy { get; set; }
            public int MaxCrew { get; set; }
            public int MaxModules { get; set; }
            public int MaxCargo { get; set; }
        }

        public class Ship
        {
            public static bool DelHealth(int AccountID, int HealthCount)
            {
                string MaxShipHealth = Ships.Ship.GetSingleData(AccountID, "ShipMaxHealth");
                string CurrentShipHealth = Ships.Ship.GetSingleData(AccountID, "ShipHealth");
                if (Convert.ToInt32(CurrentShipHealth) < 1)
                {
                    return false;
                }
                if (Convert.ToInt32(CurrentShipHealth) <= HealthCount)
                {
                    Account.SetDeath(AccountID);
                    return true;
                }
                else
                {
                    Ships.Ship.MinusData(AccountID, "ShipHealth", HealthCount.ToString());
                    return true;
                }
            }

            public static void Create(int ShipID, int ShipType, int CaptainID, int ShipCargoMax, int X, int Y, int ShipEnergy, int ShipEnergyOverweight, int ShipHealth, int ShipDef, int ShipAttack, int ShipBotsSlots, int ShipModulesSlots, int ShipSpeed, int ShipManeuv, int ShipMaxHealth, int ShipMaxEnergy, int ShipRenameCounts)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
                string sql;
                sql = "SELECT ShipID FROM ships WHERE CaptainID = " + ShipID.ToString();
                Console.WriteLine("Получение данных о наличии корабля - " + sql);
                MySqlScript script = new MySqlScript(myConnection, sql);
                myConnection.Open();

                MySqlCommand sql_commmand = new MySqlCommand("SELECT ShipID FROM ships WHERE CaptainID = @ShipID", myConnection);
                sql_commmand.Parameters.AddWithValue("@ShipID", ShipID);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                string sql_result = string.Empty;
                while (thisReader.Read())
                {
                    sql_result += thisReader["ShipID"];
                }
                thisReader.Close();
                Console.WriteLine("Создание аккаунта: результат запроса - " + sql_result);

                if (sql_result != ShipID.ToString())
                {
                    string varquery = ShipID.ToString() + ", " + ShipType.ToString() + ", " + @"""" + "Безымянный" + @"""" + ", " + CaptainID.ToString() + ", " + ShipCargoMax.ToString() + ", " + X.ToString() + ", " + Y.ToString() + ", " + ShipEnergy.ToString() + ", " + ShipEnergyOverweight.ToString() + ", " + ShipHealth.ToString() + ", " + ShipDef.ToString() + ", " + ShipAttack.ToString() + ", " + ShipBotsSlots.ToString() + ", " + ShipModulesSlots.ToString() + ", " + ShipSpeed.ToString() + ", " + ShipManeuv.ToString() + ", " + ShipMaxHealth.ToString() + ", " + ShipMaxEnergy.ToString() + ", " + ShipRenameCounts.ToString();
                    Console.WriteLine("Создание корабля: переменные для запроса (varquery) " + varquery);
                    sql = "INSERT INTO ships (ShipID, ShipType, ShipName, CaptainID, ShipCargoMax, X, Y, ShipEnergy, ShipEnergyOverweight, ShipHealth, ShipDef, ShipAttack, ShipBotsSlots, ShipModulesSlots, ShipSpeed, ShipManeuv, ShipMaxHealth, ShipMaxEnergy, ShipRenameCounts) VALUES (" + varquery + "); ";
                    Console.WriteLine("Создание корабля для ID - " + ShipID.ToString());
                    script = new MySqlScript(myConnection, sql);
                    script.Query = sql;
                    script.Connection = myConnection;
                    try
                    {
                        script.Execute();
                    }
                    catch (Exception MyEx)
                    {
                        Console.WriteLine("ОШИБКА!: " + MyEx);
                    }
                    Console.WriteLine("Создание корабля: запрос - " + sql);
                    myConnection.Close(); //Обязательно закрываем соединение!
                }
                else
                {
                    Console.WriteLine("Корабль уже найден. Не создаем новый корабль для ID - " + ShipID.ToString());
                    myConnection.Close();
                }
            }

            public static string CheckOnEvent(int AccountID)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();

                MySqlCommand sql_commmand = new MySqlCommand("SELECT AccountID FROM on_move_event WHERE AccountID = @AccountID", myConnection);
                sql_commmand.Parameters.AddWithValue("@AccountID", AccountID);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                string ShipOnEvent = string.Empty;
                while (thisReader.Read())
                {
                    ShipOnEvent += thisReader["AccountID"];
                }
                thisReader.Close();
                myConnection.Close();

                return ShipOnEvent;
            }

            public static string GetSingleData(int ShipID, string DataBlockName) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();

                MySqlCommand sql_commmand = new MySqlCommand("SELECT " + DataBlockName + " FROM ships WHERE ShipID = @ShipID", myConnection);
                sql_commmand.Parameters.AddWithValue("@ShipID", ShipID);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                string DataBlockResult = string.Empty;
                while (thisReader.Read())
                {
                    DataBlockResult += thisReader["" + DataBlockName + ""];
                }
                thisReader.Close();
                myConnection.Close();

                // Convert.ToInt64(DataBlockResult);
                return DataBlockResult;
            }

            public static bool SetSingleData(int AccountID, string DataBlockName, string NewParameter)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);

                MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
                string sql;
                bool Finished = false;

                sql = "UPDATE ships SET " + DataBlockName + " =" + @"""" + NewParameter + @"""" + " WHERE ShipID = " + AccountID.ToString() + ";";
                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - SetShipAccountSingleData] Запрос к базе для аккаунта: " + AccountID.ToString() + " datablock " + DataBlockName + " query - " + sql);
                MySqlScript script = new MySqlScript(myConnection, sql);
                myConnection.Open();
                script.Query = sql;
                script.Connection = myConnection;
                try
                {
                    script.Execute();
                }
                catch (MySqlException sqlEx)
                {
                    Console.WriteLine("ОШИБКА!: " + sqlEx);
                    myConnection.Close();

                    Finished = false;
                    return Finished;
                }

                myConnection.Close(); //Обязательно закрываем соединение!
                Finished = true;
                return Finished;
            }

            public static int AddHealth(int ShipID, int HealthCount)
            {
                string MaxShipHealth = GetSingleData(ShipID, "ShipMaxHealth");
                string CurrentShipHealth = GetSingleData(ShipID, "ShipHealth");
                if (Convert.ToInt32(MaxShipHealth) == Convert.ToInt32(CurrentShipHealth))
                {
                    return 0;
                }
                else if (Convert.ToInt32(CurrentShipHealth) < Convert.ToInt32(MaxShipHealth))
                {
                    if ((HealthCount + Convert.ToInt32(CurrentShipHealth)) <= (Convert.ToInt32(MaxShipHealth)))
                    {
                        int HealthToRecover = HealthCount + Convert.ToInt32(CurrentShipHealth);
                        SetSingleData(ShipID, "ShipHealth", HealthToRecover.ToString());
                        return HealthCount;
                    }
                    else
                    {
                        int HealthNeedToRestore = Convert.ToInt32(MaxShipHealth) - Convert.ToInt32(CurrentShipHealth);
                        int HealthTemp = Convert.ToInt32(MaxShipHealth) - Convert.ToInt32(CurrentShipHealth);
                        int FinalHealth = HealthTemp + Convert.ToInt32(CurrentShipHealth);
                        SetSingleData(ShipID, "ShipHealth", FinalHealth.ToString());
                        return HealthTemp;
                    }
                }
                else return 0;
            }

            public static bool AddData(int AccountID, string DataBlockName, string NewParameter)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);

                MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
                string sql;
                bool Finished = false;

                sql = "UPDATE ships SET " + DataBlockName + " =" + DataBlockName + " + " + @"""" + NewParameter + @"""" + "WHERE ShipID = " + AccountID.ToString() + ";";
                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - Ships.AddData] Запрос к базе для аккаунта: " + AccountID.ToString() + " datablock " + DataBlockName + " query - " + sql);
                MySqlScript script = new MySqlScript(myConnection, sql);
                myConnection.Open();
                script.Query = sql;
                script.Connection = myConnection;
                try
                {
                    script.Execute();
                }
                catch (MySqlException sqlEx)
                {
                    Console.WriteLine("ОШИБКА!: " + sqlEx);
                    myConnection.Close();
                    Finished = false;
                    return Finished;
                }

                myConnection.Close(); //Обязательно закрываем соединение!
                Finished = true;
                return Finished;
            }

            public static void AddData(int ShipID, string DataBlockName, string NewParameter, bool Minus)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);

                MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
                string sql;
                bool Finished = false;
                if (Minus == false)
                {
                    sql = "UPDATE ships SET " + DataBlockName + " =" + DataBlockName + " + " + @"""" + NewParameter + @"""" + "WHERE ShipID = " + ShipID.ToString() + ";";
                }
                else
                {
                    sql = "UPDATE ships SET " + DataBlockName + " =" + DataBlockName + " - " + @"""" + NewParameter + @"""" + "WHERE ShipID = " + ShipID.ToString() + ";";
                }
                MySqlScript script = new MySqlScript(myConnection, sql);
                myConnection.Open();
                script.Query = sql;
                script.Connection = myConnection;
                try
                {
                    script.Execute();
                }
                catch (MySqlException sqlEx)
                {
                    Console.WriteLine("ОШИБКА!: " + sqlEx);
                    myConnection.Close();
                    Finished = false;
                }

                myConnection.Close(); //Обязательно закрываем соединение!
                Finished = true;
            }

            public static bool MinusData(int AccountID, string DataBlockName, string NewParameter)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);

                MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
                string sql;
                bool Finished = false;

                sql = "UPDATE ships SET " + DataBlockName + " =" + DataBlockName + " - " + @"""" + NewParameter + @"""" + "WHERE ShipID = " + AccountID.ToString() + ";";
                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + " - Ships.MinusData] Запрос к базе для аккаунта: " + AccountID.ToString() + " datablock " + DataBlockName + " query - " + sql);
                MySqlScript script = new MySqlScript(myConnection, sql);
                myConnection.Open();
                script.Query = sql;
                script.Connection = myConnection;
                try
                {
                    script.Execute();
                }
                catch (MySqlException sqlEx)
                {
                    Console.WriteLine("ОШИБКА!: " + sqlEx);
                    myConnection.Close();
                    Finished = false;
                    return Finished;
                }

                myConnection.Close(); //Обязательно закрываем соединение!
                Finished = true;
                return Finished;
            }

            public static int GetNPCCount(int ShipID)
            {
                string HireString = string.Empty;
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM npc_data WHERE FounderID = " + ShipID + " AND Hired = 1", myConnection);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();

                int NPCCount = 0;

                while (thisReader.Read())
                {
                    string TempString = string.Empty;
                    string ID = string.Empty;
                    ID += thisReader["ID"];
                    NPCCount++;
                }
                thisReader.Close();
                myConnection.Close();
                return NPCCount;
            }
            
            public static string GetNPCList(int ShipID)
            {
                string HireString = string.Empty;
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM npc_data WHERE FounderID = " + ShipID + " AND Hired = 1", myConnection);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();

                string NPCList = string.Empty;

                while (thisReader.Read())
                {
                    string TempString = string.Empty;
                    string ID = string.Empty;
                    ID += thisReader["ID"];

                    TempString = NPC.GetDataByClanID(Convert.ToInt32(ID), "FirstName") + " " + NPC.GetDataByClanID(Convert.ToInt32(ID), "LastName") + " (/char_info_" + ID + ")" + "\n";

                    NPCList = NPCList + TempString;
                }
                thisReader.Close();
                myConnection.Close();
                return NPCList;
            }

            public static string GetNPCHireList(int ShipID)
            {
                string HireString = string.Empty;
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();
                MySqlCommand sql_commmand = new MySqlCommand("SELECT ID, FirstName, LastName, UpgradeFocus1, UpgradeFocus2, Strength, Endurance, Charisma, Intelligence, Agility, Luck FROM npc_data WHERE FounderID = " + ShipID + " AND Hired = 0" + " ORDER BY Date DESC LIMIT 5", myConnection);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();

                string HireList = string.Empty;

                while (thisReader.Read())
                {
                    string TempString = string.Empty;
                    string ID = string.Empty;
                    string FirstName = string.Empty;
                    string LastName = string.Empty;
                    string UpgradeFocus1 = string.Empty;
                    string UpgradeFocus2 = string.Empty;
                    string Strength = string.Empty;
                    string Endurance = string.Empty;
                    string Charisma = string.Empty;
                    string Intelligence = string.Empty;
                    string Agility = string.Empty;
                    string Luck = string.Empty;
                    ID += thisReader["ID"];
                    FirstName += thisReader["FirstName"];
                    LastName += thisReader["LastName"];
                    UpgradeFocus1 += thisReader["UpgradeFocus1"];
                    UpgradeFocus2 += thisReader["UpgradeFocus2"];
                    Strength += thisReader["Strength"];
                    Endurance += thisReader["Endurance"];
                    Charisma += thisReader["Charisma"];
                    Intelligence += thisReader["Intelligence"];
                    Agility += thisReader["Agility"];
                    Luck += thisReader["Luck"];

                    TempString = FirstName + " " + LastName + " (Инфо: /char_info_" + ID + ") \n";

                    HireList = HireList + TempString;
                }
                thisReader.Close();
                myConnection.Close();
                return HireList;
            }

            public static void ChangeName(int AccountID, string ShipName)
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
                string sql;
                char[] charsToTrim = { '*', ' ', '\'', '№', '@', '!', '#', '$', '%', '^', '&', '(', ')', '"', ';', ':', '?', '/' }; // Обрезаем символы
                ShipName = ShipName.Replace('(', '@');
                ShipName = ShipName.Replace(')', '@');
                ShipName = ShipName.Trim(charsToTrim);

                sql = "UPDATE ships SET ShipName =" + @"""" + ShipName + @"""" + " WHERE CaptainID = " + AccountID.ToString() + ";";
                Console.WriteLine("[" + DateTime.Now.ToString("h:mm:ss") + "- [Ships.Ship.ChangeName] Изменяем название корабля для ID: " + AccountID.ToString() + " query - " + sql);
                MySqlScript script = new MySqlScript(myConnection, sql);
                myConnection.Open();

                script.Query = sql;
                script.Connection = myConnection;
                try
                {
                    script.Execute();
                    Message.Send(AccountID, "Новое название вашего корабля: " + ShipName, 1);
                    AddData(AccountID, "ShipRenameCounts", "1", false);
                }
                catch (MySqlException sqlEx)
                {
                    Console.WriteLine(sqlEx);
                    myConnection.Close();

                }

                myConnection.Close(); //Обязательно закрываем соединение!
            }

            public static string GetModulesCount(int ShipID) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
            {
                int SQLCount = 0;
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();

                MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM ships_modules WHERE ShipID = @ShipID", myConnection);
                sql_commmand.Parameters.AddWithValue("@ShipID", ShipID);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                string DataBlockResult = string.Empty;
                while (thisReader.Read())
                {
                    SQLCount++;
                    DataBlockResult += thisReader["ID"];
                }
                thisReader.Close();
                myConnection.Close();

                // Convert.ToInt64(DataBlockResult);
                return Convert.ToString(SQLCount);
            }

            public static bool isHasModule(int ShipID, int ID) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();

                MySqlCommand sql_commmand = new MySqlCommand("SELECT ModuleID FROM ships_modules WHERE ShipID = @ShipID AND ID = @ID", myConnection);
                sql_commmand.Parameters.AddWithValue("@ShipID", ShipID);
                sql_commmand.Parameters.AddWithValue("@ID", ID);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                string DataBlockResult = string.Empty;
                while (thisReader.Read())
                {
                    DataBlockResult += thisReader["ModuleID"];
                }
                thisReader.Close();
                myConnection.Close();

                if (DataBlockResult != "")
                {
                    return true;
                }
                else return false;
            }

            public static bool isHasModuleByItemID(int ShipID, int ModuleID) // Функция позволяет получить блок данных из MySQL таблицы Users по AccountID'у
            {
                MySqlConnection myConnection = new MySqlConnection(Connect);
                myConnection.Open();

                MySqlCommand sql_commmand = new MySqlCommand("SELECT ID FROM ships_modules WHERE ShipID = " + ShipID.ToString() + " AND ModuleID = " + ModuleID.ToString() + " ", myConnection);
                sql_commmand.Parameters.AddWithValue("@ShipID", ShipID);
                sql_commmand.Parameters.AddWithValue("@ModuleID", ModuleID);
                Console.WriteLine("Ships.Ship.isHasModuleByItemID: " + sql_commmand.CommandText);
                MySqlDataReader thisReader = sql_commmand.ExecuteReader();
                string DataBlockResult = string.Empty;
                while (thisReader.Read())
                {
                    DataBlockResult += thisReader["ID"];
                }
                thisReader.Close();
                myConnection.Close();

                if (DataBlockResult != "")
                {
                    return true;
                }
                else return false;
            }
        }
    }
}
